#!/usr/bin/python

import os, sys
param=sys.argv

######################################################################
## Check before run
######################################################################

MC           = False
PRW          = True
Debug        = False
ZJetStudy    = True
DeriveCalib  = True
Bootstraps   = True
DeriveUncert = True

# Increase Statistics for Systematics by merging eta bins. False: default rscan eta binning; True: dijetJER eta binning. 
ApplyEtaRebinning = True

#jetColl = "2LC"
jetColl = "6LC"
#jetColl = "8LC"

#version = "MC16a"
#version = "MC16c"
#version = "MC16d"
version = "2015+2016data"
#version = "2017data"

# Insitu
Insitu = False # Turn on for both smoothed or not smoothed insitu calibration
InsituSmoothed = False

systs=[
    "Nominal",
#    "deltaRUp",
#    "deltaRDown",
#    "isolationUp",
#    "isolationDown",
#    "JVTUp",
#    "JVTDown",
#    "MCSherpa", # Run this only for MC
#    "Nominal4Sherpa", # MC AND DATA!! (to produce nominal correction in AbsEtabins (wtr to ApplyEtaRebinning) to be compared only w/ MCSherpa variation). 
    ]

##################################################################################
# DO NOT MODIFY
##################################################################################

# R20.7
#version = "MC15cPythia_MC15bMuProfile"
#version = "2015data"
#version = "MC15cPythia"
#version = "2016data"

# In this path you should have the folder ROOTfiles
PATH = "../"

list = range(15,75) # eta range
if ApplyEtaRebinning:
  list = range(0,16)

## Don't pay attention to this 
numericalInversion = False
pTavgType = "" # Ref + Rscan

command = ""

# Merging eta bins in TreetoHists Ouputs (to increase statistics in systematics) 
if ApplyEtaRebinning:
  for syst in systs:
    if syst == "Nominal4Sherpa":
      if "Nominal" not in systs:
        syst = "Nominal"
      else:
	continue
    command += "python DeriveInsituRscan/util/ApplyDijetJERbinning_syst.py"
    command += " --syst=" + syst
    command += " --jetColl=" + jetColl
    command += " --inputVersion=" + version
    if ZJetStudy:
      command += " --ZJetStudy=TRUE"
    if Bootstraps:
      command += " --Bootstrap=TRUE"
    if Debug:
      command += " --Debug=TRUE"
    if PRW:
      command += " --PRW=TRUE"
    command += " --path=" + PATH
    command += " && "

if DeriveCalib:
  for syst in systs:
    command += "ComputeRelativeResponse "
    if syst != "Nominal":
      command += "--syst="
      command += syst
      command += " "
    if ZJetStudy:
      command += "--ZJetStudy=TRUE "
    if MC:
      command += "--isMC=TRUE "
    if not MC:
      command += "--isMC=FALSE "
    if Bootstraps:
      command += "--Bootstrap=TRUE "
    command += "--DeriveCalib=TRUE "
    command += "--DeriveUncert=FALSE "
    if ApplyEtaRebinning:
      command += "--ApplyEtaRebinning=TRUE "
      if syst == "MCSherpa" or syst=="Nominal4Sherpa":
	command += "--UseAbsEtaBins=TRUE "
    command += "--jetColl="
    command += jetColl
    command += " --inputVersion="
    command += version
    command += " --path="
    command += PATH
    command += " "
    if Debug:
      command += "--debug=TRUE"
    if PRW:
      command += " --PRW=TRUE"
    if numericalInversion:
      command += " --nInversion=TRUE"
    if not MC:
      if Insitu:
        command += " --insitu=TRUE"	    
      if InsituSmoothed:
	command += " --insituSmoothed=TRUE"
    if pTavgType is not "":
      command += " --pTavgType="
      command += pTavgType
    command += " && "

if DeriveUncert:
  for syst in systs:
    if syst == "MCSherpa" or syst == "Nominal4Sherpa":  
      list = range(0,8)  # the Sherpa systematic is derived in absolute eta bins.
    for slice in list:
      command += "ComputeRelativeResponse "
      command += "--EtaSlice="
      Slice = str(slice)
      command += Slice
      command += " "
      if syst != "Nominal":
        command += "--syst="
        command += syst
        command += " "
      if ZJetStudy:
        command += "--ZJetStudy=TRUE "
      if MC:
        command += "--isMC=TRUE "
      if not MC:
        command += "--isMC=FALSE "
      if Bootstraps:
        command += "--Bootstrap=TRUE "
        command += "--DeriveCalib=FALSE "
        command += "--DeriveUncert=TRUE "
      if ApplyEtaRebinning:
	command += "--ApplyEtaRebinning=TRUE "
        if syst == "MCSherpa" or syst=="Nominal4Sherpa":
	  command += "--UseAbsEtabins=TRUE "
      command += "--jetColl="
      command += jetColl
      command += " --inputVersion="
      command += version
      command += " --path="
      command += PATH
      command += " "
      if Debug:
        command += "--debug=TRUE"
      if PRW:
        command += " --PRW=TRUE"
      if numericalInversion:
        command += " --nInversion=TRUE"
      if not MC:
        if Insitu:
          command += " --insitu=TRUE"	    
        if InsituSmoothed:
	  command += " --insituSmoothed=TRUE"
      if pTavgType is not "":
        command += " --pTavgType="
        command += pTavgType
      command += " && "

command = command[:-3]
command += "&"
print command
os.system(command)

