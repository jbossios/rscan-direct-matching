#include <DeriveInsituRscan/MyFitter.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#endif

#ifdef __CINT__
#pragma link C++ class MyFitter;
#endif
