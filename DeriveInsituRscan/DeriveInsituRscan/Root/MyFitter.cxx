/* **********************************************************************\
 *                                                                      *
 *  #   Name:   ComputeRelativeResponse                                 *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 22/02/16 First version              J. Bossio (jbossios@cern.ch)  *
 *  2 16/07/16 Second version             Ro        (mdevesa@cern.ch)   *
 *  3 01/01/17 Third version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include <TSystem.h>
#include <sstream>
#include <iostream>

// ROOT
#include "TCanvas.h"
#include "TFile.h"
#include "TH2F.h"
#include "TLine.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraph2DErrors.h"
#include "TLegend.h"

#include "DeriveInsituRscan/global.h"
#include "DeriveInsituRscan/MyFitter.h"

// JESResponseFitter
#include "JES_ResponseFitter/JES_BalanceFitter.h"

bool m_debug = false;

MyFitter::MyFitter(){
}

MyFitter::MyFitter(double Nsigma){
   if(m_debug) std::cout << "Initializing JES_ResponseFitter" << std::endl;
   _jesFitter = new JES_BalanceFitter(Nsigma);
   _jesFitter->SetGaus();
   _jesFitter->SetFitColor(kBlack);
   _jesFitter->SetFitOpt("REQ"); 
}

void MyFitter::GetFitRange(TH1* proj, int type, double delta, double& min, double& max){

  // Default (type==0)
  min = proj->GetMean() - proj->GetRMS()*0.9;
  max = proj->GetMean() + proj->GetRMS()*0.9;

  if(type==1){        // Two-side extended/shorter by delta
    min = proj->GetMean() - proj->GetRMS()*delta;
    max = proj->GetMean() + proj->GetRMS()*delta;
  } else if(type==2){ // Left-side extended/shorter by delta
    min = proj->GetMean() - proj->GetRMS()*delta;
  } else if(type==3){ // Right-side extended by delta
    max = proj->GetMean() + proj->GetRMS()*delta;
  } else if(type==4){ // Move range to the left by delta
    min = proj->GetMean() - proj->GetRMS()*delta;
    max = proj->GetMean() - proj->GetRMS()*delta;
  } else if(type==5){ // Move range to the right by delta
    min = proj->GetMean() + proj->GetRMS()*delta;
    max = proj->GetMean() + proj->GetRMS()*delta;
  } else { std::cout << "Error: wrong type in GetFitRange()" << std::endl;}

}

void MyFitter::Fit(TH1 *histo, double fitMin, double fitMax){

   const int   nRangeTypes = 5;
   const int   nRangeDelta = 20;
   int   rangeType[nRangeTypes]  = {1,2,3,4,5};  // Two-side extended/shorter, Left-side extended/shorter, Right-side extended/shorter, Moved to left, Moved to the right
   double rangeDelta[nRangeDelta] = {0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0}; // %RMS
   unsigned int nFits = nRangeTypes*nRangeDelta; // Number of possible fits

   //----------------------
   // Look for "best" fit
   //----------------------

   // Estimate effective number of entries	
   int Nbins = histo->GetNbinsX();
   _Neff = 0;
   for(int i=0;i<Nbins;++i) if(histo->GetBinError(i)>0)
     _Neff += pow(histo->GetBinContent(i)/histo->GetBinError(i),2);

   // Fit if enough stats
   if(_Neff>100){

     _jesFitter->Fit_Jona(histo,fitMin,fitMax);
     _Mean         = _jesFitter->GetMean();
     _MeanError    = _jesFitter->GetMeanError();
     _Sigma        = _jesFitter->GetSigma();
     _SigmaError   = _jesFitter->GetSigmaError();
     
     //----------------------------------------
     // Re-fitting if error is too large
     //----------------------------------------

     std::vector<double> errorValues;
     std::vector<double> chiValues;
     std::vector<double> meanValues;
     std::vector<int> typeValues;
     std::vector<int> deltaValues;
     if(_MeanError>0.02){
       std::cout << "Re-fitting..." << std::endl;
       // Try different ranges and save outcome
       for(int iType=0;iType<nRangeTypes;++iType){		 
         for(int iDelta=0;iDelta<nRangeDelta;++iDelta){		 
           GetFitRange(histo,rangeType[iType],rangeDelta[iDelta],fitMin,fitMax);
           _jesFitter->Fit_Jona(histo,fitMin,fitMax);
           errorValues.push_back(_jesFitter->GetMeanError());
           chiValues.push_back(_jesFitter->GetChi2Ndof());
           meanValues.push_back(_jesFitter->GetMean());
           typeValues.push_back(iType);
           deltaValues.push_back(iDelta);
         }
       }
       // Pick the fit with lower error-chi2/Ndof
       int bestType  = 0;
       int bestDelta = 8;
       double minErr = 1e9;
       double minChi2overNdof = 1e9;
       if(nFits!=typeValues.size()) std::cout << "ERROR: nFits!=typeValues.size()" << std::endl;
       for(unsigned int ierror=0;ierror<nFits;++ierror){
         if(errorValues.at(ierror)<minErr && chiValues.at(ierror)<minChi2overNdof && meanValues.at(ierror)<2 && meanValues.at(ierror)>-1 && errorValues.at(ierror)<1){
           bestType  = typeValues.at(ierror);
           bestDelta = deltaValues.at(ierror);
           minErr = errorValues.at(ierror);
           minChi2overNdof = chiValues.at(ierror);
         }
       }
       if(minChi2overNdof>1000){ // Use fit with minimum error instead
         bestType  = 0;
         bestDelta = 8;
         minErr = 1e9;	   
         for(unsigned int ierror=0;ierror<nFits;++ierror){
           if(errorValues.at(ierror)<minErr && meanValues.at(ierror)<2 && meanValues.at(ierror)>-1 && errorValues.at(ierror)<1){
             bestType  = typeValues.at(ierror);
             bestDelta = deltaValues.at(ierror);
             minErr = errorValues.at(ierror);
           }
         }
       }
       // Use best fit
       if(rangeType[bestType]!=1 && rangeDelta[bestDelta]!=0.9) std::cout << "Fit was improved!" << std::endl;
       GetFitRange(histo,rangeType[bestType],rangeDelta[bestDelta],fitMin,fitMax);
       _jesFitter->Fit_Jona(histo,fitMin,fitMax);
       _Mean = _jesFitter->GetMean();
       _MeanError = _jesFitter->GetMeanError();
       _Sigma = _jesFitter->GetSigma();
       _SigmaError = _jesFitter->GetSigmaError();
     } // bad quality of fit
     
   } else { // not enough stats to fit
     _Mean = 0; _MeanError = 0;
     _Sigma = 0, _SigmaError = 0;
   }

}

void MyFitter::DrawFitAndHisto(double minx, double maxx){
  _jesFitter->DrawFitAndHisto(minx, maxx);
}
