/* **********************************************************************\
 *                                                                      *
 *  #   Name           : CalculateSystematics                           *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 17/08/18 First version              F. Daneri (mdaneri@cern.ch)   *
\************************************************************************/

#include <TSystem.h>
#include <sstream>
#include <iostream>

#include "TCanvas.h"
#include "TFile.h"
#include "TH2F.h"
#include "TLine.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraph2DErrors.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TMath.h"

#include "DeriveInsituRscan/global.h"

TString PATH_rootfiles;
TString JetCollection;
TString MCVersion;
TString DataVersion;
TString inputNom = "";
TString inputVar = "";
TString inputStats = "";

/////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[]){

  std::string m_MCVersion = "";
  std::string m_DataVersion = "";
  std::string m_jetCollection = "";
  std::string m_systName = ""; // Default Nominal
  std::string m_generator = "Pythia"; // Default Pythia
  std::string m_path = "";
  double m_pTMin = -1.;
  bool m_zjet = false;
  bool m_applyEtaRebinning = false;

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--mcVersion=")   != std::string::npos) m_MCVersion = v[1];
    if ( opt.find("--dataVersion=")   != std::string::npos) m_DataVersion = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

    if ( opt.find("--pTMin=")   != std::string::npos) m_pTMin = std::stod(v[1]);

    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

    if ( opt.find("--ZJetStudy=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_zjet= true;
    }
    if ( opt.find("--ApplyEtaRebinning=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_applyEtaRebinning= true;
    }
    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
      if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
      if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
      if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
      if (v[1].find("MCSherpa") != std::string::npos) m_systName = "MCSherpa";
    }

  }//End: Loop over input options

  JetCollection = m_jetCollection;
  MCVersion     = m_MCVersion;
  DataVersion   = m_DataVersion;

  // Protections
  if(m_path==""){
    std::cout << "ERROR: Path should be provided, exiting" << std::endl;
    return 1;
  }
  if(JetCollection==""){
    std::cout << "ERROR: Jet collection should be provided, exiting" << std::endl;
    return 1;
  }
  if(MCVersion==""){
    std::cout << "ERROR: MC Tree version should be provided, exiting" << std::endl;
    return 1;
  }
  if(DataVersion==""){
    std::cout << "ERROR: Data Tree version should be provided, exiting" << std::endl;
    return 1;
  }
  if(m_systName==""){
    std::cout << "ERROR: m_systName needs to be set, exiting" << std::endl;
    return 1;
  }

  if(MCVersion.Contains("Pythia")) m_generator = "Pythia";

  PATH_rootfiles = m_path;

  if (m_systName == ""){std::cout << "SystName: Nominal" << std::endl;}
  else{ std::cout << "SystName: " << m_systName << std::endl;}

  inputNom = "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/";
  inputNom += DataVersion;
  inputNom += "_vs_";
  inputNom += MCVersion;
  inputNom += "/";
  inputNom += JetCollection;
  inputNom += "/Corr_vs_pTref";
  if(m_zjet) inputNom += "_ZJet";
  if(m_systName== "MCSherpa") inputNom += "_Nominal4Sherpa";
  if(m_applyEtaRebinning) inputNom+= "_etaRebinned";
  inputNom += ".root";
 
  inputVar += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/";
  inputVar += DataVersion;  
  inputVar += "_vs_"; 
  inputVar += MCVersion;
  inputVar += "/";  
  inputVar += JetCollection;
  inputVar += "/Corr_vs_pTref";
  if(m_zjet) inputVar += "_ZJet";
  inputVar +="_"+ m_systName;
  if(m_applyEtaRebinning) inputVar += "_etaRebinned";
  inputVar += ".root";

  inputStats += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Statistical_errors/";  
  inputStats += DataVersion;
  inputStats += "_vs_";  
  inputStats += MCVersion;
  inputStats += "/";
  inputStats += JetCollection;
  inputStats += "/Stat_error";
  if(m_zjet) inputStats += "_ZJetStudy";
  inputStats+="_"+ m_systName;
  if(m_applyEtaRebinning) inputStats += "_EtaRebinned";
  inputStats += ".root";

  //-----------------
  // Open ROOT files
  //-----------------
  int maxeta = 74, mineta =15;
  if(m_applyEtaRebinning) {
    mineta = 0; maxeta = 15;
    if (m_systName=="MCSherpa") maxeta=7;
  }
  
  TH1D* hStats[nEtaBins];
  TString name_stats;

  TString inputfileStats = PATH_rootfiles;
  inputfileStats += inputStats;
  std::cout << "Opening: " << inputfileStats << std::endl;
  TFile* tinStats = new TFile(inputfileStats,"read");
 
  for(int i=mineta;i<=maxeta;++i){
    name_stats = "RMS_vs_pTRef_Eta_";
    name_stats += i;
    int yeta = i-15;
    if(m_applyEtaRebinning) yeta= i;
    hStats[yeta] = (TH1D*)tinStats->Get(name_stats.Data()); 
    hStats[yeta]->SetDirectory(0);// "detach" the histogram from the file
  }

  tinStats->Close();

  // Nominal
  TString inputfileNom = PATH_rootfiles;
  inputfileNom += inputNom;
  std::cout << "Opening: " << inputfileNom << std::endl;
  TFile* tinNom = new TFile(inputfileNom,"read");

  // Get Histograms
  TH1D* hNom[nEtaBins];
  TString name;
  for(int i=mineta;i<=maxeta;++i){
    name = "Corr_vs_pT_Eta_";
    name += i;
    int yeta = i-15;
    if(m_applyEtaRebinning) yeta = i;
    hNom[yeta] = (TH1D*)tinNom->Get(name.Data());
    hNom[yeta]->SetDirectory(0);// "detach" the histogram from the file
  }

  tinNom->Close();

  // Variation
  TString inputfileVar = PATH_rootfiles;
  inputfileVar += inputVar;
  std::cout << "Opening: " << inputfileVar << std::endl;
  TFile* tinVar = new TFile(inputfileVar,"read");

  // Get Histograms
  TH1D* hVar[nEtaBins]; 
  TString name2;
  for(int i=mineta;i<=maxeta;++i){
    name2 = "Corr_vs_pT_Eta_";
    name2 += i;
    int yeta = i-15;
    if(m_applyEtaRebinning) yeta = i;
    hVar[yeta] = (TH1D*)tinVar->Get(name2.Data());
    hVar[yeta]->SetDirectory(0);
  } 
   
  tinVar->Close();

  // Get input to map Systematic Uncertainty vs ptRef to ptRscan  */

  TFile * mapFile = new TFile();
  TString mapFileName = PATH_rootfiles;
  mapFileName += "ROOTfiles/DirectMatching/Mapping/" + DataVersion + "/" + JetCollection ;
  mapFileName += "/MappingtoPtRscan";
  if(m_zjet) mapFileName += "_ZJetStudy";
  if(m_applyEtaRebinning) mapFileName += "_etaRebinned_absBins";
  mapFileName += ".root";
  mapFile = TFile::Open( mapFileName, "Read" );
  std::cout << "Openning Map File: " << mapFileName << std::endl;
 
  // Compute (Var-Nom)/Nom and fill TH2D with errors

  TH1D* hSyst[nEtaBins];
  for(int i=mineta;i<=maxeta;++i){
    name  = "hSyst_";
    name += i;
    int yeta =i-15;
    if(m_applyEtaRebinning) yeta =i;
    hSyst[yeta] = new TH1D(name.Data(),"",npTBins,pTBins);	  
  }

  float Var_Response_pT = 0;
  for(int i=mineta;i<=maxeta;++i){
    int yeta = i-15;
    if(m_applyEtaRebinning) yeta = i;
    double npTbins = hNom[yeta]->GetNbinsX();
    for(int ipT=1;ipT<=npTbins;++ipT){
      if(hVar[yeta]->GetBinContent(ipT)!=0 && hNom[yeta]->GetBinContent(ipT)!=0){
        Var_Response_pT = (hVar[yeta]->GetBinContent(ipT)/hNom[yeta]->GetBinContent(ipT))-1.0;
        hSyst[yeta]->SetBinContent(ipT,Var_Response_pT);
        hSyst[yeta]->SetBinError(ipT,hStats[yeta]->GetBinContent(ipT));
	if( (Var_Response_pT == 0.) && (hStats[yeta]->GetBinContent(ipT) ==0.)){
          hSyst[yeta]->SetBinContent(ipT,Var_Response_pT);
          hSyst[yeta]->SetBinError(ipT,1.);
        }
      }
    }
  }

  // Map systematic uncertainty
  TGraphErrors* mapping = new TGraphErrors(); // Map in TGraph1D format (one for each eta)
  TGraphAsymmErrors * Syst4Combination[nEtaBins];  // Output for Combination
  
  // Loop over eta bins
  for(int i=mineta;i<=maxeta;++i){
    name  = "Mapping_in_PtRefBins_eta_";
    int yeta = i-15;
    if(m_applyEtaRebinning) yeta =i;
    name += yeta;
    mapping = (TGraphErrors*)mapFile -> Get(name);
    // Book output TGraphAsymmErrors
    name = "Systematic_vs_pTRscan_Eta_";
    name += i ;
    Syst4Combination[yeta] = new TGraphAsymmErrors();
    Syst4Combination[yeta] -> SetName(name);
    Syst4Combination[yeta] -> GetXaxis() -> SetTitle("#it{p}_{T}^{Rscan}");
    Syst4Combination[yeta] -> GetYaxis() -> SetTitle("Relative Uncertainty");
    int asy_counter = 0;

    // Loop over pT Bins
    double npTbins = hSyst[yeta]->GetNbinsX();
    for(int ipT=1;ipT<=npTbins;++ipT){
      double Syst       = hSyst[yeta]->GetBinContent(ipT);
      double dSyst      = hSyst[yeta]->GetBinError(ipT);
      double pT         = hSyst[yeta]->GetBinCenter(ipT);
      double pT_lowedge = hSyst[yeta]->GetXaxis()->GetBinLowEdge(ipT);
      double pT_upedge  = hSyst[yeta]->GetXaxis()->GetBinUpEdge(ipT);

      // Get pTrscan from mapping
      double ptrscan = mapping -> Eval(pT);

      if(pT<m_pTMin) continue;

      // Fill the TGraphAsymmErrors
      double exl, exh, eyl, eyh;
      exl = fabs(ptrscan - mapping->Eval(pT_lowedge));
      exh = fabs(ptrscan - mapping->Eval(pT_upedge));
      eyl = dSyst;
      eyh = dSyst;
      Syst4Combination[yeta] -> SetPoint(asy_counter, ptrscan, Syst);
      Syst4Combination[yeta] -> SetPointError(asy_counter, exl, exh, eyl, eyh ); // i,ex_low,ex_high,ey_low,ey_high
      asy_counter++;
    } // loop over pT Bins
  } // loop over eta Bins



  //------------------------------
  // Save histograms to ROOT file
  //------------------------------
  TString outputFile = PATH_rootfiles;
  outputFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/";
  outputFile += DataVersion;
  outputFile += "_vs_";
  outputFile += MCVersion;
  outputFile += "/";
  outputFile += JetCollection;
  outputFile += "/SystematicUncertainty";
  if(m_zjet) outputFile += "_ZJetStudy";
  outputFile += "_";
  outputFile += m_systName;
  if(m_applyEtaRebinning) outputFile += "_etaRebinned";
  outputFile += ".root";
  std::cout << "Histograms saved in: " << outputFile.Data() << std::endl;
  TFile* tout = new TFile(outputFile.Data(),"RECREATE");
  tout->cd();
  
  for(int i=mineta;i<=maxeta;++i){
    int yeta= i-15;
    if(m_applyEtaRebinning) yeta =i;
    hSyst[yeta]->Write();            // Uncertainty vs pTref
    Syst4Combination[yeta]->Write(); // Uncertainty vs pTrscan
    if(m_systName =="MCSherpa") {
      // Unfolding the absolute eta bins for easy implementation: same treatment for all systs.
      TGraphAsymmErrors * TmpSyst = new TGraphAsymmErrors();
      TString name_tmp = "Systematic_vs_pTRscan_Eta_";
      int a = nRebEtaBins - 1 - yeta; // associating negative and positve etas in absolute eta bin
      name_tmp += a;
      TmpSyst = (TGraphAsymmErrors*)Syst4Combination[yeta]->Clone(name_tmp);
      TmpSyst -> SetNameTitle(name_tmp, name_tmp);
      TmpSyst -> GetXaxis() -> SetTitle("#it{p}_{T}^{Rscan}");
      TmpSyst -> GetYaxis() -> SetTitle("Relative Uncertainty");
      TmpSyst -> Write();
    } // end if Sherpa     
  }
  
  tout->Close();

  std::cout << ">>>> Done <<<<" << std::endl;

  return 0;

}
