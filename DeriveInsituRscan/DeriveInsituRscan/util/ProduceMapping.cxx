/* *********************************************************************\
*                                                                      *
*  #   Name:   Produce Mapping                                         *
*                                                                      *
*  #   Description: need to translate the calibration factors,         *
*  originally derived as a function of RefPt, into ProbePt.            *
*                                                                      *
*  # Based on code implemented in V+Jet insitu calibrations:           * 
*  https://gitlab.cern.ch/atlas-jetetmiss-jesjer/Insitu/               * 
*  MPF_PlottingTools/tree/master                                       *   
*                                                                      *
*  #   Date    Comments                   By                           *
* -- -------- -------------------------- ----------------------------- *
*  1 12/07/18 First version              M. Toscani (mtoscani@cern.ch) *
\* *********************************************************************/

#include "JES_ResponseFitter/JES_BalanceFitter.h"
#include "DeriveInsituRscan/Includes.h"
#include "DeriveInsituRscan/MyFitter.h"

using namespace std;

void BookOutputObjects();
void PrintLatex( const char* text, double xpos, double ypos, float fsize =0.04);
void Mapping( TH2D* hist2d, TGraphErrors* RespVs, double minNeff, double minNeffFit, TString PATH_plots, unsigned int eta, bool ApplyEtaRebinning =false );
 
int main( int argc, char* argv[] ) {
  SetAtlasStyle();
  //gROOT -> SetBatch(kTRUE);
  //gROOT -> ForceStyle();
  cout << "Reading user settings" << endl;
  
  for (int i=1; i< argc; i++){

    string opt(argv[i]); vector< string > v;
    istringstream iss(opt);
    string item;
    char delim = '=';
    while (std::getline(iss, item, delim)){ v.push_back(item); }

    if ( opt.find("--inputVersion=")   != string::npos) m_treeVersion = v[1];
    if ( opt.find("--jetColl=")   != string::npos) m_jetCollection = v[1];
    if ( opt.find("--path=")   != string::npos) m_path = v[1];

    if ( opt.find("--ZJetStudy=") != string::npos) {
      if (v[1].find("TRUE") != string::npos) m_zjet= true;
    }

    if ( opt.find("--Bootstrap=") != string::npos) {
      if (v[1].find("TRUE") != string::npos) m_bootstrap= true;
    }
    if ( opt.find("--ApplyEtaRebinning=") != string::npos) {
      if (v[1].find("TRUE") != string::npos) {
	m_applyEtaRebinning = true;
	m_useAbsEtaBins = true;
      }
    }
    if ( opt.find("--debug=")      != string::npos) {
      if (v[1].find("TRUE") != string::npos) m_debug= true;
      if (v[1].find("FALSE") != string::npos) m_debug= false;
    }

  }//End: Loop over input options

  JetCollection = m_jetCollection;
  Version   = m_treeVersion;

  if(m_path==""){
  cout << "ERROR: Path should be provided, exiting" << endl;
  return 1;
  }
  if(JetCollection==""){
  cout << "ERROR: Jet collection should be provided, exiting" << endl;
  return 1;
  }
  if(Version==""){
  cout << "ERROR: Tree version should be provided, exiting" << endl;
  return 1;
  }

  if(Version.Contains("2016")) year = "2016";
  if(Version.Contains("2017")) year = "2017";

  PATH_plots = m_path; PATH_plots += "Plots_DirectMatching/Mapping/" + Version + "/" + JetCollection; 
  PATH_rootfiles = m_path; PATH_rootfiles += "ROOTfiles/DirectMatching/";

 
  /***  OPENING INPUT FILE ***/
  inputFile = PATH_rootfiles + "TreetoHists_Outputs/";
  inputFile += "Data/"; 
  inputFile += Version;
  inputFile += "/";
  inputFile += JetCollection;
  inputFile += "/DataAll";
  if (m_zjet) inputFile += "_ZJetStudy";
  if (m_bootstrap) inputFile += "_bootstrap";
  if (m_applyEtaRebinning) inputFile += "_etaRebinned";
  inputFile += ".root";

  cout << "Openning: " << inputFile << endl;
  TFile* file = new TFile(inputFile.Data(),"READ");
	     
  if(m_debug) cout << "Successfully Opened inputFile" << endl; 


  /*** INITIALIZATIONS AND DEFINITIONS ***/
  
  /* Output TGraphs */
  BookOutputObjects();
  if(m_debug) cout << "Successfully Booked Output Objects" << endl; 


  /* TH2 from TreetoHists_outputs*/
  TH2D* hist2d;

  /* Min number of events required to attempt a fit. 
   * Use mean for min_N_eff < n <= min_N_eff_for_fit, 
   * -999 otherwise  */
  double _min_N_eff = 10;
  double _min_N_eff_for_fit = 100.; // same number hardcoded in MyFitter
  int j = 0; 

  unsigned int mineta =15;
  unsigned int maxeta =74;
  if(m_applyEtaRebinning) {
    maxeta=15+15;//compatible with defualt rscan binning procedure
    if(m_useAbsEtaBins) maxeta = 7+15;
  } 
  for( unsigned int eta = mineta; eta <= maxeta; eta++){
    TString name_tmp = TString::Format("hMappingProbePT%i",eta);
    if(m_applyEtaRebinning){
      name_tmp = TString::Format("hMappingProbePT%i",eta-15);
      if(m_useAbsEtaBins) name_tmp += "_absBins";
    }
    hist2d = (TH2D*) file -> Get(name_tmp.Data());
    if(m_debug) cout << "Retrieved histogram: "<<  name_tmp << endl; 
    int yeta = eta;
    if(m_applyEtaRebinning) yeta = eta-15;
    Mapping(hist2d, Map[eta-15], _min_N_eff, _min_N_eff_for_fit, PATH_plots, yeta, m_applyEtaRebinning);
    if(m_debug) cout << "Produced Map with Npoints= " << Map[eta-15]->GetN() << endl; 
 
    int npoints = Map[eta-15]->GetN();
    for( int n = 0; n< npoints; n++ ){
      if( Map[eta-15] -> GetY()[n] < 5. ) continue;
      graph2d -> SetPoint( j,
	  Map[eta-15] -> GetX()[n],                  // X: RefpT
	  ( hEtaBins[eta-15] + hEtaBins[eta-14] ) /2,  // Y: eta
	  Map[eta-15] -> GetY()[n] );                // Z: <RscanPt>
      graph2d -> SetPointError( j,
	  Map[eta-15] -> GetEX()[n],                 // EX: Err RefPt
	  ( hEtaBins[eta-15] - hEtaBins[eta-14] ),     // EY:  eta error, won't be used, setting it to bin width
	  Map[eta-15] -> GetEY()[n] );               // EZ: error in <RscanPt>   
      j++ ;
    }
  }

  /** OPENING OUTPUT ROOTFILE **/  
  TString outputFileName = PATH_rootfiles + "Mapping/" + Version +"/"+ JetCollection + "/MappingtoPtRscan";
  if(m_zjet) outputFileName += "_ZJetStudy";
  if(m_applyEtaRebinning) {
    outputFileName += "_etaRebinned";
    if(m_useAbsEtaBins) outputFileName += "_absBins";
  }
  outputFileName += ".root";
  TFile * tout = new TFile(outputFileName, "Recreate");
  if(m_debug) cout << "Openned OutputFile for writing: " << outputFileName <<  endl; 

  tout ->cd();
  
  unsigned int neta = netaBins;
  if (m_applyEtaRebinning) {
    neta = netaBinsReb;
    if (m_useAbsEtaBins) neta = netaBinsReb*0.5;
  }

  for( unsigned int eta = 0; eta < neta; eta++){
    Map[eta] -> Write();
    // Unfold the absolute eta bins for easy implementation
    // Systematics and corrections (excepto for Sherpa) are not calculated in abs eta bins.
    TGraphErrors * SymMap = new TGraphErrors();
    TString tmp = "Mapping_in_PtRefBins_eta_";
    int a = netaBinsReb -1 - eta;
    tmp += a;
    SymMap = (TGraphErrors*) Map[eta]->Clone(tmp);
    SymMap ->SetNameTitle(tmp,tmp); 
    SymMap -> GetXaxis() ->SetTitle("#it{p}_{T}^{Ref} [GeV]");
    SymMap -> GetYaxis() ->SetTitle("#it{p}_{T}^{Rscan} [GeV]");
    SymMap -> Write();
  }
  
  graph2d -> Write();
  tout -> Close();
  cout << ">>>>>> DONE!!<<<<<" << endl;
    
  return 0;
} // end main


void Mapping( TH2D* hist2d, TGraphErrors* RespVs, double minNeff, double minNeffFit, TString PATH_plots, unsigned int eta, bool ApplyEtaRebinning){
    
  // The JES fitting tool which should be used in all  //
  //  JES analysis.                                    //
  
  //JES_BalanceFitter *_JESfitter = new JES_BalanceFitter(1.6);  // Trying out Jona's fitter
  MyFitter* _JESfitter = new MyFitter(1.6);
  vector<double> Bins;
  vector<double> BinCenters;
  vector<double> _fitMean;
  vector<double> _fitError;
  char _namebuffer[100];
  char _titlebuffer[100], _text1[100], _text2[100], _text3[100], _text4[100], _text5[100];

  // Read bins form the histogram itself instead of
  // having to input them.
  for(int i=1;i<=hist2d->GetXaxis()->GetNbins()+1;++i) {
    Bins.push_back(hist2d->GetXaxis()->GetBinLowEdge(i));
  }

  // Determining Canvas //
  TCanvas *can = new TCanvas();
  can -> cd();
  can -> SetMargin(0.12,0.04,0.12,0.04);
  TString ps = PATH_plots;
  ps += "/MappingFits_";
  ps += eta; 
  if(m_systName!="") ps += "" + m_systName;
  if(ApplyEtaRebinning) ps += "_etaRebinned";
  ps += ".pdf";
  can -> Print(ps +"[");

  // loop over (Ref)pt Bins
  for(int i=0;i<=hist2d->GetXaxis()->GetNbins()-1;++i){
    sprintf( _namebuffer, "Mapping_bin_%i", i);
    TH1D * _hist = hist2d->ProjectionY( _namebuffer, i+1, i+1, "e" );
    double bincenter = hist2d->GetXaxis()->GetBinCenter(i+1);
    BinCenters.push_back( bincenter );

    string xTitle = "#it{p}_{T}^{Rscan} [GeV]";
    _hist->GetXaxis()->SetTitle(xTitle.c_str());
    _hist->GetXaxis()->SetTitleSize(0.03);
    
    // Drawing Range
    double draw_minx = _hist ->GetMean() - _hist -> GetRMS()*4;
    double draw_maxx = _hist ->GetMean() + _hist -> GetRMS()*4;
    _hist->SetAxisRange(draw_minx,draw_maxx,"X");
    _hist->SetMarkerColor(kBlack);  
    _hist->Draw();

    // Fit Range
    double fit_minx = _hist ->GetMean() - _hist ->GetRMS()*0.9;
    double fit_maxx = _hist ->GetMean() + _hist ->GetRMS()*0.9;
    double N_eff = pow(_hist->GetSum(),2) / _hist->GetSumw2()->GetSum();
    double mu(0.0),muError(0.0);
    if ( N_eff >= minNeff ) {
      mu = _hist->GetMean();
      muError = _hist->GetMeanError(); //rms / sqrt( N_eff );
      if (N_eff >= minNeffFit){
	_JESfitter->Fit( _hist, fit_minx, fit_maxx);
	_JESfitter->DrawFitAndHisto( draw_minx, draw_maxx );
	mu = _JESfitter -> GetMean();
	muError = _JESfitter -> GetMeanError();
      }
      _fitMean.push_back(mu);
      _fitError.push_back(muError);
    } else {
    _fitMean.push_back(0.);
    _fitError.push_back(-0.);
    }

    // Legends
    sprintf( _titlebuffer, "%.0f #leq p_{T}^{Ref} < %.0f [GeV]", Bins[i], Bins[i+1] );
    PrintLatex(_titlebuffer, 0.16, 0.86);
    if(ApplyEtaRebinning) sprintf( _text1, "%.1f #leq #eta < %.1f", hRebEtaBins[eta], hRebEtaBins[eta] );
    else{sprintf( _text1, "%.1f #leq #eta < %.1f", hEtaBins[eta-15], hEtaBins[eta-14] );}
    PrintLatex(_text1, 0.16, 0.80);
    if(PATH_plots.Contains("2LC")) sprintf( _text2, "anti-#it{k}_{t} jets, R = 0.2, LC+JES+GSC");
    else if (PATH_plots.Contains("6LC")) sprintf( _text2, "anti-#it{k}_{t} jets, R = 0.6, LC+JES+GSC");
    PrintLatex(_text2, 0.16, 0.75);
    sprintf( _text3, "Entries: %0.2f",_hist->GetEntries());
    PrintLatex(_text3, 0.16, 0.70);
    sprintf( _text4, "EffEntries: %0.2f", N_eff);
    PrintLatex(_text4, 0.16,0.65);
    if(PATH_plots.Contains("2015+2016")) sprintf( _text5, "2015+2016 Data");
    else if (PATH_plots.Contains("2017")) sprintf( _text5, "2017");
    PrintLatex(_text5, 0.16,0.6);
   
    can ->Print(ps);
  } // end loop over Ref pt bins
  
  can ->Print(ps +"]");

  // Fill the OutPut TGraph.
  int n = 0;
  for( unsigned int it = 0; it < _fitMean.size(); it++ ){  
    if ( _fitMean[it] < 5.) continue;
    RespVs->SetPoint( n, BinCenters[it], _fitMean[it] );
    RespVs->SetPointError( n, (Bins[it+1] - Bins[it])/2, _fitError[it] );
    n++;
  }
  

} //Map Definiton

/*-----------------------------------------------------*/

void BookOutputObjects(){
  TString tmp;
  for(unsigned int eta = 0; eta< netaBins; eta++){
    tmp = "Mapping_in_PtRefBins_eta_";
    tmp += eta; 
    Map[eta] = new TGraphErrors();
    Map[eta] -> SetNameTitle(tmp, tmp);
    Map[eta] -> GetXaxis() ->SetTitle("#it{p}_{T}^{Ref} [GeV]");
    Map[eta] -> GetYaxis() ->SetTitle("#it{p}_{T}^{Rscan} [GeV]");
  }
  for(unsigned int eta = 0; eta< netaBinsReb; eta++){
    tmp = "Mapping_in_PtRefBins_eta_";
    tmp += eta; 
    tmp += "etaRebinned";
    MapReb[eta] = new TGraphErrors();
    MapReb[eta] -> SetNameTitle(tmp, tmp);
    MapReb[eta] -> GetXaxis() ->SetTitle("#it{p}_{T}^{Ref} [GeV]");
    MapReb[eta] -> GetYaxis() ->SetTitle("#it{p}_{T}^{Rscan} [GeV]");
  }
  
  tmp = "Map2D";
  graph2d = new TGraph2DErrors();
  graph2d -> SetNameTitle(tmp,tmp);
  graph2d -> GetXaxis() -> SetTitle("#it{p}_{T}^{Ref}");
  graph2d -> GetYaxis() -> SetTitle("#eta");
  graph2d -> GetZaxis() -> SetTitle("#it{p}_{T}^{Rscan}");
  
  tmp = "Map2D_etaRebinned";
  graph2dReb = new TGraph2DErrors();
  graph2dReb -> SetNameTitle(tmp,tmp);
  graph2dReb -> GetXaxis() -> SetTitle("#it{p}_{T}^{Ref}");
  graph2dReb -> GetYaxis() -> SetTitle("#eta");
  graph2dReb -> GetZaxis() -> SetTitle("#it{p}_{T}^{Rscan}");
}

/*-----------------------------------------------------*/

void PrintLatex( const char* text, double xpos, double ypos, float fsize) {
  double x = xpos;
  double y = ypos;
  TLatex *l = new TLatex(x,y, text);
  l->SetNDC();
  l->SetTextFont(42);
  l->SetTextColor(1);
  l->SetTextSize(fsize);
  l->Draw("same");
}


