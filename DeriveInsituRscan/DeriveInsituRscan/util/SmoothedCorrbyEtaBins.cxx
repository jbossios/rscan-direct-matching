/* **********************************************************************\
 *                                                                      *
 *  #   Name           : SmoothedCorrbyEtaBins                          *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 31/03/16 First version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include <TSystem.h>
#include <sstream>
#include <iostream>
#include <vector>

#include "TCanvas.h"
#include "TFile.h"
#include "TH2F.h"
#include "TLine.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraph2DErrors.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"

#include "DeriveInsituRscan/global.h"

std::vector<TGraph> SplitTGraph2DbyEta( TGraph2D * gr , TString name);
std::vector<TGraphErrors> SplitTGraph2DErrorsbyEta( TGraph2DErrors * gr , TString name);

TString PATH("");
TString JetCollection = "";
TString MCVersion     = "";  
TString DataVersion   = "";

int main( int argc, char* argv[] ){

  std::string m_MCVersion = "";
  std::string m_DataVersion = "";
  std::string m_jetCollection = "";
  std::string m_systName = ""; // Default Nominal
  std::string m_path = "";
  bool m_numericalInversion = false;
  bool m_applyMap = false;
  bool m_compareTGraphs = false;
  bool m_zjet = false;
  bool m_noSmoothing = false;

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }
    
    if ( opt.find("--mcVersion=")   != std::string::npos) m_MCVersion = v[1];
    if ( opt.find("--dataVersion=")   != std::string::npos) m_DataVersion = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

    if ( opt.find("--nInversion=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
      if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
    }
    if ( opt.find("--noSmoothing=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_noSmoothing= true;
    }  
    if ( opt.find("--ApplyMap=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_applyMap= true;
    }
     if ( opt.find("--ZJetStudy=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_zjet= true;
    }   
    if ( opt.find("--CompareTGraphs=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_compareTGraphs= true;
    }

    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
      if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
      if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
      if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
      if (v[1].find("MCSherpa") != std::string::npos) m_systName = "MCSherpa";
    }

  }//End: Loop over input options
  
  JetCollection = m_jetCollection;
  MCVersion     = m_MCVersion;
  DataVersion   = m_DataVersion;
  PATH          = m_path;

  if(m_path==""){
    std::cout << "ERROR: Path should be provided, exiting" << std::endl;
    return 0;
  }
  if(JetCollection==""){
    std::cout << "ERROR: Jet collection should be provided, exiting" << std::endl;
    return 0;
  }
  if(MCVersion==""){
    std::cout << "ERROR: MC Tree version should be provided, exiting" << std::endl;
    return 0;
  }
  if(DataVersion==""){
    std::cout << "ERROR: Data Tree version should be provided, exiting" << std::endl;
    return 0;
  }

  TString inputFile = PATH;
  inputFile += "ROOTfiles/DirectMatching/";
  inputFile += "SmoothingTool_Outputs/";
  inputFile += DataVersion;
  inputFile += "_vs_";
  inputFile += MCVersion;
  inputFile += "/";
  inputFile += JetCollection;
  inputFile += "/Correction_";
  if(m_noSmoothing) inputFile += "not";
  inputFile += "Smoothed";
  if(m_systName!=""){
    inputFile += "_";
    inputFile += m_systName;
  }
  if(m_numericalInversion) inputFile += "_numericalInversion";
  if(m_zjet) inputFile += "_ZJet";
  inputFile += ".root";
  
  //-----------------
  // Open ROOT file
  //-----------------

  if(m_applyMap) std::cout << "Need to Split Smoothed Correction TGraph2D by eta bins!" << std::endl;
  
  std::cout << "Opening: " << inputFile << std::endl;
  TFile* tin = new TFile(inputFile,"read");
  TH2D* hcorr2D;
  TH1D* hcorr[nhEtaBins];
  TString name = "AntiKt";
  name += JetCollection; name += "Topo_EtaInterCalibration";
  hcorr2D = (TH2D*)tin->Get(name.Data()); 
  hcorr2D->SetDirectory(0);// "detach" the histogram from the file

  TH2D* tcorr2D = nullptr;
  TH1D* tcorr[nhEtaBins];
  TString sname = "Correction_TH2DFormat"; 
  if(!m_noSmoothing){
    tcorr2D = (TH2D*)tin->Get(sname.Data());
    tcorr2D->SetDirectory(0);
  }

  TString hname, tname;
  // Loop over Eta Bins
  for(int i=15;i<=74;++i){
    hname = "SmoothedCorr_vs_pT_Eta_";
    if(m_noSmoothing) hname = "Corr_vs_pT_Eta_";
    tname = "Corr_vs_pT_Eta_";
    hname += i;
    tname += i;
    hcorr[i-15] = new TH1D(hname.Data(),"",npTBins,pTBins);
    hcorr[i-15]->SetDirectory(0);
    tcorr[i-15] = new TH1D(tname.Data(),"",npTBins,pTBins);
    tcorr[i-15]->SetDirectory(0);

    // Loop over pT Bins
    double npTbins = hcorr2D->GetNbinsX();
    for(int ipT=1;ipT<=npTbins;++ipT){
      double R = hcorr2D->GetBinContent(ipT,i-14);
      double dR = hcorr2D->GetBinError(ipT,i-14);
      hcorr[i-15]->SetBinContent(ipT,R);
      hcorr[i-15]->SetBinError(ipT,dR);
    }
    if(m_applyMap && !m_noSmoothing){
      npTbins = tcorr2D->GetNbinsX();
      for(int ipT=1;ipT<=npTbins;++ipT){
	double R = tcorr2D->GetBinContent(ipT,i-14);
	double dR = tcorr2D->GetBinError(ipT,i-14);
	tcorr[i-15]->SetBinContent(ipT,R);
	tcorr[i-15]->SetBinError(ipT,dR);
      }
    }
  }

  name += "_Tgraph";
  TGraph2D * gr;
  std::vector<TGraph> Smo;
   
  if(m_compareTGraphs && m_applyMap && !m_noSmoothing){
    gr = (TGraph2D*)tin->Get(name.Data());
    Smo = SplitTGraph2DbyEta(gr, "SmoothedCorr_vs_pT_Eta_");
  }
  tin->Close();

  //------------------------------
  // Save histograms to ROOT file
  //------------------------------
  TString outputFile = PATH;
  if(!m_noSmoothing) outputFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/";
  else{outputFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/";}
  outputFile += DataVersion;
  outputFile += "_vs_";
  outputFile += MCVersion;
  outputFile += "/";
  outputFile += JetCollection;
  if(!m_noSmoothing) outputFile += "/SmoothedCorr_vs_pTRscan";
  else{outputFile += "/Corr_vs_pTRscan_TH1s";}
  if(m_systName!=""){
    outputFile += "_";
    outputFile += m_systName;
  }
  if(m_numericalInversion) outputFile += "_numericalInversion";
  if(m_zjet) outputFile += "_ZJet";
  outputFile += ".root";
  std::cout << "Histograms saved in: " << outputFile.Data() << std::endl;
  TFile* tout = new TFile(outputFile.Data(),"RECREATE");
  tout->cd();
  for(int i=15;i<=74;++i){
    hcorr[i-15]->Write();
  }
  if(m_applyMap && !m_noSmoothing){
    for(int i=15;i<=74;++i){
      tcorr[i-15]->Write();
    }
    if(m_compareTGraphs && !m_noSmoothing){
      for(unsigned int j = 0; j< Smo.size(); j++){
	Smo[j].Write();
      }
    }
  }
  
  tout->Close();

  std::cout << ">>>> Done <<<<" << std::endl;

  return 0;

}

/****************** USEFUL FUNCTIONS *****************/

std::vector<TGraphErrors> SplitTGraph2DErrorsbyEta( TGraph2DErrors * gr, TString name ){

  std::vector<TGraphErrors> vec;
  std::vector<double> etaValues;
  
  for(unsigned int i = 0; i< nhEtaBins; i++){
   etaValues.push_back( hEtaBins[i] + 0.05 );
  }
  for(unsigned int eta = 0; eta< etaValues.size(); eta++ ){
    TString tname = name;
    tname += eta;
    TGraphErrors g = TGraphErrors();
    g.SetName(tname);
    g.GetXaxis()->SetTitle("p_{T}^{Rscan} [GeV]");
    g.GetYaxis()->SetTitle("Correction");
    
    double myEta=etaValues[eta];
    int counter = 0;
    
    for( int i = 0; i< gr->GetN(); i++ ){
      double x = gr ->GetX()[i], y = gr ->GetY()[i], z = gr->GetZ()[i], ex = 0.001, ez = gr->GetEZ()[i];
      if(fabs(y-myEta)>0.01) continue;   // Pick up only the pT bins for that particular eta
      g.SetPoint(counter, x, z);
      g.SetPointError(counter, ex, ez);
      counter ++;
    } //fill tgraph for one eta
    vec.push_back( g );
  } // filled al etas.
  return vec;
}

/*-------------------------------------------------------------------------------------------------*/

std::vector<TGraph> SplitTGraph2DbyEta( TGraph2D * gr , TString name){

  std::vector<TGraph> vec;
  std::vector<double> etaValues;
  
  for(unsigned int i = 0; i< nhEtaBins; i++){
   etaValues.push_back( hEtaBins[i] + 0.05 );
  }
  for(unsigned int eta = 0; eta< etaValues.size(); eta++ ){
    TString tname = name;
    tname += (eta+15);
    TGraph g = TGraph();
    g.SetName(tname);
    g.GetXaxis()->SetTitle("p_{T}^{Rscan} [GeV]");
    g.GetYaxis()->SetTitle("Correction");
    
    double myEta=etaValues[eta];
    int counter = 0;
    
    for( int i = 0; i< gr->GetN(); i++ ){
      double x = gr ->GetX()[i], y = gr ->GetY()[i], z = gr->GetZ()[i];
      if(fabs(y-myEta)>0.01) continue;   // Pick up only the pT bins for that particular eta
      g.SetPoint(counter, x, z);
      counter ++;
    } //fill tgraph for one eta
    vec.push_back( g );
  } // filled al etas.
  return vec;
}
