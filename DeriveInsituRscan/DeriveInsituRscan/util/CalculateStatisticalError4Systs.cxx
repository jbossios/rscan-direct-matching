/* **********************************************************************\
 *                                                                      *
 *  #   Name           : CalculateStatisticalError4Systs                *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 17/08/18 First version              F. Daneri and J. Bossio       *
\************************************************************************/

#include <TSystem.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include "TCanvas.h"
#include "TFile.h"
#include "TH2F.h"
#include "TLine.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraph2DErrors.h"
#include "TTree.h"
#include <TROOT.h>

#include "DeriveInsituRscan/global.h"

using namespace std;

TString JetCollection;
TString MCVersion;
TString DataVersion;
TString PATH_rootfiles;
TString inputMC_Nom = "";
TString inputData_Nom = "";
TString inputMC_Var = "";
TString inputData_Var = "";

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[]){

  gROOT->ProcessLine(".L loader.C+");

  std::string m_MCVersion = "";
  std::string m_DataVersion = "";
  std::string m_jetCollection = "";
  std::string m_systName = ""; // Default Nominal
  std::string m_generator = "Pythia"; // Default Pythia
  std::string m_path = "";
  std::string m_etaSlice = "";
  bool m_numericalInversion = false;
  bool m_mc15c_mc15bmuprofile = false;
  bool m_mc15c = false;
  bool m_mc16a = false;
  bool m_mc16c = false;
  bool m_mc16d = false;
  bool m_insitu = false;
  bool m_zjet = false;
  bool m_debugHists = false;
  bool m_applyEtaRebinning = false;

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--etaSlice=")   != std::string::npos) m_etaSlice = v[1];

    if ( opt.find("--mcVersion=")   != std::string::npos) m_MCVersion = v[1];
    if ( opt.find("--dataVersion=")   != std::string::npos) m_DataVersion = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

    if ( opt.find("--insitu=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_insitu= true;
      if (v[1].find("FALSE") != std::string::npos) m_insitu= false;
    }

    if ( opt.find("--nInversion=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
      if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
    }

    if ( opt.find("--ZJetStudy=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_zjet= true;
    }
    if ( opt.find("--ApplyEtaRebinning=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_applyEtaRebinning= true;
    }
    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
      if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
      if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
      if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
      if (v[1].find("MCSherpa") != std::string::npos) m_systName = "MCSherpa";
    }
    
    if ( opt.find("--debugHists=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debugHists= true;
      if (v[1].find("FALSE") != std::string::npos) m_debugHists= false;
    }
  }//End: Loop over input options

  JetCollection = m_jetCollection;
  MCVersion     = m_MCVersion;
  DataVersion   = m_DataVersion;

  // Protections
  if(m_path==""){
    std::cout << "ERROR: Path should be provided, exiting" << std::endl;
    return 1;
  }
  if(JetCollection==""){
    std::cout << "ERROR: Jet collection should be provided, exiting" << std::endl;
    return 1;
  }
  if(MCVersion==""){
    std::cout << "ERROR: MC Tree version should be provided, exiting" << std::endl;
    return 1;
  }
  if(DataVersion==""){
    std::cout << "ERROR: Data Tree version should be provided, exiting" << std::endl;
    return 1;
  }
  if(m_systName==""){
    std::cout << "ERROR: m_systName needs to be set, exiting" << std::endl;
    return 1; 
  }

  if(MCVersion.Contains("Pythia")) m_generator = "Pythia";

  if(MCVersion.Contains("MC15bMuProfile")) m_mc15c_mc15bmuprofile = true;
  else if(MCVersion.Contains("MC15")){m_mc15c = true;}
  else if(MCVersion.Contains("MC16a")){m_mc16a = true;}
  else if(MCVersion.Contains("MC16c")){m_mc16c = true;}
  else if(MCVersion.Contains("MC16d")){m_mc16d = true;}

  PATH_rootfiles = m_path;

  if(m_insitu) std::cout << "Data input with Insitu correction applied" << std::endl;

  if (m_systName == ""){std::cout << "SystName: Nominal" << std::endl;}
  else{ std::cout << "SystName: " << m_systName << std::endl;}

  // Input Nominal files

  inputMC_Nom = "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/";
  inputMC_Nom += "MC/";
  inputMC_Nom += MCVersion;
  inputMC_Nom += "/";
  inputMC_Nom += JetCollection;
  if(m_mc15c_mc15bmuprofile) inputMC_Nom += "/MC15c_MC15bmuProfile_";
  if(m_mc15c) inputMC_Nom += "/MC15c_";
  if(m_mc16a) inputMC_Nom += "/MC16a_";
  if(m_mc16c) inputMC_Nom += "/MC16c_";
  if(m_mc16d) inputMC_Nom += "/MC16d_";
  if(!m_zjet) inputMC_Nom += m_generator;
  else{ inputMC_Nom +="ZJetStudy";}
  inputMC_Nom += "_Outputs_vs_pT";
  if( m_systName == "MCSherpa" ) inputMC_Nom += "_Nominal4Sherpa";
  if(m_numericalInversion) {inputMC_Nom += "_numericalInversion";}
  inputMC_Nom += "_etaSlice_";
  inputMC_Nom += m_etaSlice;
  if(m_applyEtaRebinning) inputMC_Nom += "_EtaRebinned";
  inputMC_Nom += ".root";
  inputData_Nom += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/";
  inputData_Nom += "Data/";  
  inputData_Nom += DataVersion;
  inputData_Nom += "/";  
  inputData_Nom += JetCollection;
  inputData_Nom += "/Outputs_vs_pT";
  if(m_zjet) inputData_Nom += "_ZJetStudy";
  if( m_systName == "MCSherpa" ) inputData_Nom += "_Nominal4Sherpa";
  if(m_numericalInversion){ inputData_Nom += "_numericalInversion";}
  if(m_insitu){ inputData_Nom += "_Insitu";}
  inputData_Nom += "_etaSlice_";
  inputData_Nom += m_etaSlice;
  if(m_applyEtaRebinning) inputData_Nom += "_EtaRebinned";
  inputData_Nom += ".root";

  //-----------------
  // Open ROOT files
  //-----------------
  // Data
  TString inputfileData_Nom = PATH_rootfiles;
  inputfileData_Nom += inputData_Nom;
  std::cout << "Opening: " << inputfileData_Nom << std::endl;
  TFile* tinData_Nom = new TFile(inputfileData_Nom,"read");
 
  // MC
  TString inputfileMC_Nom = PATH_rootfiles;
  inputfileMC_Nom += inputMC_Nom;
  std::cout << "Opening: " << inputfileMC_Nom << std::endl;
  TFile* tinMC_Nom = new TFile(inputfileMC_Nom,"read");

  // Input Variation files

  inputMC_Var = "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/";
  inputMC_Var += "MC/";
  inputMC_Var += MCVersion;
  inputMC_Var += "/";
  inputMC_Var += JetCollection;
  if(m_mc15c_mc15bmuprofile) inputMC_Var += "/MC15c_MC15bmuProfile_";
  if(m_mc15c) inputMC_Var += "/MC15c_";
  if(m_mc16a) inputMC_Var += "/MC16a_";
  if(m_mc16c) inputMC_Var += "/MC16c_";
  if(m_mc16d) inputMC_Var += "/MC16d_";
  if(!m_zjet) inputMC_Var += m_generator;
  else{ inputMC_Var +="ZJetStudy";}
  inputMC_Var += "_Outputs_vs_pT";
  inputMC_Var+= "_"+m_systName;
  if(m_numericalInversion) {inputMC_Var += "_numericalInversion";}
  inputMC_Var += "_etaSlice_";
  inputMC_Var += m_etaSlice;
  if(m_applyEtaRebinning) inputMC_Var += "_EtaRebinned";
  inputMC_Var += ".root";

  inputData_Var += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/";
  inputData_Var += "Data/";  
  inputData_Var += DataVersion;
  inputData_Var += "/";  
  inputData_Var += JetCollection;
  inputData_Var += "/Outputs_vs_pT";
  if(m_zjet) inputData_Var += "_ZJetStudy";
  if(m_systName!="" && m_systName!="MCSherpa"){ inputData_Var+="_"+ m_systName;}
  if(m_systName == "MCSherpa") inputData_Var += "_Nominal4Sherpa";
  if(m_numericalInversion){ inputData_Var += "_numericalInversion";}
  if(m_insitu){ inputData_Var += "_Insitu";}
  inputData_Var += "_etaSlice_";
  inputData_Var += m_etaSlice;
  if(m_applyEtaRebinning) inputData_Var += "_EtaRebinned";
  inputData_Var += ".root";

  //-----------------
  // Open ROOT files
  //-----------------
  // Data
  TString inputfileData_Var = PATH_rootfiles;
  inputfileData_Var += inputData_Var;
  std::cout << "Opening: " << inputfileData_Var << std::endl;
  TFile* tinData_Var = new TFile(inputfileData_Var,"read");
 
  // MC
  TString inputfileMC_Var = PATH_rootfiles;
  inputfileMC_Var += inputMC_Var;
  std::cout << "Opening: " << inputfileMC_Var << std::endl;
  TFile* tinMC_Var = new TFile(inputfileMC_Var,"read");

  // Get Hists with Rel. Response for data and MC for each eta bin
 
  // Get one hist with the corrections for each replica
  TH1D* hCorr_Nom[nReplicas];
  TH1D* hData_Nom[nReplicas];
  TH1D* hMC_Nom[nReplicas];
  TH1D* hCorr_Var[nReplicas];
  TH1D* hData_Var[nReplicas];
  TH1D* hMC_Var[nReplicas];
  TString name;
  TString name_clone_Corr;

  cout << "Loop over replicas" << endl;
  for(int j=0; j<nReplicas; j++){
      name = "Response_vs_pTRef_Replica_";
      name += j;
      name_clone_Corr = "Corr_vs_pT_Replica_";
      name_clone_Corr += j;
   
      hData_Nom[j] = (TH1D*)tinData_Nom->Get(name.Data()); // Data
      hData_Nom[j]->SetDirectory(0);// "detach" the histogram from the file
      hMC_Nom[j] = (TH1D*)tinMC_Nom->Get(name.Data()); // MC
      hMC_Nom[j]->SetDirectory(0);// "detach" the histogram from the file
      hCorr_Nom[j] = (TH1D*)hMC_Nom[j]->Clone(name_clone_Corr);
      hCorr_Nom[j]->Divide(hData_Nom[j]);
      hCorr_Nom[j]->SetDirectory(0);// "detach" the histogram from the file

      hData_Var[j] = (TH1D*)tinData_Var->Get(name.Data()); // Data
      hData_Var[j]->SetDirectory(0);// "detach" the histogram from the file
      hMC_Var[j] = (TH1D*)tinMC_Var->Get(name.Data()); // MC
      hMC_Var[j]->SetDirectory(0);// "detach" the histogram from the file
      hCorr_Var[j] = (TH1D*)hMC_Var[j]->Clone(name_clone_Corr);
      hCorr_Var[j]->Divide(hData_Var[j]);
      hCorr_Var[j]->SetDirectory(0);// "detach" the histogram from the file
  }

  TH1D* h_RMS_vs_pTRef;
  TString Name_RMS_vs_pTRef;
  Name_RMS_vs_pTRef = "RMS_vs_pTRef_Eta_";
  Name_RMS_vs_pTRef += m_etaSlice;
  h_RMS_vs_pTRef = new TH1D(Name_RMS_vs_pTRef.Data(),"",npTBins,pTBins);

  TH1D * h_SystematicValue[43];
  TString Naming;
  
  // Compute RMS  
  // Loop over pT bins
  for(int ipT=0; ipT<43; ipT++){
   
      float RMS = 0;
      float r_value = 0;
      float r_value_square = 0;
      int good_fit = 0;
      
      if(m_debugHists){
	Naming = "SystematicValue_Eta_";
	Naming += m_etaSlice;
	Naming += "_Pt_";
	Naming += ipT;
	h_SystematicValue[ipT] = new TH1D(Naming.Data(),"",10000,-0.5,0.5);
      }

      // Loop over replicas
      for(int j=0; j<nReplicas; j++){
         if(hCorr_Var[j]->GetBinContent(ipT)!=0 && hCorr_Nom[j]->GetBinContent(ipT)!=0){
           float Var_Response_pT = (hCorr_Var[j]->GetBinContent(ipT)/hCorr_Nom[j]->GetBinContent(ipT))-1.0;
	   
	   if(m_debugHists) h_SystematicValue[ipT]->Fill(Var_Response_pT);
	 
           r_value += Var_Response_pT;
           r_value_square += Var_Response_pT * Var_Response_pT;
           good_fit++;
         }  
      } // END: loop over replicas
      if(r_value != 0){
         RMS = sqrt( fabs( r_value_square/good_fit - (r_value/good_fit) * (r_value/good_fit) ) );
         h_RMS_vs_pTRef->SetBinContent(ipT,RMS);
      }
  } //end loop over ipT (pT bins)

  //------------------------------
  // Save histograms to ROOT file
  //------------------------------
  
  std::cout << "Saving plots into a ROOT file..." << std::endl;

  // Output file name
  TString outputfile("");
  outputfile += PATH_rootfiles;
  outputfile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Statistical_errors/";
  outputfile += DataVersion;
  outputfile += "_vs_";
  outputfile += MCVersion;
  outputfile += "/";
  outputfile += JetCollection	;
  outputfile += "/Stat_error_";
  if(m_zjet) outputfile += "ZJetStudy_";
  outputfile += "etaSlice_";
  outputfile += m_etaSlice;
  outputfile += "_"+m_systName;
  if(m_applyEtaRebinning) outputfile +="_EtaRebinned";
  outputfile += ".root";

  // Opening output file
  TFile* tout = new TFile(outputfile,"recreate");
  std::cout << "output file: " << outputfile << std::endl;
  tout->cd();

  h_RMS_vs_pTRef->Write();

  if(m_debugHists){
    for(int ipT=0; ipT<43; ipT++){
      h_SystematicValue[ipT]->Write();
    }
  }
  tout->Close();

  for(int j=0; j<nReplicas; j++){
      delete hData_Nom[j];
      delete hMC_Nom[j];
      delete hData_Var[j];
      delete hMC_Var[j];
      delete hCorr_Nom[j];
      delete hCorr_Var[j];
  }
  tinData_Nom->Close();
  tinMC_Nom->Close();
  tinData_Var->Close();
  tinMC_Var->Close();


  return 0;

}


