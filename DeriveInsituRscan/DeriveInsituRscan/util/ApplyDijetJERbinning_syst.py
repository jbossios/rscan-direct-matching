#!/usr/bin/python
import os, sys
import array
from ROOT import *

param=sys.argv

######################################################################
## Author : M. Toscani (mtoscani@cern.ch)    Updated: 22/10/2018    ## 
## ################################################################ ##
## Description:                                                     ##
## The calibration is derivated in eta bins of width 0.1. For the   ##
## systematic calculation we use a coarser binning from DijetJER,   ##
## so we need to rebin the TreetoHists Outputs.                     ##
##                                                                  ##
## Compilation Needed: because this code needs to manipulate        ##
## objects from  BootstrapGenerator Package, MUST run setup.sh      ##
## beforehand. Steered by Run_ComputeRelativeResponse.py            ## 
######################################################################

## Default Values:
CoarseBinning = False
PRW = False
Debug = False
ZJetStudy = False
Bootstrap = False
jetColl = ""
syst = ""
version = ""
path_dir = ""
MergeMap = False
######################################################################
## Loop over input paramenters
######################################################################

for par in param:
  if "--CoarseBinning=TRUE" in par:
    CoarseBinning = True   # false: default
  if "--PRW=TRUE" in par:
    PRW = True
  if "--Debug=TRUE" in par:
    Debug = True
  if "--ZJetStudy=TRUE" in par:
    ZJetStudy = True
  if "--Bootstrap=TRUE" in par:
    Bootstrap = True
  if "--jetColl" in par:
    jetColl = par.split("=")[1]
  if "--syst" in par:
    syst = par.split("=")[1]
  if "--inputVersion" in par:
    version = par.split("=")[1]
  if "--path" in par:
    path_dir = par.split("=")[1]

if path_dir is "":
  print "No path specified, exiting!"
  sys.exit(0)
if version is "":
  print "No version specified, exiting!"
  sys.exit(0)
if jetColl is "":
  print "No Jet Collection specified, exiting!"
  sys.exit(0)
if syst is "":
  print "No systematic specified, exiting!"
  sys.exit(0)

##################################################################################
# DO NOT MODIFY
##################################################################################
if CoarseBinning:
  nEtaBins = 13 
  EtaBins  = [ -3.,-2.5,-1.8,-1.3,-0.7,-0.2, 0., 0.2, 0.7, 1.3, 1.8, 2.5, 3.] 
  etabins = array.array('d', EtaBins)
else: 
  nEtaBins = 17 
  EtaBins = [-3.,-2.8, -2.5, -1.8, -1.3, -1.0, -0.7,-0.2, 0., 0.2, 0.7, 1.0, 1.3, 1.8, 2.5, 2.8, 3.]
  etabins = array.array('d', EtaBins)


nOldEtaBins = 90;
OldEtaBins = [-4.5, -4.4, -4.3, -4.2, -4.1, -4.0, -3.9, -3.8, -3.7, -3.6, -3.5, -3.4, -3.3, -3.2, -3.1, -3.0, -2.9, -2.8, -2.7, -2.6, -2.5, -2.4, -2.3, -2.2, -2.1, -2.0, -1.9, -1.8, -1.7, -1.6, -1.5, -1.4, -1.3, -1.2, -1.1, -1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1 ,0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4.0, 4.1, 4.2, 4.3, 4.4, 4.5]

MergeAbsEtaBins = True # Requested only for Sherpa Systematic.

path_dir += "ROOTfiles/DirectMatching/TreetoHists_Outputs/"

print "Eta Rebinning for sample: ",version 
PATH = path_dir
if "MC" in version:
  PATH += "MC/"
else:
  PATH += "Data/"
PATH += version + "/" + jetColl + "/"
inputFileName = PATH
outputFileName = PATH
print "Doing systematic: ", syst

if "MC" in version:
  inputFileName += version + "_"
  if ZJetStudy:
    inputFileName += "ZJetStudy_"
  else:
    inputFileName += "Pythia_"
  inputFileName += "JZAll_"
  if PRW:
    inputFileName += "PRW"
  else:
    inputFileName += "noPRW"
else:
  inputFileName += "DataAll"
  if ZJetStudy:
    inputFileName += "_ZJetStudy"
if syst != "Nominal":
  inputFileName += "_" 
  if syst =="deltaRUp":
    inputFileName += "DeltaRUp"
  elif syst == "deltaRDown":
    inputFileName += "DeltaRDown"
  elif syst == "isolationUp":
    inputFileName += "IsolationUp"
  elif syst == "isolationDown":
    inputFileName += "IsolationDown"
  else:
    inputFileName += syst
if Bootstrap:
  inputFileName += "_bootstrap"

outputFileName = inputFileName + "_etaRebinned"
if CoarseBinning:
  outputFileName += "_coarse.root"
else:
  outputFileName += ".root"
inputFileName += ".root"
inputFile = TFile.Open(inputFileName, "read")
outputFile = TFile.Open(outputFileName,"recreate")

if not inputFile:
  print "File: ", inputFileName, " not found, exiting"
  sys.exit(0)
print "Opened: ", inputFileName," for eta-rebinning" 

if syst == "Nominal" and "MC" not in version:
  MergeMap = True

for eta in range(0,len(EtaBins)-1):
  inputFile.cd()
  if Debug:
    print "New etaBin #: ", eta, ", adding oldEtaBins #: "
  boot = []
  for beta in range(OldEtaBins.index(EtaBins[eta]),OldEtaBins.index(EtaBins[eta+1])):
    if Debug:
      print beta, ";"
    tmp_name = "Response_vs_pTRef_Eta_"
    new_name = tmp_name
    tmp_name += str(beta)
    new_name += str(eta)
    oldHisto = inputFile.Get(tmp_name)
    if MergeMap:
      tmp_mapname = "hMappingProbePT"
      new_mapname = tmp_mapname
      tmp_mapname += str(beta)
      new_mapname += str(eta)
      oldMapHisto = inputFile.Get(tmp_mapname)
    if Bootstrap:
      tmp_name_boot = tmp_name + "_replica"
      new_name_boot = new_name + "_replica"
      oldBootHisto = TH2DBootstrap()
      inputFile.GetObject(tmp_name_boot,oldBootHisto)
      boot.append(oldBootHisto)
    if beta is OldEtaBins.index(EtaBins[eta]):
      newHisto = oldHisto.Clone(new_name)
      newHisto.SetDirectory(0)
      if MergeMap:
	newMapHisto = oldMapHisto.Clone(new_mapname)
	newMapHisto.SetTitle(new_mapname)
	newMapHisto.SetDirectory(0)
    else:
      newHisto.Add(oldHisto)
      if MergeMap:
	newMapHisto.Add(oldMapHisto)
  if Bootstrap:
    newBootHisto = boot[0]
    for h in range(1,len(boot)):
      newBootHisto.Add(boot[h])
    newBootHisto.SetName(new_name_boot)
  outputFile.cd()
  newHisto.Write()
  if MergeMap:
    newMapHisto.Write()
  if Bootstrap:
    newBootHisto.Write()

inputFile.cd()
inputFile.Close()

outputFile.cd()
# Adding a Second loop to produce mapping and Sherpa systematic (for this I need nominal data and nominal MC as well) in abs eta bins:
if MergeAbsEtaBins and (syst == "MCSherpa" or MergeMap or (syst == "Nominal" and "MC" in version)):
  for eta in range(0,(len(EtaBins)-1)/2):
    tmp_name1 = "Response_vs_pTRef_Eta_" + str(eta)
    tmp_name2 = "Response_vs_pTRef_Eta_" + str(len(EtaBins)-2 - eta)
    if Debug:
      print "Merging etabin: ", str(eta), " with ", str(len(EtaBins) - 2 - eta)
    new_name = tmp_name1 + "_absBins"
    oldHisto1 = TH2F()
    oldHisto2 = TH2F()
    outputFile.GetObject(tmp_name1,oldHisto1)
    outputFile.GetObject(tmp_name2,oldHisto2)
    newHisto  = oldHisto1.Clone(new_name)
    newHisto.Add(oldHisto2)
    newHisto.Write()
    if MergeMap:
      tmp_mapname1 = "hMappingProbePT"
      tmp_mapname2 = tmp_mapname1
      tmp_mapname1 += str(eta)
      tmp_mapname2 += str( len(EtaBins)-2 -eta)
      new_mapname = tmp_mapname1 + "_absBins"
      oldMapHisto1 = TH2D()
      oldMapHisto2 = TH2D()
      outputFile.GetObject(tmp_mapname1,oldMapHisto1)
      outputFile.GetObject(tmp_mapname2,oldMapHisto2)
      newMapHisto = oldMapHisto1.Clone(new_mapname)
      newMapHisto.Add(oldMapHisto2)
      newMapHisto.Write()
    if Bootstrap:
      tmp_name_boot1 = tmp_name1 + "_replica"
      tmp_name_boot2 = tmp_name2 + "_replica"
      new_name_boot = tmp_name_boot1 + "_absBins"
      oldBootHisto1 = TH2DBootstrap()
      outputFile.GetObject(tmp_name_boot1,oldBootHisto1)
      oldBootHisto2 = TH2DBootstrap()
      outputFile.GetObject(tmp_name_boot2,oldBootHisto2)
      newBootHisto = oldBootHisto1
      newBootHisto.SetName(new_name_boot)
      newBootHisto.Add(oldBootHisto2)
      newBootHisto.Write() 

outputFile.Close()
print "Saving rebinned histograms in: ", outputFileName
print "<<<< Eta Rebinning done >>>>"



