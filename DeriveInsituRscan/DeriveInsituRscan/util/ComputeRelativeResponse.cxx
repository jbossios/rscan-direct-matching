/* **********************************************************************\
 *                                                                      *
 *  #   Name:   ComputeRelativeResponse                                 *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 22/02/16 First version              J. Bossio (jbossios@cern.ch)  *
 *  2 16/07/16 Second version             Ro        (mdevesa@cern.ch)   *
 *  3 01/01/17 Third version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include <TSystem.h>
#include <sstream>
#include <iostream>

// Bootstrap package
#include "../../BootstrapGenerator/BootstrapGenerator/TH2DBootstrap.h"
#include "../../BootstrapGenerator/BootstrapGenerator/TH1DBootstrap.h"
#include "../../BootstrapGenerator/BootstrapGenerator/BootstrapGenerator.h"

// ROOT
#include "TCanvas.h"
#include "TFile.h"
#include "TH2F.h"
#include "TLine.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraph2DErrors.h"
#include "TLegend.h"
#include "TLatex.h"

#include "DeriveInsituRscan/global.h"
#include "DeriveInsituRscan/MyFitter.h"

//-------------------------------------------------------//

TString JetCollection;
TString treeVersion;  // version of tree which was used to produce root file with TreetoROOT
TString pTavgType;
TString PATH_plots;
TString PATH_rootfiles;

//---------- Eta Bins ----------------
int minEtaBintoUse = 15; // can be modified in main if ApplyEtaRebinning is requested
int maxEtaBintoUse = 74; // can be modified in main if ApplyEtaRebinning is requested
const int minEtaBin = 15;
const int maxEtaBin = 74;
const int nEtaBinstoUse = (maxEtaBin-minEtaBin)+1; // needs to be of type const, to define size of arrays, always set to fine Rscan binning

const int nAbsEtaBins = 90;
TString AbsEtaBins[nAbsEtaBins+1] = {"-4.5", "-4.4", "-4.3", "-4.2", "-4.1", "-4.0", "-3.9", "-3.8", "-3.7", "-3.6", "-3.5", "-3.4", "-3.3", "-3.2", "-3.1", "-3.0", "-2.9", "-2.8", "-2.7", "-2.6", "-2.5", "-2.4", "-2.3", "-2.2", "-2.1", "-2.0", "-1.9", "-1.8", "-1.7", "-1.6", "-1.5", "-1.4", "-1.3", "-1.2", "-1.1", "-1.0", "-0.9", "-0.8", "-0.7", "-0.6", "-0.5", "-0.4", "-0.3", "-0.2", "-0.1" ,"0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0", "2.1", "2.2", "2.3", "2.4", "2.5", "2.6", "2.7", "2.8", "2.9", "3.0", "3.1", "3.2", "3.3", "3.4", "3.5", "3.6", "3.7", "3.8", "3.9", "4.0", "4.1", "4.2", "4.3", "4.4", "4.5"};
int nEtaBinsReb = 15;
TString AbsEtaBinsRebinned[nAbsEtaBins+1] = {"-3", "-2.8","-2.5", "-1.8", "-1.3","-1.0", "-0.7", "-0.2","0.","0.2","0.7","1.0","1.3","1.8","2.5","2.8","3."};

// For the Sherpa Systematic we take absolute eta bins:
int nEtaBinsReb_abs = 7;
TString AbsEtaBinsRebinned_abs[nAbsEtaBins+1] = {"3.","2.8","2.5","1.8","1.3","1.0","0.7","0.2","0.0"};
//int nEtaBinsReb_Coarse = 11;
//TString AbsEtaBinsRebinned_Coarse[nAbsEtaBins+1] = {"-3", "-2.5", "-1.8", "-1.3", "-0.7", "-0.2","0.","0.2","0.7","1.3","1.8","2.5","3."};

double response_minaxis = 0.4;
double response_maxaxis = 1.8;
double scaleRMS         = 0.9;

int main( int argc, char* argv[] ) {

   std::cout << "Reading user settings" << std::endl;

   std::string m_pTavgType = "";
   std::string m_treeVersion = "";
   std::string m_jetCollection = "";
   std::string m_systName = ""; // Default Nominal
   std::string m_generator = "Pythia"; // Default Pythia
   std::string m_path = "";
   std::string m_etaSlice = "";
   bool m_deriveUncert = false;
   bool m_deriveCalib  = true;
   bool m_bootstrap = false;
   bool m_isMC = true;
   bool m_debug = false;
   bool m_numericalInversion = false;
   bool m_mc15c_mc15bmuprofile = false; 
   bool m_mc15c = false;
   bool m_mc16a = false;
   bool m_mc16c = false;
   bool m_mc16d = false;
   bool m_PRW   = false;
   bool m_zjet  = false;
   bool m_insitu = false; // if true "Insitu" is added to the inputFile and outputFile strings
   bool m_insituSmoothed = false;
   bool m_applyEtaRebinning = false;
   bool m_useAbsEtaBins = false;
   /// bool m_CoarseBinning = false; // not fully implemented (WIP)

   //-------------------------
   // Decode the user settings 
   //-------------------------
   for (int i=1; i< argc; i++){

     std::string opt(argv[i]); std::vector< std::string > v;

     std::istringstream iss(opt);

     std::string item;
     char delim = '=';

     while (std::getline(iss, item, delim)){
       v.push_back(item);
     }

     if ( opt.find("--EtaSlice=")   != std::string::npos) m_etaSlice = v[1];

     if ( opt.find("--pTavgType=")   != std::string::npos) m_pTavgType = v[1];
     
     if ( opt.find("--inputVersion=")   != std::string::npos) m_treeVersion = v[1];
     
     if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

     if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

     if ( opt.find("--insitu=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_insitu= true;
       if (v[1].find("FALSE") != std::string::npos) m_insitu= false;
     }

     if ( opt.find("--insituSmoothed=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_insituSmoothed= true;
       if (v[1].find("FALSE") != std::string::npos) m_insituSmoothed= false;
     }

     if ( opt.find("--PRW=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_PRW= true;
       if (v[1].find("FALSE") != std::string::npos) m_PRW= false;
     }

     if ( opt.find("--isMC=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_isMC= true;
       if (v[1].find("FALSE") != std::string::npos) m_isMC= false;
     }

     if ( opt.find("--ZJetStudy=") != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_zjet= true;
     }

     if ( opt.find("--debug=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_debug= true;
       if (v[1].find("FALSE") != std::string::npos) m_debug= false;
     }

     if ( opt.find("--nInversion=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
       if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
     }

     if ( opt.find("--Bootstrap=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_bootstrap= true;
       if (v[1].find("FALSE") != std::string::npos) m_bootstrap= false;
     }

     if ( opt.find("--DeriveUncert=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_deriveUncert= true;
       if (v[1].find("FALSE") != std::string::npos) m_deriveUncert= false;
     }

     if ( opt.find("--DeriveCalib=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_deriveCalib= true;
       if (v[1].find("FALSE") != std::string::npos) m_deriveCalib= false;
     }

     if ( opt.find("--syst=") != std::string::npos){
       if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
       if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
       if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
       if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
       if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
       if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
       if (v[1].find("MCSherpa") != std::string::npos) m_systName = "MCSherpa";
       if (v[1].find("Nominal4Sherpa") != std::string::npos) m_systName = "Nominal4Sherpa";
     }
 
     if ( opt.find("--ApplyEtaRebinning=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_applyEtaRebinning= true;
       if (v[1].find("FALSE") != std::string::npos) m_applyEtaRebinning= false;
     }
     if ( opt.find("--UseAbsEtaBins=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_useAbsEtaBins= true;
       if (v[1].find("FALSE") != std::string::npos) m_useAbsEtaBins= false;
     }
     /*
     if ( opt.find("--CoarseBinning=")      != std::string::npos) {
       if (v[1].find("TRUE") != std::string::npos) m_CoarseBinning= true;
       if (v[1].find("FALSE") != std::string::npos) m_CoarseBinning= false;
     }
     */
   }//End: Loop over input options

   JetCollection = m_jetCollection;
   treeVersion   = m_treeVersion;
   pTavgType     = m_pTavgType;

   // Protections
   if(m_systName=="MCSherpa" && !m_isMC){
     std::cout << "ERROR: Systematic MCSherpa should be used only for MC, exiting" << std::endl;
     return 1;
   }
   if(m_path==""){
     std::cout << "ERROR: Path should be provided, exiting" << std::endl;
     return 1;
   }
   if(JetCollection==""){
     std::cout << "ERROR: Jet collection should be provided, exiting" << std::endl;
     return 1;
   }
   if(treeVersion==""){
     std::cout << "ERROR: Tree version should be provided, exiting" << std::endl;
     return 1;
   }
   if(m_systName!="" && !m_bootstrap){
     std::cout << "ERROR: Systematics need bootstraps, exiting" << std::endl;
     return 1;
   }
   if(!m_deriveCalib && !m_deriveUncert){
     std::cout << "ERROR: m_deriveCalib and m_deriveUncert are false, one should be true, exiting" << std::endl;
     return 1;
   }
   if(m_deriveCalib && m_deriveUncert){
     std::cout << "ERROR: m_deriveCalib and m_deriveUncert are true, only one should be true, exiting" << std::endl;
     return 1;
   }
   if(m_deriveUncert && m_etaSlice==""){
     std::cout << "ERROR: m_etaSlice needs to be set, exiting" << std::endl;
     return 1;
   }

   if(m_isMC && treeVersion.Contains("MC15bMuProfile")) m_mc15c_mc15bmuprofile = true;
   else if(m_isMC && treeVersion.Contains("MC15")) m_mc15c = true;
   else if(m_isMC && treeVersion.Contains("MC16a")) m_mc16a = true;
   else if(m_isMC && treeVersion.Contains("MC16c")) m_mc16c = true;
   else if(m_isMC && treeVersion.Contains("MC16d")) m_mc16d = true;

   if(m_isMC && treeVersion.Contains("Pythia")) m_generator = "Pythia";

   PATH_plots = m_path; PATH_plots += "Plots_DirectMatching/";
   PATH_rootfiles = m_path;

   if (m_systName == "") std::cout << "SystName: Nominal" << std::endl;
   else{ std::cout << "SystName: " << m_systName << std::endl;}

   if(m_isMC) m_insitu = false;
   if(m_insitu) std::cout << "Input with Insitu correction applied" << std::endl;

   if (m_useAbsEtaBins && !m_applyEtaRebinning) std::cout << "WARNING: absolute eta bins are meant to be taken from the dijetJER eta binning" << std::endl;
   if (!(m_systName == "MCSherpa" || m_systName == "Nominal4Sherpa") &&  m_useAbsEtaBins ) std::cout << "WARNING: Abs Eta binning is meant for the calculation of the Sherpa systematic only!!" << std::endl; 


   //---------- Eta Rebinning ----------------//

   if (m_applyEtaRebinning){
     std::cout << "Merging Eta bins to increase statistics in systematics! Eta Binning is set to DijetJER binning" <<std::endl;
     minEtaBintoUse = 0;
     maxEtaBintoUse = nEtaBinsReb; // (+15: compatibility with Default eta binning loop)
     for( int i = 0; i< nAbsEtaBins+1; i++) AbsEtaBins[i] = AbsEtaBinsRebinned[i];
     if (m_useAbsEtaBins) {
       std::cout << "Using absolute eta bins from the DijetJER binning " << std::endl;
       for( int i = 0; i< nAbsEtaBins+1; i++) AbsEtaBins[i] = AbsEtaBinsRebinned_abs[i];
       maxEtaBintoUse = nEtaBinsReb_abs;
     }	 
     /*if (m_CoarseBinning){
       std::cout << "Coarse eta binning was chosen!" << std::endl;
       for( int i = 0; i< nAbsEtaBins +1; i++) AbsEtaBins[i] = AbsEtaBinsRebinned_Coarse[i];
       maxEtaBintoUse = nEtaBinsReb_Coarse; // (+15: compatibility with Default eta binning loop)
     }*/
   }

   //--------------------
   // MyFitter
   //--------------------
   
   if(m_debug) std::cout << "Initializing JES_ResponseFitter" << std::endl;

   double _NSigmaForFit = 1.5;
   MyFitter* myFitter = new MyFitter(_NSigmaForFit);

   //-----------------
   // Book Histograms
   //-----------------

   if(m_debug) std::cout << "Booking histograms" << std::endl;

   TH1D* h_Response_vs_pT[nEtaBinstoUse];
   TH1D* h_Resolution_vs_pT[nEtaBinstoUse];
   TH1D* h_Response_vs_pT_replicas[nReplicas];
   TString Name;
   if(m_deriveCalib){
     for (int i = minEtaBintoUse; i <= maxEtaBintoUse; i++ ) {
       Name = "Response_vs_pTRef_Eta_";
       Name += i;
       h_Response_vs_pT[i-minEtaBintoUse] = new TH1D(Name.Data(),"",npTBins,pTBins);
       h_Response_vs_pT[i-minEtaBintoUse]->Sumw2();
       Name = "Resolution_vs_pTRef_Eta_";
       Name += i;
       h_Resolution_vs_pT[i-minEtaBintoUse] = new TH1D(Name.Data(),"",npTBins,pTBins);
       h_Resolution_vs_pT[i-minEtaBintoUse]->Sumw2();
     }
   }
   if(m_bootstrap && m_deriveUncert){
     for (int i = 0; i < nReplicas; i++ ) {
       Name = "Response_vs_pTRef_Replica_";
       Name += i;
       h_Response_vs_pT_replicas[i] = new TH1D(Name.Data(),"",npTBins,pTBins);
     }
   }

   //-----------------
   // Open Input File
   //-----------------

   TString inputFile = PATH_rootfiles;
   inputFile+= "ROOTfiles/";
   inputFile+= "DirectMatching/";
   if(m_isMC){
     inputFile += "TreetoHists_Outputs/MC/";
     inputFile += treeVersion;
     inputFile += "/";
     inputFile += JetCollection;
     if(m_mc15c_mc15bmuprofile) inputFile += "/MC15c_MC15bmuProfile_";
     if(m_mc15c) inputFile += "/MC15c_";
     if(m_mc16a) inputFile += "/MC16a_";
     if(m_mc16c) inputFile += "/MC16c_";
     if(m_mc16d) inputFile += "/MC16d_";
     if(!m_zjet) {
       inputFile += m_generator;
     } else {
       inputFile += "ZJetStudy";
     }
     inputFile += "_JZAll";
     if(m_PRW) inputFile += "_PRW";
     else{inputFile += "_noPRW";}
     if(!(m_systName == "" || m_systName == "Nominal4Sherpa")){  // Nominal4Sherpa should be same input file that nominal, but output will be different.
       inputFile += "_";
       inputFile += m_systName;
     }
     if(m_numericalInversion) inputFile += "_numericalInversion_";
     if(m_bootstrap) inputFile += "_bootstrap";
     if(m_applyEtaRebinning) inputFile += "_etaRebinned";
     inputFile += ".root";
   } else{
     inputFile += "TreetoHists_Outputs/Data/";
     inputFile += treeVersion;
     inputFile += "/";
     inputFile += JetCollection;
     inputFile += "/DataAll";
     if(m_zjet) inputFile += "_ZJetStudy";
     if(!(m_systName == "" || m_systName == "Nominal4Sherpa")){  // Nominal4Sherpa should be same input file that nominal, but output will be different.
       inputFile += "_";
       inputFile += m_systName;
     }
     if(m_numericalInversion) inputFile += "_numericalInversion_";
     if(m_insitu) inputFile += "_Insitu";
     if(m_insitu && !m_insituSmoothed) inputFile += "notSmoothed";
     if(m_bootstrap) inputFile += "_bootstrap";
     if(m_applyEtaRebinning) inputFile += "_etaRebinned";
     inputFile += ".root";
   }
   std::cout << "Openning: " << inputFile << std::endl;
   TFile* file = new TFile(inputFile.Data(),"READ");

   //----------------------
   // Response  vs pTRef
   //----------------------

   double min, max; // Range of Fit
   TH2D* histo;
   TH2Bootstrap* histo_bootstrap;
   TString h_name;

   if(m_deriveCalib){
     double x_0 = 0.16;
     double position_0 = 0.86;
     double position_1 = 0.86-0.05;
     double position_2 = 0.86-0.1;
     double position_3 = 0.86-0.15;
     double position_4 = 0.86-0.20;
     TString text_0;
     TString text_1;
     TString text_2;
     TString text_3;
     TString text_4;
     TLatex *TextBlock0;
     TLatex *TextBlock1;
     TLatex *TextBlock2;
     TLatex *TextBlock3;
     TLatex *TextBlock4;
   	
     TCanvas *can5 = new TCanvas();
     can5->SetMargin(0.12,0.04,0.12,0.04);
     TString ps4 = PATH_plots;

     ps4 += "ResponseFitter/";
     if(m_isMC) ps4 += "MC/";
     else{ps4 += "Data/";}
     ps4 += treeVersion;
     ps4 += "/";
     ps4 += JetCollection;
     ps4 += "/Response_Fits";
     if(m_systName!=""){
       ps4 += "_";
       ps4 += m_systName;
     }
     if(m_insitu) ps4 += "_Insitu";
     if(m_insitu && !m_insituSmoothed) ps4 += "notSmoothed";
     if(m_applyEtaRebinning) ps4 += "_etaRebinned";
     ps4 += ".pdf";
     can5->Print(ps4+"[");
     can5->SetLogy(0);
     
     // Loop over eta bins
     for (int i = minEtaBintoUse; i <=maxEtaBintoUse; i++ ) {
       if(!m_bootstrap){
         TString name_tmp = TString::Format("Response_vs_pTRef_Eta_%i",i);
         if (m_useAbsEtaBins) name_tmp += "_absBins";
	 histo = (TH2D*)file->Get(name_tmp.Data());
         if(m_debug) std::cout << "Getting histo: " << name_tmp.Data() << std::endl;
       } else { // Get Nominal from Bootstrap
         TString name_tmp = TString::Format("Response_vs_pTRef_Eta_%i_replica",i);
	 if (m_useAbsEtaBins) name_tmp += "_absBins";
         histo_bootstrap = (TH2Bootstrap*)file->Get(name_tmp.Data());
         TH2D* tmp_nom = (TH2D*)histo_bootstrap->GetNominal();
         histo = (TH2D*)tmp_nom->Clone("h_nominal");
       }
       // Loop over pT bins
       for(int j=0;j<npTBins;++j){//use the same bins
            
         h_name += "projx_";
         h_name += pTStrBins[j];
         h_name += "_";
         h_name += pTStrBins[j+1];
       
         int low  = histo->GetXaxis()->FindBin(pTBins[j]);
         int high = histo->GetXaxis()->FindBin(pTBins[j+1])-1;
         TH1D* histo_px = histo->ProjectionY(h_name,low,high,"e");

         histo_px->SetAxisRange(0,2,"X");
         histo_px->SetXTitle("p_{T}^{Rscan}/p_{T}^{Ref}");
         histo_px->GetXaxis()->SetTitleOffset(1.3);
         histo_px->SetMarkerColor(kBlack);
         histo_px->Draw();

         // Fit Range
         min = histo_px->GetMean() - histo_px->GetRMS()*scaleRMS;
         max = histo_px->GetMean() + histo_px->GetRMS()*scaleRMS;

         histo_px->SetAxisRange(response_minaxis,response_maxaxis,"X");

         // Fit
         double Mean, MeanError, Sigma, SigmaError, Neff;
         myFitter->Fit(histo_px,min,max);
         Mean       = myFitter->GetMean();
         MeanError  = myFitter->GetMeanError();
         Sigma      = myFitter->GetSigma();
         SigmaError = myFitter->GetSigmaError();
         Neff       = myFitter->Neff();

         if(!(Mean==0 && MeanError==0 && Sigma==0 && SigmaError==0)) myFitter->DrawFitAndHisto(0.,2.);
       
         text_0 = "#scale[0.7]{"+AbsEtaBins[i]+" #leq #eta < "+AbsEtaBins[i+1]+"}";
	 if (m_useAbsEtaBins) text_0 = "#scale[0.7]{"+AbsEtaBins[i+1]+" #leq |#eta| < "+AbsEtaBins[i]+"}";
         text_1 = "#scale[0.7]{"+pTStrBins[j]+" GeV #leq p_{T}^{Ref} < "+pTStrBins[j+1]+" GeV}";
         if(JetCollection=="2LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.2, LC+JES}";
         else if(JetCollection=="3LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.3, LC+JES}";
         else if(JetCollection=="5LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.5, LC+JES}";
         else if(JetCollection=="6LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.6, LC+JES}";
         else if(JetCollection=="7LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.7, LC+JES}";
         else if(JetCollection=="8LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.8, LC+JES}";
         TString entries = std::to_string(histo_px->GetEntries());
         TString effEntries = std::to_string(Neff);
         text_3 = "#scale[0.7]{Entries: "+entries+"}";
         text_4 = "#scale[0.7]{EffEntries: "+effEntries+"}";
         TextBlock0 = new TLatex(x_0,position_0, text_0.Data());
         TextBlock1 = new TLatex(x_0,position_1, text_1.Data());
         TextBlock2 = new TLatex(x_0,position_2, text_2.Data());
         TextBlock3 = new TLatex(x_0,position_3, text_3.Data());
         TextBlock4 = new TLatex(x_0,position_4, text_4.Data());
         TextBlock0->SetNDC();
         TextBlock1->SetNDC();
         TextBlock2->SetNDC();
         TextBlock3->SetNDC();
         TextBlock4->SetNDC();
         TextBlock0->Draw("same");
         TextBlock1->Draw("same");
         TextBlock2->Draw("same");
         TextBlock3->Draw("same");
         TextBlock4->Draw("same");
         can5->SetLogx(0); 

         histo_px->GetXaxis()->SetMoreLogLabels();
         histo_px->SetStats(0);
         can5->Print(ps4);

         // Store Response and Resolution in histograms
         double Response = Mean;
         double ResponseError = MeanError;
         if (Response!=0 && ResponseError!=0){
           h_Response_vs_pT[i-minEtaBintoUse]->SetBinContent(j+1,Response);
           h_Response_vs_pT[i-minEtaBintoUse]->SetBinError(j+1,ResponseError);
         }
         if(Neff>100){
           double Resolution = Sigma/Mean;
           double ResolutionError = SigmaError/Mean + (Sigma*MeanError)/(Mean*Mean); 
           h_Resolution_vs_pT[i-minEtaBintoUse]->SetBinContent(j+1,Resolution);
           h_Resolution_vs_pT[i-minEtaBintoUse]->SetBinError(j+1,ResolutionError);
         }
       }//END: Loop over pT bins
     }//END: Loop over eta bins
     can5->Print(ps4+"]");
     delete can5;

     //---------------------
     // Response 
     //--------------------
     TCanvas *can6 = new TCanvas();
     can6->SetMargin(0.12,0.04,0.12,0.04);
     TString ps5 = PATH_plots;
     ps5 += "ResponseFitter/";
     if(m_isMC) ps5 += "MC/";
     else{ps5 += "Data/";}
     ps5 += treeVersion;
     ps5 += "/";
     ps5 += JetCollection;
     ps5 += "/Response_vs_pT";
     if(m_systName!=""){
       ps5 += "_";
       ps5 += m_systName;
     }
     if(m_insitu) ps5 += "_Insitu";
     if(m_insitu && !m_insituSmoothed) ps5 += "notSmoothed";
     if(m_applyEtaRebinning) ps5 += "_etaRebinned";
     ps5 += ".pdf";

     can6->Print(ps5+"[");
     for (int i = minEtaBintoUse; i <= maxEtaBintoUse; i++ ) {
       can6->SetLogx(); 
       
       h_Response_vs_pT[i-minEtaBintoUse]->SetXTitle("p_{T}^{Ref} [GeV]");
       h_Response_vs_pT[i-minEtaBintoUse]->SetYTitle("<p_{T}^{Rscan}/p_{T}^{Ref}>");
       h_Response_vs_pT[i-minEtaBintoUse]->GetXaxis()->SetMoreLogLabels();
       h_Response_vs_pT[i-minEtaBintoUse]->SetStats(0);
       h_Response_vs_pT[i-minEtaBintoUse]->SetAxisRange(0,2,"Y");
       h_Response_vs_pT[i-minEtaBintoUse]->SetMarkerColor(kBlack);
       h_Response_vs_pT[i-minEtaBintoUse]->Draw();
 
       text_0 = "#scale[0.8]{"+AbsEtaBins[i]+" #leq #eta < "+AbsEtaBins[i+1]+"}";
       if (m_useAbsEtaBins) text_0 = "#scale[0.7]{"+AbsEtaBins[i+1]+" #leq |#eta| < "+AbsEtaBins[i]+"}";
       if(JetCollection=="2LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.2, LC+JES}";
       else if(JetCollection=="3LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.3, LC+JES}";
       else if(JetCollection=="5LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.5, LC+JES}";
       else if(JetCollection=="6LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.6, LC+JES}";
       else if(JetCollection=="7LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.7, LC+JES}";
       else if(JetCollection=="8LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.8, LC+JES}";
       TextBlock0 = new TLatex(x_0,position_0, text_0.Data());
       TextBlock1 = new TLatex(x_0,position_1, text_1.Data());
       TextBlock0->SetNDC();
       TextBlock1->SetNDC();
       TextBlock0->Draw("same");
       TextBlock1->Draw("same");
       double nbins = h_Response_vs_pT[i-minEtaBintoUse]->GetNbinsX();
       double minimum = h_Response_vs_pT[i-minEtaBintoUse]->GetXaxis()->GetBinLowEdge(1);
       double maximum = h_Response_vs_pT[i-minEtaBintoUse]->GetXaxis()->GetBinUpEdge(nbins);
       TLine* line = new TLine(minimum, 0, maximum, 0);
       line->SetLineStyle(2);
       line->SetLineColor(kBlue);
       line->Draw("same");
       can6->Print(ps5);
       can6->SetLogx(0); 
       delete line;
     }
     can6->Print(ps5+"]");
     delete can6;

     //---------------------
     // Resolution 
     //---------------------
     TCanvas *can7 = new TCanvas();
     can7->SetMargin(0.12,0.04,0.12,0.04);
     TString ps7 = PATH_plots;
     ps7 += "ResponseFitter/";
     if(m_isMC) ps7 += "MC/";
     else{ps7 += "Data/";}
     ps7 += treeVersion;
     ps7 += "/";
     ps7 += JetCollection;
     ps7 += "/Resolution_vs_pT";
     if(m_systName!=""){
       ps7 += "_";
       ps7 += m_systName;
     }
     if(m_insitu) ps7 += "_Insitu";
     if(m_insitu && !m_insituSmoothed) ps7 += "notSmoothed";
     if(m_applyEtaRebinning) ps7 += "_etaRebinned";
     ps7 += ".pdf";

     can7->Print(ps7+"[");
     for (int i = minEtaBintoUse; i <= maxEtaBintoUse; i++ ) {
       can7->SetLogx(); 
       
       h_Resolution_vs_pT[i-minEtaBintoUse]->SetXTitle("p_{T}^{Ref} [GeV]");
       h_Resolution_vs_pT[i-minEtaBintoUse]->SetYTitle("Relative Resolution");
       h_Resolution_vs_pT[i-minEtaBintoUse]->GetXaxis()->SetMoreLogLabels();
       h_Resolution_vs_pT[i-minEtaBintoUse]->SetStats(0);
       h_Resolution_vs_pT[i-minEtaBintoUse]->SetAxisRange(0,0.5,"Y");
       h_Resolution_vs_pT[i-minEtaBintoUse]->SetMarkerColor(kBlack);
       h_Resolution_vs_pT[i-minEtaBintoUse]->Draw();
 
       text_0 = "#scale[0.8]{"+AbsEtaBins[i]+" #leq #eta < "+AbsEtaBins[i+1]+"}";
       if (m_useAbsEtaBins) text_0 = "#scale[0.7]{"+AbsEtaBins[i+1]+" #leq |#eta| < "+AbsEtaBins[i]+"}";
       if(JetCollection=="2LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.2, LC+JES}";
       else if(JetCollection=="3LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.3, LC+JES}";
       else if(JetCollection=="5LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.5, LC+JES}";
       else if(JetCollection=="6LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.6, LC+JES}";
       else if(JetCollection=="7LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.7, LC+JES}";
       else if(JetCollection=="8LC") text_1 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.8, LC+JES}";
       TextBlock0 = new TLatex(x_0,position_0, text_0.Data());
       TextBlock1 = new TLatex(x_0,position_1, text_1.Data());
       TextBlock0->SetNDC();
       TextBlock1->SetNDC();
       TextBlock0->Draw("same");
       TextBlock1->Draw("same");
       double nbins = h_Resolution_vs_pT[i-minEtaBintoUse]->GetNbinsX();
       double minimum = h_Resolution_vs_pT[i-minEtaBintoUse]->GetXaxis()->GetBinLowEdge(1);
       double maximum = h_Resolution_vs_pT[i-minEtaBintoUse]->GetXaxis()->GetBinUpEdge(nbins);
       TLine* line = new TLine(minimum, 0, maximum, 0);
       line->SetLineStyle(2);
       line->SetLineColor(kBlue);
       line->Draw("same");
       can7->Print(ps7);
       can7->SetLogx(0); 
       delete line;
     }
     can7->Print(ps7+"]");
     delete can7;
     delete TextBlock0;
     delete TextBlock1;
     delete TextBlock2;
     delete TextBlock3;
     delete TextBlock4;
   }//END: m_deriveCalib

   if(m_deriveUncert){

     // Get TH2Bootstrap
     if(m_debug) std::cout  << "Opening EtaBin: " << m_etaSlice << std::endl;
     TString name_tmp = "Response_vs_pTRef_Eta_";
     name_tmp += m_etaSlice;
     name_tmp +="_replica";
     if (m_useAbsEtaBins) name_tmp += "_absBins";
     histo_bootstrap = (TH2Bootstrap*)file->Get(name_tmp.Data());

     double x_0 = 0.16;
     double position_0 = 0.86;
     double position_1 = 0.86-0.05;
     double position_2 = 0.86-0.1;
     double position_3 = 0.86-0.15;
     double position_4 = 0.86-0.20;
     TString text_0;
     TString text_1;
     TString text_2;
     TString text_3;
     TString text_4;
     TLatex *TextBlock0;
     TLatex *TextBlock1;
     TLatex *TextBlock2;
     TLatex *TextBlock3;
     TLatex *TextBlock4;

     TCanvas *canM = new TCanvas();
     canM->SetMargin(0.12,0.04,0.12,0.04);
     TString psM = PATH_plots;

     psM += "ResponseFitter/";
     if(m_isMC) psM += "MC/";
     else{psM += "Data/";}
     psM += treeVersion;
     psM += "/";
     psM += JetCollection;
     psM += "/Response_Fits";
     if(m_systName!=""){
       psM += "_";
       psM += m_systName;
     }
     if(m_insitu) psM += "_Insitu";
     if(m_insitu && !m_insituSmoothed) psM += "notSmoothed";
     if(m_applyEtaRebinning) psM += "_etaRebinned";
     psM += "_etaSlice_";
     psM += m_etaSlice;
     psM += ".pdf";
     if(m_debug) canM->Print(psM+"[");
     canM->SetLogy(0);

     // Loop over Bootstrap Replicas
     for (int ii = 0; ii<nReplicas; ii++){
       TString name_tmp2 = "Bootstrap_Replica_"; name_tmp2 += ii;
       TH2D* tmp = (TH2D*)histo_bootstrap->GetReplica(ii);
       histo = (TH2D*)tmp->Clone(name_tmp2);
       if(m_debug) std::cout << "Replica number: " << ii << std::endl;

       // Loop over pT bins
       for(int j=0;j<npTBins;++j){//use the same bins

         h_name += "projx_";
         h_name += pTStrBins[j];
         h_name += "_";
         h_name += pTStrBins[j+1];

         int low  = histo->GetXaxis()->FindBin(pTBins[j]);
         int high = histo->GetXaxis()->FindBin(pTBins[j+1])-1;
         TH1D* histo_px = histo->ProjectionY(h_name,low,high,"e");

         histo_px->SetAxisRange(0,2,"X");
         histo_px->SetXTitle("p_{T}^{Rscan}/p_{T}^{Ref}");
         histo_px->GetXaxis()->SetTitleOffset(1.3);
         histo_px->SetMarkerColor(kBlack);
         histo_px->Draw();

	 // Fit Range
         min = histo_px->GetMean() - histo_px->GetRMS()*scaleRMS;
         max = histo_px->GetMean() + histo_px->GetRMS()*scaleRMS;

         histo_px->SetAxisRange(response_minaxis,response_maxaxis,"X");

         // Fit
         double Mean, MeanError, Neff;
         myFitter->Fit(histo_px,min,max);
         Mean       = myFitter->GetMean();
         MeanError  = myFitter->GetMeanError();
         Neff       = myFitter->Neff();

         if(!(Mean==0 && MeanError==0)) myFitter->DrawFitAndHisto(0.,2.);

	 text_0 = "#scale[0.7]{"+AbsEtaBins[std::stoi(m_etaSlice)]+" #leq #eta < "+AbsEtaBins[std::stoi(m_etaSlice)+1]+"}";
         if (m_useAbsEtaBins) text_0 = "#scale[0.7]{"+AbsEtaBins[std::stoi(m_etaSlice)+1]+" #leq |#eta| < "+AbsEtaBins[std::stoi(m_etaSlice)]+"}";
	 text_1 = "#scale[0.7]{"+pTStrBins[j]+" GeV #leq p_{T}^{Ref} < "+pTStrBins[j+1]+" GeV}";
         if(JetCollection=="2LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.2, LC+JES}";
         else if(JetCollection=="3LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.3, LC+JES}";
         else if(JetCollection=="5LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.5, LC+JES}";
         else if(JetCollection=="6LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.6, LC+JES}";
         else if(JetCollection=="7LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.7, LC+JES}";
         else if(JetCollection=="8LC") text_2 = "#scale[0.7]{anti-#it{k}_{t} jets, R = 0.8, LC+JES}";
         TString entries = std::to_string(histo_px->GetEntries());
         TString effEntries = std::to_string(Neff);
         text_3 = "#scale[0.7]{Entries: "+entries+"}";
         text_4 = "#scale[0.7]{EffEntries: "+effEntries+"}";
         TextBlock0 = new TLatex(x_0,position_0, text_0.Data());
         TextBlock1 = new TLatex(x_0,position_1, text_1.Data());
         TextBlock2 = new TLatex(x_0,position_2, text_2.Data());
         TextBlock3 = new TLatex(x_0,position_3, text_3.Data());
         TextBlock4 = new TLatex(x_0,position_4, text_4.Data());
         TextBlock0->SetNDC();
         TextBlock1->SetNDC();
         TextBlock2->SetNDC();
         TextBlock3->SetNDC();
         TextBlock4->SetNDC();
         TextBlock0->Draw("same");
         TextBlock1->Draw("same");
         TextBlock2->Draw("same");
         TextBlock3->Draw("same");
         TextBlock4->Draw("same");
         canM->SetLogx(0); 

         histo_px->GetXaxis()->SetMoreLogLabels();
         histo_px->SetStats(0);
         if(m_debug) canM->Print(psM);

         // Store Response in histograms
         double Response = Mean;
         double ResponseError = MeanError;
         if (Response!=0 && ResponseError!=0){
           h_Response_vs_pT_replicas[ii]->SetBinContent(j+1,Response);
           h_Response_vs_pT_replicas[ii]->SetBinError(j+1,ResponseError);
         }
         delete histo_px;
       }//END: Loop over pT bins
       delete tmp;
       delete histo;
     }//END: Loop over replicas
     if(m_debug) canM->Print(psM+"]");
     delete canM;
   }//END: m_deriveUncert
   
   delete myFitter;

   //------------------------------
   // Save histograms to ROOT file
   //------------------------------
   
   TString outputFile = PATH_rootfiles;
   outputFile+= "ROOTfiles/";
   outputFile+="DirectMatching/";
   outputFile += "DeriveInsituRscan_Outputs/RelativeResponse/";
   if(m_isMC) outputFile += "/MC/";
   else{outputFile += "/Data/";}
   outputFile += treeVersion;
   outputFile += "/";
   outputFile += JetCollection;
   if(m_isMC){
     if(m_mc15c_mc15bmuprofile) outputFile += "/MC15c_MC15bmuProfile_";
     if(m_mc15c) outputFile += "/MC15c_";
     if(m_mc16a) outputFile += "/MC16a_";
     if(m_mc16c) outputFile += "/MC16c_";
     if(m_mc16d) outputFile += "/MC16d_";
     if(!m_zjet) {outputFile += m_generator;}
     else {outputFile += "ZJetStudy";}
     outputFile += "_Outputs_vs_pT";
   }
   else{
     outputFile += "/Outputs_vs_pT";
     if(m_zjet){
       outputFile += "_";
       outputFile += "ZJetStudy";
     }
   }
   if(m_systName!=""){
       outputFile += "_";
       outputFile += m_systName;
   }
   if(m_numericalInversion) outputFile += "_numericalInversion";
   if(m_insitu) outputFile += "_Insitu";
   if(m_insitu && !m_insituSmoothed) outputFile += "notSmoothed";
   if(pTavgType!=""){
     outputFile += "_";
     if(pTavgType=="Ref") outputFile += "pTRefAvg";
     else if(pTavgType=="Rscan") outputFile += "pTRscanAvg";
   }
   if(m_deriveUncert){
     outputFile += "_etaSlice_";
     outputFile += m_etaSlice;
   }
   if(m_applyEtaRebinning) outputFile += "_EtaRebinned";
   outputFile += ".root";
   std::cout << "Histograms saved in: " << outputFile.Data() << std::endl;
   TFile* tout = new TFile(outputFile.Data(),"RECREATE");
   tout->cd(); 
   if(m_deriveCalib){
     for (int i = minEtaBintoUse; i <= maxEtaBintoUse; i++ ) {
       h_Response_vs_pT[i-minEtaBintoUse]->Write();
       h_Resolution_vs_pT[i-minEtaBintoUse]->Write();
     }
   }
   if(m_deriveUncert){
     for (int j = 0; j < nReplicas; j++) {
       h_Response_vs_pT_replicas[j]->Write();
     }
   }
   tout->Close();
   file->Close();
   delete file;
   delete tout;

   return 0;
   std::cout << ">>>>>>>> Done <<<<<<<<"<<std::endl;

}

void GetFitRange(TH1D* proj, int type, double delta, double& min, double& max){

  // Default (type==0)
  min = proj->GetMean() - proj->GetRMS()*scaleRMS;
  max = proj->GetMean() + proj->GetRMS()*scaleRMS;

  if(type==1){        // Two-side extended/shorter by delta
    min = proj->GetMean() - proj->GetRMS()*delta;
    max = proj->GetMean() + proj->GetRMS()*delta;
  } else if(type==2){ // Left-side extended/shorter by delta
    min = proj->GetMean() - proj->GetRMS()*delta;
  } else if(type==3){ // Right-side extended by delta
    max = proj->GetMean() + proj->GetRMS()*delta;
  } else if(type==4){ // Move range to the left by delta
    min = proj->GetMean() - proj->GetRMS()*delta;
    max = proj->GetMean() - proj->GetRMS()*delta;
  } else if(type==5){ // Move range to the right by delta
    min = proj->GetMean() + proj->GetRMS()*delta;
    max = proj->GetMean() + proj->GetRMS()*delta;
  } else { std::cout << "Error: wrong type in GetFitRange()" << std::endl;}
}


