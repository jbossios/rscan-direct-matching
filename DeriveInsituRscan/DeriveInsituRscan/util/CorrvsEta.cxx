/* **********************************************************************\
 *                                                                      *
 *  #   Name           : CorrvsEta                                      *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 01/04/16 First version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include <TSystem.h>
#include <sstream>
#include <iostream>

#include "TCanvas.h"
#include "TFile.h"
#include "TH2F.h"
#include "TLine.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraph2DErrors.h"

#include "DeriveInsituRscan/global.h"

TString PATH("");
TString JetCollection = "";
TString MCVersion = "";
TString DataVersion = "";

int main( int argc, char* argv[] ){

  std::string m_MCVersion = "";
  std::string m_DataVersion = "";
  std::string m_jetCollection = "";
  std::string m_systName = ""; // Default Nominal
  std::string m_path = "";
  bool m_numericalInversion = false;
  bool m_zjet = false;
  bool m_applyMap = true;
  bool m_noSmoothing = false;

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--mcVersion=")   != std::string::npos) m_MCVersion = v[1];
    if ( opt.find("--dataVersion=")   != std::string::npos) m_DataVersion = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

    if ( opt.find("--nInversion=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
      if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
    }
    if ( opt.find("--ZJetStudy=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_zjet= true;
    }
    if ( opt.find("--ApplyMap=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_applyMap= true;
    } else {m_applyMap= false;}

    if ( opt.find("--noSmoothing=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_noSmoothing= true;
    }

    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
      if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
      if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
      if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
      if (v[1].find("MCSherpa") != std::string::npos) m_systName = "MCSherpa";
    }

  }//End: Loop over input options

  JetCollection = m_jetCollection;
  MCVersion     = m_MCVersion;
  DataVersion   = m_DataVersion;
  PATH          = m_path;

  if(m_path==""){
    std::cout << "ERROR: Path should be provided, exiting" << std::endl;
    return 0;
  }
  if(JetCollection==""){
    std::cout << "ERROR: Jet collection should be provided, exiting" << std::endl;
    return 0;
  }
  if(MCVersion==""){
    std::cout << "ERROR: MC Tree version should be provided, exiting" << std::endl;
    return 0;
  }
  if(DataVersion==""){
    std::cout << "ERROR: Data Tree version should be provided, exiting" << std::endl;
    return 0;
  }

  TString inputFile = PATH;
  inputFile += "ROOTfiles/DirectMatching/SmoothingTool_Outputs/";
  inputFile += DataVersion;
  inputFile += "_vs_";
  inputFile += MCVersion;
  inputFile += "/";
  inputFile += JetCollection;
  inputFile += "/Correction_";
  if(m_noSmoothing) inputFile += "not";
  inputFile += "Smoothed";
  if(m_systName!=""){
    inputFile += "_";
    inputFile += m_systName;
  }
  if(m_numericalInversion) inputFile += "_numericalInversion";
  if(m_zjet) inputFile += "_ZJet";
  inputFile += ".root";

  TString inputCorr = PATH;
  inputCorr += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/";
  inputCorr += DataVersion; 
  inputCorr += "_vs_";
  inputCorr += MCVersion;
  inputCorr += "/";
  inputCorr += JetCollection;
  inputCorr += "/Corr_vs_";
  if(m_applyMap) inputCorr += "pTrscan";
  else{inputCorr += "pTref";}
  if(m_zjet) inputCorr += "_ZJet";
  if(m_systName!=""){
    inputCorr += "_";
    inputCorr += m_systName;
  }
  if(m_numericalInversion) inputCorr += "_numericalInversion";
  inputCorr += ".root";

  //-----------------
  // Open ROOT file
  //-----------------

  std::cout << "Opening: " << inputFile << std::endl;
  TFile* tin = new TFile(inputFile,"read");
  std::cout << "Opening: " << inputCorr << std::endl;
  TFile* tinCorr = nullptr;
  if(!m_applyMap ) tinCorr = new TFile(inputCorr,"read");

  // TH2D Correction Histogram
  TH2D* hcorr2D = nullptr;
  TString namecorr = "hcorr2D";
  if(!m_applyMap) {
    hcorr2D = (TH2D*)tinCorr->Get(namecorr.Data()); 
    hcorr2D->SetDirectory(0);// "detach" the histogram from the file
  }
  // TH2D Smoothed correction Histogram
  TH2D* hsmoothedcorr2D;
  TString namesmoothedcorr = "AntiKt";
  namesmoothedcorr += JetCollection; namesmoothedcorr += "Topo_EtaInterCalibration";
  std::cout << "Reading histogram: " << namesmoothedcorr << std::endl;
  hsmoothedcorr2D = (TH2D*)tin->Get(namesmoothedcorr.Data()); 
  hsmoothedcorr2D->SetDirectory(0);// "detach" the histogram from the file
  if(m_applyMap){
    hcorr2D  = (TH2D*)tin ->Get("Correction_TH2DFormat");
    hcorr2D ->SetDirectory(0);
  }

  // TH1D Correction and Smoothed correction vs Eta for each pT Bin
  TH1D* hcorr[npTBins];
  TH1D* hsmoothedcorr[npTBins];

  double npTbins = hcorr2D->GetNbinsX();

  TString hnamecorr;
  TString hnamesmoothedcorr;
  // Loop over pT Bins
  for(int ipT=1;ipT<=npTbins;++ipT){
    // Initialize histograms (one per pT bin)
    hnamecorr = "Corr_vs_Eta_pT_";
    hnamecorr += ipT;
    hnamesmoothedcorr = "SmoothedCorr_vs_Eta_pT_";
    hnamesmoothedcorr += ipT;
    hcorr[ipT-1] = new TH1D(hnamecorr.Data(),"",nhEtaBins,hEtaBins);
    hcorr[ipT-1]->SetDirectory(0);
    hsmoothedcorr[ipT-1] = new TH1D(hnamesmoothedcorr.Data(),"",nhEtaBins,hEtaBins);
    hsmoothedcorr[ipT-1]->SetDirectory(0);
    // Loop over Eta Bins
    for(int i=15;i<=74;++i){
      double Rcorr = hcorr2D->GetBinContent(ipT,i-14);
      double dRcorr = hcorr2D->GetBinError(ipT,i-14);
      hcorr[ipT-1]->SetBinContent(i-14,Rcorr);
      hcorr[ipT-1]->SetBinError(i-14,dRcorr);
      if(!m_noSmoothing){
        double Rsmoothedcorr = hsmoothedcorr2D->GetBinContent(ipT,i-14);
        double dRsmoothedcorr = hsmoothedcorr2D->GetBinError(ipT,i-14);
        hsmoothedcorr[ipT-1]->SetBinContent(i-14,Rsmoothedcorr);
        hsmoothedcorr[ipT-1]->SetBinError(i-14,dRsmoothedcorr);
      }
    }
  } 
  
  tin->Close();

  //------------------------------
  // Save histograms to ROOT file
  //------------------------------
  TString outputFile = PATH;
  if(!m_noSmoothing) outputFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorrvsEta/";
  else{ outputFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/";}
  outputFile += DataVersion;
  outputFile += "_vs_";
  outputFile += MCVersion;
  outputFile += "/";
  outputFile += JetCollection;
  if(!m_noSmoothing) outputFile += "/SmoothedCorr_vs_Eta";
  else{outputFile += "/Corr_vs_Eta";}
  if(!m_applyMap) outputFile += "_ptRefBins";
  if(m_systName!=""){
    outputFile += "_";
    outputFile += m_systName;
  }
  if(m_numericalInversion) outputFile += "_numericalInversion";
  if(m_zjet) outputFile += "_ZJet";
  outputFile += ".root";
  std::cout << "Histograms saved in: " << outputFile.Data() << std::endl;
  TFile* tout = new TFile(outputFile.Data(),"RECREATE");
  tout->cd();
  for(int ipT=1;ipT<=npTbins;++ipT){
    hcorr[ipT-1]->Write();
    if(!m_noSmoothing) hsmoothedcorr[ipT-1]->Write();
  }
  tout->Close();

  std::cout << ">>>> Done <<<<" << std::endl;

  return 0;

}
