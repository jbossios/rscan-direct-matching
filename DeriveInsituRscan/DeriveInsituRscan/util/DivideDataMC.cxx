/* **********************************************************************\
 *                                                                      *
 *  #   Name           : DivideDataMC                                   *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 28/02/16 First version             J. Bossio (jbossios@cern.ch)   *
 *  2 20/07/18 Second version            M. Toscani (mtoscani@cern.ch)  *
\************************************************************************/

#include <TSystem.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <map>
#include <iterator>
#include <algorithm>
#include "TROOT.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH2F.h"
#include "TLine.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraph2DErrors.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "DeriveInsituRscan/global.h"

TString JetCollection;
TString MCVersion;
TString DataVersion;
TString PATH_rootfiles;
TString inputMC = "";
TString inputData = "";

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[]){

  std::string m_MCVersion = "";
  std::string m_DataVersion = "";
  std::string m_jetCollection = "";
  std::string m_systName = ""; // Default Nominal
  std::string m_generator = "Pythia"; // Default Pythia
  std::string m_path  = "";
  double m_pTMin = -1.;
  bool m_numericalInversion = false;
  bool m_mc15c_mc15bmuprofile = false;
  bool m_mc15c = false;
  bool m_mc16a = false;
  bool m_mc16c = false;
  bool m_mc16d = false;
  bool m_insitu = false;
  bool m_zjet = false;
  bool m_applyMap = false;
  bool m_applyEtaRebinning = false;
  bool m_useRebinnedMap4Calib = true;

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){
    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--mcVersion=")   != std::string::npos) m_MCVersion = v[1];
    if ( opt.find("--dataVersion=")   != std::string::npos) m_DataVersion = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];
    
    if ( opt.find("--pTMin=")   != std::string::npos) m_pTMin = std::stod(v[1]);

    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

    if ( opt.find("--insitu=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_insitu= true;
      if (v[1].find("FALSE") != std::string::npos) m_insitu= false;
    }

    if ( opt.find("--ApplyMap=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_applyMap= true;
      if (v[1].find("FALSE") != std::string::npos) m_applyMap= false;
    }
   
    if ( opt.find("--ApplyEtaRebinning=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_applyEtaRebinning= true;
      if (v[1].find("FALSE") != std::string::npos) m_applyEtaRebinning= false;
    }
    if ( opt.find("--nInversion=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
      if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
    }
    if ( opt.find("--RebinnedMap4Calib=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_useRebinnedMap4Calib= true;
      if (v[1].find("FALSE") != std::string::npos) m_useRebinnedMap4Calib= false;
    }
    if ( opt.find("--ZJetStudy=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_zjet= true;
    }
    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
      if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
      if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
      if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
      if (v[1].find("MCSherpa") != std::string::npos) m_systName = "MCSherpa";
      if (v[1].find("Nominal4Sherpa") != std::string::npos) m_systName = "Nominal4Sherpa";
    }

  }//End: Loop over input options

  JetCollection = m_jetCollection;
  MCVersion     = m_MCVersion;
  DataVersion   = m_DataVersion;

  if(m_path==""){
    std::cout << "ERROR: Path should be provided, exiting" << std::endl;
    return 0;
  }
  if(JetCollection==""){
    std::cout << "ERROR: Jet collection should be provided, exiting" << std::endl;
    return 0;
  }
  if(MCVersion==""){
    std::cout << "ERROR: MC Tree version should be provided, exiting" << std::endl;
    return 0;
  }
  if(DataVersion==""){
    std::cout << "ERROR: Data Tree version should be provided, exiting" << std::endl;
    return 0;
  }

  if(MCVersion.Contains("Pythia")) m_generator = "Pythia";

  if(MCVersion.Contains("MC15bMuProfile")) m_mc15c_mc15bmuprofile = true;
  else if(MCVersion.Contains("MC15")){m_mc15c = true;}
  else if(MCVersion.Contains("MC16a")){m_mc16a = true;}
  else if(MCVersion.Contains("MC16c")){m_mc16c = true;}
  else if(MCVersion.Contains("MC16d")){m_mc16d = true;}

  PATH_rootfiles = m_path;

  if(m_insitu) std::cout << "Data input with Insitu correction applied" << std::endl;

  if (m_systName == ""){std::cout << "SystName: Nominal" << std::endl;}
  else{ std::cout << "SystName: " << m_systName << std::endl;}

  inputMC = "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/";
  inputMC += "MC/";
  inputMC += MCVersion;
  inputMC += "/";
  inputMC += JetCollection;
  if(m_mc15c_mc15bmuprofile) inputMC += "/MC15c_MC15bmuProfile_";
  if(m_mc15c) inputMC += "/MC15c_";
  if(m_mc16a) inputMC += "/MC16a_";
  if(m_mc16c) inputMC += "/MC16c_";
  if(m_mc16d) inputMC += "/MC16d_";
  if(!m_zjet) inputMC += m_generator;
  else{ inputMC +="ZJetStudy";}
  inputMC += "_Outputs_vs_pT";
  if(m_systName!=""){ inputMC+= "_"+m_systName;}
  if(m_applyEtaRebinning) inputMC+="_EtaRebinned";
  if(m_numericalInversion) {inputMC += "_numericalInversion";}
  inputMC += ".root";
  inputData += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/";
  inputData += "Data/";  
  inputData += DataVersion;
  inputData += "/";  
  inputData += JetCollection;
  inputData += "/Outputs_vs_pT";
  if(m_zjet){ inputData += "_"; inputData += "ZJetStudy";}
  if(m_systName!="" && m_systName!="MCSherpa"){ inputData+="_"+ m_systName;}
  if(m_systName=="MCSherpa") inputData += "_Nominal4Sherpa";
  if(m_applyEtaRebinning) inputData+="_EtaRebinned";
  if(m_numericalInversion){ inputData += "_numericalInversion";}
  if(m_insitu){ inputData += "_Insitu";}
  inputData += ".root";


  //-----------------
  // Open ROOT files
  //-----------------

  /*--------------  Data --------------*/
  TString inputfileData = PATH_rootfiles;
  inputfileData += inputData;
  std::cout << "Opening: " << inputfileData << std::endl;
  TFile* tinData = new TFile(inputfileData,"read");

  // Get Histograms
  int mineta =15, maxeta=74;
  if(m_applyEtaRebinning){
    mineta=0; maxeta=15;
    if (m_systName =="MCSherpa" || m_systName == "Nominal4Sherpa") {mineta=0; maxeta=7;}
  }
 
  TH1D* hData[nEtaBins];
  TString name;
   for(int i=mineta;i<=maxeta;++i){
    name = "Response_vs_pTRef_Eta_";
    name += i;
    int yeta = i-15;
    if(m_applyEtaRebinning) yeta=i;
    hData[yeta] = (TH1D*)tinData->Get(name.Data()); // Data
    hData[yeta] -> SetDirectory(0);// "detach" the histogram from the file
  }
  
  tinData->Close();
 
  /*-------------- MC----------------*/
  TString inputfileMC = PATH_rootfiles;
  inputfileMC += inputMC;
  std::cout << "Opening: " << inputfileMC << std::endl;
  TFile* tinMC = new TFile(inputfileMC,"read");


  /*----------------- MAP --------------------*/
  /* Map Correction vs ptRef back to ptRscan  */
  
  TFile * mapFile = new TFile(); 
  if(m_applyMap){
    TString mapFileName = PATH_rootfiles + "ROOTfiles/DirectMatching/Mapping/" + DataVersion + "/" + JetCollection ;
    mapFileName += "/MappingtoPtRscan";
    if(m_zjet) mapFileName += "_ZJetStudy";
    if(m_applyEtaRebinning || m_useRebinnedMap4Calib) mapFileName += "_etaRebinned_absBins";
    mapFileName += ".root";
    // Retrieve MapFile:
    mapFile = TFile::Open( mapFileName, "Read" );  
    std::cout << "Openning Map File: " << mapFileName << std::endl;
    std::cout<< "Going to apply pTMap"<<std::endl;
  }

  /*----------- BOOKING --------------*/
  TGraph2DErrors* gCorr = new TGraph2DErrors();
  TH2D* hcorr2D = new TH2D("hcorr2D","",npTBins,pTBins,nhEtaBins,hEtaBins);
  hcorr2D->SetDirectory(0);// "detach" the histogram from the file

  TH1D* EtaBinning = new TH1D("EtaBinning","Eta Binning of Corr_vs_pTRscan_",nhEtaBins,hEtaBins);
  EtaBinning->SetDirectory(0);
  TH1D* hMC[nEtaBins];
  TH1D* hCorr[nEtaBins];
  TString name_clone_Corr;
  
  TGraphErrors* mapping = new TGraphErrors(); // Retrieving Map in TGraph1D format (one for each eta):
  TGraph2DErrors* mCorr = new TGraph2DErrors();    // output Mapped Corr
  TGraphAsymmErrors * Corr4Combination[nEtaBins];  // output for Combination
 
  if(m_applyMap){
    TString name_tmp = "Correction_vs_pTRscan";
    mCorr -> SetNameTitle(name_tmp,name_tmp);
    mCorr -> GetXaxis() -> SetTitle("#it{p}_{T}^{Rscan}");
    mCorr -> GetYaxis() -> SetTitle("#eta");
    mCorr -> GetZaxis() -> SetTitle("Correction");   

  }   
  TString name_tmp = ""; 
 
  // Mapping old eta indexes to new eta indexes:
  std::map<int, int> MapMap;
  for (int neweta = 0; neweta<16; neweta++){
    for (int oldeta = std::distance(hEtaBins,std::find(hEtaBins,hEtaBins + nhEtaBins + 1, hRebEtaBins[neweta])); oldeta < std::distance(hEtaBins,std::find(hEtaBins,hEtaBins + nhEtaBins + 1, hRebEtaBins[neweta+1])); oldeta++){
    MapMap[oldeta]=neweta;
    //std::cout<<"Old Eta: "<< oldeta<< " corresponds to range: ["<< hEtaBins[oldeta] << ":"<<hEtaBins[oldeta+1]<<"] is mapped to new eta: "<< MapMap[oldeta]<< " and is contained in new eta bin: ["<< hRebEtaBins[neweta]  << ";"<<hRebEtaBins[neweta+1] <<"]"<<std::endl;
    }
  } 

  int n = 0; // TGraph2D point
  for(int i=mineta;i<=maxeta;++i){
    int yeta = i-15;
    if(m_applyEtaRebinning) yeta=i;

    //---------
    // MC/Data
    //---------

    name = "Response_vs_pTRef_Eta_";
    name += i;
    name_clone_Corr = "Corr_vs_pT_Eta_";
    name_clone_Corr += i;
    name_tmp = "Mapping_in_PtRefBins_eta_";
    if(m_useRebinnedMap4Calib && m_systName=="") name_tmp += MapMap[yeta];
    else{name_tmp += yeta;}
    //std::cout<< "Getting: "<< name << "; and Map: "<<name_tmp <<std::endl; 

    // retrieve MC histogram:
    hMC[yeta] = (TH1D*)tinMC->Get(name.Data()); // MC
    hMC[yeta]->SetDirectory(0);// "detach" the histogram from the file
    // calculate division: MC/Data:
    hCorr[yeta] = (TH1D*)hMC[yeta]->Clone(name_clone_Corr); // MC/Data
    hCorr[yeta]->Divide(hData[yeta]);
    hCorr[yeta]->SetDirectory(0);// "detach" the histogram from the file
    // retrieve Map:
    if(m_applyMap) mapping = (TGraphErrors*)mapFile -> Get(name_tmp);
    // Book output AsymmErrors :
    name_tmp = "Corr_vs_pTRscan_Eta_";
    name_tmp += i ;
    Corr4Combination[yeta] = new TGraphAsymmErrors();
    Corr4Combination[yeta] -> SetName(name_tmp);
    Corr4Combination[yeta] -> GetXaxis() -> SetTitle("#it{p}_{T}^{Rscan}");
    Corr4Combination[yeta] -> GetYaxis() -> SetTitle("Correction");   
    int asy_counter = 0;

    //-----------------------
    // Make a TGraph2DErrors
    //-----------------------
    
    // TODO
    // Do we need this TGraph2D still? If so, make it compatible with new eta binning.

    // Loop over pT Bins
    double npTbins = hCorr[yeta]->GetNbinsX();
    for(int ipT=1;ipT<=npTbins;++ipT){
      double R          = hCorr[yeta]->GetBinContent(ipT);
      double dR         = hCorr[yeta]->GetBinError(ipT);
      double pT         = hCorr[yeta]->GetBinCenter(ipT);
      double pT_lowedge = hCorr[yeta]->GetXaxis()->GetBinLowEdge(ipT);
      double pT_upedge  = hCorr[yeta]->GetXaxis()->GetBinUpEdge(ipT);
      double pT_err     = pT_upedge - pT_lowedge;
      double eta        = Etabins[i]+0.05;
      double eta_err    = 0.1;
      if(m_applyEtaRebinning){
        eta = (hRebEtaBins[i]+hRebEtaBins[i+1])*0.5;
	eta_err = (hRebEtaBins[i+1]-hRebEtaBins[i]);
      }
      
      if(R == 0.) continue; // skip calibration == 0.

      hcorr2D->SetBinContent(ipT,i-14,R);
      hcorr2D->SetBinError(ipT,i-14,dR);

      // Fill Graph
      gCorr->SetPoint(n,pT,eta,R);
      gCorr->SetPointError(n,pT_err,eta_err,dR);

      if(pT<m_pTMin) continue;
      // Fill Mapped Graph:
      double ptrscan = mapping -> Eval(pT);
      double ptrscan_err = 0.001; // never mind for closure studies
      mCorr->SetPoint(n, ptrscan, eta, R);
      mCorr->SetPointError(n,ptrscan_err,eta_err,dR);
      n++;

      // Fill the AsymmErrors TGraph
      double exl, exh, eyl, eyh;
      exl = fabs(ptrscan - mapping->Eval(pT_lowedge));
      exh = fabs(ptrscan - mapping->Eval(pT_upedge));
      eyl = dR;
      eyh = dR;
      Corr4Combination[yeta] -> SetPoint(asy_counter, ptrscan, R);
      Corr4Combination[yeta] -> SetPointError(asy_counter, exl, exh, eyl, eyh ); // i,ex_low,ex_high,ey_low,ey_high
      asy_counter++;
    } // loop over pT Bins.
  } // loop over eta Bins.
  
  tinMC->Close();
  mapFile->Close();

  //------------------------------
  // Save histograms to ROOT file
  //------------------------------
  TString outputFile = PATH_rootfiles;  // Corr_vs_ptref.root
  TString outputFile2 = ""; // Corr_vs_ptrscan.root (Mapped)
  outputFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/";
  outputFile += DataVersion;
  outputFile += "_vs_";
  outputFile += MCVersion;
  outputFile += "/";
  outputFile += JetCollection;
  outputFile += "/Corr_vs_pT";
  outputFile2 = outputFile;
  if(m_numericalInversion) outputFile += "rscan_numericalInversion_";
  else{outputFile += "ref";}
  outputFile2 += "rscan";
  if(m_zjet){
    outputFile += "_ZJet";
    outputFile2 += "_ZJet";
  }
  if(m_systName!=""){
   outputFile += "_";
   outputFile += m_systName;
   outputFile2 += "_" + m_systName;
  }
  if(m_applyEtaRebinning) {
    outputFile += "_etaRebinned";
    outputFile2 += "_etaRebinned";
  }
  if(m_insitu) outputFile += "_Insitu";
  outputFile += ".root";
  outputFile2 += ".root";

  std::cout << "Histograms saved in: " << outputFile.Data() << std::endl;
 
  /*------------  Corr_vs_pTRef.root -------------*/

  TFile* tout = new TFile(outputFile.Data(),"RECREATE");
  tout->cd();
  for(int i=mineta;i<=maxeta;++i){
    if(m_applyEtaRebinning) hCorr[i]->Write();
    else {hCorr[i-15]->Write();}
  }
  gCorr->Write();
  hcorr2D->Write();
  tout->Close();

  /*------------  Corr_vs_pTRscan.root -------------*/

  TFile* tout2;
  if(m_applyMap){
    tout2 = new TFile(outputFile2.Data(),"RECREATE");
    tout2 -> cd();
    mCorr -> Write();

    EtaBinning ->Write();

    for(int j =mineta; j<=maxeta;j++){
      if(m_applyEtaRebinning) Corr4Combination[j]->Write();
      else{Corr4Combination[j-15]->Write();}
    }
    tout2->Close();
    std::cout << "Mapped TGraph saved in: " << outputFile2.Data() << std::endl;
  }

  std::cout << ">>>> Done <<<<" << std::endl;

  return 0;

}

