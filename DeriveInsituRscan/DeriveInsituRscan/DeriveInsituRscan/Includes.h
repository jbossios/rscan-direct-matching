#include "global.h"
#include <TSystem.h>
#include <sstream>
#include <iostream>

// ROOT
#include "TCanvas.h"
#include "TFile.h"
#include "TH2F.h"
#include "TLine.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraph2DErrors.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TStyle.h"
#include "TGraphErrors.h"

#include "AtlasUtils.h"
#ifndef __CINT__
#include "AtlasStyle.C"
#include "AtlasLabels.C"

using namespace std;

TString PATH_plots = "";
TString PATH_rootfiles = "";
TString inputFile = "";

TString JetCollection;
TString Version; 
TString year;

string m_pTavgType = "";
string m_treeVersion = "";
string m_jetCollection = "";
string m_systName = ""; // Default Nominal
string m_generator = "Pythia"; // Default Pythia
string m_path = "";

bool m_isMC = true;
bool m_debug = false;
bool m_mc15c = false;
bool m_mc16a = false;
bool m_mc16c = false;
bool m_mc16d = false;
bool m_insitu = false; 
bool m_PRW   = false;
bool m_zjet  = false;
bool m_bootstrap = false;
bool m_applyEtaRebinning = false;
bool m_useAbsEtaBins = false;
/*------------------------------------------------------*/

const int netaBins = 60;
TGraphErrors * Map[netaBins];
TGraph2DErrors * graph2d;

const int netaBinsReb = 16;
TGraphErrors * MapReb[netaBinsReb];
TGraph2DErrors * graph2dReb;

#endif
