#ifndef MYFITTER_H
#define MYFITTER_H

#include "TH1D.h"

class JES_BalanceFitter;

class MyFitter{
 
 private:
 
  JES_BalanceFitter *_jesFitter;	 
  double _Mean, _MeanError, _Sigma, _SigmaError, _Neff;
	 
 public:

  MyFitter ();
  MyFitter (double Nsigma);

  void GetFitRange(TH1* proj, int type, double delta, double& min, double& max);
  void Fit(TH1 *histo, double fitMin, double fitMax);
  void DrawFitAndHisto(double minx=-999, double maxx=-1000);
  inline double GetMean(){return _Mean;}
  inline double GetMeanError(){return _MeanError;}
  inline double GetSigma(){return _Sigma;}
  inline double GetSigmaError(){return _SigmaError;}
  inline double Neff(){return _Neff;}

};

#endif // MYFITTER_H
