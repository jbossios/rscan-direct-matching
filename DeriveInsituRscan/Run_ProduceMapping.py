#!/usr/bin/python

import os, sys

param=sys.argv

######################################################################
## Check before run
######################################################################

Debug     = False
ZJetStudy = True
Bootstrap = False # not specific to bootstrap, mapping info is also available on bootstrap output of TreetoHists

jetColl = "2LC"
#jetColl = "6LC"
#jetColl = "8LC"

version = "2015+2016data"
#version = "2017data"

##################################################################################
# DO NOT MODIFY
##################################################################################

# R20.7
#version = "2015data"
#version = "2016data"

# Systematics are produced in a coarser absolute eta binning. We merge TreetoHists Ouputs in these bins and produce the eta-rebinned mapping. PLUS: default is to produce Calibration using the absolute eta-rebinned Map
ApplyEtaRebinning = True

# In this path you should have the folder ROOTfiles
PATH = "../"

command = ""
# Merging eta bins in TreetoHists Ouputs (to increase statistics in systematics)
if ApplyEtaRebinning:
  command += "python DeriveInsituRscan/util/ApplyDijetJERbinning_syst.py"
  command += " --syst=Nominal"
  command += " --jetColl=" + jetColl
  command += " --inputVersion=" + version
  if ZJetStudy:
    command += " --ZJetStudy=TRUE"
  if Bootstrap:
    command += " --Bootstrap=TRUE"
  if Debug:
    command += " --Debug=TRUE"
  command += " --path=" + PATH
  command += " && "

command += "ProduceMapping "
command += " "
if ZJetStudy:
  command += "--ZJetStudy=TRUE "
if Bootstrap:
  command += "--Bootstrap=TRUE "
if ApplyEtaRebinning:
  command += "--ApplyEtaRebinning=TRUE "
command += "--jetColl="
command += jetColl
command += " --inputVersion="
command += version
command += " --path="
command += PATH
if Debug:
  command += " --debug=TRUE"

command += " && "

command = command[:-3]
command += "&"
print command
os.system(command)

