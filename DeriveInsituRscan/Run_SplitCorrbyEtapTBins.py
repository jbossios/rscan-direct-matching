#!/usr/bin/python

import os, sys

param=sys.argv

########################################################
## Check this before run
########################################################

Debug     = False
ZJetStudy = True
CompareTGraphs = False # Compares TH2 formats.

# To produce Corr vs eta in ptRef bins set ApplyMap=False, otherwise True
ApplyMap = False

jetColl = "2LC"
#jetColl = "6LC"
#jetColl = "8LC"

MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

systs=[
    "Nominal",
#    "deltaRUp",
#    "deltaRDown",
#    "isolationUp",
#    "isolationDown",
#    "JVTUp",
#    "JVTDown",
#    "MCSherpa",
]

##################################################################################
# DO NOT MODIFY
##################################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"


# R20.7
#MCVersion = "MC15cPythia_MC15bMuProfile"
#DataVersion = "2015data"

numericalInversion = False

command = ""

# Smoothed correction in eta bins

if ApplyMap:
  for syst in systs:
    command += "SmoothedCorrbyEtaBins"
    if syst != "Nominal":
      command += " --syst="
      command += syst
    command += " --jetColl="
    command += jetColl
    command += " --mcVersion="
    command += MCVersion
    command += " --dataVersion="
    command += DataVersion
    command += " --path="
    command += PATH
    command += " --noSmoothing=TRUE"
    if ZJetStudy:
      command += " --ZJetStudy=TRUE"
    command += " --ApplyMap=TRUE"
    if CompareTGraphs:
      command += " --CompareTGraphs=TRUE"
    if numericalInversion:
      command += " --nInversion=TRUE"

    command += " && "

# Smoothed correction in pT bins
for syst in systs:
  command += "CorrvsEta"
  if syst != "Nominal":
    command += " --syst="
    command += syst
  if ZJetStudy:
    command += " --ZJetStudy=TRUE"
  if ApplyMap:
    command += " --ApplyMap=TRUE"
  elif not ApplyMap:
    command += " --ApplyMap=FALSE"
  command += " --jetColl="
  command += jetColl
  command += " --mcVersion="
  command += MCVersion
  command += " --dataVersion="
  command += DataVersion
  command += " --path="
  command += PATH
  command += " --noSmoothing=TRUE"
  if numericalInversion:
    command += " --nInversion=TRUE"
  command += " && "

command = command[:-3]
command += "&"
print command
os.system(command)



