#!/usr/bin/python

import os, sys

param=sys.argv

########################################################
## Check this before run
########################################################

jetColl = "2LC"
#jetColl = "6LC"
#jetColl = "8LC"

Debug     = False
ZJetStudy = True
ApplyMap  = True
CalculateSystematic = False # Must be True Except for Nominal
ApplyEtaRebinning = False # Must be True except for Nominal

debugHists = False # Produce histogram of systematic value taken by each replica in pt and eta bins

MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

systs=[
    "Nominal",
#    "deltaRUp",
#    "deltaRDown",
#    "isolationUp",
#    "isolationDown",
#    "JVTUp",
#    "JVTDown",
#    "MCSherpa",
#    "Nominal4Sherpa",
]

##################################################################################
# DO NOT MODIFY
##################################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

# Min pT for the final calibration (decided based on the statistics)
pTMin = -1 # Default: no pTmin
if not ZJetStudy: # valid for 2015+2016data calibrations
  pTMin = 45 # (GeV) for both 2LC and 6LC jets

RebinnedMap4Calib = True

# R20.7
#MCVersion = "MC15cPythia_MC15bMuProfile"
#DataVersion = "2015data"
#MCVersion = "MC15cPythia"
#DataVersion = "2016data"

list=range(15,75) # Eta range
if ApplyEtaRebinning:
  list=range(0,16)

# Protections
if MCVersion is "MC16a" and DataVersion is "2017data":
  print "MC16a should not be used with 2017data, use MC16c or MC16d instead, exiting"
  sys.exit(0)
if MCVersion is "MC16c" and DataVersion is "2015+2016data":
  print "MC16c should not be used with 2015+2016data, use MC16a or MC16d instead, exiting"
  sys.exit(0)

numericalInversion = False
ImproveErrors = False
Insitu = False

command = ""
for syst in systs:
  command += "DivideDataMC"
  if syst != "Nominal":
    command += " --syst="
    command += syst
  command += " --jetColl="
  command += jetColl
  command += " --mcVersion="
  command += MCVersion
  command += " --dataVersion="
  command += DataVersion
  command += " --path="
  command += PATH
  if pTMin != -1:
    command += " --pTMin="
    command += str(pTMin)
  if ZJetStudy:
    command += " --ZJetStudy=TRUE"
  if ApplyEtaRebinning:
    command += " --ApplyEtaRebinning=TRUE"
  if ApplyMap:
    command += " --ApplyMap=TRUE"
  if not RebinnedMap4Calib:
    command += " --RebinnedMap4Calib=FALSE"
  if numericalInversion:
    command += " --nInversion=TRUE"
  if Insitu:
    command += " --insitu=TRUE"	  
  command += " && "

if CalculateSystematic:
  for syst in systs:
    if syst == "Nominal4Sherpa": # this is the Nominal correction to be compared w/ Sherpa.
      continue
    if syst == "MCSherpa":
      list = range(0,8) # the Sherpa systematic is derived in absolute eta bins.
    if syst != "Nominal":
      for slice in list: # loop over eta bins
        command += "CalculateStatisticalError4Systs"
        command += " --etaSlice="
        Slice = str(slice)
        command += Slice
        command += " --syst="
        command += syst
        command += " --jetColl="
        command += jetColl
        command += " --mcVersion="
        command += MCVersion
        command += " --dataVersion="
        command += DataVersion
        command += " --path="
        command += PATH
        if ZJetStudy:
          command += " --ZJetStudy=TRUE"
	if debugHists:
	  command += " --debugHists=TRUE"
	if ApplyEtaRebinning:
	  command += " --ApplyEtaRebinning=TRUE"
	command += " && "

      # Merge outputs of CalculateStatisticalError4Systs
      command += "hadd -f "
      command += PATH
      command += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Statistical_errors/"
      command += DataVersion
      command += "_vs_"
      command += MCVersion
      command += "/"
      command += jetColl
      command += "/Stat_error_"
      if ZJetStudy:
        command += "ZJetStudy_"
      if syst == "deltaRUp":
        command += "DeltaRUp"
      elif syst == "deltaRDown":
        command += "DeltaRDown"
      elif syst == "isolationUp":
        command += "IsolationUp"
      elif syst == "isolationDown":
        command += "IsolationDown"
      else:
	command += syst
      if ApplyEtaRebinning:
	command += "_EtaRebinned"
      command += ".root "
      
      for slice in list: # loop over eta bins
	command += PATH
        command += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Statistical_errors/"
        command += DataVersion
        command += "_vs_"
        command += MCVersion
        command += "/"
        command += jetColl
        command += "/Stat_error_"
        if ZJetStudy:
          command += "ZJetStudy_"
        command += "etaSlice_"
        Slice = str(slice)
        command += Slice
        command += "_"
        if syst == "deltaRUp":
          command += "DeltaRUp"
        elif syst == "deltaRDown":
          command += "DeltaRDown"
        elif syst == "isolationUp":
          command += "IsolationUp"
        elif syst == "isolationDown":
          command += "IsolationDown"
        else:
          command += syst
	if ApplyEtaRebinning:
	  command += "_EtaRebinned"
        command += ".root "
      command += "&& "

      command += "CalculateSystematics"
      command += " --syst="
      command += syst
      command += " --jetColl="
      command += jetColl
      command += " --mcVersion="
      command += MCVersion
      command += " --dataVersion="
      command += DataVersion
      command += " --path="
      command += PATH
      if pTMin != -1:
        command += " --pTMin="
        command += str(pTMin)
      if ZJetStudy:
        command += " --ZJetStudy=TRUE"
      if ApplyEtaRebinning:
	command += " --ApplyEtaRebinning=TRUE"
      command += " && "

command = command[:-3]
command += "&"
print command
os.system(command)

