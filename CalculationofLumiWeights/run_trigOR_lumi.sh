#!/bin/sh

help() {
    echo
    echo "Specify two arguments: TriggerOR and periods/runnumber"
    echo "Example: L1_J10orL1_FJ10 D_E_F"
}

error() {
    echo -e "\nERROR:"; echo "  $1";
}

[[ $# -lt 2 ]] && { help; return; exit; }

echo
echo "Step 1: Will compile"
echo

code="TrigORLumi.C"
exec=./trig_OR_lumi.exe
CMD="g++ `$ROOTSYS/bin/root-config --cflags --glibs` -o $exec $code"
echo $CMD

success=0
 # compile
$CMD && success=1

[[ $success = 0 ]] && { error "Compilation failed"; return; exit; }

$exec $1 $2

echo
echo "All done"
echo
