#!/usr/bin/python

import os, sys

param=sys.argv

Periods=[
  "D",
  "E",
  "F",
  "E_D_F",
]

Triggers=[
  "HLT_j360",
  "HLT_j260",
  "HLT_j200",
  "HLT_j175",
  "HLT_j150",
  "HLT_j110",
  "HLT_j85",
]

command = ""
for p in Periods:
  for i in Triggers:
    command += "source run_trigOR_lumi.sh "+i+"or"+i+" "+p+" > logs/"+i+"_"+p+" && "
print command
os.system(command)
