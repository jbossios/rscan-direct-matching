#!/bin/bash
echo "Usage: source getLumiPlots.sh <triggerlist> <grl>"

triglist=`cat $1`
grl=$2

echo $triglist

mkdir log
mkdir plots

# run lumicalc
for trigger in ${triglist}
do
   iLumiCalc.exe --trigger=$trigger --xml=${grl} --plots --lumitag=OflLumi-13TeV-001 --livetrigger=L1_EM12 &> log/${trigger}.log
done

mv *.root plots
