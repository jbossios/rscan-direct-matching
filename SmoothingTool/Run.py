#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys

# Check
jetColl = "2LC"
#jetColl = "6LC"
#jetColl = "8LC"

# Check
ZJetStudy = True
Smooth    = False # If False the calibrations will simply be mapped to pTrscan

# Check
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

systs=[
    "Nominal",
#    "deltaRUp",
#    "deltaRDown",
#    "isolationUp",
#    "isolationDown", 
#    "JVTUp",
#    "JVTDown",
#    "MCSherpa",
]            

##################################################################################
# DO NOT MODIFY
##################################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

ApplyMap  = True  # Map calibration and smoothed calibration (if Smooth True) to pTrscan

# R 20.7
#MCVersion = "MC15cPythia_MC15bMuProfile"
#DataVersion = "2015data"
#MCVersion = "MC15cPythia"
#DataVersion = "2016data"
numericalInversion = False

command = ""

for syst in systs:
  command += "Smoother"
  if syst != "Nominal":
    command += " --syst="
    command += syst
  command += " --jetColl="
  command += jetColl
  command += " --mcVersion="
  command += MCVersion
  command += " --dataVersion="
  command += DataVersion
  command += " --path="
  command += PATH
  if ZJetStudy:
    command += " --ZJetStudy=TRUE"
  if ApplyMap:
    command += " --ApplyMap=TRUE"
  if Smooth:
    command += " --Smooth=TRUE"
  command += " && "

command = command[:-3]
command += "&"
print command
os.system(command)

