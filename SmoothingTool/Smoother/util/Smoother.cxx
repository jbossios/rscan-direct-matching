/* **********************************************************************\
 *                                                                      *
 *  #   Name           : DivideDataMC                                   *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 28/02/16 First version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include "Smoother/Smoother.h"

TString PATH = "";

bool numericalInversion = false;
TString DataVersion = "";
TString MCVersion = "";
TString JetCollection = "";


/////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[]){

  std::string m_jetCollection = "";
  std::string m_dataVersion = "";     // xAOD version
  std::string m_mcVersion = "";       // xAOD version
  std::string m_systName = ""; // Default Nominal
  std::string m_path = "";
  bool m_numericalInversion = false;
  bool m_zjet = false;
  bool m_applyMap  = false;
  
  //-------------------------
  // Decode the user settings
  //-------------------------

  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--dataVersion=")   != std::string::npos) m_dataVersion = v[1];
    if ( opt.find("--mcVersion=")   != std::string::npos) m_mcVersion = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

    if ( opt.find("--nInversion=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
      if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
    }
    if ( opt.find("--ZJetStudy=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_zjet= true;
      if (v[1].find("FALSE") != std::string::npos) m_zjet= false;
    }
    if ( opt.find("--Smooth=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_smooth= true;
      if (v[1].find("FALSE") != std::string::npos) m_smooth= false;
    }
    if ( opt.find("--ApplyMap=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_applyMap= true;
      if (v[1].find("FALSE") != std::string::npos) m_applyMap= false;
    }
    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
      if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
      if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
      if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
      if (v[1].find("MCSherpa") != std::string::npos) m_systName = "MCSherpa";
    }

  }//End: Loop over input options

  if(m_applyMap && m_smooth) std::cout << "Smoothing Mapped Correction ... " << std::endl;
  if(m_applyMap) std::cout << "Mapping Calibration ... " << std::endl;
  
  if(m_dataVersion==""){
    std::cout << "Error: data version not specified, exiting" << std::endl;
    return 1;
  }
  if(m_mcVersion==""){
    std::cout << "Error: mc version not specified, exiting" << std::endl;
    return 1;
  }
  if(m_jetCollection==""){
    std::cout << "Error: jet collection not specified, exiting" << std::endl;
    return 1;
  }
  numericalInversion = m_numericalInversion;
  JetCollection = m_jetCollection;
  PATH = m_path;
  DataVersion = m_dataVersion;
  MCVersion   = m_mcVersion;


  TString inputfile = PATH;
  inputfile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/";
  inputfile += DataVersion;
  inputfile += "_vs_";
  inputfile += MCVersion;
  inputfile += "/";
  inputfile += JetCollection;
  inputfile += "/Corr_vs_pTrscan";
  if(m_zjet) inputfile += "_ZJet";
  if(m_systName!=""){
    inputfile += "_";
    inputfile += m_systName;
  }
  inputfile += ".root";

  //-----------------
  // Open ROOT files
  //-----------------

  std::cout << "Opening: " << inputfile << std::endl;
  TFile* tin = new TFile(inputfile,"read");

  // Get TGraph2DErrors

  TString name = "Graph2D";
  if(m_applyMap) name = "Correction_vs_pTRscan";   // mapped Correction
  TGraph2DErrors* tgraph = (TGraph2DErrors*)tin->Get(name.Data()); 
  tgraph->SetDirectory(0);// "detach" the histogram from the file

  tin->Close();
 
  //----------
  // Smoother 
  //----------

  double pTmin = 55.;
  
  if((JetCollection=="6LC" || JetCollection=="8LC") && !m_zjet ) pTmin = 55.;
  if(m_zjet){
    if(JetCollection=="2LC") pTmin = 15.;
    if(JetCollection=="6LC") pTmin = 35.;
  }
  
  double pTmax = 1530.; // not used, defined by GetpTmax()
  TH2D* Calibration_smoothed = new TH2D();
  TGraph2DErrors * mCalibration_smoothed = new TGraph2DErrors();
  TH2D* hCalibration_smoothed = new TH2D();
  TH2D* hCalibration = new TH2D();
  TH2D* hCalibration_noErrors = new TH2D();

  if(m_applyMap) {
    if(m_smooth){
      // TGraph with smoothed calibration
      mCalibration_smoothed = SmoothedTGraph2D(tgraph, "AntiKt"+JetCollection+"Topo_EtaInterCalibration_Tgraph");
      // Histogram with smoothed calibration
      hCalibration_smoothed = Make2DHistogram(mCalibration_smoothed, "AntiKt"+JetCollection+"Topo_EtaInterCalibration",true);
      // Histogram with calibration
      hCalibration = Make2DHistogram(tgraph,"Correction_TH2DFormat", false, false);
    } else {
      // Histogram with calibration and no errors
      hCalibration = Make2DHistogram(tgraph,"AntiKt"+JetCollection+"Topo_EtaInterCalibration",true);
      // Histogram with calibration and errors
      hCalibration_noErrors = Make2DHistogram(tgraph,"Correction_TH2DFormat", false, false);
    }
  } else { 
    Calibration_smoothed = SmoothPtEtaGraph(tgraph,"AntiKt"+JetCollection+"Topo_EtaInterCalibration",pTmin,pTmax); 
  }

  TH1D* hAbsEmpty = new TH1D("AntiKt"+JetCollection+"Topo_InsituCalib","",1,pTBins[0],pTBins[npTBins]);
  hAbsEmpty->Fill(100);

  //------------------------------
  // Save histograms to ROOT file
  //------------------------------
  TString outputFile = PATH;
  outputFile += "ROOTfiles/DirectMatching/SmoothingTool_Outputs/";
  outputFile += DataVersion;
  outputFile += "_vs_";
  outputFile += MCVersion;
  outputFile += "/";
  outputFile += JetCollection;
  outputFile += "/Correction_";
  if(!m_smooth) outputFile += "not";
  outputFile += "Smoothed";
  if(m_systName!=""){
    outputFile += "_";
    outputFile += m_systName;
  }
  if(m_zjet) outputFile += "_ZJet";
  outputFile += ".root";
  std::cout << "Output File: " << outputFile.Data() << std::endl;
  TFile* tout = new TFile(outputFile.Data(),"RECREATE");
  tout->cd();
  if(m_applyMap) {
    mCalibration_smoothed->Write();  // saving the Smoothed TGraph
    hCalibration_smoothed->Write();  // and the Smoothed Interpolated TH2D for closure testing
    hCalibration->Write(); 
    if(!m_smooth) hCalibration_noErrors->Write();
  } else{ Calibration_smoothed ->Write();} // Final Rscan Insitu calibration
  hAbsEmpty->Write();            // Empty histogram for Absolute insitu correction
  tout->Close();

  std::cout << ">>>> Done <<<<" << std::endl;

  return 0;

}

TH2D *MakePtEtaHisto(TString hname) {
  TH2D *h = new TH2D(hname,"",npTBins,pTBins,nEtaBins,EtaBins);
  h->SetXTitle("jet p_{T} [GeV]"); h->SetYTitle("jet #eta_{det}");
  return h;
}

// Produce Smoothed TH2D Histogram from TGraph2D Correction:
TH2D *SmoothPtEtaGraph(TGraph2DErrors *g2d, TString hname, double pTmin, double pTmax) {

  TH2D *h = MakePtEtaHisto(hname);

  for (int ieta=1;ieta<=h->GetNbinsY();++ieta){

    double eta=h->GetYaxis()->GetBinCenter(ieta);
    double myEta=eta;

    /* Don't needed because JetCalibTools already freeze the correction for |eta|>3.0
    if ( etaMax!=-99 && fabs(eta) > etaMax ){
      if ( eta>0 )  myEta = h->GetYaxis()->GetBinCenter( h->GetYaxis()->FindBin(etaMax-1e-6) );
      else          myEta = h->GetYaxis()->GetBinCenter( h->GetYaxis()->FindBin(-etaMax+1e-6) );
    }*/

    // Loop over pT bins
    for (int ipt=1;ipt<=h->GetNbinsX();++ipt) {

      double pt=h->GetXaxis()->GetBinCenter(ipt);

      double myPt = pt;
      pTmin = GetpTmin(g2d,myEta);
      pTmax = GetpTmax(g2d,myEta,pTmin);
      if ( pTmin!=-99 && pt < pTmin )  {
        myPt = h->GetXaxis()->GetBinCenter(h->GetXaxis()->FindBin(pTmin+1e-6));
      }
      if ( pTmax!=-99 && pt > pTmax )  {
        myPt = h->GetXaxis()->GetBinCenter(h->GetXaxis()->FindBin(pTmax-1e-6));
      }

      double R=GetSmoothedValue(g2d,myPt,myEta, _pTSmooth, _etaSmooth);
      double dR = 0;

      h->SetBinContent(ipt,ieta,R);
      h->SetBinError(ipt,ieta,dR);

    }
  }

  return h;
}

// Produce Smoothed TGraph2D from TGraph2D Correction (calib points shifted from "bin center" due to mapping procedure):
TGraph2DErrors *SmoothedTGraph2D(TGraph2DErrors *g2d, TString hname) {

  TGraph2DErrors * gr = new TGraph2DErrors();
  gr -> SetNameTitle(hname,hname);

  std::vector<double> etaValues;
  for(unsigned int i = 0; i< nEtaBins; i++){
   etaValues.push_back( EtaBins[i] + 0.05 );
  }
  int counter = 0;
  for ( unsigned int ieta=0; ieta < etaValues.size(); ++ieta){
    double myEta=etaValues[ieta];

    std::vector<double> ptValues;
    for( int i = 0; i< g2d->GetN(); i++){
      double _p = g2d ->GetX()[i], _e = g2d ->GetY()[i];
      if(fabs(_e-myEta)>0.01) continue; // Pick up only the pT bins for that particular eta
      ptValues.push_back( _p );
    }

    // Loop over pT bins
    for ( unsigned int ipt=0; ipt < ptValues.size(); ++ipt) {
      double myPt = ptValues[ipt];
      double R=GetSmoothedValue(g2d,myPt,myEta, _pTSmooth, _etaSmooth);
      gr->SetPoint(counter, myPt, myEta, R);
      counter ++;
    }  // ptValues
  }  // etaValues
  return gr;
}


double GetSmoothedValue(TGraph2DErrors *g, double x, double y, double _pTSmooth, double _etaSmooth) { // x:pT, y:Eta
  double sumw=0, sumwz=0;
  for (int i=0; i<g->GetN();++i) {
    double xi=g->GetX()[i], yi=g->GetY()[i], zi=g->GetZ()[i];
    //if(fabs(yi-y)>0.01) continue; // fixing the correction in eta
    if(zi==0) continue;
    double pTSmooth = _pTSmooth;
    double etaSmooth = _etaSmooth;
    double dx = (x-xi)/pTSmooth; if (m_logx) dx /= x;
    double dy = (y-yi)/etaSmooth; if (m_logy) dy /= y;
    double dr = sqrt(dx*dx+dy*dy);
    double wi = TMath::Gaus(dr);
    if (m_useError) wi *= 1.0/pow(g->GetEZ()[i],2);
    sumw += wi; sumwz += wi*zi;
  }
  return sumwz/sumw;
}


double GetpTmin(TGraph2DErrors *g, double eta){
  // Find where to start the freezing
  double pTmin = -99;
  for (int i=0; i<g->GetN();++i) { // Loop over pT bins
    double xi=g->GetX()[i], yi=g->GetY()[i], zi=g->GetZ()[i];
    if(fabs(yi-eta)>0.01) continue; // Pick up only the pT bins for that particular eta
    if(zi!=0){
      pTmin = xi;
      break;
    }
  }
  return pTmin;
}

double GetpTmax(TGraph2DErrors *g, double eta, double pTmin){
  // Find where to start the freezing
  double pTmax = -99;
  for (int i=0; i<g->GetN();++i) { // Loop over pT bins
    double xi=g->GetX()[i], yi=g->GetY()[i], zi=g->GetZ()[i];
    if(fabs(yi-eta)>0.01) continue; // Pick up only the pT bins for that particular eta
    if(zi==0 && xi>pTmin){
      pTmax = g->GetX()[i-1];
      break;
    }
  }
  return pTmax;
}

/* ------------------ USEFUL FUNCTIONS for managing TGraph2d ------------------------------------*/


std::vector<TGraphErrors> SplitTGraph2DErrorsbyEta( TGraph2DErrors * gr ){
  std::vector<TGraphErrors> vec;
  std::vector<double> etaValues;
  for(unsigned int i = 0; i< nEtaBins; i++){
   etaValues.push_back( EtaBins[i] + 0.05 );
  }
  TString tname;
  for(unsigned int eta = 0; eta< etaValues.size(); eta++ ){
    tname = "Corr_vs_pTrscan_Eta_";
    tname += eta;
    TGraphErrors g = TGraphErrors();
    g.SetName(tname);
    g.GetXaxis()->SetTitle("p_{T}^{Rscan} [GeV]");
    g.GetYaxis()->SetTitle("Correction");

    double myEta=etaValues[eta];
    int counter = 0;

    for( int i = 0; i< gr->GetN(); i++ ){
      double x = gr ->GetX()[i], y = gr ->GetY()[i], z = gr->GetZ()[i], ex = 0.001, ez = gr->GetEZ()[i];
      if(fabs(y-myEta)>0.01) continue;   // Pick up only the pT bins for that particular eta
      g.SetPoint(counter, x, z);
      g.SetPointError(counter, ex, ez);
      counter ++;
    } //fill tgraph for one eta
    vec.push_back( g );
  } // filled al etas.
  return vec;
}

/*-------------------------------------------------------------------------------------------------*/

std::vector<TGraph> SplitTGraph2DbyEta( TGraph2D * gr ){
  std::vector<TGraph> vec;
  std::vector<double> etaValues;

  for(unsigned int i = 0; i< nEtaBins; i++){
   etaValues.push_back( EtaBins[i] + 0.05 );
  }
  TString tname;
  for(unsigned int eta = 0; eta< etaValues.size(); eta++ ){
    tname = "Corr_vs_pTrscan_Eta_";
    tname += eta;
    TGraph g = TGraph();
    g.SetName(tname);
    g.GetXaxis()->SetTitle("p_{T}^{Rscan} [GeV]");
    g.GetYaxis()->SetTitle("Correction");

    double myEta=etaValues[eta];
    int counter = 0;

    for( int i = 0; i< gr->GetN(); i++ ){
      double x = gr ->GetX()[i], y = gr ->GetY()[i], z = gr->GetZ()[i];
      if(fabs(y-myEta)>0.01) continue;   // Pick up only the pT bins for that particular eta
      g.SetPoint(counter, x, z);
      counter ++;
    } //fill tgraph for one eta
    vec.push_back( g );
  } // filled al etas.
  return vec;
}

// Convert Smoothed (Mapped) TGraph2D into TH2D format to easy apply insitu correction.
TH2D *Make2DHistogram(TGraph2DErrors * gr, TString name, bool freeze, bool noErrors){
  TH2D * histo = new TH2D(name, name, npTBins, pTBins, nEtaBins, EtaBins);
  histo->SetXTitle("p_{T}^{Rscan} [GeV]");
  histo->SetYTitle("#eta_{det}");
  histo->SetZTitle("Correction");

  std::vector<TGraphErrors> g = SplitTGraph2DErrorsbyEta( gr );

  for(int ieta = 1; ieta <= nEtaBins; ieta++){
    double minpt = g[ieta-1].GetX()[0];
    double maxpt = g[ieta-1].GetX()[g[ieta-1].GetN()-1];
    int minBin = -99, maxBin = -999;
    for( int ipt = 1; ipt <= npTBins; ipt++ ){
     if ( minpt >= pTBins[ipt-1] && minpt < pTBins[ipt] ) minBin = ipt;
     if ( maxpt >= pTBins[ipt-1] && maxpt < pTBins[ipt] ) maxBin = ipt;
    }
    // Fill in calibration points
    for ( int bin = minBin; bin <= maxBin; bin++ ){
      double binCenter  = histo -> GetXaxis() -> GetBinCenter( bin );
      double binContent = g[ieta-1].Eval( binCenter );
      double dr=999, aux;
      int index=999;

      histo -> SetBinContent(bin, ieta, binContent );
      if(noErrors) {histo -> SetBinError( bin, ieta, 0.);
      }else{
        for(int point = 0; point < g[ieta-1].GetN(); point++){
          aux = fabs( binCenter - g[ieta-1].GetX()[point] );
          if( aux < dr ) {
            dr = aux;
            index = point;
          }
        }
        histo->SetBinError( bin, ieta, g[ieta-1].GetEY()[index] );
      }
    }  // loop over pt bins
    if(freeze){
      // Freezing below minBin
      for ( int bin = 1; bin < minBin; bin ++ ){
        double binCenter = histo -> GetXaxis() -> GetBinCenter( minBin );
        double binContent = g[ieta-1].Eval( binCenter );
        histo -> SetBinContent( bin,ieta,binContent );
        histo -> SetBinError( bin,ieta, 0.);
      }
      // Freezing above maxBin
      for ( int bin = maxBin + 1; bin <=npTBins; bin ++ ){
        double binCenter = histo -> GetXaxis() -> GetBinCenter( maxBin );
        double binContent = g[ieta-1].Eval( binCenter );
        histo -> SetBinContent( bin,ieta,binContent );
        histo -> SetBinError( bin,ieta, 0.);
      }
    }
  } // loop over eta bins
  return histo;
}
