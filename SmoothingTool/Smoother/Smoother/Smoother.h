#include <TSystem.h>
#include <sstream>
#include <iostream>
#include <vector>

#include "TCanvas.h"
#include "TFile.h"
#include "TH2F.h"
#include "TLine.h"
#include "TH1D.h"
#include "TMath.h"
#include "TProfile.h"
#include "TString.h"
#include "TGraph2DErrors.h"
#include "TGraphErrors.h"

/********** Binning ***********/

// pT binning
const int npTBins = 43+3;
double pTBins[npTBins+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941., 6200., 7780., 9763.};
// eta binning
const int nEtaBins = 60;
double EtaBins[nEtaBins+1] = {-3.0, -2.9, -2.8, -2.7, -2.6, -2.5, -2.4, -2.3, -2.2, -2.1, -2.0, -1.9, -1.8, -1.7, -1.6, -1.5, -1.4, -1.3, -1.2, -1.1, -1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1 ,0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0};

/* ----------- FUNCTIONS Specific for Smoother -----------*/

// Smoother
double _pTSmooth = 0.1;
double _etaSmooth = 0.05;
bool m_useError  = true;
bool m_logx      = true;
bool m_logy      = false;
bool m_smooth    = false; // then the calibrations will be simply mapped to pTrscan


TH2D *SmoothPtEtaGraph(TGraph2DErrors *g2d, TString hname, double pTmin=-99, double pTmax=-99);
TH2D *MakePtEtaHisto(TString);
TGraph2DErrors* SmoothedTGraph2D(TGraph2DErrors *g2d, TString hname);
TH2D *Make2DHistogram(TGraph2DErrors * gr, TString name, bool freeze = true, bool noErrors = true);
double GetSmoothedValue(TGraph2DErrors *g, double x, double y, double _pTSmooth = 0.2 , double _etaSmooth = 0.05 );
double GetpTmin(TGraph2DErrors *g2d, double);
double GetpTmax(TGraph2DErrors *g2d, double,double);


