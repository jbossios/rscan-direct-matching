#!/usr/bin/python

import os, sys

param=sys.argv

collections=[
  "2LC",
  "6LC",
]

DirectMatching = True
  
####################################################################################
# DO NOT EDIT
####################################################################################

MC = [
  "MC16a",
  "MC16a",
  "MC16a",
#  "MC16c",
  "MC16d",
]

Data = [
  "R21_2015data",
  "R21_2016data",
  "2015+2016data",
#  "2017data",
  "2017data",
]

for i in range(0,len(MC)):
  xAODcodeVersion_MC = MC[i]
  xAODcodeVersion_Data = Data[i]
  Folder = Data[i]+"_vs_"+MC[i]

  command = ""

  command  = "mkdir ../ROOTfiles/"
  print command
  os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../ROOTfiles/DirectMatching/"
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../ROOTfiles/DirectMatching/JetCalibToolsConfigs/"
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../ROOTfiles/DirectMatching/JetCalibToolsConfigs/"
    command += Folder
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../ROOTfiles/DirectMatching/SystematicInputsToCombination/"
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../ROOTfiles/DirectMatching/SystematicInputsToCombination/"
    command += Folder
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../ROOTfiles/DirectMatching/Mapping/"
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../ROOTfiles/DirectMatching/Mapping/"
    command += xAODcodeVersion_Data
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../ROOTfiles/DirectMatching/TriggerEmulation/"
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../ROOTfiles/DirectMatching/TriggerEmulation/"
    command += xAODcodeVersion_Data
    print command
    os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command  += "TreetoHists_Outputs/"  
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command  += "TreetoHists_Outputs/Data/"  
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command  += "TreetoHists_Outputs/MC/"  
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command  += "TreetoHists_Outputs/Data/"  
  command += xAODcodeVersion_Data
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command += "TreetoHists_Outputs/MC/"  
  command += xAODcodeVersion_MC
  print command
  os.system(command)


# DeriveInsituRscan outputs (Relative Response)


  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command += "/DeriveInsituRscan_Outputs/"
  print command
  os.system(command)


  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command += "/DeriveInsituRscan_Outputs/RelativeResponse/"
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching"
  command += "/DeriveInsituRscan_Outputs/RelativeResponse"
  command += "/Data/"
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command += "DeriveInsituRscan_Outputs/RelativeResponse/"
  command += "MC/"
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command += "DeriveInsituRscan_Outputs/RelativeResponse/"
  command += "Data/"
  command += xAODcodeVersion_Data
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command += "DeriveInsituRscan_Outputs/RelativeResponse/"
  command += "MC/"
  command += xAODcodeVersion_MC
  print command
  os.system(command)


  # DeriveInsituRscan outputs (Correction)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command += "/DeriveInsituRscan_Outputs/Correction/"
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True: 
    command  += "DirectMatching/"
  command += "DeriveInsituRscan_Outputs/Correction/"
  command += Folder
  print command
  os.system(command)

  # DeriveInsituRscan outputs (Statistical_errors)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True:
    command  += "DirectMatching/"
  command += "/DeriveInsituRscan_Outputs/Statistical_errors/"
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/"
  if DirectMatching==True:
    command  += "DirectMatching/"
  command += "DeriveInsituRscan_Outputs/Statistical_errors/"
  command += Folder
  print command
  os.system(command)

  # DeriveInsituRscan outputs (SmoothedCorr)
  command  = "mkdir ../ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/"
  print command
  os.system(command)

  # DeriveInsituRscan outputs (SmoothedCorr)
  command  = "mkdir ../ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/"
  command += Folder
  print command
  os.system(command)


  # DeriveInsituRscan Outputs (vsEta)
  command  = "mkdir ../ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorrvsEta/"
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorrvsEta/"
  command += Folder
  print command
  os.system(command)

  # SmoothingTool Outputs
  command  = "mkdir ../ROOTfiles/DirectMatching/SmoothingTool_Outputs/"
  print command
  os.system(command)

  command  = "mkdir ../ROOTfiles/DirectMatching/SmoothingTool_Outputs/"
  command += Folder
  print command
  os.system(command)



# Plots

  command  = "mkdir ../Plots/"
  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/"
  print command
  os.system(command)

  command  = "mkdir ../Plots/Response_Data_vs_MC"
  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/Response_Data_vs_MC"
  print command
  os.system(command)
  
  command  = "mkdir ../Plots/Mapping"
  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/Mapping/"
  print command
  os.system(command)

  command  = "mkdir ../Plots/Mapping/"
  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/Mapping/"
  command += xAODcodeVersion_Data
  print command
  os.system(command)

  command  = "mkdir ../Plots/ResponseFitter/"
  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/ResponseFitter/"
  print command
  os.system(command)

  command  = "mkdir ../Plots/ResponseFitter/"
  if DirectMatching==True: 
    command = "mkdir ../Plots_DirectMatching/ResponseFitter/Data"
  print command
  os.system(command)

  command  = "mkdir ../Plots/ResponseFitter/"
  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/ResponseFitter/MC"
  print command
  os.system(command)

  command  = "mkdir ../Plots/ResponseFitter/"
  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/ResponseFitter/Data/"
  command += xAODcodeVersion_Data
  print command
  os.system(command)

  command  = "mkdir ../Plots/ResponseFitter/"
  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/ResponseFitter/MC/"
  command += xAODcodeVersion_MC
  print command
  os.system(command)

  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/Smoothing"
    print command
    os.system(command)

  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/Smoothing/"
    command += Folder
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../Plots_DirectMatching/Kinematics"
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../Plots_DirectMatching/Kinematics/"
    command += Folder
    print command
    os.system(command)


  if DirectMatching==True:
    command  = "mkdir ../Plots_DirectMatching/Calibrations"
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../Plots_DirectMatching/Calibrations/"
    command += Folder
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../Plots_DirectMatching/Systematics"
    print command
    os.system(command)

  if DirectMatching==True:
    command  = "mkdir ../Plots_DirectMatching/Systematics/"
    command += Folder
    print command
    os.system(command)

  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/TriggerEmulation/"
    print command
    os.system(command)

  if DirectMatching==True: 
    command  = "mkdir ../Plots_DirectMatching/TriggerEmulation/"
    command +=xAODcodeVersion_Data
    print command
    os.system(command)

  
  # loop over jet collections
  for coll in collections:
    # TreetoHists outputs
    command  = "mkdir ../ROOTfiles/"
    if DirectMatching==True: 
      command  += "DirectMatching/"
    command  += "TreetoHists_Outputs/Data/"
    command += xAODcodeVersion_Data
    command += "/"
    command += coll
    print command
    os.system(command)
  
    command  = "mkdir ../ROOTfiles/"
    if DirectMatching==True: 
      command  += "DirectMatching/"
    command  += "TreetoHists_Outputs/MC/"
    command += xAODcodeVersion_MC
    command += "/"
    command += coll
    print command
    os.system(command)

    # DeriveInsituRscan outputs

    command  = "mkdir ../ROOTfiles/"
    if DirectMatching==True: 
      command  += "DirectMatching/"
    command  += "DeriveInsituRscan_Outputs/RelativeResponse/Data/"
    command += xAODcodeVersion_Data
    command += "/"
    command += coll
    print command
    os.system(command)
  
    command  = "mkdir ../ROOTfiles/"
    if DirectMatching==True: 
      command  += "DirectMatching/"
    command  += "DeriveInsituRscan_Outputs/RelativeResponse/MC/"
    command += xAODcodeVersion_MC
    command += "/"
    command += coll
    print command
    os.system(command)

    # Mapping

    command  = "mkdir ../ROOTfiles/"
    if DirectMatching==True:
      command += "DirectMatching/"
    command += "Mapping/"
    command += xAODcodeVersion_Data
    command += "/"
    command += coll
    print command
    os.system(command)

  
  
    # DeriveInsituRscan outputs (Correction)
  
    command  = "mkdir ../ROOTfiles/"
    if DirectMatching==True: 
      command  += "DirectMatching/"
    command  += "DeriveInsituRscan_Outputs/Correction/"
    command += Folder
    command += "/"
    command += coll
    print command
    os.system(command)

    # DeriveInsituRscan outputs (Statistical_errors)

    command  = "mkdir ../ROOTfiles/"
    if DirectMatching==True:
      command  += "DirectMatching/"
    command  += "DeriveInsituRscan_Outputs/Statistical_errors/"
    command += Folder
    command += "/"
    command += coll
    print command
    os.system(command)

 
    # Plots
  
    command  = "mkdir ../Plots/"
    if DirectMatching==True: 
      command  = "mkdir ../Plots_DirectMatching/"
    command  += "ResponseFitter/Data/"
    command += xAODcodeVersion_Data
    command += "/"
    command += coll
    print command
    os.system(command)

    command  = "mkdir ../Plots/"
    if DirectMatching==True: 
      command  = "mkdir ../Plots_DirectMatching/"
    command  += "ResponseFitter/MC/"
    command += xAODcodeVersion_MC
    command += "/"
    command += coll
    print command
    os.system(command)

    command  = "mkdir ../Plots/"
    if DirectMatching==True: 
      command  = "mkdir ../Plots_DirectMatching/"
    command  += "Mapping/"
    command += xAODcodeVersion_Data
    command += "/"
    command += coll
    print command
    os.system(command)


    # DeriveInsituRscan outputs (SmoothedCorr)
    command  = "mkdir ../ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/"
    command += Folder
    command += "/"
    command += coll
    print command
    os.system(command)

    # DeriveInsituRscan outputs (vsEta)
    command  = "mkdir ../ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorrvsEta/"
    command += Folder
    command += "/"
    command += coll
    print command
    os.system(command)

    # SmoothingTool outputs
    command  = "mkdir ../ROOTfiles/DirectMatching/SmoothingTool_Outputs/"
    command += Folder
    command += "/"
    command += coll
    print command
    os.system(command)

    command  = "mkdir ../Plots_DirectMatching/Response_Data_vs_MC/"
    command += Folder
    print command
    os.system(command)

    command  = "mkdir ../Plots_DirectMatching/Response_Data_vs_MC/"
    command += Folder
    command += "/"
    command += coll
    print command
    os.system(command)

    if DirectMatching==True: 
      command  = "mkdir ../Plots_DirectMatching/Smoothing/"
      command += Folder
      command += "/"
      command += coll
      print command
      os.system(command)

    if DirectMatching==True:
      command  = "mkdir ../Plots_DirectMatching/Kinematics/"
      command += Folder
      command += "/"
      command += coll
      print command
      os.system(command)

    if DirectMatching==True:
      command  = "mkdir ../Plots_DirectMatching/Calibrations/"
      command += Folder
      command += "/"
      command += coll
      print command
      os.system(command)

    if DirectMatching==True:
      command  = "mkdir ../Plots_DirectMatching/Systematics/"
      command += Folder
      command += "/"
      command += coll
      print command

    if DirectMatching==True: 
      command  = "mkdir ../Plots_DirectMatching/TriggerEmulation/"
      command += xAODcodeVersion_Data
      command += "/"
      command += coll
      print command
      os.system(command)



  

