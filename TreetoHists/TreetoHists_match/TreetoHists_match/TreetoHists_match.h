#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <sstream>
#include <utility>
#include <math.h>

#include <TROOT.h>
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TProfile.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"

#include "global.h"

#include "../../BootstrapGenerator/BootstrapGenerator/TH2DBootstrap.h"
#include "../../BootstrapGenerator/BootstrapGenerator/TH1DBootstrap.h"
#include "../../BootstrapGenerator/BootstrapGenerator/BootstrapGenerator.h"

TString PATH;
TString DataoutputFile;
TString MCinputFile;
TString MCoutputFile;
TString JZSlice;
TString JetCollection;
TString version;

const int nAsymmBins = 480;
const int nEtaBins = 90;
const int nCorrFactorsBins = 1000;

// Input tree
TTree *tree;
TFile *ft;
TFile *tout;

//------------
// Histograms
//------------

// Mu Distributions
TH1D* h_averageMu;
TH1D* h_averageMu_noPRW;
TH1D* h_actualMu;
TH1D* h_actualMu_noPRW;

std::map<std::string,TH1D*> h_pTavg_Triggers;

TH1D* h_pT_Ref;
TH1D* h_pT_Rscan;
TH1D* h_pT_RefAll;
TH1D* h_pT_RscanAll;
TH1D* h_ZpT;

TH1D* h_DeltaR;

TH1D* h_DeltaRRef_iso;
TH1D* h_DeltaRRscan_iso;

// DR(rscan,rscan) and minDR(rscan,rscan) distributions for each pTrscan bin (before isolation)
TH1D* h_minDR_rscan_rscan[npTavgBins];
TH1D* h_DR_rscan_rscan[npTavgBins];

// DR(ref,ref) and minDR(ref,ref) distributions for each pTref bin (before isolation)
TH1D* h_minDR_ref_ref[npTavgBins];
TH1D* h_DR_ref_ref[npTavgBins];
 
TH1D* h_cutflow;

// Relative response histograms (one for each detector eta bin)
TH2F* h_Response_pTRef_Eta[nEtaBins];
TH2D* h_PtMap_Eta[nEtaBins];

// Bootstrap histograms
TH2D* Bootstrap_Nominal_Eta[nEtaBins];
TH2D* Bootstrap_Replica_Eta[nEtaBins];

TH2DBootstrap* fBootstrap_TH2[nEtaBins];
TH1DBootstrap* fBootstrap_TH1[nEtaBins];

// PRW
TH1D* PRWwgt; // PRW weights for each pTavg bin
  
struct prop {
    TLorentzVector Sel_Jet;
    bool           Used;
    bool           Iso;
    double         DetectorEta;
    double         JVT;
} ;
std::vector<prop> Selected_RscanJets;  
std::vector<prop> Selected_RefJets;  

struct prop_sel {
    TLorentzVector Sel_JetRef;
    TLorentzVector Sel_JetRscan;
    double         dR;
    double         detEta_Ref;  
    double         detEta_Rscan;  
} ;
std::vector<prop_sel> Selected_matched_Jets;


double Radius;
int ptmin; 

//-----------
// Variables
//-----------

// Event Info
int runNumber;
Long64_t eventNumber;
int mcEventNumber;
float mcEventWeight;
int lumiBlock;
int bcid;
int NPV;
float mu;
float actualMu;
double rho;
float wgt;
float weight_pileup;

// Muons
int nMuon;
std::vector<float> *mu_pt = 0;
std::vector<float> *mu_eta = 0;
std::vector<float> *mu_phi = 0;
std::vector<float> *mu_E = 0;
std::vector<float> *mu_ch = 0;
std::vector<float> *mu_RecoWg= 0;
std::vector<float> *mu_IsoWg= 0;
Double_t TriggerEff; // MuonTriggerSF

TLorentzVector mu1, mu2, Z0;


// Reco Jets
int nRefJets; 
std::vector<float> *RefJet_pt = 0;
std::vector<float> *RefJet_phi = 0;
std::vector<float> *RefJet_eta = 0;
std::vector<float> *RefJet_E = 0;
std::vector<int> *RefJet_clean_passLooseBad = 0;
std::vector<float> *RefJet_JVT = 0;
std::vector<float> *RefJet_gscScalePt = 0;
std::vector<float> *RefJet_constScaleEta = 0;

int nRscanJets;
std::vector<float> *RscanJet_pt = 0;
std::vector<float> *RscanJet_phi = 0;
std::vector<float> *RscanJet_eta = 0;
std::vector<float> *RscanJet_E = 0;
std::vector<int> *RscanJet_clean_passLooseBad = 0;
std::vector<float> *RscanJet_JVT = 0;
std::vector<float> *RscanJet_constScaleEta = 0;

// Truth Jets
int nTruthRefJets;
std::vector<float> *TruthRefJet_pt = 0;
std::vector<float> *TruthRefJet_eta = 0;
std::vector<float> *TruthRefJet_phi = 0;
std::vector<float> *TruthRefJet_E = 0;

// vector of Passed Triggers
std::vector<std::string> *passedTriggers = 0;

// for bootstrap
 Int_t nreplica = 100;
 BootstrapGenerator *fGenerator;

class iJet : public TLorentzVector {
  private:
    double detectorEta;
    int    passLooseBad;
  public:
    inline double getDetectorEta(){return detectorEta;}
    inline void   setDetectorEta(double eta){detectorEta = eta;}
    inline void   setCleaning(float pass){passLooseBad = pass;}
    inline float  getCleaning(){return passLooseBad;}
};

  
