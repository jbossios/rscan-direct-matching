//
// global.h
//
// Binning for Inclusive and Dijet analysis
//
// Jonathan Bossio - 15 June 2015
//
///////////////////////////////////////////

//--------
// Binning
//--------

// ystar bins for mass spectra
static const int nystarBins = 7;
double ystarBins[nystarBins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.5};

// y bins for pT spectra
static const int nyBins = 6;
double yBins[nyBins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0};

// y bins for y histograms
static const int nyBinsyHisto = 60;
double yBinsyHisto[nyBinsyHisto+1];

// eta bins for eta histogram
static const int netaBins = 90;
double etaBins[netaBins+1];

// phi bins for phi histogram
static const int nphiBins = 64;
double phiBins[nphiBins+1];

// ystar bins for ystar histogram
const int nDelYBins = 9;
double delYBins[nDelYBins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.4 };   // 9 bins

//-----------------
// Inclusive jet pT
//-----------------

// Different pt bins for each y bin
const int npTavgBins = 43+3;
double pTavgBins[npTavgBins+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941., 6200., 7780., 9763.};

// Triggers
std::vector<std::string > Triggers;

// Dijets Weights
const int nTriggers = 8;
double wgtDijets[nTriggers][nTriggers] = {0};

