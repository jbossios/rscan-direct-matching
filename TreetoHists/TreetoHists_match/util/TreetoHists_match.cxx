/* **********************************************************************\
 *                                                                      *
 *  #   Name:   TreetoHists_match      	                                *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 26/11/15 First version              J. Bossio (jbossios@cern.ch)  *
 *  2 29/09/16 Second version             R. Devesa (mdevesa@cern.ch)   *
 *  3 29/09/17 Third version              J. Bossio (jbossios@cern.ch)  *
 *  4 19/03/18 Fourth version             F. Daneri (mdaneri@cern.ch)   *
\************************************************************************/

#include "../TreetoHists_match/TreetoHists_match.h"

// Variables used to estimate closure
bool applyInsitu = false;
TString insituFile = "";
TString rel_histoname = "JETALGO_EtaInterCalibration";
TString PATH_user = "";
TString InputFile_user = "";

// Parameters of the direct matching method
float JVTthreshold = 0.59; // 2016 Moriod Recs (default working point) (Nominal)
float JVTpTmax     = 60;   // 2016 Moriond Recs
float JVTetamax    = 2.4;
double DRmatch     = 0.2;  // Default (Nominal) DR between Ref and the Rscan jet
double ISOfactor   = 2;    // Default (Nominal) factor for the isolation condition

// MC Campaign
bool MC15c = false;
bool MC15c_MC15bmuProfile = false;
bool MC16a = false;
bool MC16c = false;
bool MC16d = false;

bool ComputeSampleWeight = false;

// Data
TString Data = ""; // 2015, 2016 or 2017
bool R21 = false;

bool EOS = true;

bool Bootstrap = false;

bool PRW = false;
TString DatainputFile;
bool MC = false;
bool m_debug      = false;
bool m_debugHists = false;
int  firstTrig = 0; // HLT_j15 (except for 8LC: HLT_j25)
TString Generator = "Pythia"; // Default
TString Version   = "";

// Z+jets & Dijets
double Ref_ptMin       = 20;// Cut after isolation requirement
double Ref_ptMin_iso   = 7; // Minimum pTref for isolation requeriement
double Rscan_ptMin_iso = 7; // Rscan minimum at Isolation requirement

// Z+jets
bool ZJet = false;
float muon_MinPt = 28; // same threshold in DxAODtoZTree

void EnableBranches();
void SetBranchAddress();
void BookHistograms();

std::map<int,std::pair<double,double>> ReadAMIvalues(TString AMIfile);

struct SortByPt {
  template <class A, class B> bool operator() (A a, B b){
    return a.Pt() >= b.Pt();
  }
};

bool myfunction (int i,int j);

int main(int argc, char* argv[]) {

  gROOT->ProcessLine(".L loader.C+");
  
  std::string jzSlice;
  std::string jetColl;
  std::string version  = "";
  std::string treePart = "";
  std::string systName = ""; // Default Nominal
  std::string generator = "Pythia"; // Default Pythia
  std::string pathUser = "";
  std::string inputFileUser = "";
  std::string m_path = "";
  //---------------------------
  // Decoding the user settings
  //---------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
        v.push_back(item);
    }

    if ( opt.find("--JZSlice=")   != std::string::npos) jzSlice = v[1];

    // Only for 2016data 
    if ( opt.find("--pathUSER=")   != std::string::npos) pathUser = v[1];
    if ( opt.find("--inputFileUSER=")   != std::string::npos) inputFileUser = v[1];
    
    if ( opt.find("--jetColl=")   != std::string::npos) jetColl = v[1];
    
    if ( opt.find("--insituFile=")   != std::string::npos) insituFile = v[1];

    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];
    
    if ( opt.find("--version=")   != std::string::npos) version = v[1];
    
    if ( opt.find("--PRW=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) PRW= true;
      if (v[1].find("FALSE") != std::string::npos) PRW= false;
    }

    if ( opt.find("--EOS=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) EOS= true;
      if (v[1].find("FALSE") != std::string::npos) EOS= false;
    }

    if ( opt.find("--ZJetStudy=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) ZJet= true;
      if (v[1].find("FALSE") != std::string::npos) ZJet= false;
    }
	
    if ( opt.find("--debug=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debug= true;
      if (v[1].find("FALSE") != std::string::npos) m_debug= false;
    }

    if ( opt.find("--debugHists=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debugHists= true;
      if (v[1].find("FALSE") != std::string::npos) m_debugHists= false;
    }

    if ( opt.find("--Bootstrap=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) Bootstrap= true;
      if (v[1].find("FALSE") != std::string::npos) Bootstrap= false;
    }

    if ( opt.find("--computeSampleWeights=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) ComputeSampleWeight= true;
      if (v[1].find("FALSE") != std::string::npos) ComputeSampleWeight= false;
    }

    if ( opt.find("--part=")  != std::string::npos){
      if (v[1].find("1") != std::string::npos) treePart = "Part1";
      else if (v[1].find("2") != std::string::npos) treePart = "Part2";
      else if (v[1].find("3") != std::string::npos) treePart = "Part3";
      else if (v[1].find("4") != std::string::npos) treePart = "Part4";
      else if (v[1].find("5") != std::string::npos) treePart = "Part5";
    }

    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) systName = "DeltaRDown";
      if (v[1].find("isolationUp") != std::string::npos) systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) systName = "IsolationDown";
      if (v[1].find("JVTDown") != std::string::npos) systName = "JVTDown";
      if (v[1].find("JVTUp") != std::string::npos) systName = "JVTUp";
      if (v[1].find("MCsherpa") != std::string::npos) systName = "MCSherpa";
    }
    
  }//End: Loop over input options

  if(ZJet) std::cout<< "Running Z+jets"<< std::endl;
  else{ std::cout<< "Running Dijets"<< std::endl;}

  JZSlice = jzSlice;
  JetCollection = jetColl;
  Version = version;
  PATH_user = pathUser;
  InputFile_user = inputFileUser;

  if(Version==""){
    std::cout << "ERROR: Version should be specified, exiting" << std::endl;
    return 1;
  }

  // Decide if MC
  if(Version.Contains("MC")) MC = true;
  
  // Pick up type of MC
  if(MC){
    if(Version.Contains("MC15")){
      if(Version.Contains("MC15bMuProfile")) MC15c_MC15bmuProfile = true;
      else{MC15c = true;}
    } else if (Version.Contains("MC16")){
      R21 = true;
      if(Version.Contains("MC16a")) MC16a = true;
      else if (Version.Contains("MC16c")) MC16c = true;
      else{MC16d = true;}
    }
  }

  // Pick up which data use
  if(!MC){
    if(Version.Contains("2015data")) Data = "2015";
    if(Version.Contains("2016data")) Data = "2016";
    if(Version.Contains("2017data")) Data = "2017";
  }

  // R21?
  if(Version.Contains("R21") || Data=="2017") R21 = true;

  // Pick up Generator
  if(Version.Contains("Pythia")) Generator = "Pythia";
  if(Version.Contains("Powheg")) Generator = "Powheg";
  if(Version.Contains("PowhegHerwig")) Generator = "PowhegHerwig";

  if(systName=="MCSherpa" && !MC){
    std::cout << "ERROR: Systematic MCSherpa should be run only in MC, exiting" << std::endl;
    return 1;
  }
  
  if (systName == "DeltaRUp")        DRmatch = 0.3;
  else if (systName == "DeltaRDown") DRmatch = 0.1;
  else if (systName == "IsolationUp")   ISOfactor = 1.5;
  else if (systName == "IsolationDown") ISOfactor = 2.5;
  else if (systName == "JVTDown") JVTthreshold = 0.11;
  else if (systName == "JVTUp")   JVTthreshold = 0.91;
  
  if (systName == ""){ std::cout << "\nSystName: Nominal" << std::endl;}
  else{ std::cout << "SystName: " << systName << std::endl;}

  // Protection: PRW only available for R21
  if(!R21 || !MC) PRW = false;
  if(PRW) std::cout << "PRW is going to be applied!" << std::endl;

  // Protection
  if(m_path==""){
    std::cout << "ERROR: Path should be provided, exiting" << std::endl;
     return 1;
  }

  // Path to trees
  if(!EOS){ //pcubas
    if(MC && systName=="MCSherpa") PATH = "/3/jbossios/Insitu_outputs/MC/v_21/MC15c_MC15bmuProfile_25ns_Sherpa/"; // pcuba002
    else if(MC && Generator=="Pythia" && MC15c_MC15bmuProfile) PATH = "/1/jbossios/Insitu_outputs/Trees/MC/MC15c_MC15bmuProfile_25ns_Pythia/"; // pcuba002
    else if(MC && Generator=="Pythia" && MC15c) PATH = "/4/jbossios/Insitu_outputs/Trees/MC/MC15c_25ns_Pythia/"; // pcuba001
    else if(!MC && Data=="2015"){ PATH = "/4/jbossios/Insitu_outputs/Trees/Data/20_7_2015data_withForwardJetTriggers/";} // pcuba002
    else if(!MC && Data=="2016"){
      if(PATH_user=="") std::cout << "ERROR: No pathUSER specified in Run.py" << std::endl;
      PATH = PATH_user;
    } // pcuba002
  } else { // EOS 
    if(!ZJet){
      if(MC && systName=="MCSherpa" && !R21) PATH = "root://eosatlas//eos/atlas/user/j/jbossios/rscan_2017/Trees/MC/MC15c_MC15bmuProfile_Sherpa/";
      if(MC && systName=="MCSherpa" && R21) PATH = "root://eosatlas///eos/atlas/user/m/mdaneri/Sherpa_Jona/MC16a_Sherpa/";
      else if(MC && Generator=="Pythia" && MC15c_MC15bmuProfile) PATH = "root://eosatlas//eos/atlas/user/j/jbossios/rscan_2017/Trees/MC/MC15c_MC15bmuProfile_Pythia/";
      else if(MC && Generator=="Pythia" && MC15c) PATH = "root://eosatlas//eos/atlas/user/m/mdaneri/MC15c_25ns_Pythia/";
      else if(MC && MC16a) PATH = "/eos/atlas/atlascerngroupdisk/perf-jets/Rscan/R21/MC16a/";
      else if(!MC && Data=="2015" && !R21){ PATH = "root://eosatlas//eos/atlas/user/j/jbossios/rscan_2017/Trees/Data/2015data/";}
      else if(!MC && Data=="2016" && !R21){
	if(PATH_user=="")      std::cout << "ERROR: No pathUSER specified in Run.py" << std::endl;
	if(InputFile_user=="") std::cout << "ERROR: No inputFileUSER specified in Run.py" << std::endl;
	PATH = PATH_user;
      } else if(!MC && R21){
	if(PATH_user=="")      std::cout << "ERROR: No pathUSER specified in Run.py" << std::endl;
	if(InputFile_user=="") std::cout << "ERROR: No inputFileUSER specified in Run.py" << std::endl;
	PATH = PATH_user;
      }
    } else { // ZJet
      // RELEASE 21 location for ZJet Trees
      if(MC && MC16a) PATH = "/eos/user/m/mtoscani/Rel21/MC16a/";
      if(MC && MC16a && systName=="MCSherpa") PATH = "/eos/user/m/mtoscani/Rel21/MC16a/Sherpa/"; 
      else if(MC && MC16d ) PATH = "/eos/user/m/mtoscani/Rel21/MC16d/";
      if(MC && MC16d && systName=="MCSherpa") PATH = "/eos/user/m/mtoscani/Rel21/MC16d/Sherpa/";
      else if(!MC && Data=="2017") PATH = "/eos/user/m/mtoscani/Rel21/data17/";
      else if(!MC && Data=="2016") PATH = "/eos/user/m/mtoscani/Rel21/data16/";
      else if(!MC && Data=="2015") PATH = "/eos/user/m/mtoscani/Rel21/data15/";
    } // end ZJet
  } // end EOS
 

  if(JetCollection!="2LC" && JetCollection!="3LC" && JetCollection!="5LC" && JetCollection!="6LC" && JetCollection!="7LC" && JetCollection!="8LC"){
    std::cout << "ERROR: wrong jet collection, exiting" << std::endl;
    return 1;
  }
  if(JetCollection=="8LC") firstTrig = 1;

  std::cout << "Reference jets: AntiKt4EMTopoJets " << std::endl;
  std::cout << "Rscan jets: AntiKt"<<JetCollection<<"TopoJets " << std::endl;
  
  if(insituFile!=""){
    applyInsitu = true;
    std::cout << "Insitu correction is going to be applied to R-scan jets" << std::endl;
  }
  
  if(!ZJet){
    if(JZSlice!="All")  MCinputFile = "JZ"+JZSlice+"_tree/JZ"+JZSlice+"_tree.root";
    else{ MCinputFile += "JZAll_tree.root"; }
  } else { 
    if(JZSlice!="All") MCinputFile = "JZ"+JZSlice+"_tree.root";
    else{MCinputFile = "trees.root";}
  }
 
  DatainputFile = "Data";
  
  if(!ZJet){
    if(treePart=="") DatainputFile += "All_tree.root";
    else if(treePart=="Part1"){ DatainputFile += "Part1_tree.root";
    } else if(treePart=="Part2"){ DatainputFile += "Part2_tree.root";
    } else if(treePart=="Part3"){ DatainputFile += "Part3_tree.root";
    } else if(treePart=="Part4"){ DatainputFile += "Part4_tree.root";
    } else if(treePart=="Part5"){ DatainputFile += "Part5_tree.root";
    }	  
  } else { //ZJet
    DatainputFile = "trees.root";
  }
  
  // Check
  if(MC15c_MC15bmuProfile){MCoutputFile = "/MC15c_MC15bmuProfile_";}
  else if(MC15c){MCoutputFile = "/MC15c_";}
  else if(MC16a){MCoutputFile = "/MC16a_";}
  else if(MC16c){MCoutputFile = "/MC16c_";}
  else if(MC16d){MCoutputFile = "/MC16d_";}
  if(!ZJet){
    MCoutputFile += Generator;
  }
  if(ZJet) MCoutputFile += "ZJetStudy";
  MCoutputFile += "_JZ"+JZSlice;
  if(PRW) MCoutputFile += "_PRW";
  else{MCoutputFile += "_noPRW";}
  if(systName!=""){
    MCoutputFile += "_";
    MCoutputFile += systName;
  }
  if(Bootstrap) MCoutputFile += "_bootstrap";
  MCoutputFile += ".root";

  // Data
  DataoutputFile = "/Data_25ns";
  if(treePart!=""){
    DataoutputFile += "_";
    DataoutputFile += treePart;
  }
  if(!MC && (Data == "2016" || R21) && !ZJet){
    // extract period
    int index_period = PATH_user.Index("period")+6;
    TString period = PATH_user[index_period];
    DataoutputFile += "_period";
    DataoutputFile += period;
    // extract number of file
    int index_number = InputFile_user.Index(".tree")-6;
    TString numberFile = InputFile_user(index_number,6);
    DataoutputFile += "_file";
    DataoutputFile += numberFile;
  } else if(ZJet) {
    DataoutputFile += "_"+ Data +  "_ZJetStudy";
  }
  if(systName!=""){
    DataoutputFile += "_";
    DataoutputFile += systName;
  }
  if(applyInsitu) DataoutputFile += "_Insitu";
  if(applyInsitu && insituFile.Contains("notSmoothed")) DataoutputFile += "notSmoothed";
  if(Bootstrap) DataoutputFile += "_bootstrap.root";
  else{DataoutputFile += ".root";}

  //----------
  // Triggers
  //----------

  if(R21){
    Triggers.push_back("HLT_j15");
    if(Data == "2015") Triggers.push_back("HLT_j25");
    Triggers.push_back("HLT_j35");
    Triggers.push_back("HLT_j45");
    Triggers.push_back("HLT_j55");
    Triggers.push_back("HLT_j60");
    Triggers.push_back("HLT_j85");
    Triggers.push_back("HLT_j110");
    Triggers.push_back("HLT_j150");
    Triggers.push_back("HLT_j175");
    if(Data == "2015") Triggers.push_back("HLT_j200");
    Triggers.push_back("HLT_j260");
    if(Data == "2015") Triggers.push_back("HLT_j360");
    if(Data == "2016"){
      Triggers.push_back("HLT_j380");
      Triggers.push_back("HLT_j400");
    }
  } else{
    Triggers.push_back("HLT_j15");
    Triggers.push_back("HLT_j25");
    Triggers.push_back("HLT_j35");
    Triggers.push_back("HLT_j45");
    Triggers.push_back("HLT_j55");
    Triggers.push_back("HLT_j60");
    Triggers.push_back("HLT_j85");
    Triggers.push_back("HLT_j110");
    Triggers.push_back("HLT_j150");
    Triggers.push_back("HLT_j175");
    Triggers.push_back("HLT_j200");
    Triggers.push_back("HLT_j260");
    if(Data == "2015") Triggers.push_back("HLT_j360");
    if(Data == "2016"){
      Triggers.push_back("HLT_j380");
      Triggers.push_back("HLT_j400");
    }
  }

  // Trigger thresholds
  std::map<std::string,double> minPtTriggers;
  std::map<std::string,double> maxPtTriggers;
  if(JetCollection=="2LC" && Data == "2015" && !R21){
    minPtTriggers["HLT_j15"] = 25.;
    maxPtTriggers["HLT_j15"] = 35.;
    minPtTriggers["HLT_j25"] = 35.;
    maxPtTriggers["HLT_j25"] = 45.;
    minPtTriggers["HLT_j35"] = 45.;
    maxPtTriggers["HLT_j35"] = 55.;
    minPtTriggers["HLT_j45"] = 55.;
    maxPtTriggers["HLT_j45"] = 70.;
    minPtTriggers["HLT_j55"] = 70.;
    maxPtTriggers["HLT_j55"] = 85.;
    minPtTriggers["HLT_j60"] = 85.;
    maxPtTriggers["HLT_j60"] = 116.;
    minPtTriggers["HLT_j85"] = 116.;
    maxPtTriggers["HLT_j85"] = 134.;
    minPtTriggers["HLT_j110"] = 134.;
    maxPtTriggers["HLT_j110"] = 194.;
    minPtTriggers["HLT_j150"] = 194.;
    maxPtTriggers["HLT_j150"] = 216.;
    minPtTriggers["HLT_j175"] = 216.;
    maxPtTriggers["HLT_j175"] = 264.;
    minPtTriggers["HLT_j200"] = 264.;
    maxPtTriggers["HLT_j200"] = 318.;
    minPtTriggers["HLT_j260"] = 318.;
    maxPtTriggers["HLT_j260"] = 442.;
    minPtTriggers["HLT_j360"] = 442.;
    maxPtTriggers["HLT_j360"] = 10000.;
  } else if(Data == "2015" && R21){
    minPtTriggers["HLT_j15"] = 35.;
    maxPtTriggers["HLT_j15"] = 45.;
    minPtTriggers["HLT_j25"] = 45.;
    maxPtTriggers["HLT_j25"] = 55.;
    minPtTriggers["HLT_j35"] = 55.;
    maxPtTriggers["HLT_j35"] = 70.;
    minPtTriggers["HLT_j45"] = 80.;
    maxPtTriggers["HLT_j45"] = 85.;
    minPtTriggers["HLT_j55"] = 85.;
    maxPtTriggers["HLT_j55"] = 100.;
    minPtTriggers["HLT_j60"] = 100.;
    maxPtTriggers["HLT_j60"] = 116.;
    minPtTriggers["HLT_j85"] = 116.;
    maxPtTriggers["HLT_j85"] = 152.;
    minPtTriggers["HLT_j110"] = 152.;
    maxPtTriggers["HLT_j110"] = 194.;
    minPtTriggers["HLT_j150"] = 194.;
    maxPtTriggers["HLT_j150"] = 240.;
    minPtTriggers["HLT_j175"] = 240.;
    maxPtTriggers["HLT_j175"] = 264.;
    minPtTriggers["HLT_j200"] = 264.;
    maxPtTriggers["HLT_j200"] = 318.;
    minPtTriggers["HLT_j260"] = 318.;
    maxPtTriggers["HLT_j260"] = 442.;
    minPtTriggers["HLT_j360"] = 442.;
    maxPtTriggers["HLT_j360"] = 10000.;
  } else if(JetCollection=="2LC" && Data == "2016" && !R21){
    minPtTriggers["HLT_j15"] = 25.;
    maxPtTriggers["HLT_j15"] = 35.;
    minPtTriggers["HLT_j25"] = 35.;
    maxPtTriggers["HLT_j25"] = 45.;
    minPtTriggers["HLT_j35"] = 45.;
    maxPtTriggers["HLT_j35"] = 55.;
    minPtTriggers["HLT_j45"] = 55.;
    maxPtTriggers["HLT_j45"] = 70.;
    minPtTriggers["HLT_j55"] = 70.;
    maxPtTriggers["HLT_j55"] = 85.;
    minPtTriggers["HLT_j60"] = 85.;
    maxPtTriggers["HLT_j60"] = 116.;
    minPtTriggers["HLT_j85"] = 116.;
    maxPtTriggers["HLT_j85"] = 134.;
    minPtTriggers["HLT_j110"] = 134.;
    maxPtTriggers["HLT_j110"] = 194.;
    minPtTriggers["HLT_j150"] = 194.;
    maxPtTriggers["HLT_j150"] = 216.;
    minPtTriggers["HLT_j175"] = 216.;
    maxPtTriggers["HLT_j175"] = 318.;
    minPtTriggers["HLT_j200"] = 1e6; // Skip this trigger (turned off in 2016 data)
    maxPtTriggers["HLT_j200"] = 1e7; // Skip this trigger (turned off in 2016 data)
    minPtTriggers["HLT_j260"] = 318.;
    maxPtTriggers["HLT_j260"] = 442.;
    minPtTriggers["HLT_j380"] = 442.;
    maxPtTriggers["HLT_j380"] = 478.;
    minPtTriggers["HLT_j400"] = 478.;
    maxPtTriggers["HLT_j400"] = 10000.;
  } else if(Data == "2016" && R21){
    minPtTriggers["HLT_j15"] = 45.;
    maxPtTriggers["HLT_j15"] = 55.;
    minPtTriggers["HLT_j35"] = 55.;
    maxPtTriggers["HLT_j35"] = 70.;
    minPtTriggers["HLT_j45"] = 70.;
    maxPtTriggers["HLT_j45"] = 85.;
    minPtTriggers["HLT_j55"] = 85.;
    maxPtTriggers["HLT_j55"] = 100.;
    minPtTriggers["HLT_j60"] = 100.;
    maxPtTriggers["HLT_j60"] = 116.;
    minPtTriggers["HLT_j85"] = 116.;
    maxPtTriggers["HLT_j85"] = 152.;
    minPtTriggers["HLT_j110"] = 152.;
    maxPtTriggers["HLT_j110"] = 194.;
    minPtTriggers["HLT_j150"] = 194.;
    maxPtTriggers["HLT_j150"] = 216.;
    minPtTriggers["HLT_j175"] = 216.;
    maxPtTriggers["HLT_j175"] = 318.;
    minPtTriggers["HLT_j260"] = 318.;
    maxPtTriggers["HLT_j260"] = 478.;
    minPtTriggers["HLT_j380"] = 478.;
    maxPtTriggers["HLT_j380"] = 516.;
    minPtTriggers["HLT_j400"] = 516.;
    maxPtTriggers["HLT_j400"] = 10000.;
  } else if(JetCollection=="6LC" && Data == "2015" && !R21){
    minPtTriggers["HLT_j15"] = 45.;
    maxPtTriggers["HLT_j15"] = 55.;
    minPtTriggers["HLT_j25"] = 55.;
    maxPtTriggers["HLT_j25"] = 70.;
    minPtTriggers["HLT_j35"] = 1e6; // Skipping this trigger
    maxPtTriggers["HLT_j35"] = 1e7; // Skipping this trigger
    minPtTriggers["HLT_j45"] = 70.;
    maxPtTriggers["HLT_j45"] = 85.;
    minPtTriggers["HLT_j55"] = 85.;
    maxPtTriggers["HLT_j55"] = 100.;
    minPtTriggers["HLT_j60"] = 100.;
    maxPtTriggers["HLT_j60"] = 134.;
    minPtTriggers["HLT_j85"] = 134.;
    maxPtTriggers["HLT_j85"] = 172.;
    minPtTriggers["HLT_j110"] = 172.;
    maxPtTriggers["HLT_j110"] = 216.;
    minPtTriggers["HLT_j150"] = 216.;
    maxPtTriggers["HLT_j150"] = 264.;
    minPtTriggers["HLT_j175"] = 264.;
    maxPtTriggers["HLT_j175"] = 290.;
    minPtTriggers["HLT_j200"] = 290.;
    maxPtTriggers["HLT_j200"] = 376.;
    minPtTriggers["HLT_j260"] = 376.;
    maxPtTriggers["HLT_j260"] = 478.;
    minPtTriggers["HLT_j360"] = 478.;
    maxPtTriggers["HLT_j360"] = 10000.;
  } else if(JetCollection=="6LC" && Data == "2016" && !R21){
    minPtTriggers["HLT_j15"] = 45.;
    maxPtTriggers["HLT_j15"] = 55.;
    minPtTriggers["HLT_j25"] = 55.;
    maxPtTriggers["HLT_j25"] = 70.;
    minPtTriggers["HLT_j35"] = 70.;
    maxPtTriggers["HLT_j35"] = 85.;
    minPtTriggers["HLT_j45"] = 85.;
    maxPtTriggers["HLT_j45"] = 100.;
    minPtTriggers["HLT_j55"] = 100.;
    maxPtTriggers["HLT_j55"] = 116.;
    minPtTriggers["HLT_j60"] = 116.;
    maxPtTriggers["HLT_j60"] = 134.;
    minPtTriggers["HLT_j85"] = 134.;
    maxPtTriggers["HLT_j85"] = 172.;
    minPtTriggers["HLT_j110"] = 172.;
    maxPtTriggers["HLT_j110"] = 216.;
    minPtTriggers["HLT_j150"] = 216.;
    maxPtTriggers["HLT_j150"] = 240.;
    minPtTriggers["HLT_j175"] = 240.;
    maxPtTriggers["HLT_j175"] = 346.;
    minPtTriggers["HLT_j200"] = 1e6; // Skip this trigger (turned off in 2016 data)
    maxPtTriggers["HLT_j200"] = 1e7; // Skip this trigger (turned off in 2016 data)
    minPtTriggers["HLT_j260"] = 346.;
    maxPtTriggers["HLT_j260"] = 478.;
    minPtTriggers["HLT_j380"] = 478.;
    maxPtTriggers["HLT_j380"] = 516.;
    minPtTriggers["HLT_j400"] = 516.;
    maxPtTriggers["HLT_j400"] = 10000.;
  } else if(JetCollection=="8LC"){
    minPtTriggers["HLT_j25"] = 70.;
    maxPtTriggers["HLT_j25"] = 85.;
    minPtTriggers["HLT_j35"] = 1e6; // Skipping this trigger
    maxPtTriggers["HLT_j35"] = 1e7; // Skipping this trigger
    minPtTriggers["HLT_j45"] = 85.;
    maxPtTriggers["HLT_j45"] = 100.;
    minPtTriggers["HLT_j55"] = 100.;
    maxPtTriggers["HLT_j55"] = 116.;
    minPtTriggers["HLT_j60"] = 116.;
    maxPtTriggers["HLT_j60"] = 152.;
    minPtTriggers["HLT_j85"] = 152.;
    maxPtTriggers["HLT_j85"] = 172.;
    minPtTriggers["HLT_j110"] = 172.;
    maxPtTriggers["HLT_j110"] = 216.;
    minPtTriggers["HLT_j150"] = 216.;
    maxPtTriggers["HLT_j150"] = 264.;
    minPtTriggers["HLT_j175"] = 264.;
    maxPtTriggers["HLT_j175"] = 290.;
    minPtTriggers["HLT_j200"] = 290.;
    maxPtTriggers["HLT_j200"] = 376.;
    minPtTriggers["HLT_j260"] = 376.;
    maxPtTriggers["HLT_j260"] = 478.;
    minPtTriggers["HLT_j360"] = 478.;
    maxPtTriggers["HLT_j360"] = 10000.;
  }

  std::map<std::string,int> TriggertoInt;
  std::map<int,std::string> InttoTrigger;
  if(Data=="2015"){
    InttoTrigger[0] = "HLT_j15";
    InttoTrigger[1] = "HLT_j25";
    InttoTrigger[2] = "HLT_j35";
    InttoTrigger[3] = "HLT_j45";
    InttoTrigger[4] = "HLT_j55";
    InttoTrigger[5] = "HLT_j60";
    InttoTrigger[6] = "HLT_j85";
    InttoTrigger[7] = "HLT_j110";
    InttoTrigger[8] = "HLT_j150";
    InttoTrigger[9] = "HLT_j175";
    InttoTrigger[10] = "HLT_j200";
    InttoTrigger[11] = "HLT_j260";
    InttoTrigger[12] = "HLT_j360";
    TriggertoInt["HLT_j15"] = 0;
    TriggertoInt["HLT_j25"] = 1;
    TriggertoInt["HLT_j35"] = 2;
    TriggertoInt["HLT_j45"] = 3;
    TriggertoInt["HLT_j55"] = 4;
    TriggertoInt["HLT_j60"] = 5;
    TriggertoInt["HLT_j85"] = 6;
    TriggertoInt["HLT_j110"] = 7;
    TriggertoInt["HLT_j150"] = 8;
    TriggertoInt["HLT_j175"] = 9;
    TriggertoInt["HLT_j200"] = 10;
    TriggertoInt["HLT_j260"] = 11;
    TriggertoInt["HLT_j360"] = 12;
  } else if(Data=="2016"){
    InttoTrigger[0] = "HLT_j15";
    InttoTrigger[1] = "HLT_j35";
    InttoTrigger[2] = "HLT_j45";
    InttoTrigger[3] = "HLT_j55";
    InttoTrigger[4] = "HLT_j60";
    InttoTrigger[5] = "HLT_j85";
    InttoTrigger[6] = "HLT_j110";
    InttoTrigger[7] = "HLT_j150";
    InttoTrigger[8] = "HLT_j175";
    InttoTrigger[9] = "HLT_j260";
    InttoTrigger[10] = "HLT_j380";
    InttoTrigger[11] = "HLT_j400";
    TriggertoInt["HLT_j15"] = 0;
    TriggertoInt["HLT_j35"] = 1;
    TriggertoInt["HLT_j45"] = 2;
    TriggertoInt["HLT_j55"] = 3;
    TriggertoInt["HLT_j60"] = 4;
    TriggertoInt["HLT_j85"] = 5;
    TriggertoInt["HLT_j110"] = 6;
    TriggertoInt["HLT_j150"] = 7;
    TriggertoInt["HLT_j175"] = 8;
    TriggertoInt["HLT_j260"] = 9;
    TriggertoInt["HLT_j380"] = 10;
    TriggertoInt["HLT_j400"] = 11;
  }

  int trigUnprescaled = 0;
  if(Data=="2015") trigUnprescaled = TriggertoInt["HLT_j360"];
  else if(Data=="2016") trigUnprescaled = TriggertoInt["HLT_j400"];
  else if(Data=="2017") trigUnprescaled = TriggertoInt["HLT_j420"];

  if (JetCollection=="2LC"){Radius=.2;ptmin=minPtTriggers[InttoTrigger[firstTrig]];}
  if (JetCollection=="3LC"){Radius=.3;ptmin=minPtTriggers[InttoTrigger[firstTrig]];}
  if (JetCollection=="5LC"){Radius=.5;ptmin=minPtTriggers[InttoTrigger[firstTrig]];}
  if (JetCollection=="6LC"){Radius=.6;ptmin=minPtTriggers[InttoTrigger[firstTrig]];}
  if (JetCollection=="7LC"){Radius=.7;ptmin=minPtTriggers[InttoTrigger[firstTrig]];}
  if (JetCollection=="8LC"){Radius=.8;ptmin=minPtTriggers[InttoTrigger[firstTrig]];}

  // Reading weights for pT distributions in data
  std::vector<std::vector<double> > wgtPrescale;
  if(!MC && !ZJet){
    if(m_debug) std::cout << "Reading Luminosities" << std::endl;
    // Input File
    std::ifstream in;
    TString lumifile = "../CalculationofLumiWeights/Tables/";
    if(Data=="2015") lumifile += "data2015/";
    else if(Data=="2016") lumifile += "data2016/";
    else if(Data=="2017") lumifile += "data2017/";
    lumifile += "lumi.txt";
    std::cout << "Reading luminosity from: " << lumifile << std::endl;
    in.open(lumifile,std::ifstream::in);

    double t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12;

    // Reading values
    if(Data=="2015"){
      while (in >> t0 >> t1 >> t2 >> t3 >> t4 >> t5 >> t6 >> t7 >> t8 >> t9 >> t10 >> t11 >> t12) {
        std::vector<double> temp;
        temp.push_back(t0);
        temp.push_back(t1);
        temp.push_back(t2);
        temp.push_back(t3);
        temp.push_back(t4);
        temp.push_back(t5);
        temp.push_back(t6);
        temp.push_back(t7);
        temp.push_back(t8);
        temp.push_back(t9);
        temp.push_back(t10);
        temp.push_back(t11);
        temp.push_back(t12);
        wgtPrescale.push_back(temp);
      }
    } else if(Data=="2016"){
      while (in >> t0 >> t1 >> t2 >> t3 >> t4 >> t5 >> t6 >> t7 >> t8 >> t9 >> t10 >> t11) {
        std::vector<double> temp;
        temp.push_back(t0);
        temp.push_back(t1);
        temp.push_back(t2);
        temp.push_back(t3);
        temp.push_back(t4);
        temp.push_back(t5);
        temp.push_back(t6);
        temp.push_back(t7);
        temp.push_back(t8);
        temp.push_back(t9);
        temp.push_back(t10);
        temp.push_back(t11);
        wgtPrescale.push_back(temp);
      }
    }
    in.close();
  } // !MC && !ZJet
  
  //-----------------------------
  // Defining eta bins
  //-----------------------------
 
  for (unsigned int i = 0; i <= netaBins; i++ ) {
    etaBins[i]= -4.5 + i/10.0;
  }
 
  //-----------------------
  // Getting File
  //-----------------------

  TString inputFile = PATH;
  if(MC) inputFile += MCinputFile;
  else{
    if((Data=="2016" || R21) && !ZJet) inputFile += InputFile_user;
    else{inputFile += DatainputFile;}
  }

  std::cout << "Opening: " << inputFile << std::endl;
  ft = TFile::Open(inputFile.Data());
  ft->cd();
  if(ft == NULL) {
    std::cout<<"Returned null file: "<<ft<<", exiting"<<std::endl;
    return 1;
  }
  
  //-----------------------
  // Getting Tree
  //-----------------------

  TString treeName = "nominal";
  if(ZJet) treeName = "Ztree";
  TString directoryName = "Tree";
  if(R21 && !ZJet){
    TDirectory* TDirF_tmp = ft->GetDirectory(directoryName.Data());
    TDirF_tmp->GetObject(treeName.Data(), tree);
  } else { tree = (TTree*)ft->Get(treeName.Data()); }

   
  //---------------------------
  // Enable necessary branches
  //---------------------------
  EnableBranches();
  SetBranchAddress();
  // Number of events
  int entries = tree->GetEntries();
  if(m_debug) std::cout << "Total number of entries: " << entries << std::endl;
  if(m_debug) entries = 10000;
  if(m_debug) std::cout << "Running over " << entries << " entries" << std::endl;

  //---------------------
  // Booking Histograms
  //---------------------
  if(m_debug) std::cout << "Booking Histograms" << std::endl;
  BookHistograms();
  if(m_debug) std::cout << "Histograms Booked" << std::endl;

  double weight = 1.;
  
  int Skippedevents_Cleaning = 0;

  // Objects needed for insitu calibration factors
  TFile* iFile = NULL;
  TH2D* m_insituCorr = NULL;
  double m_insituEtaMax = -999;
  double m_insituPtMin  = -999;
  double m_insituPtMax  = -999;
  if(!MC && applyInsitu){
    // Open input file with calibration factors
    // Append path to insituFile
    TString dataVersion = m_path; dataVersion += "ROOTfiles/DirectMatching/JetCalibToolsConfigs/";
    if(Version == "R21_2015data" || Version == "R21_2016data") dataVersion += "2015+2016data_vs_MC16a";
    else if(Version == "2017data") dataVersion += "2017data_vs_MC16d";
    dataVersion += "/";
    insituFile.Insert(0,dataVersion);
    std::cout << "Reading In-situ correction factors from: " << insituFile << std::endl;
    iFile = new TFile(insituFile,"READ");
    if(!iFile){
      std::cout << "ERROR: Cannot open InsituCalibrationFile: " << insituFile << ", exiting" << std::endl;
      return 1;
    }

    // Identify jet algorithm
    TString jetAlgo = "";
    if(JetCollection=="2LC") jetAlgo = "AntiKt2LCTopo";
    else if(JetCollection=="3LC") jetAlgo = "AntiKt3LCTopo";
    else if(JetCollection=="4LC") jetAlgo = "AntiKt4LCTopo";
    else if(JetCollection=="6LC") jetAlgo = "AntiKt6LCTopo";
    else if(JetCollection=="7LC") jetAlgo = "AntiKt7LCTopo";
    else if(JetCollection=="8LC") jetAlgo = "AntiKt8LCTopo";

    // Input histogram name
    rel_histoname.ReplaceAll("JETALGO",jetAlgo);

    m_insituCorr = (TH2D*)iFile->Get(rel_histoname);
    if( m_insituCorr==NULL ){
      std::cout << "No residual in-situ histograms could be retrieved. Aborting..." << std::endl;
      return 1;
    } else {
      gROOT->cd();
      m_insituEtaMax = m_insituCorr->GetYaxis()->GetBinLowEdge(m_insituCorr->GetNbinsY()+1);
      m_insituPtMin = m_insituCorr->GetXaxis()->GetBinLowEdge(1);
      m_insituPtMax = m_insituCorr->GetXaxis()->GetBinLowEdge(m_insituCorr->GetNbinsX()+1);
    }
  }

  std::vector<iJet> NewRscanJets; // R-scan jets with insitu calibration applied (if requested)

  // Get pyAMI values and metadata when computing sample weights
  double Denominator = 1;
  std::map<int,std::pair<double,double>> MyAMImap;
 
  TString AMIfile = "";
  if(MC && ComputeSampleWeight){
    AMIfile = "../pyAMI/Outputs/";
    if(MC16a) AMIfile += "MC16a";
    if(MC16c) AMIfile += "MC16c";
    if(MC16d) AMIfile += "MC16d";
    if(systName!="MCSherpa"){
      if(ZJet) AMIfile += "PowhegPythia_ZJets.txt";
      else{ AMIfile += "Pythia.txt";}
    }else{
      if(ZJet) {AMIfile += "Sherpa_ZJets.txt";}
      else{AMIfile += "Sherpa.txt";}
    }
    TH1D* cutflow; // Histogram with the total number of events
    TString cutflowhistoname;
    cutflowhistoname = "MetaData_EventCount";
    std::cout << "Getting TH1D: " << cutflowhistoname << std::endl;
    if(ZJet){
      //Open MetaData Histogram
      cutflow = (TH1D*)ft->Get(cutflowhistoname);
      Denominator = cutflow -> GetBinContent(3);  // sum of weights
      std::cout << "Sum of Weights: " << Denominator << std::endl;
    } else {
      // Open File metadata file
      TString TotalNevents1file = PATH;
      TotalNevents1file += "JZ"+JZSlice+"_metadata/JZ"+JZSlice+"_metadata.root";
      TFile* file1events = new TFile(TotalNevents1file,"read");
      // Getting the number of events for this JZX sample
      cutflow = (TH1D*)file1events->Get(cutflowhistoname);
      Denominator = cutflow->GetBinContent(1);
      std::cout << "Number of events: " << Denominator << std::endl;
    } // !ZJet
    MyAMImap = ReadAMIvalues(AMIfile); 
  } // end ComputeSampleWeight

  //------------------
  // Loop over entries
  //------------------
  std::cout << "Loop over entries" << std::endl;
  for (int entry=0; entry<entries; ++entry) {
    
    //---------------
    // Cleaning values
    //---------------
    
    if(m_debug) std::cout << "Cleaning vectors" << std::endl;

    runNumber = -999;
    eventNumber = -999;
    mcEventNumber = -999;
    mcEventWeight = -999;
    lumiBlock = -999;
    bcid = -999;
    wgt = -999.;
    weight_pileup = -999.;
    NPV = -999.;
    mu = -999.;
    weight = 1.;

    // Cleaning vectors
    RefJet_pt->clear();
    RscanJet_pt->clear();
    RefJet_phi->clear();
    RscanJet_phi->clear();
    RefJet_eta->clear();
    RscanJet_eta->clear();
    RefJet_clean_passLooseBad->clear();
    RefJet_JVT->clear();
    RefJet_constScaleEta->clear();
    RscanJet_E->clear();
    RefJet_E->clear();
    if(MC){
      TruthRefJet_pt->clear();
      TruthRefJet_eta->clear();
      TruthRefJet_phi->clear();
      TruthRefJet_E->clear();
    }
    if(!MC && !ZJet) passedTriggers->clear();

    // READY TO START//
    bool should_skip = false; // Jet Cleaning
    if(m_debug) std::cout << "Event number: " << entry << std::endl;
    if(m_debug) std::cout << "Before GetEntry()" << std::endl;
    tree->GetEntry(entry);
    if(m_debug) std::cout << "After GetEntry()" << std::endl;

    h_cutflow->Fill(0); // Total Number of events
    h_cutflow->GetXaxis()-> SetBinLabel(1,"AllEvent") ;
    // Show status
    if(entry % 1000000 == 0) std::cout << "Entry = " << entry << " of " << entries << std::endl;

    // Filling Weight
    double xs  = 1;
    double eff = 1;
    if(MC){
      if(!ComputeSampleWeight) weight = wgt;
      else{
	std::map<int,std::pair<double,double>>::iterator it;  // <mcChannelNumber,<xs,eff>>
	it = MyAMImap.find( runNumber );
	if( it != MyAMImap.end() ){
	  xs = it->second.first;
	  if (ZJet) xs = xs*1e6; // fb
	  eff = it->second.second; 
          weight = xs*eff*mcEventWeight/Denominator;
	}else{
	  std::cout << "mcChannelNumber:"<< runNumber <<" not found in AMIfile. Exiting!" << std::endl;
          std::exit(EXIT_FAILURE);
	}
      }
    }else{weight = 1;}    // Data

    h_averageMu_noPRW->Fill(mu,weight);
    h_actualMu_noPRW ->Fill(actualMu,weight);
    if(PRW) weight *= weight_pileup;

    if(m_debug){
      std::cout << "mcEventWeight: " << mcEventWeight << std::endl;
      std::cout << "mcEventWeight*SampleWeight: " << xs*eff*mcEventWeight/Denominator << std::endl;
      std::cout << "PileUpWeight: " << weight_pileup << std::endl;
      std::cout << "FinalWeight: " << weight << std::endl;
    }
    // Fill Mu distribution
    h_averageMu->Fill(mu,weight);
    h_actualMu ->Fill(actualMu,weight);

    if(Bootstrap){
      // Generate Poisson-distributed random numbers. Set seed based on run/event #
      if(MC){
        fGenerator->Generate(runNumber, mcEventNumber, runNumber);
      }
      else{
        fGenerator->Generate(runNumber, eventNumber, -1);
      }
    }

    int nRscanJets = RscanJet_pt->size();

    //-------------------------------------------------------     
    // Apply Insitu correction in R-scan jets if requested
    //-------------------------------------------------------     
    if(!MC && applyInsitu){ // in Data only
      if(m_debug) std::cout << "Applying the insitu correction to R-scan jets" << std::endl;
      NewRscanJets.clear();

      // Loop over R-scan jets
      if(m_debug) std::cout << "Loop over R-scan jets to apply insitu correction" << std::endl;
      if(m_debug) std::cout << "nRscanJets: " << nRscanJets << std::endl;
      for(int i=0;i<nRscanJets;++i){
        float detectorEta = RscanJet_constScaleEta->at(i);

	TLorentzVector jet;
	jet.SetPtEtaPhiE( RscanJet_pt->at(i), RscanJet_eta->at(i), RscanJet_phi->at(i), RscanJet_E->at(i) );

	double etaMax = m_insituEtaMax;
	double ptMin  = m_insituPtMin;
	double ptMax  = m_insituPtMax;
	double myPt   = RscanJet_pt->at(i);
	double myEta  = detectorEta;

	if(m_debug) std::cout << "pT before insitu calibration: " << myPt << std::endl;
        //protection against values outside the histogram range, snap back to the lowest/highest bin edge
        if ( myPt <= ptMin ) myPt = ptMin + 1e-6;
        else if ( myPt >= ptMax ) myPt = ptMax - 1e-6;
        if (myEta <= -etaMax) myEta = 1e-6 - etaMax;
        else if (myEta >= etaMax) myEta = etaMax - 1e-6;

        jet = jet*(m_insituCorr->Interpolate(myPt,myEta));

        iJet tempJet;
	tempJet.SetPtEtaPhiE(jet.Pt(),jet.Eta(),jet.Phi(),jet.E());
        tempJet.setDetectorEta(RscanJet_constScaleEta->at(i));
 
        if(m_debug) std::cout << "pT after insitu calibration: " << tempJet.Pt() << std::endl;

        // Save jet into a new vector to be able to sort
        NewRscanJets.push_back(tempJet);   

      } //END: Loop over R-scan jets
   
      // Sort vector of calibrated R-scan jets
      if(m_debug) std::cout << "Sort R-scan jets after applying insitu calibration" << std::endl;
      if(NewRscanJets.size() > 0){
        std::sort(NewRscanJets.begin(), NewRscanJets.end(), SortByPt());
      }

      // Replace uncalibrated with calibrated jets
      if(m_debug) std::cout << "Loop over R-scan jets to replace uncalibrated with insitu calibrated ones" << std::endl;
      for(int i=0;i<nRscanJets;++i){
        if(m_debug) std::cout << "Replacing pT by insitu corrected pT" << std::endl;
        RscanJet_pt->at(i)  = NewRscanJets.at(i).Pt();
        RscanJet_eta->at(i) = NewRscanJets.at(i).Eta();
        RscanJet_phi->at(i) = NewRscanJets.at(i).Phi();
        RscanJet_E->at(i)   = NewRscanJets.at(i).E();
        RscanJet_constScaleEta->at(i)      = NewRscanJets.at(i).getDetectorEta();
      }

    } //END: Insitu correction applied (if requested)

    //-----------------
    // Event Selection
    //-----------------

    int nRefJets = RefJet_pt->size();
    // At least one Ref and one R-scan jet
    if(nRefJets<1 || nRscanJets<1) {continue;} // skip event
    
    if(ZJet){

      if(m_debug) std::cout<< "ZJet debugging: Event Selection due to muons..."<< std::endl;

      // Event Selection due to muons:

      // Skip to the next event if the number of muons is not 2
      if(nMuon!=2){
	if (m_debug) std::cout << "ZJet debugging: #Muons!=2 -> next event" << std::endl;
	continue;				
      }

      mu1.SetPtEtaPhiE( mu_pt -> at(0), mu_eta -> at(0), mu_phi -> at(0), mu_E -> at(0) );
      mu2.SetPtEtaPhiE( mu_pt -> at(1), mu_eta -> at(1), mu_phi -> at(1), mu_E -> at(1) );

      // Skip to the next event if either muon has pT < muon_MinPt
      if( mu1.Pt() < muon_MinPt || mu2.Pt() < muon_MinPt ){
	if (m_debug) std::cout << " ZJet debugging: low muon_pt -> next event" << std::endl;
	continue;
      }
      // Skip to the next event if either muon has rapidity > 2.47
      if( mu1.Rapidity() >= 2.47 || mu2.Rapidity() >= 2.47 ) {
	if (m_debug) std::cout << "ZJet debugging: high rapidity -> next event" << std::endl;
	continue;
      }
      // Skip to the next event for same sign muons
      // (one should be positive and the other should be negative)
      if( ( mu_ch -> at(0) * mu_ch -> at(1) ) > 0 ){
	if (m_debug) std::cout << "ZJet debugging: same charge muons -> next event" << std::endl;
	continue;
      }

      // select Z0	
      Z0 = mu1 + mu2;

      // Skip to the next loop for Z0 with a mass outside the range (80, 116)
      if( Z0.M() < 80 || Z0.M() > 116 ){
        if(m_debug) std::cout << "ZJet debugging: Z mass out of range -> next event" << std::endl;
        continue;
      }
    }	// end ZJet: due to muons

    h_cutflow->Fill(1);// At least one Ref and one Rscan jet
    h_cutflow->GetXaxis()-> SetBinLabel(2,"EventSelec") ;
     
    //----------------------
    // Remove pileup events
    //----------------------
    // (for lower slices this cut prevents the leading jets from being pileup jets)
    if(MC && nRefJets>1){
      double pTavg = ( RefJet_pt->at(0) + RefJet_pt->at(1) )*0.5;
      if( TruthRefJet_pt->size() == 0 || (pTavg/TruthRefJet_pt->at(0)>1.4) ){
        continue; // skip event
      }
      if(Generator=="Powheg" && runNumber==426005 && mcEventNumber==1652845) continue;
      if(Generator=="PowhegHerwig" && runNumber==426101 && mcEventNumber==134075) continue;
      if(Generator=="PowhegHerwig" && runNumber==426101 && mcEventNumber==824127) continue;
      if(Generator=="PowhegHerwig" && runNumber==426101 && mcEventNumber==1648212) continue;
      if(Generator=="PowhegHerwig" && runNumber==426101 && mcEventNumber==1659362) continue;
      if(Generator=="PowhegHerwig" && runNumber==426102 && mcEventNumber==623057) continue;
      if(Generator=="PowhegHerwig" && runNumber==426102 && mcEventNumber==914175) continue;
      if(Generator=="PowhegHerwig" && runNumber==426103 && mcEventNumber==169995) continue;
      if(Generator=="PowhegHerwig" && runNumber==426104 && mcEventNumber==1373420) continue;
      if(Generator=="PowhegHerwig" && runNumber==426104 && mcEventNumber==1412964) continue;
      if(Generator=="PowhegHerwig" && runNumber==426105 && mcEventNumber==832.34600) continue;
    }

    h_cutflow->Fill(2); // Pileup Events
    h_cutflow->GetXaxis()-> SetBinLabel(3,"Pileup Hg") ;

    // Fill pT distributions after pileup events cut
    if(m_debug) std::cout << "Filling pT distributions" << std::endl;
    if(ZJet) h_ZpT ->Fill(Z0.Pt(), weight);
  
    //-----------------
    // Event Cleaning 
    //-----------------
    if(m_debug) std::cout << "Jet Cleaning" << std::endl;
    // Removing events only due to LooseBad
    // If jets is pileup the event is kept
    for(int j=0;j<nRefJets;++j){
      if(RefJet_clean_passLooseBad->at(j)!=1){ // jet not passing LooseBad criteria
	if(RefJet_pt->at(j)>=JVTpTmax || fabs(RefJet_constScaleEta->at(j))>=JVTetamax){ // JVT not applicable
	  should_skip = true; // Skip event
	  break;
	}
	else if(RefJet_JVT->at(j)>=JVTthreshold){ // JVT applicable but is not pileup
	  should_skip = true; 
 	  break;
	}
      }
    }
    if(should_skip){
      Skippedevents_Cleaning++;	    
      continue; // skip event due to a non clean jet
    }

    h_cutflow->Fill(3); // Jet Cleaning
    h_cutflow->GetXaxis()-> SetBinLabel(4,"Jet Cleaning") ;
    
    //---------------------------
    // Selection of Ref Jets
    //---------------------------

    if(m_debug) std::cout << "Select Ref Jets" << std::endl;
    Selected_RefJets.clear();
    
    if(ZJet){ 

      // Check to see that the number of Det Eta values and the 
      // number of calibrated jets are the same for the current event
      if( RefJet_constScaleEta -> size() != RefJet_pt-> size() ) std::cout << "DetEta and calibration jet pT vec size should be same" << std::endl;

      // Loop over the Ref Jets in current event.
      for(int k = 0; k < nRefJets; ++k){
	
	h_pT_RefAll  ->Fill(RefJet_pt->at(k),weight);
	// Select the jets that have |eta| < 3.0 and pT > Ref_pTMin
	if( (RefJet_pt -> at(k) <= Ref_ptMin_iso) || ( fabs( RefJet_constScaleEta->at(k)) > 3. )) continue;

	TLorentzVector jetRef;
	jetRef.SetPtEtaPhiE( RefJet_pt->at(k), RefJet_eta->at(k), RefJet_phi->at(k), RefJet_E->at(k) );

	// Find the Delta R between the current jet and each of the muons
	float dRjetlep1 = jetRef.DeltaR( mu1 );
	float dRjetlep2 = jetRef.DeltaR( mu2 );

	// Check RefJets have Delta R > 0.35 with respect to both muons (no overlap)
	if( fabs( dRjetlep1 ) > 0.35 && fabs( dRjetlep2 ) > 0.35 ) {
	  prop Jet_prop; 
	  Jet_prop.Sel_Jet = jetRef;
	  Jet_prop.Used    = false;
	  Jet_prop.Iso     = true;
	  Jet_prop.JVT     = RefJet_JVT->at(k);
	  Jet_prop.DetectorEta = RefJet_constScaleEta->at(k);
	 
	  Selected_RefJets.push_back(Jet_prop); 
	} // end if dRjetlep 
      } // end k: loop over ref jets.

      if ( Selected_RefJets.size()==0 ){
	if(m_debug) std::cout << "ZJet debugging: no RefJets left, muon overlap OR failed eta range OR failed deltaPhiCut"<< std::endl;
	continue; // to next event
      }	
    } // if ZJet

    if(!ZJet) {
      for(int k=0; k<nRefJets; ++k){
        TLorentzVector jetRef;
        jetRef.SetPtEtaPhiE( RefJet_pt->at(k), RefJet_eta->at(k), RefJet_phi->at(k), RefJet_E->at(k) );
        // Jets with 7Gev or more and |eta|<3.0 are only saved
        if(jetRef.Pt()> Ref_ptMin_iso && fabs(RefJet_constScaleEta->at(k))<3.0){ 
	  prop Jet_prop;
	  Jet_prop.Sel_Jet = jetRef;
	  Jet_prop.Used    = false;
	  Jet_prop.Iso     = true;
	  Jet_prop.JVT     = RefJet_JVT->at(k);
	  Jet_prop.DetectorEta = RefJet_constScaleEta->at(k);
	  Selected_RefJets.push_back(Jet_prop); 
	}
      } // END: Loop over Ref jets
    } // !ZJet
    if(m_debug) std::cout << "Number of Selected Ref jets: "<<Selected_RefJets.size() << std::endl;
     //---------------------------
    // Selection of Rscan Jets
    //---------------------------
    if(m_debug) std::cout << "Select Rscan Jets" << std::endl;
    Selected_RscanJets.clear();
    
    for(int m=0;m < nRscanJets; ++m){
        
        h_pT_RscanAll->Fill(RscanJet_pt->at(m),weight);
        
	TLorentzVector jetRscan;
        jetRscan.SetPtEtaPhiE( RscanJet_pt->at(m), RscanJet_eta->at(m), RscanJet_phi->at(m), RscanJet_E->at(m) );
	// Jet Selection
	if(RscanJet_pt->at(m) <= Rscan_ptMin_iso || fabs(RscanJet_constScaleEta->at(m))>3.0) continue;
	prop Jet_prop;
	Jet_prop.Sel_Jet = jetRscan;
	Jet_prop.Used    = false;  
	Jet_prop.Iso     = true;
	Jet_prop.DetectorEta = RscanJet_constScaleEta->at(m);
	
	Selected_RscanJets.push_back(Jet_prop); 
    } // END: Loop over Rscan jets
    if(m_debug) std::cout << "Number of Selected R-scan jets: "<<Selected_RscanJets.size() << std::endl;
    
    if (Selected_RscanJets.size()<1 || Selected_RefJets.size()<1){continue;} // At least 1 ref and 1 rscan jet
    
  
    //-------------------
    // Isolation
    //-------------------

    // Rscan-Rscan isolation requirement
    if(m_debug) std::cout << "Isolation requirement for R-scan jets" << std::endl;
    double DR_rscanrscan = 0;
    // First loop over R-scan jets
    for (unsigned int m=0 ; m < Selected_RscanJets.size(); ++m){
      double DR_rsrs_min   = 1e10; // Save minimum DR between R-scan jets
      int    minDR_rsindex = m;    // Jet Index of the DR_rsrs_min;
      if(Selected_RscanJets[m].Iso == false) continue; // Already checked and discarded
      // Second loop over R-scan jets
      for (unsigned int h=0; h < Selected_RscanJets.size() ;++h){
	if(h==m) continue;
        // Calculate DR between the two R-scan jets
        DR_rscanrscan = Selected_RscanJets[m].Sel_Jet.DeltaR(Selected_RscanJets[h].Sel_Jet);
	if(m_debugHists){
          for (int i = 0; i < npTavgBins; ++i ){
            if(Selected_RscanJets[m].Sel_Jet.Pt()>pTavgBins[i] && Selected_RscanJets[m].Sel_Jet.Pt()<pTavgBins[i+1]){
              h_DR_rscan_rscan[i]->Fill(DR_rscanrscan,weight);
	    }
          }
	}
        // Find minimum
        if (DR_rscanrscan<DR_rsrs_min){
	  DR_rsrs_min   = DR_rscanrscan;
	  minDR_rsindex = h;
	}
      } //END: Second loop over R-scan jets
      // Fill histograms
      if(m_debugHists){
        if(DR_rsrs_min!=1e10) h_DeltaRRscan_iso->Fill(DR_rsrs_min, weight);
        for (int i = 0; i < npTavgBins; ++i ){
          if(Selected_RscanJets[m].Sel_Jet.Pt()>pTavgBins[i] && Selected_RscanJets[m].Sel_Jet.Pt()<pTavgBins[i+1]){
            h_minDR_rscan_rscan[i]->Fill(DR_rsrs_min,weight);
	  }
        }
      }
      // Check if isolated
      if(!(DR_rsrs_min > ISOfactor * Radius)) { // If NOT isolated
        Selected_RscanJets[m].Iso = false;
        Selected_RscanJets[minDR_rsindex].Iso = false;
      }
    } //END: First loop over R-scan jets
    
    // Ref-Ref isolation requirement
    if(m_debug) std::cout << "Isolation requirement for Ref jets" << std::endl;
    double DR_refref=0;
    // First loop over ref jets
    for (unsigned int mm=0; mm < Selected_RefJets.size(); ++mm){
      double DR_refref_min  = 1e10; // Save minimum DR between R-scan jets
      int    minDR_refindex = mm;   // Jet Index of the DR_refref_min
      if(Selected_RefJets[mm].Iso == false) continue; // Already checked and discarded
      // Second loop over ref jets
      for (unsigned int hh=0; hh < Selected_RefJets.size(); ++hh){
	if(hh==mm) continue;
        // Calculate DR between the two ref jets
        DR_refref = Selected_RefJets[mm].Sel_Jet.DeltaR(Selected_RefJets[hh].Sel_Jet);
	if(m_debugHists){
          for (int i = 0; i < npTavgBins; ++i ){
            if(Selected_RefJets[mm].Sel_Jet.Pt()>pTavgBins[i] && Selected_RefJets[mm].Sel_Jet.Pt()<pTavgBins[i+1]){
              h_DR_ref_ref[i]->Fill(DR_refref,weight);
            }
          }
	}
        // Find minimum
        if (DR_refref<DR_refref_min){
	  DR_refref_min  = DR_refref;
	  minDR_refindex = hh;
        }
      } // END: Second loop over ref jets
      // Fill histograms
      if(m_debugHists){
        if(DR_refref_min!=1e10) h_DeltaRRef_iso->Fill(DR_refref_min, weight);
        for (int i = 0; i < npTavgBins; ++i ){
          if(Selected_RefJets[mm].Sel_Jet.Pt()>pTavgBins[i] && Selected_RefJets[mm].Sel_Jet.Pt()<pTavgBins[i+1]){
            h_minDR_ref_ref[i]->Fill(DR_refref_min,weight);
	  }
	}
      }
      // Check if isolated
      if(!(DR_refref_min > ISOfactor * 0.4)) { // If NOT isolated
        Selected_RefJets[mm].Iso = false;
        Selected_RefJets[minDR_refindex].Iso = false;
      }
    } // END: First loop over ref jets
 
    // Cut on Ref Pt after Isolation Requirement.
    for (unsigned int u=0; u < Selected_RefJets.size(); ++u){
      if(ZJet){ if(Selected_RefJets[u].Sel_Jet.Pt() < Ref_ptMin) Selected_RefJets[u].Used = true;
      } else{
        if(Selected_RefJets[u].Sel_Jet.Pt() < ptmin) Selected_RefJets[u].Used = true;
      }
    }

    //-------------------------------------------------
    // Match, Isolation and jet selection requirement (Ref-Ref & Rscan-Rscan)
    //-------------------------------------------------
    if(m_debug) std::cout << "DR Matching" << std::endl;
    
    Selected_matched_Jets.clear();
    
    double DR;
    double DR_min;
    double detEta_Rscan = 0;
    double detEta_Ref   = 0;
    TLorentzVector jet1, jet2;
    int ref_index   = -1;
    int rscan_index = -1;
    // Loop over R-scan jets
    for (unsigned int m=0; m<Selected_RscanJets.size(); ++m){
      DR_min = 1e10;
      // used?
      if (Selected_RscanJets[m].Used == true) {continue;} 
      // isolated
      if( Selected_RscanJets[m].Iso == false) {continue;}
      // Loop over Ref jets
      for (unsigned int n=0; n<Selected_RefJets.size(); ++n){
        bool keep_jet = false; // pileup jet by default
        //used?
	if (Selected_RefJets[n].Used == true) {continue;}
	//isolation
        if (Selected_RefJets[n].Iso == false) {continue;}
	// Skip pileup jets
	if(Selected_RefJets[n].Sel_Jet.Pt()>=JVTpTmax || fabs(Selected_RefJets[n].DetectorEta)>=JVTetamax){ // JVT not applicable
	  keep_jet = true;
	}
	else if(Selected_RefJets[n].JVT>=JVTthreshold){ // JVT applicable but is not pileup
	  keep_jet = true;
	}
	if(!keep_jet) continue;
        
	// DR Matching
	DR = Selected_RscanJets[m].Sel_Jet.DeltaR(Selected_RefJets[n].Sel_Jet);
        // Find minimum DR
 	if(DR<DR_min){
	  DR_min = DR;
	  jet1 = Selected_RefJets[n].Sel_Jet;
	  detEta_Ref = Selected_RefJets[n].DetectorEta;
	  ref_index = n;
	  jet2 = Selected_RscanJets[m].Sel_Jet;
	  detEta_Rscan = Selected_RscanJets[n].DetectorEta;
	  rscan_index = m;
  	}
      } // END: Loop over Ref jets

      // Fill histogram
      if(m_debugHists) h_DeltaR->Fill(DR_min, weight);

      // Match requirement
      if( DR_min < DRmatch){
	prop_sel Jet_prop_sel;
	Jet_prop_sel.Sel_JetRscan  = jet2;
	Jet_prop_sel.Sel_JetRef    = jet1;
	Jet_prop_sel.dR            = DR_min;
	Jet_prop_sel.detEta_Ref    = detEta_Ref;
	Jet_prop_sel.detEta_Rscan  = detEta_Rscan;
         
	Selected_matched_Jets.push_back(Jet_prop_sel);
	
	Selected_RscanJets[rscan_index].Used = true;
	Selected_RefJets[ref_index].Used     = true;
      }
    
    } // END: Loop over R-scan jets

    if (Selected_matched_Jets.size()>0){ // At least one R-scan matched jet
      h_cutflow->Fill(4); // Jet Cleaning
      h_cutflow->GetXaxis()-> SetBinLabel(5,"Matching") ; 
    } 
    
    //------------------------------------------------
    // Fill Histograms
    //------------------------------------------------

  
    if( MC || ZJet ){ //MC only
      if(m_debug) std::cout << "Filling Relative Response Histograms (MC only)" << std::endl;
      // Loop over Matched R-scan jets
      for (unsigned int l=0; l<Selected_matched_Jets.size(); ++l){

        double RefJet_pT    = Selected_matched_Jets[l].Sel_JetRef.Pt();
        double RscanJet_pT  = Selected_matched_Jets[l].Sel_JetRscan.Pt();
        double RscanJet_Eta = Selected_matched_Jets[l].detEta_Rscan;     // Detector Eta

	// Fill Histograms
        h_pT_Ref  ->Fill(RefJet_pT,weight);
        h_pT_Rscan->Fill(RscanJet_pT,weight);

        // Compute relative response
        double response = RscanJet_pT / RefJet_pT;
        
        // Fill TH2D relative response plots vs R-scan pT (depending on detector eta)
        for (unsigned int i=0; i<nEtaBins; i++) {
          if(RscanJet_Eta >= etaBins[i] && RscanJet_Eta < etaBins[i+1]){
  	    // Fill relative response distributions
	    h_Response_pTRef_Eta[i]->Fill(RefJet_pT,response,weight); 
	    //Fill bootstrap
            if(Bootstrap){fBootstrap_TH2[i]->Fill(RefJet_pT,response,weight);}
            // Fill Mapping
	    if( !applyInsitu && systName == "") h_PtMap_Eta[i] -> Fill(RefJet_pT,RscanJet_pT,weight);
          }
        }

      } // END: Loop over Matched R-scan jets
    } // END: Filling MC histograms
 
    if(!MC && !ZJet){// Data only
      if(m_debug) std::cout << "Filling Relative Response Histograms (Data only)" << std::endl;
      // Loop over Matched R-scan jets
      for (unsigned int l=0; l<Selected_matched_Jets.size(); ++l){
        double RefJet_pT    = Selected_matched_Jets[l].Sel_JetRef.Pt();
        double RscanJet_pT  = Selected_matched_Jets[l].Sel_JetRscan.Pt();
        double RscanJet_Eta = Selected_matched_Jets[l].detEta_Rscan; // Detector Eta
       
        // Looking to trigger
        bool passed_trig = false;

        // Select corresponding trigger
        std::string trigger = "";
        for(unsigned int m=firstTrig;m<Triggers.size();m++){
	  double TriggerObjectpT = RefJet_pT;
          if(TriggerObjectpT>=minPtTriggers[Triggers.at(m)] && TriggerObjectpT<maxPtTriggers[Triggers.at(m)]) trigger = Triggers.at(m);
        }
        if(m_debug) std::cout << "Using trigger: " << trigger << std::endl;
      
        // See if trigger was fired
        for(unsigned int lll=0;lll<passedTriggers->size();lll++){// Loop over passed triggers
          if(passedTriggers->at(lll)==trigger) passed_trig = true;
        }// end passedTriggers
      
        if (passed_trig){

          if(m_debug) std::cout << "Trigger was fired" << std::endl;

	  int trig = TriggertoInt[trigger];
          if(m_debug) std::cout << "Calculating weight" << std::endl;
          double dataWGT = wgtPrescale.at(trigUnprescaled).at(trigUnprescaled)/wgtPrescale.at(trig).at(trig);
 
          // Fill Histograms
          h_pT_Ref  ->Fill(RefJet_pT,dataWGT);
          h_pT_Rscan->Fill(RscanJet_pT,dataWGT);

          // Compute relative response
          double response = RscanJet_pT / RefJet_pT;

          // Fill TH2D relative response plots vs R-scan pT (depending on detector eta)
          for (int i=0; i<nEtaBins; i++) {
            if(RscanJet_Eta >= etaBins[i] && RscanJet_Eta < etaBins[i+1]){
              // Fill Relative response distributions
      	      h_Response_pTRef_Eta[i]->Fill(RefJet_pT,response);
              //Fill Bootstrap
              if(Bootstrap){fBootstrap_TH2[i]->Fill(RefJet_pT,response);}
              // Fill Mapping
	      if( !applyInsitu && systName == "") h_PtMap_Eta[i] -> Fill(RefJet_pT,RscanJet_pT);
            }
          }
        } // END: Only if trigger was fired
      } // END: Loop over Matched R-scan jets
    } // END: Filling Data histograms

    if(m_debug) std::cout << "Entry analyzed" << std::endl;
  } // End: Loop over entries

  //------------------------------------
  // Saving Histogramas into a ROOT file
  //------------------------------------
  
  std::cout << "Saving hists into a ROOT file..." << std::endl;

  // Output file name
  TString outputfile = m_path;
  if(MC) outputfile += "ROOTfiles/DirectMatching/TreetoHists_Outputs/MC/";
  else{ outputfile += "ROOTfiles/DirectMatching/TreetoHists_Outputs/Data/";}
  outputfile += Version; outputfile += "/"; outputfile += JetCollection;
  if(MC) outputfile += MCoutputFile;
  else{ outputfile += DataoutputFile;}

  // Opening output file
  tout = new TFile(outputfile,"recreate");
  std::cout << "output file = " << outputfile << std::endl;
  tout->cd();

  // Writing histograms
  h_pT_Ref->Write();
  h_pT_Rscan->Write();
  if(MC || ZJet) {
    h_pT_RefAll->Write();
    h_pT_RscanAll->Write();
  }
  
  if(ZJet) h_ZpT->Write();

  for (int i=0; i<nEtaBins; i++){
      h_Response_pTRef_Eta[i]->Write();
      if(Bootstrap){fBootstrap_TH2[i]->Write();}
      if( !applyInsitu && systName == "") h_PtMap_Eta[i] -> Write();
  }
 
  if(m_debugHists){
    h_DeltaR->Write();
    h_DeltaRRef_iso ->Write();
    h_DeltaRRscan_iso ->Write();
    for (int i = 0; i < npTavgBins; ++i ){
      h_minDR_rscan_rscan[i]->Write();
      h_DR_rscan_rscan[i]->Write();
      h_minDR_ref_ref[i]->Write();
      h_DR_ref_ref[i]->Write();
    }
  }
 
  h_cutflow->Write();
  h_averageMu->Write();
  h_averageMu_noPRW->Write();
  h_actualMu ->Write();
  h_actualMu_noPRW ->Write();
  
  if(!MC && !ZJet){
    for(unsigned int m=firstTrig;m<Triggers.size();m++){
      h_pTavg_Triggers[Triggers.at(m)]->Write();
    }
  }
  
  tout->Close();
  
  //-------------------------
  // LOG file
  //-------------------------

  std::string logOutName = m_path;
  if(MC) logOutName += "ROOTfiles/DirectMatching/TreetoHists_Outputs/MC/";
  else{ logOutName += "ROOTfiles/DirectMatching/TreetoHists_Outputs/Data/";}
  logOutName += Version;
  logOutName += "/";
  logOutName += JetCollection;
  logOutName += "/";
  logOutName += "log";
  if(MC){
    logOutName += "_";
    logOutName += JZSlice;
  }
  else{
    if(treePart!=""){
      logOutName += "_";
      logOutName += treePart;
    }
    if(!ZJet && Data == "2016"){
      // extract period
      int index_period = PATH_user.Index("period")+6;
      TString period = PATH_user[index_period];
      logOutName += "_period";
      logOutName += period;
      // extract number of file
      int index_number = InputFile_user.Index(".tree")-6;
      TString numberFile = InputFile_user(index_number,6);
      logOutName += "_file";
      logOutName += numberFile;
    }
  }
  
  if(systName!=""){
    logOutName += "_";
    logOutName += systName;
  }
  if(applyInsitu) logOutName += "_Insitu";
  logOutName += ".txt";

  std::cout << "Logfile: " << logOutName << std::endl;
  
  std::cout << ">>>>>> Done! <<<<<<" << std::endl;

  return 0;

}




/*
 *
 *
 * Personal Functions
 *
 *
 *
 * */


void EnableBranches(){

  tree->SetBranchStatus("*",0); //disable all branches

  if(MC){
    tree->SetBranchStatus("mcChannelNumber",1);
    tree->SetBranchStatus("mcEventNumber",1);
    tree->SetBranchStatus("mcEventWeight",1);
  } else { 
    tree->SetBranchStatus("runNumber",1);
    tree->SetBranchStatus("eventNumber",1);
    //tree->SetBranchStatus("lumiBlock",1);
    //tree->SetBranchStatus("bcid",1);
    if(!ZJet)tree->SetBranchStatus("passedTriggers",1);
  }
  tree->SetBranchStatus("NPV",1);
  tree->SetBranchStatus("averageInteractionsPerCrossing",1);
  tree->SetBranchStatus("actualInteractionsPerCrossing",1);

  if(MC){
    if(!ComputeSampleWeight) tree->SetBranchStatus("weight",1);
    if(!ZJet) {tree->SetBranchStatus("weight_pileup",1);}
    else {tree->SetBranchStatus("PileUpWgh",1);}
  }

  tree->SetBranchStatus("RefJet_pt",1);
  tree->SetBranchStatus("RefJet_eta",1);
  tree->SetBranchStatus("RefJet_phi",1);
  tree->SetBranchStatus("RefJet_E",1);
  tree->SetBranchStatus("RefJet_clean_passLooseBad",1);
  tree->SetBranchStatus("RefJet_Jvt",1);
  if(R21 && !ZJet){tree->SetBranchStatus("RefJet_detectorEta",1);}
  else{tree->SetBranchStatus("RefJet_constScaleEta",1);}

  if(MC){
    tree->SetBranchStatus("TruthRefJet_pt",1);
    tree->SetBranchStatus("TruthRefJet_eta",1);
    tree->SetBranchStatus("TruthRefJet_phi",1);
    tree->SetBranchStatus("TruthRefJet_E",1);
  }
  
  if(ZJet){
    tree->SetBranchStatus("n_muon",1);
    tree->SetBranchStatus("mu_pt",1);
    tree->SetBranchStatus("mu_eta",1);
    tree->SetBranchStatus("mu_phi",1);
    tree->SetBranchStatus("mu_E",1);
    tree->SetBranchStatus("mu_ch",1);
    tree->SetBranchStatus("mu_RecoWg",1);
    tree->SetBranchStatus("mu_RecoWg",1);
    tree->SetBranchStatus("TriggerEff",1);
  }

  if(JetCollection=="6LC"){
    if(!ZJet){ 
      tree->SetBranchStatus("6LCJet_pt",1);
      tree->SetBranchStatus("6LCJet_eta",1);
      tree->SetBranchStatus("6LCJet_phi",1);
      tree->SetBranchStatus("6LCJet_E",1);
      if(R21){tree->SetBranchStatus("6LCJet_detectorEta",1);}
      else{tree->SetBranchStatus("6LCJet_constScaleEta",1);}
    } else { //ZJet
      tree->SetBranchStatus("LC6Jet_pt",1);
      tree->SetBranchStatus("LC6Jet_eta",1);
      tree->SetBranchStatus("LC6Jet_phi",1);
      tree->SetBranchStatus("LC6Jet_E",1);
      tree->SetBranchStatus("LC6Jet_constScaleEta",1);
    }
  }
  if(JetCollection=="2LC"){
    if(!ZJet){
      tree->SetBranchStatus("2LCJet_pt",1);
      tree->SetBranchStatus("2LCJet_eta",1);
      tree->SetBranchStatus("2LCJet_phi",1);
      tree->SetBranchStatus("2LCJet_E",1);
      if(R21){tree->SetBranchStatus("2LCJet_detectorEta",1);}
      else{tree->SetBranchStatus("2LCJet_constScaleEta",1);}
    }else{ //ZJet
      tree->SetBranchStatus("LC2Jet_pt",1);
      tree->SetBranchStatus("LC2Jet_eta",1);
      tree->SetBranchStatus("LC2Jet_phi",1);
      tree->SetBranchStatus("LC2Jet_E",1);
      tree->SetBranchStatus("LC2Jet_constScaleEta",1);
    }
  }

  if(JetCollection=="8LC"){
    tree->SetBranchStatus("8LCJet_pt",1);
    tree->SetBranchStatus("8LCJet_eta",1);
    tree->SetBranchStatus("8LCJet_phi",1);
    tree->SetBranchStatus("8LCJet_E",1);
    if(R21){tree->SetBranchStatus("8LCJet_detectorEta",1);}
    else{tree->SetBranchStatus("8LCJet_constScaleEta",1);}
  }

}//END: EnableBranches()

void SetBranchAddress(){
  // Event
  if(MC){
    tree->SetBranchAddress("mcChannelNumber",&runNumber);
    tree->SetBranchAddress("mcEventNumber",&mcEventNumber);
    tree->SetBranchAddress("mcEventWeight",&mcEventWeight);
  } else {
    tree->SetBranchAddress("runNumber",&runNumber);
    tree->SetBranchAddress("eventNumber",&eventNumber);
    if (!ZJet) tree->SetBranchAddress("passedTriggers",&passedTriggers);
  }
  tree->SetBranchAddress("NPV",&NPV);
  tree->SetBranchAddress("averageInteractionsPerCrossing",&mu);
  tree->SetBranchAddress("actualInteractionsPerCrossing",&actualMu);

  if (ZJet){ // Muons 
    tree->SetBranchAddress("n_muon",&nMuon);
    tree->SetBranchAddress("mu_pt",&mu_pt);
    tree->SetBranchAddress("mu_eta",&mu_eta);
    tree->SetBranchAddress("mu_phi",&mu_phi);
    tree->SetBranchAddress("mu_E",&mu_E);
    tree->SetBranchAddress("mu_ch",&mu_ch);
    tree->SetBranchAddress("mu_RecoWg",&mu_RecoWg);
    tree->SetBranchAddress("mu_RecoWg",&mu_IsoWg);
    tree->SetBranchAddress("TriggerEff",&TriggerEff);
  }

  // Reco Jets  
  if(MC){
    if(!ComputeSampleWeight) tree->SetBranchAddress("weight",&wgt);
    if(!ZJet) {tree->SetBranchAddress("weight_pileup",&weight_pileup);
    }else{ tree->SetBranchAddress("PileUpWgh",&weight_pileup); }
  }

  tree->SetBranchAddress("RefJet_pt",&RefJet_pt);
  tree->SetBranchAddress("RefJet_eta",&RefJet_eta);
  tree->SetBranchAddress("RefJet_E",&RefJet_E);
  tree->SetBranchAddress("RefJet_phi",&RefJet_phi);
  tree->SetBranchAddress("RefJet_clean_passLooseBad",&RefJet_clean_passLooseBad);
  tree->SetBranchAddress("RefJet_Jvt",&RefJet_JVT);
  if(R21 && !ZJet){tree->SetBranchAddress("RefJet_detectorEta",&RefJet_constScaleEta);}
  else{tree->SetBranchAddress("RefJet_constScaleEta",&RefJet_constScaleEta);}
 
  if(MC){
    tree->SetBranchAddress("TruthRefJet_pt",&TruthRefJet_pt);
    tree->SetBranchAddress("TruthRefJet_eta",&TruthRefJet_eta);
    tree->SetBranchAddress("TruthRefJet_phi",&TruthRefJet_phi);
    tree->SetBranchAddress("TruthRefJet_E",&TruthRefJet_E);
  }

  if(JetCollection=="6LC"){
    if(!ZJet){
      tree->SetBranchAddress("6LCJet_pt",&RscanJet_pt);
      tree->SetBranchAddress("6LCJet_eta",&RscanJet_eta);
      tree->SetBranchAddress("6LCJet_E",&RscanJet_E);
      tree->SetBranchAddress("6LCJet_phi",&RscanJet_phi);
      if(R21){tree->SetBranchAddress("6LCJet_detectorEta",&RscanJet_constScaleEta);}
      else{tree->SetBranchAddress("6LCJet_constScaleEta",&RscanJet_constScaleEta);}
    } else {  //ZJet
      tree->SetBranchAddress("LC6Jet_pt",&RscanJet_pt);
      tree->SetBranchAddress("LC6Jet_eta",&RscanJet_eta);
      tree->SetBranchAddress("LC6Jet_E",&RscanJet_E);
      tree->SetBranchAddress("LC6Jet_phi",&RscanJet_phi);
      tree->SetBranchAddress("LC6Jet_constScaleEta",&RscanJet_constScaleEta);
    }
  }

  if(JetCollection=="2LC"){
    if(!ZJet){
      tree->SetBranchAddress("2LCJet_pt",&RscanJet_pt);
      tree->SetBranchAddress("2LCJet_eta",&RscanJet_eta);
      tree->SetBranchAddress("2LCJet_E",&RscanJet_E);
      tree->SetBranchAddress("2LCJet_phi",&RscanJet_phi);
      if(R21){tree->SetBranchAddress("2LCJet_detectorEta",&RscanJet_constScaleEta);}
      else{tree->SetBranchAddress("2LCJet_constScaleEta",&RscanJet_constScaleEta);}
    } else { // ZJet
      tree->SetBranchAddress("LC2Jet_pt",&RscanJet_pt);
      tree->SetBranchAddress("LC2Jet_eta",&RscanJet_eta);
      tree->SetBranchAddress("LC2Jet_E",&RscanJet_E);
      tree->SetBranchAddress("LC2Jet_phi",&RscanJet_phi);
      tree->SetBranchAddress("LC2Jet_constScaleEta",&RscanJet_constScaleEta);
 
    }
  }

  if(JetCollection=="8LC"){
    tree->SetBranchAddress("8LCJet_pt",&RscanJet_pt);
    tree->SetBranchAddress("8LCJet_eta",&RscanJet_eta);
    tree->SetBranchAddress("8LCJet_E",&RscanJet_E);
    tree->SetBranchAddress("8LCJet_phi",&RscanJet_phi);
    if(R21){tree->SetBranchAddress("8LCJet_detectorEta",&RscanJet_constScaleEta);}
    else{tree->SetBranchAddress("8LCJet_constScaleEta",&RscanJet_constScaleEta);}
  }
}//END: SetBranchAddress()

void BookHistograms(){
  // pT_Avg distributions by trigger for Turn On Curves (xSection method)
  TString BaseName_pTavg = "h_pTavg_";
  TString TempName_pTavg = "";
  for(unsigned int m=firstTrig;m<Triggers.size();m++){
    TempName_pTavg = BaseName_pTavg + Triggers.at(m);
    h_pTavg_Triggers[Triggers.at(m)] = new TH1D(TempName_pTavg.Data(),"",250,0,2000);
    h_pTavg_Triggers[Triggers.at(m)]->Sumw2();
  }

  // jet pT Distributions

  // before all jet selections
  h_pT_RefAll = new TH1D("j_pT_RefAll","",npTavgBins,pTavgBins);
  h_pT_RefAll->Sumw2();
  h_pT_RscanAll = new TH1D("j_pT_RscanAll","",npTavgBins,pTavgBins);
  h_pT_RscanAll->Sumw2();

  // after all selections
  h_pT_Ref = new TH1D("j_pT_Ref","",npTavgBins,pTavgBins);
  h_pT_Ref->Sumw2();
  h_pT_Rscan = new TH1D("j_pT_Rscan","",npTavgBins,pTavgBins);
  h_pT_Rscan->Sumw2();

  // after pileup events cut
  if(ZJet) {
    h_ZpT = new TH1D("ZpT","",npTavgBins,pTavgBins);
    h_ZpT -> Sumw2();
  }

  if(Bootstrap){fGenerator = new BootstrapGenerator("Generator", "Generator", 0);}

  int nAsymmBins = 480;
  const int nAsymmBinsArray = 481;
  double AsymmBins[nAsymmBinsArray];
  for(int i=0;i<=nAsymmBins;++i) {
    AsymmBins[i] = -3.0 + i*0.0125;
  }
  
  TString name_tmp;
  TString name_tmp1;
  TString name_tmp2;
  for (int i = 0; i < nEtaBins; i++ ) {
    name_tmp = "Response_vs_pTRef_Eta_";
    name_tmp += i;
    h_Response_pTRef_Eta[i] = new TH2F(name_tmp.Data(),"",npTavgBins,pTavgBins,nAsymmBins,AsymmBins);
    h_Response_pTRef_Eta[i]->Sumw2();
    
    // Mapping : will be used to translate calibration, derived in RefPt, back to ProbePT.
    name_tmp1 = "hMappingProbePT";
    name_tmp1 += i;
    double maxpT = 1000.;
    int nBins = 4000.;
    if(!ZJet){
      maxpT = 3000;
      nBins = 3000;
    }
    h_PtMap_Eta[i] = new TH2D(name_tmp1.Data(),name_tmp1.Data(),npTavgBins,pTavgBins,nBins,0.,maxpT); // enough for Z+Jet Studies.
    h_PtMap_Eta[i] -> GetXaxis()->SetTitle("Ref Jet pT [GeV]");
    h_PtMap_Eta[i] -> GetYaxis()->SetTitle("Rscan Jet pT [GeV]"); 
    h_PtMap_Eta[i] -> Sumw2();
    
    // bootstrap histos
    if(Bootstrap){
      name_tmp2 = "Response_vs_pTRef_Eta_";
      name_tmp2 += i;
      name_tmp2 += "_replica";
      fBootstrap_TH2[i] = new TH2DBootstrap(name_tmp2.Data(), "", npTavgBins,pTavgBins,nAsymmBins,AsymmBins, nreplica, fGenerator);
      fBootstrap_TH2[i]->Sumw2();
    }
  }

  // DR distributions
  
  if(m_debugHists){
  
    // minDR(ref,rscan) distribution (full pT range)
    h_DeltaR = new TH1D("DeltaR","",1000,0,10);
    h_DeltaR->Sumw2();
  
    // minDR(ref,ref) distribution (full pT range)
    h_DeltaRRef_iso = new TH1D("DeltaR_ref_iso","",1000,0,10);
    h_DeltaRRef_iso->Sumw2();
  
    // minDR(rscan,rscan) distribution (full pT range)
    h_DeltaRRscan_iso = new TH1D("DeltaR_rscan_iso","",1000,0,10);
    h_DeltaRRscan_iso->Sumw2();

    // minDR(rscan,rscan) distribution for each pTrscan bin
    // minDR(ref,ref) distribution for each pTref bin
    for (int i = 0; i < npTavgBins; ++i ) {
      name_tmp = "minDR_rscan_rscan_pT_";
      name_tmp += i;
      h_minDR_rscan_rscan[i] = new TH1D(name_tmp.Data(),"",1000,0,10);
      h_minDR_rscan_rscan[i]->Sumw2();
      name_tmp = "minDR_ref_ref_pT_";
      name_tmp += i;
      h_minDR_ref_ref[i] = new TH1D(name_tmp.Data(),"",1000,0,10);
      h_minDR_ref_ref[i]->Sumw2();
    }

    // DR(rscan,rscan) distribution for each pTrscan bin
    // DR(ref,ref) distribution for each pTef bin
   
    for (int i = 0; i < npTavgBins; ++i ) {
      name_tmp = "DR_rscan_rscan_pT_";
      name_tmp += i;
      h_DR_rscan_rscan[i] = new TH1D(name_tmp.Data(),"",1000,0,10);
      h_DR_rscan_rscan[i]->Sumw2();
      name_tmp = "DR_ref_ref_pT_";
      name_tmp += i;
      h_DR_ref_ref[i] = new TH1D(name_tmp.Data(),"",1000,0,10);
      h_DR_ref_ref[i]->Sumw2();
    }

  } // end m_debug hists

  h_cutflow = new TH1D("Cutflow","",11,-0.5,10.5);
  h_cutflow->Sumw2();

  h_averageMu       = new TH1D("AverageMu","",80,0,80);
  h_averageMu_noPRW = new TH1D("AverageMu_noPRW","",80,0,80);
  h_actualMu        = new TH1D("ActualMu","",100,0,100);
  h_actualMu_noPRW  = new TH1D("ActualMu_noPRW","",100,0,100);

}//END: BookHistograms()

std::map<int,std::pair<double,double>> ReadAMIvalues(TString AMIfile){

  std::map<int,std::pair<double,double>> MyMap;
  std::pair<double, double> amiInfo;
  // Open File with AMI values
  std::vector<double> runNumbers;
  std::vector<double> xss;
  std::vector<double> effs;
  
  // Input File
  std::ifstream in;
  std::cout << "Reading AMI values from: " << AMIfile << std::endl;
  in.open(AMIfile,std::ifstream::in);
  double t1, t2;
  int t0;
  // Reading values
  while (in >> t0 >> t1 >> t2) {
    MyMap[t0]=std::make_pair(t1,t2);
  }
  in.close();
  return MyMap;
}

