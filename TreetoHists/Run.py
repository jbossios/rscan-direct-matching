#!/usr/bin/python
import os, sys
from ROOT import *
param=sys.argv

list=[
#  "All", # For Data and MC16 Powheg+Pythia (ZJets)
  "0", # not available for MC16a Sherpa
  "1", # For Sherpa Z+Jets use Slices 0-13
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
  "13",
  ]

#coll = "2LC";
coll = "6LC";
#coll = "8LC";

MC         = False
PRW        = True # Only meaningful for MC R21
ZJetStudy  = True
Bootstrap  = False
Debug      = False # More verbosity and run over only 10 events
DebugHists = False # Additional histograms (DR, minDR, etc)

# The sample is decided with the variable version. Examples:
#version = "MC16a" # To compare with 2015+2016 data
#version = "MC16c" # To compare with 2017 data
#version = "MC16d" # To compare with 2017 data (RECOMMENDED FOR 2017data)
version = "R21_2015data"
#version = "R21_2016data"
#version = "2017data"
# If you want to produce the MCSherpa systematic uncertainty, use the same version as the rest of the systs

systs=[
    "Nominal",
#    "deltaRUp"   ,
#    "deltaRDown",
#    "isolationUp",
#    "isolationDown",
#    "JVTDown",
#    "JVTUp",
#    "MCsherpa", # Run this only for MC
]

insituFile = "" # No insitu calibration will be applied
# Examples
#insituFile = "2015data_vs_MC15cPythia_MC15bMuProfile/InsituCalibration_Rscan_2LC.root"
#insituFile = "2015data_vs_MC15cPythia_MC15bMuProfile/InsituCalibration_Rscan_6LC.root"
#insituFile = "2015data_vs_MC15cPythia_MC15bMuProfile/InsituCalibration_Rscan_8LC.root"

R21_Data2015 = [
#  "/eos/atlas/user/j/jbossios/Rscan/R21/data15/user.jbossios.06_04_2018_data15_GSCbugFix_v1.periodD._tree.root/",
#  "/eos/atlas/user/j/jbossios/Rscan/R21/data15/user.jbossios.08_03_2018_data15_GSCbugFix_v1.periodE._tree.root/",
#  "/eos/atlas/user/j/jbossios/Rscan/R21/data15/user.jbossios.08_03_2018_data15_GSCbugFix_v1.periodF._tree.root/",
#  "/eos/atlas/user/j/jbossios/Rscan/R21/data15/user.jbossios.08_03_2018_data15_GSCbugFix_v1.periodG._tree.root/",
#  "/eos/atlas/user/j/jbossios/Rscan/R21/data15/user.jbossios.08_03_2018_data15_GSCbugFix_v1.periodH._tree.root/",
#  "/eos/atlas/user/j/jbossios/Rscan/R21/data15/user.jbossios.08_03_2018_data15_GSCbugFix_v1.periodJ._tree.root/",
]
R21_Data2016 = [
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v1.periodA._tree.root/",
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v1.periodB._tree.root/",
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v1.periodC._tree.root/",
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v2.periodD._tree.root/",
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v1.periodE._tree.root/",
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v1.periodF._tree.root/",
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v1.periodG._tree.root/",
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v1.periodI._tree.root/",
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v1.periodK._tree.root/",
#  "/eos/user/g/gotero/jona/Rscan/R21/data16/user.jbossios.08_03_2018_data16_GSCbugFix_v1.periodL._tree.root/",
]
Data2017 = [
		
]

####################################################################################
# DO NOT MODIFY
####################################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

# For 20.7 data2015 only
Parts = [
# 2015data is divided into 4 parts
#    "1",
#    "2",
#    "3",
#    "4",
]

# R20.7
Data2016 = [
#  "/eos/user/m/mdaneri/Rscan/Trees/Data/20_7_2016data/user.jbossios.07_07_2017_2016data.periodA._tree.root/",
#  "/eos/user/j/jbossios/Rscan/Trees/Data/20_7_2016data/user.jbossios.15_06_2017_2016data.periodB._tree.root/",
#  "/eos/user/j/jbossios/Rscan/Trees/Data/20_7_2016data/user.jbossios.15_06_2017_2016data.periodC._tree.root/",
#  "/eos/user/j/jbossios/Rscan/Trees/Data/20_7_2016data/user.jbossios.15_06_2017_2016data.periodD._tree.root/",
#  "/eos/user/j/jbossios/Rscan/Trees/Data/20_7_2016data/user.jbossios.15_06_2017_2016data.periodE._tree.root/",
#  "/eos/user/j/jbossios/Rscan/Trees/Data/20_7_2016data/user.jbossios.15_06_2017_2016data.periodF._tree.root/",
#  "/eos/user/j/jbossios/Rscan/Trees/Data/20_7_2016data/user.jbossios.15_06_2017_2016data.periodG._tree.root/",
#  "/eos/user/j/jbossios/Rscan/Trees/Data/20_7_2016data/user.jbossios.15_06_2017_2016data.periodI._tree.root/",
#  "/eos/user/m/mdaneri/Rscan//Trees/Data/20_7_2016data/user.jbossios.15_06_2017_2016data.periodK._tree.root/",
#  "/eos/user/m/mdaneri/Rscan//Trees/Data/20_7_2016data/user.jbossios.15_06_2017_2016data.periodL._tree.root/",
]

ComputeSampleWeights = True
EOS = True
#EOS = False # Only for UBA people

command = ""

# Works for R20 and R21
if MC:
  for syst in systs:
    for slice in list:
      command += "TreetoHists_match "
      command += "--JZSlice="
      command += slice
      command += " --version="
      command += version
      command += " --jetColl="
      command += coll
      command += " --isMC=TRUE"
      command += " --path="
      command += PATH
      if ZJetStudy:
	command += " --ZJetStudy=TRUE"
      if ComputeSampleWeights:
        command += " --computeSampleWeights=TRUE"
      if not EOS:
        command += " --EOS=FALSE"
      if syst != "Nominal":
        command += " --syst="
	command += syst
      if Debug:
        command += " --debug=TRUE"
      if DebugHists:
        command += " --debugHists=TRUE"
      if PRW:
        command += " --PRW=TRUE"  
      if Bootstrap:
        command += " --Bootstrap=TRUE"
      command += " && "

# R20.7 2015data only
if not MC and version is "2015data":
  for syst in systs:
    for i in Parts:
      command += "TreetoHists_match "
      command += "--jetColl="
      command += coll
      command += " --version="
      command += version
      command += " --isMC=FALSE"
      if not EOS:
        command += " --EOS=FALSE"	      
      if syst != "Nominal":
        command += " --syst="
	command += syst
      if insituFile is not "":
        command += " --insituFile=" 
	command += insituFile
      if Bootstrap:
        command += " --Bootstrap=TRUE"
      if Debug:
        command += " --debug=TRUE"
      if DebugHists:
        command += " --debugHists=TRUE"
      command += " --part="
      command += i
      command += " && "

# R20.7 2016data only
if not MC and version == "2016data":
  for syst in systs:
    for path in Data2016:
      for File in os.listdir(path):
        # Check if root file contains a tree           
        iFile = TFile.Open(path+File,"read")
        if not iFile.Get("nominal"):
          continue
        # Run if exist the tree  
        command += "TreetoHists_match "
        command += "--jetColl="
        command += coll
        command += " --version="
        command += version
        command += " --isMC=FALSE"
        if not EOS:
          command += " --EOS=FALSE"
        if syst != "Nominal":
          command += " --syst="
          command += syst
        if insituFile is not "":
          command += " --insituFile="
          command += insituFile
        if Bootstrap:
          command += " --Bootstrap=TRUE"
        if Debug:
          command += " --debug=TRUE"
        if DebugHists:
          command += " --debugHists=TRUE"
        command += " --pathUSER="
        command += path
        command += " --inputFileUSER="
        command += File
        command += " && "

TDirectoryFileName = "Tree"
TTreeName = "nominal"

# R21 data only
if not MC and version != "2016data" and version != "2015data" and not ZJetStudy:
  for syst in systs:
    List = []
    if version == "R21_2015data":
      List = R21_Data2015
    if version == "R21_2016data":
      List = R21_Data2016
    if version == "2017data":
      List = Data2017
    for path in List:
      for File in os.listdir(path):
        # Check if root file contains a tree
        iFile = TFile.Open(path+File,"read")
	tree = TTree()
	TDir = iFile.GetDirectory(TDirectoryFileName)
	tree = TDir.Get(TTreeName)
        if not tree:
          print "No TTree found in "+path+File+", file skipped"
          continue
        # Run if exist the tree
        command += "TreetoHists_match "
        command += "--jetColl="
        command += coll
        command += " --version="
        command += version
        command += " --isMC=FALSE"
        command += " --path="
        command += PATH
        if not EOS:
          command += " --EOS=FALSE"
        if syst != "Nominal":
          command += " --syst="
          command += syst
        if insituFile is not "":
          command += " --insituFile="
          command += insituFile
        if Bootstrap:
          command += " --Bootstrap=TRUE"
        if Debug:
          command += " --debug=TRUE"
        if DebugHists:
          command += " --debugHists=TRUE"
        command += " --pathUSER="
        command += path
        command += " --inputFileUSER="
        command += File
        command += " && "

# R21 data only
if not MC and ZJetStudy:
  for syst in systs:
    command += "TreetoHists_match "
    command += "--jetColl="
    command += coll
    command += " --version="
    command += version
    command += " --isMC=FALSE"
    command += " --path="
    command += PATH
    if not EOS:
      command += " --EOS=FALSE"
    if syst != "Nominal":
      command += " --syst="
      command += syst
    if ZJetStudy:
      command += " --ZJetStudy=TRUE"
    if insituFile is not "":
      command += " --insituFile="
      command += insituFile
    if Bootstrap:
      command += " --Bootstrap=TRUE"
    if Debug:
      command += " --debug=TRUE"
    if DebugHists:
      command += " --debugHists=TRUE"
    command += " && "


command = command[:-3]
command += "&"
print command
os.system(command)
 
