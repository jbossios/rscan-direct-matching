#!/usr/bin/python

import os, sys

param=sys.argv

list=[
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
  "13",
]

coll = "2LC";
#coll = "6LC";
#coll = "8LC";

MC  = True
PRW = True
Bootstrap = False
ZJetStudy = True

# R21
version = "MC16a"
#version = "MC16c"
#version = "MC16d"
#version = "2015+2016data"
#version = "2017data"

# R20.7
#version = "2015data"
#version = "MC15cPythia_MC15bMuProfile"
#version = "2016data"
#version = "MC15cPythia"

# For data only
Parts = [
    "1",
    "2",
    "3",
    "4",
]

systs=[
    "Nominal",
#    "DeltaRUp"   ,
#    "DeltaRDown",
#    "IsolationUp",
#    "IsolationDown",
#    "JVTDown",
#    "JVTUp",
#     "MCSherpa",
]

insitu = False # only for data
insituNotSmoothed = True # True if in situ calibrations were not smoothed

####################################################################################
####################################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"
command = ""

PATH += "ROOTfiles/DirectMatching/TreetoHists_Outputs/"

for syst in systs:
  command += "hadd "+PATH
  if MC:
    command += "MC/"	  
    command += version
    command += "/"
    command += coll
    if version is "MC15cPythia_MC15bMuProfile":
      command += "/MC15c_MC15bmuProfile_Pythia_noPRW_JZAll"
    if version is "MC15cPythia":
      command += "/MC15c_Pythia_noPRW_JZAll"
    if version is "MC16a":
      command += "/MC16a_"
    if version is "MC16c":
      command += "/MC16c_"
    if version is "MC16d":
      command += "/MC16d_"
    if ZJetStudy:
      command += "ZJetStudy_JZAll"
    else:
      command += "Pythia_JZAll"
    if not PRW:
      command += "_noPRW"
    else:
      command += "_PRW"
    if syst is not "Nominal":
      command += "_"	    
      command += syst
    if Bootstrap:
      command += "_bootstrap.root "
    else:
      command += ".root "
    for slice in list:
      if version is "MC15cPythia_MC15bMuProfile":
        command += PATH+"MC/"+version+"/"+coll+"/MC15c_MC15bmuProfile_Pythia_noPRW_JZ"+slice
      if version is "MC15cPythia":
        command += PATH+"MC/"+version+"/"+coll+"/MC15c_Pythia_noPRW_JZ"+slice
      if version is "MC16a":
        command += PATH+"MC/"+version+"/"+coll+"/MC16a_"
      if version is "MC16c":
        command += PATH+"MC/"+version+"/"+coll+"/MC16c_"
      if version is "MC16d":
        command += PATH+"MC/"+version+"/"+coll+"/MC16d_"
      if ZJetStudy:
	command += "ZJetStudy_JZ"+slice
      else:
	command += "Pythia_JZ"+slice
      if not PRW:
	command += "_noPRW"
      else:
	command += "_PRW"
      if syst is not "Nominal":
        command += "_"
        command += syst
      if Bootstrap:
        command += "_bootstrap.root "
      else:
        command += ".root "
    command += "&& "
  if not MC and version == "2015data":
    command += "Data/"
    command += version
    command += "/"
    command += coll
    command += "/DataAll"
    if syst is not "Nominal":
      command += "_"	    
      command += syst
    if insitu:
      command += "_Insitu"	    
      if insituNotSmoothed:
	command += "notSmoothed"
    command += ".root "
    for part in Parts:
      command += PATH+"Data/"+version+"/"+coll+"/Data_25ns_Part"+part
      if syst is not "Nominal":
        command += "_"
        command += syst
      if insitu:
        command += "_Insitu"	
        if insituNotSmoothed:
          command += "notSmoothed"
    if Bootstrap:
      command += "_bootstrap.root "
    else:
      command += ".root "      
    command += "&& "
  if not MC and version == "2016data":
    command += "Data/"
    command += version
    command += "/"
    command += coll
    command += "/DataAll"
    if syst is not "Nominal":
      command += "_"	    
      command += syst
    if insitu:
      command += "_Insitu"	
      if insituNotSmoothed:
        command += "notSmoothed"
    if Bootstrap:
      command += "_bootstrap.root "
    else:
      command += ".root "    
    for part in Parts:
      command += PATH+"Data/"+version+"/"+coll+"/Data_25ns_period*"
      if syst is not "Nominal":
        command += "_"
        command += syst
      if insitu:
        command += "_Insitu"	
        if insituNotSmoothed:
          command += "notSmoothed"
    if Bootstrap:
      command += "_bootstrap.root "
    else:
      command += ".root "      
    command += "&& "
  if not MC and version == "2015+2016data":
    command += "Data/"
    command += version
    command += "/"
    command += coll
    command += "/DataAll"
    if syst is not "Nominal":
      command += "_"	    
      command += syst
    if insitu:
      command += "_Insitu"
      if insituNotSmoothed:
        command += "notSmoothed"
    if Bootstrap:
      command += "_bootstrap.root "
    else:
      command += ".root "	    
    command += PATH+"Data/R21_2015data/"+coll+"/Data_25ns_period*"
    if syst is not "Nominal":
      command += "_"
      command += syst
    if insitu:
      command += "_Insitu"
      if insituNotSmoothed:
        command += "notSmoothed"
    if Bootstrap:
      command += "_bootstrap.root "
    else:
      command += ".root " 
    command += PATH+"Data/R21_2016data/"+coll+"/Data_25ns_period*"
    if syst is not "Nominal":
      command += "_"
      command += syst
    if insitu:
      command += "_Insitu"
      if insituNotSmoothed:
        command += "notSmoothed"
    if Bootstrap:
      command += "_bootstrap.root "
    else:
      command += ".root " 
    command += "&& "
  if not MC and version == "2017data":
    command += "Data/"
    command += version
    command += "/"
    command += coll
    command += "/DataAll"
    if syst is not "Nominal":
      command += "_"	    
      command += syst
    if insitu:
      command += "_Insitu"
      if insituNotSmoothed:
        command += "notSmoothed"
    if Bootstrap:
      command += "_bootstrap.root "
    else:
      command += ".root "	    
    command += PATH+"Data/2017data/"+coll+"/Data_25ns_period*"
    if syst is not "Nominal":
      command += "_"
      command += syst
    if insitu:
      command += "_Insitu"
      if insituNotSmoothed:
        command += "notSmoothed"
    if Bootstrap:
      command += "_bootstrap.root "
    else:
      command += ".root " 
    command += "&& "

command = command[:-2]
print command
os.system(command)
