#ifndef run_h
#define run_h

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <string>
#include <map>
#include <fstream>
#include <utility>
#include <math.h>

#include <TROOT.h>
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TH1.h"
#include "TLorentzVector.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"

#include <stdio.h>


using namespace std;

int RecoMatch_index(TLorentzVector myjet, vector<TLorentzVector> jets, TLorentzVector &matchingJet, int &index, double &DRmin);
int Match_index_inv(TLorentzVector myjet, vector<TLorentzVector> jets, int index_veto, double DRmin);
double DRmin(TLorentzVector myjet, vector<TLorentzVector> jets, double PtMin);
int GetYStarBin(double yStar);

struct vJet {
    TLorentzVector Vec;
    bool           Used;
    bool           Iso;
};

std::vector<vJet> Selected_RscanJets;
std::vector<vJet> Selected_RefJets;

struct vJetPair {
    TLorentzVector vRef;
    TLorentzVector vRscan;
    double         dR;
};
std::vector<vJetPair> Selected_matched_Jets;

#endif

