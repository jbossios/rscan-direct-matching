//
// global.h
//
// Binning for Inclusive and Dijet analysis
//
// Jonathan Bossio - 15 June 2015
//
///////////////////////////////////////////

#ifndef bin_h
#define bin_h

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <string>
#include <map>
#include <fstream>
#include <utility>
#include <math.h>

#include <TROOT.h>
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TLorentzVector.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"

#include <stdio.h>

using namespace std;


//--------
// Binning
//--------

// eta bins for eta histogram
static const int netaBins = 90;
double etaBins[netaBins+1];

// Different pt bins for each y bin
const int nptBins_0 = 43;
const int nptBins_1 = 43;
const int nptBins_2 = 43;
const int nptBins_3 = 43;
const int nptBins_4 = 43;
const int nptBins_5 = 43;
double ptBins_0_aux[nptBins_0+1] = {15.,20.,25.,35.,45.,55.,70.,85.,100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_1_aux[nptBins_1+1] = {15.,20.,25.,35.,45.,55.,70.,85.,100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_2_aux[nptBins_2+1] = {15.,20.,25.,35.,45.,55.,70.,85.,100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_3_aux[nptBins_3+1] = {15.,20.,25.,35.,45.,55.,70.,85.,100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_4_aux[nptBins_4+1] = {15.,20.,25.,35.,45.,55.,70.,85.,100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double ptBins_5_aux[nptBins_5+1] = {15.,20.,25.,35.,45.,55.,70.,85.,100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941.};
double * ptBins_0 = ptBins_0_aux;
double * ptBins_1 = ptBins_1_aux;
double * ptBins_2 = ptBins_2_aux;
double * ptBins_3 = ptBins_3_aux;
double * ptBins_4 = ptBins_4_aux;
double * ptBins_5 = ptBins_5_aux;

// Triggers
std::vector<std::string > Triggers;

const double DataLuminosity = 1714.32;

#endif
