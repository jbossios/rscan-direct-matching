/* ****************************************************************************\
 *                                                                            *
 *  #   Name:   Trigger Emulation                                             *
 *                                                                            *
 *  #   Date    Comments                        By                            *
 * -- -------- -------------------------------  ----------------------------- *
 *  1 21/02/18 First version on Git             J. Bossio (jbossios@cern.ch)  *
\******************************************************************************/

#include <vector>
#include <string>

#include "TreetoROOT/global.h"
#include "TStopwatch.h"
#include "TreetoROOT/TreetoROOT.h"
#include "TROOT.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include <iostream>
#include <stdlib.h>
#include <sstream>

using namespace std;

//--------------------------------------------------------------
// Check
//--------------------------------------------------------------

TString PATH = "";

bool m_debug = false;

TString InputFile = "";

// Dataset
TString Dataset = ""; // 2015, 2016 or 2017

// JVT
float JVTthreshold = 0.59; // 2016 Moriod Recs (default working point) (Nominal)
float JVTpTmax = 60;       // 2016 Moriond Recs
float JVTetamax = 2.4;     //

// Isolation
double Rjet      = 1;
double isoFactor = 2;
double dR_match  = 0.2;

TString Emu_vs_pT = "rscan";
//TString Emu_vs_pT = "ref";

//--------------------------------------------------------------
//--------------------------------------------------------------

int main(int argc, char* argv[]) {

    gROOT->ProcessLine(".L loader.C+");

    std::string jetColl;
    std::string pathUser = "";
    std::string inputFileUser = "";
    std::string date = "";
    std::string dataset = "";

    //---------------------------
    // Decoding the user settings
    //---------------------------
    for (int i=1; i< argc; i++){

      std::string opt(argv[i]); std::vector< std::string > v;

      std::istringstream iss(opt);

      std::string item;
      char delim = '=';

      while (std::getline(iss, item, delim)){
          v.push_back(item);
      }

      if ( opt.find("--pathUSER=")   != std::string::npos) pathUser = v[1];
      if ( opt.find("--inputFileUSER=")   != std::string::npos) inputFileUser = v[1];
      if ( opt.find("--jetColl=")   != std::string::npos) jetColl = v[1];
      if ( opt.find("--date=")   != std::string::npos) date = v[1];
      if ( opt.find("--dataset=")   != std::string::npos) dataset = v[1];

      if ( opt.find("--debug=")      != std::string::npos) {
        if (v[1].find("TRUE") != std::string::npos) m_debug= true;
        if (v[1].find("FALSE") != std::string::npos) m_debug= false;
      }

    }//End: Loop over input options

    PATH = pathUser;
    InputFile = inputFileUser;
    Dataset = dataset;

    if(jetColl=="2LC") Rjet=.2;
    if(jetColl=="3LC") Rjet=.3;
    if(jetColl=="5LC") Rjet=.5;
    if(jetColl=="6LC") Rjet=.6;
    if(jetColl=="7LC") Rjet=.7;
    if(jetColl=="8LC") Rjet=.8; 

    std::cout << "R-scan Jet Collection: " << jetColl << std::endl;

    //----------
    // Triggers
    //----------

    if(Dataset=="R21_2015data"){
      Triggers.push_back("HLT_j15");
      Triggers.push_back("HLT_j25");
      Triggers.push_back("HLT_j35");
      Triggers.push_back("HLT_j45");
      Triggers.push_back("HLT_j55");
      Triggers.push_back("HLT_j60");
      Triggers.push_back("HLT_j85");
      Triggers.push_back("HLT_j110");
      Triggers.push_back("HLT_j150");
      Triggers.push_back("HLT_j175");
      Triggers.push_back("HLT_j200");
      Triggers.push_back("HLT_j260");
      Triggers.push_back("HLT_j320");
      Triggers.push_back("HLT_j360");
    } else if(Dataset=="R21_2016data"){
      Triggers.push_back("HLT_j15");
      Triggers.push_back("HLT_j25");
      Triggers.push_back("HLT_j35");
      Triggers.push_back("HLT_j45");
      Triggers.push_back("HLT_j55");
      Triggers.push_back("HLT_j60");
      Triggers.push_back("HLT_j85");
      Triggers.push_back("HLT_j110");
      Triggers.push_back("HLT_j150");
      Triggers.push_back("HLT_j175");
      Triggers.push_back("HLT_j260");
      Triggers.push_back("HLT_j380");
      Triggers.push_back("HLT_j400");
    } else if(Dataset=="2017data"){
      Triggers.push_back("HLT_j15");
      Triggers.push_back("HLT_j25");
      Triggers.push_back("HLT_j35");
      Triggers.push_back("HLT_j45");
      Triggers.push_back("HLT_j60");
      Triggers.push_back("HLT_j85");
      Triggers.push_back("HLT_j110");
      Triggers.push_back("HLT_j175");
      Triggers.push_back("HLT_j260");
      Triggers.push_back("HLT_j360");
      Triggers.push_back("HLT_j420");
    }

    std::map<std::string,bool> PassEvent_byTrigger;

    TString inputFile = PATH; inputFile += InputFile;

    //-----------------------
    // Getting Tree
    //-----------------------

    std::cout << "Opening input file: " << inputFile << std::endl;

    //TFile* ft = new TFile(inputFile.Data(),"open");
    TFile* ft = TFile::Open(inputFile.Data());
    ft->cd();

    if(ft == NULL) {
      std::cout<<"Returned null file: "<<ft<<std::endl;
      return 0;
    }

    TString treeName = "nominal";
    TString directoryName = "Tree";
    TTree *tree = new TTree();
    TDirectory* TDirF_tmp = ft->GetDirectory(directoryName.Data());
    TDirF_tmp->GetObject(treeName.Data(),tree);
    //tree = (TTree*)ft->Get("nominal"); // Check

    if(tree==NULL){
      std::cout<<"Returned null tree"<<std::endl;
      return 1;
    }

    //----------
    // Variables
    //----------

    // Event variables
    int runNumber=0;
    Long64_t evNumber=0;

    // offline jets
    std::vector<float> *jet_pt=0;
    std::vector<float> *jet_eta=0;
    std::vector<float> *jet_phi=0;
    std::vector<float> *jet_E=0;
    std::vector<float> *jet_JVT=0;
    std::vector<int> *jet_clean_passLooseBad = 0;
    std::vector<float> *jet_constScaleEta=0;

    // R-scan jet
    std::vector<float> *RscanJet_pt=0;
    std::vector<float> *RscanJet_eta=0;
    std::vector<float> *RscanJet_phi=0;
    std::vector<float> *RscanJet_E=0;
    std::vector<int> *RscanJet_clean_passLooseBad = 0;
    std::vector<float> *RscanJet_constScaleEta=0;

    // HLT jets
    std::vector<float> *HLTjet_pt=0;
    std::vector<float> *HLTjet_eta=0;
    std::vector<float> *HLTjet_phi=0;
    std::vector<float> *HLTjet_E=0;

    // L1 jets
    std::vector<float> *L1jet_eta=0;
    std::vector<float> *L1jet_phi=0;
    std::vector<float> *L1jet_et88=0;

    // Passed Triggers
    std::vector<std::string> *passedTriggers = 0;

    //-------------------
    // Necessary branches
    //-------------------

    tree->SetBranchStatus("*",0); //disable all branches

    tree->SetBranchStatus("RefJet_pt",1);
    tree->SetBranchStatus("RefJet_eta",1);
    tree->SetBranchStatus("RefJet_phi",1);
    tree->SetBranchStatus("RefJet_E",1);
    tree->SetBranchStatus("RefJet_Jvt",1);
    tree->SetBranchStatus("RefJet_clean_passLooseBad",1);
    tree->SetBranchStatus("RefJet_detectorEta",1);

   if(jetColl=="2LC"){
      tree->SetBranchStatus("2LCJet_pt",1);
      tree->SetBranchStatus("2LCJet_eta",1);
      tree->SetBranchStatus("2LCJet_phi",1);
      tree->SetBranchStatus("2LCJet_E",1);
      tree->SetBranchStatus("2LCJet_clean_passLooseBad",1);
      tree->SetBranchStatus("2LCJet_detectorEta",1);
    } else if(jetColl=="3LC"){
      tree->SetBranchStatus("3LCJet_pt",1);
      tree->SetBranchStatus("3LCJet_eta",1);
      tree->SetBranchStatus("3LCJet_phi",1);
      tree->SetBranchStatus("3LCJet_E",1);
      tree->SetBranchStatus("3LCJet_clean_passLooseBad",1);
      tree->SetBranchStatus("3LCJet_detectorEta",1);
    } else if(jetColl=="5LC"){
      tree->SetBranchStatus("5LCJet_pt",1);
      tree->SetBranchStatus("5LCJet_eta",1);
      tree->SetBranchStatus("5LCJet_phi",1);
      tree->SetBranchStatus("5LCJet_E",1);
      tree->SetBranchStatus("5LCJet_clean_passLooseBad",1);
      tree->SetBranchStatus("5LCJet_detectorEta",1);
    } else if(jetColl=="6LC"){
      tree->SetBranchStatus("6LCJet_pt",1);
      tree->SetBranchStatus("6LCJet_eta",1);
      tree->SetBranchStatus("6LCJet_phi",1);
      tree->SetBranchStatus("6LCJet_E",1);
      tree->SetBranchStatus("6LCJet_clean_passLooseBad",1);
      tree->SetBranchStatus("6LCJet_detectorEta",1);
    } else if(jetColl=="7LC"){
      tree->SetBranchStatus("7LCJet_pt",1);
      tree->SetBranchStatus("7LCJet_eta",1);
      tree->SetBranchStatus("7LCJet_phi",1);
      tree->SetBranchStatus("7LCJet_E",1);
      tree->SetBranchStatus("7LCJet_clean_passLooseBad",1);
      tree->SetBranchStatus("7LCJet_detectorEta",1);
    } else if(jetColl=="8LC"){
      tree->SetBranchStatus("8LCJet_pt",1);
      tree->SetBranchStatus("8LCJet_eta",1);
      tree->SetBranchStatus("8LCJet_phi",1);
      tree->SetBranchStatus("8LCJet_E",1);
      tree->SetBranchStatus("8LCJet_clean_passLooseBad",1);
      tree->SetBranchStatus("8LCJet_detectorEta",1);
    }

    tree->SetBranchStatus("HLTJet_pt",1);
    tree->SetBranchStatus("HLTJet_eta",1);
    tree->SetBranchStatus("HLTJet_phi",1);
    tree->SetBranchStatus("HLTJet_E",1);

    tree->SetBranchStatus("L1Jet_eta",1);
    tree->SetBranchStatus("L1Jet_phi",1);
    tree->SetBranchStatus("L1Jet_et8x8",1);

    tree->SetBranchStatus("runNumber",1);
    tree->SetBranchStatus("eventNumber",1);
    tree->SetBranchStatus("passedTriggers",1);

    
    tree->SetBranchAddress("runNumber",&runNumber);
    tree->SetBranchAddress("eventNumber",&evNumber);
    tree->SetBranchAddress("passedTriggers",&passedTriggers);

    tree->SetBranchAddress("RefJet_pt",&jet_pt);
    tree->SetBranchAddress("RefJet_eta",&jet_eta);
    tree->SetBranchAddress("RefJet_phi",&jet_phi);
    tree->SetBranchAddress("RefJet_E",&jet_E);
    tree->SetBranchAddress("RefJet_Jvt",&jet_JVT);
    tree->SetBranchAddress("RefJet_clean_passLooseBad",&jet_clean_passLooseBad);
    tree->SetBranchAddress("RefJet_detectorEta",&jet_constScaleEta);

    if(jetColl=="2LC"){
      tree->SetBranchAddress("2LCJet_pt",&RscanJet_pt);
      tree->SetBranchAddress("2LCJet_eta",&RscanJet_eta);
      tree->SetBranchAddress("2LCJet_phi",&RscanJet_phi);
      tree->SetBranchAddress("2LCJet_E",&RscanJet_E);
      tree->SetBranchAddress("2LCJet_clean_passLooseBad",&RscanJet_clean_passLooseBad);
      tree->SetBranchAddress("2LCJet_detectorEta",&RscanJet_constScaleEta);
    } else if(jetColl=="3LC"){
      tree->SetBranchAddress("3LCJet_pt",&RscanJet_pt);
      tree->SetBranchAddress("3LCJet_eta",&RscanJet_eta);
      tree->SetBranchAddress("3LCJet_phi",&RscanJet_phi);
      tree->SetBranchAddress("3LCJet_E",&RscanJet_E);
      tree->SetBranchAddress("3LCJet_clean_passLooseBad",&RscanJet_clean_passLooseBad);
      tree->SetBranchAddress("3LCJet_detectorEta",&RscanJet_constScaleEta);
    } else if(jetColl=="5LC"){
      tree->SetBranchAddress("5LCJet_pt",&RscanJet_pt);
      tree->SetBranchAddress("5LCJet_eta",&RscanJet_eta);
      tree->SetBranchAddress("5LCJet_phi",&RscanJet_phi);
      tree->SetBranchAddress("5LCJet_E",&RscanJet_E);
      tree->SetBranchAddress("5LCJet_clean_passLooseBad",&RscanJet_clean_passLooseBad);
      tree->SetBranchAddress("5LCJet_detectorEta",&RscanJet_constScaleEta);
    } else if(jetColl=="6LC"){
      tree->SetBranchAddress("6LCJet_pt",&RscanJet_pt);
      tree->SetBranchAddress("6LCJet_eta",&RscanJet_eta);
      tree->SetBranchAddress("6LCJet_phi",&RscanJet_phi);
      tree->SetBranchAddress("6LCJet_E",&RscanJet_E);
      tree->SetBranchAddress("6LCJet_clean_passLooseBad",&RscanJet_clean_passLooseBad);
      tree->SetBranchAddress("6LCJet_detectorEta",&RscanJet_constScaleEta);
    } else if(jetColl=="7LC"){
      tree->SetBranchAddress("7LCJet_pt",&RscanJet_pt);
      tree->SetBranchAddress("7LCJet_eta",&RscanJet_eta);
      tree->SetBranchAddress("7LCJet_phi",&RscanJet_phi);
      tree->SetBranchAddress("7LCJet_E",&RscanJet_E);
      tree->SetBranchAddress("7LCJet_clean_passLooseBad",&RscanJet_clean_passLooseBad);
      tree->SetBranchAddress("7LCJet_detectorEta",&RscanJet_constScaleEta);
    } else if(jetColl=="8LC"){
      tree->SetBranchAddress("8LCJet_pt",&RscanJet_pt);
      tree->SetBranchAddress("8LCJet_eta",&RscanJet_eta);
      tree->SetBranchAddress("8LCJet_phi",&RscanJet_phi);
      tree->SetBranchAddress("8LCJet_E",&RscanJet_E);
      tree->SetBranchAddress("8LCJet_clean_passLooseBad",&RscanJet_clean_passLooseBad);
      tree->SetBranchAddress("8LCJet_detectorEta",&RscanJet_constScaleEta);
    }

    tree->SetBranchAddress("HLTJet_pt",&HLTjet_pt);
    tree->SetBranchAddress("HLTJet_eta",&HLTjet_eta);
    tree->SetBranchAddress("HLTJet_phi",&HLTjet_phi);
    tree->SetBranchAddress("HLTJet_E",&HLTjet_E);

    tree->SetBranchAddress("L1Jet_eta",&L1jet_eta);
    tree->SetBranchAddress("L1Jet_phi",&L1jet_phi);
    tree->SetBranchAddress("L1Jet_et8x8",&L1jet_et88);

    // Number of entries
    int entries = tree->GetEntries();

    if(m_debug) entries = 10;

    //--------------------
    // Booking Histograms
    //--------------------

    // Numerator histograms
    std::vector<TH1D*> pT_offline_num;
    std::vector<TH1D*> pT_offline_den;
    TH1D* temp;
    for(unsigned int i=0;i<Triggers.size();++i){
      TString name = "pT_offline_num_";
      name += Triggers.at(i);
      temp = new TH1D(name.Data(),name.Data(),600,0,600);
      pT_offline_num.push_back(temp);
      name = "pT_offline_den_";
      name += Triggers.at(i);
      temp = new TH1D(name.Data(),name.Data(),600,0,600);
      pT_offline_den.push_back(temp);
    }

    //------------------
    // Loop over entries
    //------------------
    for(int entry=0; entry<entries; entry++) {

      bool should_skip = false;

      tree->GetEntry(entry);

      if(entry % 1000000 == 0) std::cout << "Entry " << entry << " of " << entries << std::endl;

      // Number of jets
      int njet = jet_pt->size();
      int nrscanjet = RscanJet_pt->size();
      int nHLTjet = HLTjet_pt->size();
      int nL1jet = L1jet_eta->size();

      if(m_debug){
        std::cout << "Number of Ref jets: "   << njet << std::endl;
	std::cout << "Number of Rscan jets: " << nrscanjet << std::endl;
	std::cout << "Number of HLT jets: "   << nHLTjet << std::endl;
	std::cout << "Number of L1 jets: "    << nL1jet << std::endl;
      }

      //----------------
      // Event Selection
      //----------------

      // At least 1 Ref and 1 Rscan jets
      if(njet<1 || nrscanjet<1) continue; // skip event

      if(m_debug) std::cout << "At least 1 Ref and 1 Rscan jets" << std::endl;

      //-----------------
      // Event Cleaning
      //-----------------
      
      // Removing events only due to LooseBad
      // If jet is pileup the event is kept
      for(int j=0;j<njet;++j){
        if(jet_clean_passLooseBad->at(j)!=1){ // jet not passing LooseBad criteria
          if(jet_pt->at(j)>=JVTpTmax || fabs(jet_constScaleEta->at(j))>=JVTetamax){ // JVT not applicable
            should_skip = true; // Skip event
            break;
          }
          else if(jet_JVT->at(j)>=JVTthreshold){ // JVT applicable but is not pileup
            should_skip = true;
            break;
          }
        }
      }
      if(should_skip) continue; // skip event due to a non clean jet

      if(m_debug) std::cout << "After event cleaning" << std::endl;


      //------------------------------
      // Selection of Reference Jets
      //------------------------------

      if(m_debug) std::cout << "Selection of Reference Jets" << std::endl;
      Selected_RefJets.clear();
      for(unsigned int k=0;k<jet_pt->size();++k){
	TLorentzVector jetRef;
	jetRef.SetPtEtaPhiE(jet_pt->at(k), jet_eta->at(k), jet_phi->at(k), jet_E->at(k));
	// Selecting HS jets with pT>10GeV and |eta|<3.0
	if(jetRef.Pt()>10 && fabs(jet_constScaleEta->at(k))<3.0){
	  vJet Jet_tmp;
          Jet_tmp.Vec  = jetRef;
          Jet_tmp.Used = false;
          Jet_tmp.Iso  = false;
	  Jet_tmp.DetectorEta = jet_constScaleEta->at(k);
	  Jet_tmp.JVT  = jet_JVT->at(k);
          Selected_RefJets.push_back(Jet_tmp);
	}
      }
      if(m_debug) std::cout << "Number of selected Reference jets: " << Selected_RefJets.size() << std::endl;

      //------------------------------
      // Selection of R-scan Jets
      //------------------------------
      if(m_debug) std::cout << "Selecting R-scan jets" << std::endl;
      Selected_RscanJets.clear();
      for(unsigned int m=0;m<RscanJet_pt->size();++m){
        TLorentzVector jetRscan;
        jetRscan.SetPtEtaPhiE( RscanJet_pt->at(m), RscanJet_eta->at(m), RscanJet_phi->at(m), RscanJet_E->at(m) );
	// Selecting HS jets with pT>10GeV and |eta|<3.0
        if(RscanJet_pt->at(m)>10 || fabs(RscanJet_constScaleEta->at(m))<3.0){
          vJet Jet_tmp;
          Jet_tmp.Vec  = jetRscan;
          Jet_tmp.Used = false;
          Jet_tmp.Iso  = false;
          Selected_RscanJets.push_back(Jet_tmp);	
	}	
      }
      if(m_debug) std::cout << "Number of selected R-scan jets: " << Selected_RscanJets.size() << std::endl;

      // At least one REf and one R-scan jet
      if(Selected_RscanJets.size()<1 || Selected_RefJets.size()<1) continue;

      // Loop over R-scan matched jets
      if(Selected_RscanJets.size()>0){
        int ipair = 0;
        float pT_offl = 0;
	if(Emu_vs_pT == "ref") pT_offl = Selected_RefJets.at(ipair).Vec.Pt();
	else{pT_offl = Selected_RscanJets.at(ipair).Vec.Pt();}

        if(m_debug && Emu_vs_pT=="ref")   std::cout << "pTRef["<<ipair<<"]: " << pT_offl << std::endl;
	else if(m_debug && Emu_vs_pT=="rscan") std::cout << "pTRscan["<<ipair<<"]: " << pT_offl << std::endl;

	for(unsigned int itrigger=0;itrigger<Triggers.size();++itrigger) PassEvent_byTrigger[Triggers.at(itrigger)] = false;

        // Decidying to use event for Triggers >= HLT_j45
        for(int j=0;j<nL1jet;++j){
          if (L1jet_et88->at(j) <= 15) continue;
	  for(unsigned int itrigger=0;itrigger<Triggers.size();++itrigger){
	    if(Triggers.at(itrigger)=="HLT_j15") continue;
	    if(Triggers.at(itrigger)=="HLT_j25") continue;
	    if(Triggers.at(itrigger)=="HLT_j35") continue;
	    PassEvent_byTrigger[Triggers.at(itrigger)] = true;
	  }
        }

        // L1_RD0_FILLED (HLT_j15, HLT_j25, and HLT_j35)
        for(unsigned int lll=0;lll<passedTriggers->size();lll++){// Loop over passed triggers
          if(passedTriggers->at(lll)=="L1_RD0_FILLED") {
            PassEvent_byTrigger["HLT_j15"] = true;
            PassEvent_byTrigger["HLT_j25"] = true;
            PassEvent_byTrigger["HLT_j35"] = true;
          }
        }

        // Safe check
        if(nHLTjet == 0 ) {
          cout<<"Problem, Skip event"<<endl;
          cout<<"nJet: "<<njet<<endl;
          cout<<"nHLT: "<<nHLTjet<<endl;
          cout<<"nL1: "<<nL1jet<<endl;
          std::cout << "runNumber: " << runNumber << std::endl;
          std::cout << "eventNumber: " << evNumber << std::endl;
          continue; // skip event
        }

       // Loop over Triggers
       for(unsigned int t=0; t<Triggers.size();t++) {

        if(m_debug) std::cout << "Trigger: " << Triggers.at(t) << std::endl;
        if(m_debug) std::cout << "PassEvent_byTrigger: " << PassEvent_byTrigger[(Triggers.at(t)).c_str()] << std::endl;

        // L1 passed? 
        if(!PassEvent_byTrigger[(Triggers.at(t)).c_str()]){
          continue; // Skip trigger
        }

        if(m_debug) std::cout << "Trigger passed" << std::endl;

        TString chain = Triggers.at(t);

        // Filling denominator distributions
	
	if(m_debug) std::cout << "Filling Denominator distributions" << std::endl;

        float et_cut = 1e6;
        float l1_cut = 1e6;

        pT_offline_den.at(t)->Fill(pT_offl);
        if(chain == "HLT_j420") {
          et_cut = 420;
          l1_cut = 100;
        } else if(chain == "HLT_j400") {
          et_cut = 400;
          l1_cut = 100;
	} else if(chain == "HLT_j380") {
          et_cut = 380;
          l1_cut = 100;
        } else if(chain == "HLT_j360") {
          et_cut = 360;
          l1_cut = 100;
        } else if (chain == "HLT_j260") {
          et_cut = 260;
          l1_cut = 75;
        } else if (chain == "HLT_j200") {
          et_cut = 200;
          l1_cut = 50;
        } else if (chain == "HLT_j175") {
          et_cut = 175;
          l1_cut = 50;
        } else if (chain == "HLT_j150") {
          et_cut = 150;
          l1_cut = 40;
        } else if (chain == "HLT_j110") {
          et_cut = 110;
          l1_cut = 30;
        } else if (chain == "HLT_j85") {
          et_cut = 85;
          l1_cut = 20;
        } else if (chain == "HLT_j60") {
          et_cut = 60;
          l1_cut = 20;
        } else if (chain == "HLT_j55") {
          et_cut = 55;
          l1_cut = 15;
        } else if (chain == "HLT_j45") {
          et_cut = 45;
          l1_cut = 15;
        } else if (chain == "HLT_j35") {
          et_cut = 35;
        } else if (chain == "HLT_j25") {
          et_cut = 25;
        } else if (chain == "HLT_j15") {
          et_cut = 15;
        } else continue;


        //--------------------------
        // Emulation of the trigger
        //--------------------------
	
	if(m_debug) std::cout << "Emulation of the trigger" << std::endl;

        // Loop over HLT jets
	if(m_debug) std::cout << "Loop over HLT jets" << std::endl;
        vector<TLorentzVector> HLT_Jets;
        for(int j=0;j<nHLTjet;++j) {

          if (HLTjet_E->at(j)/cosh(HLTjet_eta->at(j))<=et_cut) continue;
          // Save HLT jets within the phase-space region of the HLT triggers
          if (fabs(HLTjet_eta->at(j))>=3.2) continue;

          float  hlt_pt  = HLTjet_pt->at(j);
          float  hlt_eta = HLTjet_eta->at(j);
          float  hlt_phi = HLTjet_phi->at(j);
          float  hlt_e   = HLTjet_E->at(j);

          TLorentzVector v1;
          v1.SetPtEtaPhiE(hlt_pt,hlt_eta,hlt_phi,hlt_e);
          
          bool pass_L1_100 = false;

	  if(chain == "HLT_j15" || chain == "HLT_j25" || chain == "HLT_j35") pass_L1_100 = true;
	  else{ // Others triggers
            for(int jj=0;jj<nL1jet;++jj) { // Loop over L1 jets
  
              if(L1jet_et88->at(jj) <= l1_cut) continue;
 
              pass_L1_100 = true;

            }
	  }

          if(pass_L1_100) HLT_Jets.push_back(v1); //without dr matching
          
        }


        //Debugging: 
        //Evaluation 1 must pass if the emulation is ok for both prescaled and prescaled chains
        //Evaluation 2 might not pass for prescaled chains

        bool pass=false;
        for(unsigned int lll=0;lll<passedTriggers->size();lll++){// Loop over passed triggers
          if (passedTriggers->at(lll) == chain) {
            pass = true;
            if (HLT_Jets.size() ==0) {
              cout<<"EVALUATION 1 FAILED! "<<"chain: "<<chain<<endl;
            }
          }
        }

        if (HLT_Jets.size()>0) {
          if (!pass && chain=="HLT_j400") {// Prescaled triggers will probably fail
            cout<<"EVALUATION 2 FAILED! "<<"chain: "<<chain<<endl;
          }
        }


        // Filling numerator distributions
	
	if(m_debug) std::cout << "Filling Numerator Distributions" << std::endl;

        if(HLT_Jets.size()>0) {
          pT_offline_num.at(t)->Fill(pT_offl);		
        }

       }//END: loop over Triggers

      }//Loop over pTavg

      runNumber = -999;
      jet_pt->clear();
      jet_eta->clear();
      jet_phi->clear();
      jet_E->clear();
      jet_clean_passLooseBad->clear();
      HLTjet_pt->clear();
      HLTjet_eta->clear();
      HLTjet_phi->clear();
      HLTjet_E->clear();
      L1jet_eta->clear();
      L1jet_phi->clear();
      L1jet_et88->clear();

    } // END: Loop over entries

    //------------------------------------
    // Saving Histogramas into a ROOT file
    //------------------------------------

    std::cout << "Saving plots into a ROOT file..." << std::endl;

    // Check
    TString outputfile = "../ROOTfiles/DirectMatching/TriggerEmulation/";
    outputfile += Dataset;
    outputfile += "/";
    outputfile += date;
    outputfile += "_";
    outputfile += jetColl;
    if(Emu_vs_pT=="ref") outputfile += "_vspTref";
    outputfile += "_";
    // extract period
    int index_period = PATH.Index("period")+6;
    TString period = PATH[index_period];
    outputfile += "_period";
    outputfile += period;
    // extract number of file
    int index_number = InputFile.Index(".tree")-6;
    TString numberFile = InputFile(index_number,6);
    outputfile += "_file";
    outputfile += numberFile;
    outputfile += ".root";

    TFile* tout = new TFile(outputfile,"recreate");
    std::cout << "Output file: " << outputfile << std::endl;
    tout->cd();

    for(unsigned int itrigger=0;itrigger<Triggers.size();++itrigger){
      pT_offline_num.at(itrigger)->Write();
      pT_offline_den.at(itrigger)->Write();
    }

    tout->Close();

    ft->Close(); //FIX the memory problem?

    std::cout << "Done!" << std::endl;

}//END: main()


