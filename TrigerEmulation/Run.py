#!/usr/bin/python
#from os import listdir
import os, sys
import ROOT

Debug = False

# Date version of turn on curves
Date = "DATE"

Dataset = "R21_2015data" # Options: R21_2015data, R21_2016data, 2017data

coll = "2LC";
#coll = "6LC";
#coll = "8LC";

Samples2015 = []

Samples2016 = []

Samples2017 = []

######################################################################
## DO NOT EDIT
######################################################################

command = ""
if Dataset is "R21_2015data":
  Samples = Samples2015
elif Dataset is "R21_2016data":
  Samples = Samples2016
elif Dataset is "2017data":
  Samples = Samples2017
else:
  print "Dataset not recognised, exiting"
  sys.exit(0)

TDirectoryFileName = "Tree"
TTreeName = "nominal"

for path in Samples:
  for File in os.listdir(path):
    # Check if root file contains a tree
    iFile = ROOT.TFile.Open(path+"/"+File,"read")
    if not iFile:
      print "TFile not found, skipping file"
      continue
    tree = ROOT.TTree()
    TDir = iFile.GetDirectory(TDirectoryFileName)
    if not TDir:
      print "TDirectoryFile not found, skipping file"
      continue
    tree = TDir.Get(TTreeName)
    if not tree:
      print "TTree not found, skipping file"
      continue
    # Run if exist the tree  
    command += "TreetoROOT"
    command += " --jetColl="
    command += coll
    command += " --date="
    command += Date
    command += " --dataset="
    command += Dataset
    if Debug:
      command += " --debug=TRUE"
    command += " --pathUSER="
    command += path+"/"
    command += " --inputFileUSER="
    command += File
    command += " && "

command = command[:-3]
command += "&"
print command
os.system(command)
