#! /bin/bash

setupATLAS
mkdir ErrFiles
mkdir LogFiles
mkdir Outputs
cd source
setupATLAS
asetup AnalysisBase,21.2.19,here
cd ../build
cmake ../source
make -j
source ${CMTCONFIG}/setup.sh
cd ..
