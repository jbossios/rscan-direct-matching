#!/usr/bin/python

import os, sys

param=sys.argv

#////////////////////////////////////////////////////////////////////////////
#/////                           EDIT                                   /////
#////////////////////////////////////////////////////////////////////////////

# Date
date = "XX_YY_ZZ_Text"

localPATH = ""

user = "USER"

# Sample
# R20.7
JETM9_25ns_2016Data        = False                  # 2016 data
JETM9_25ns_20_7_2015Data   = False                  # 20.7 2015 data
JETM9_25ns_MC_Pythia_MC15c = True                   # to compare with 2016 data
JETM9_25ns_MC_Sherpa_MC15c = True                   # to compare with 2016 data
JETM9_25ns_MC_Pythia_MC15c_MC15bmuProfile = False   # to compare with 20.7 2015 data
JETM9_25ns_MC_Sherpa_MC15c_MC15bmuProfile = False   # to compare with 20.7 2015 data
# R21
JETM9_R21_2015Data         = False
JETM9_R21_2016Data         = False
JETM9_R21_2017Data         = False
JETM9_MC16a_Pythia         = False # to compare with 2015+2016 data
JETM9_MC16a_Sherpa         = False # to compare with 2015+2016 data
JETM9_MC16c_Pythia         = False # to compare with 2017 data
JETM9_MC16c_Sherpa         = False # to compare with 2017 data
JETM9_MC16d_Pythia         = False # to compare with 2017 data (RECOMMENDED for 2017data)
JETM9_MC16d_Sherpa         = False # to compare with 2017 data (NOT USE - MISSING SLICES)

# R-scan Jet Collection(s)
JetColl = "All" # 2LC and 6LC (R21); 2LC, 6LC and 8LC (R20.7)
#JetColl = "2LC" # Not available for R21
#JetColl = "6LC" # Not available for R21

# JZX samples
list=[
    "JZ0", # Not available for Sherpa
    "JZ1",
    "JZ2",
    "JZ3",
    "JZ4",
    "JZ5",
    "JZ6",
    "JZ7",
    "JZ8",
    "JZ9", # Sherpa (MC15c w/MC15b mu-profile) up to JZ9 only
    "JZ10",
    "JZ11",
    "JZ12",
]

R21_2015DataContainers = [
   "data15_13TeV:data15_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM9.grp15_v01_p3213", # Missing runs v89 GRL
   "data15_13TeV:data15_13TeV.periodE.physics_Main.PhysCont.DAOD_JETM9.grp15_v01_p3213",
   "data15_13TeV:data15_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM9.grp15_v01_p3213",
   "data15_13TeV:data15_13TeV.periodG.physics_Main.PhysCont.DAOD_JETM9.grp15_v01_p3213",
   "data15_13TeV:data15_13TeV.periodH.physics_Main.PhysCont.DAOD_JETM9.grp15_v01_p3213",
   "data15_13TeV:data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_JETM9.grp15_v01_p3213",
]

R21_2016DataContainers = [
   "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213", # OK v89 GRL
   "data16_13TeV:data16_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213",
   "data16_13TeV:data16_13TeV.periodC.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213",
   "data16_13TeV:data16_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213",
   "data16_13TeV:data16_13TeV.periodE.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213",
   "data16_13TeV:data16_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213",
   "data16_13TeV:data16_13TeV.periodG.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213",
   "data16_13TeV:data16_13TeV.periodI.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213",
   "data16_13TeV:data16_13TeV.periodK.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213",
   "data16_13TeV:data16_13TeV.periodL.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p3213",
]

R21_2017DataContainers = [
  "user.ohaldik:data17_13TeV.periodB.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodC.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodD.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodE.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX", # 334737 older ftag
  "user.ohaldik:data17_13TeV.periodF.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodH.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodI.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodK.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
]

Data2016=[
    "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 296939 - 300287 (30 runs) 
    "data16_13TeV:data16_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 300345 - 300908 (13 runs)
    "data16_13TeV:data16_13TeV.periodC.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 301973 - 302393 (14 runs)
    "data16_13TeV:data16_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 302737 - 303560 (20 runs)
    "data16_13TeV:data16_13TeV.periodE.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 303638 - 303892 ( 8 runs)
    "data16_13TeV:data16_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 303943 - 304494 (13 runs)
    "data16_13TeV:data16_13TeV.periodG.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 305291 - 306714 (27 runs)
    "data16_13TeV:data16_13TeV.periodI.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 307124 - 308084 (22 runs)
    "data16_13TeV:data16_13TeV.periodK.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 309311 - 309759 ( 8 runs)
    "data16_13TeV:data16_13TeV.periodL.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 310015 - 311481 (27 runs)
]

# 20.7 2015 data samples
Data2015_20_7=[
    "data15_13TeV:data15_13TeV.00276262.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D3
    "data15_13TeV:data15_13TeV.00276329.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D3
    "data15_13TeV:data15_13TeV.00276336.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D3
    "data15_13TeV:data15_13TeV.00276416.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D4
    "data15_13TeV:data15_13TeV.00276511.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D4
    "data15_13TeV:data15_13TeV.00276689.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D4
    "data15_13TeV:data15_13TeV.00276778.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D6
    "data15_13TeV:data15_13TeV.00276790.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D6
    "data15_13TeV:data15_13TeV.00276952.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D6
    "data15_13TeV:data15_13TeV.00276954.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #D6
    "data15_13TeV:data15_13TeV.00278880.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E2
    "data15_13TeV:data15_13TeV.00278912.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E2
    "data15_13TeV:data15_13TeV.00278968.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E3
    "data15_13TeV:data15_13TeV.00279169.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E3
    "data15_13TeV:data15_13TeV.00279259.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E3
    "data15_13TeV:data15_13TeV.00279279.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E3
    "data15_13TeV:data15_13TeV.00279284.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E3
    "data15_13TeV:data15_13TeV.00279345.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E4
    "data15_13TeV:data15_13TeV.00279515.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E4
    "data15_13TeV:data15_13TeV.00279598.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E4
    "data15_13TeV:data15_13TeV.00279685.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E4
    "data15_13TeV:data15_13TeV.00279764.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E5
    "data15_13TeV:data15_13TeV.00279813.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E5
    "data15_13TeV:data15_13TeV.00279867.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E5
    "data15_13TeV:data15_13TeV.00279928.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #E5
    "data15_13TeV:data15_13TeV.00279932.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #F1
    "data15_13TeV:data15_13TeV.00279984.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #F1
    "data15_13TeV:data15_13TeV.00280231.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G1
    "data15_13TeV:data15_13TeV.00280319.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G1
    "data15_13TeV:data15_13TeV.00280368.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G1
    "data15_13TeV:data15_13TeV.00280423.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G1
    "data15_13TeV:data15_13TeV.00280464.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G1
    "data15_13TeV:data15_13TeV.00280500.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G2
    "data15_13TeV:data15_13TeV.00280520.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G2
    "data15_13TeV:data15_13TeV.00280614.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G2
    "data15_13TeV:data15_13TeV.00280673.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G2
    "data15_13TeV:data15_13TeV.00280753.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G2
    "data15_13TeV:data15_13TeV.00280853.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G2
    "data15_13TeV:data15_13TeV.00280862.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G3
    "data15_13TeV:data15_13TeV.00280950.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G3
    "data15_13TeV:data15_13TeV.00280977.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G4
    "data15_13TeV:data15_13TeV.00281070.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G4
    "data15_13TeV:data15_13TeV.00281074.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G4
    "data15_13TeV:data15_13TeV.00281075.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #G4
    "data15_13TeV:data15_13TeV.00281317.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #H2
    "data15_13TeV:data15_13TeV.00281385.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #H2
    "data15_13TeV:data15_13TeV.00281411.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #H3
    "data15_13TeV:data15_13TeV.00282625.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J1
    "data15_13TeV:data15_13TeV.00282631.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J1
    "data15_13TeV:data15_13TeV.00282712.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J1
    "data15_13TeV:data15_13TeV.00282784.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J2
    "data15_13TeV:data15_13TeV.00282992.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J3
    "data15_13TeV:data15_13TeV.00283074.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J3
    "data15_13TeV:data15_13TeV.00283155.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J3
    "data15_13TeV:data15_13TeV.00283270.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J3
    "data15_13TeV:data15_13TeV.00283429.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J4
    "data15_13TeV:data15_13TeV.00283608.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J4
    "data15_13TeV:data15_13TeV.00283780.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J4
    "data15_13TeV:data15_13TeV.00284006.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J4
    "data15_13TeV:data15_13TeV.00284154.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J5
    "data15_13TeV:data15_13TeV.00284213.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J6
    "data15_13TeV:data15_13TeV.00284285.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J6
    "data15_13TeV:data15_13TeV.00284420.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J6 
    "data15_13TeV:data15_13TeV.00284427.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J6 
    "data15_13TeV:data15_13TeV.00284484.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2950", #J6
]

#////////////////////////////////////////////////////////////////////////////
#////////////////////////////////////////////////////////////////////////////

if JETM9_25ns_2016Data or JETM9_25ns_20_7_2015Data or JETM9_25ns_MC_Pythia_MC15c or JETM9_25ns_MC_Sherpa_MC15c or JETM9_25ns_MC_Pythia_MC15c_MC15bmuProfile or JETM9_25ns_MC_Sherpa_MC15c_MC15bmuProfile:
  localPATH += "/xAODAnaHelpers/data/" 
else:
  localPATH += "/source/xAODAnaHelpers/data/" 

if JETM9_25ns_20_7_2015Data:
  for Dataset in Data2015_20_7:

    # Setting the correct sample
    sample = Dataset
    dataset = Dataset[26:34]

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    command += " --submitDir="
    command += "Outputs_"
    command += Dataset

    # Config
    command += " --config "+localPATH+"2015/xah_run_DATA"
    if JetColl is not "All":
      command += "_"
      command += JetColl
    command += ".json"

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += dataset

    # Logfiles
    command += " > ../GridSubmissions/log_20_7_2015data_"
    command += dataset
    command += " 2> ../GridSubmissions/errors_20_7_2015data_"
    command += dataset

    command += " &"

    print command
    os.system(command)

if JETM9_25ns_2016Data:
  for Dataset in Data2016:

    # Setting the correct sample
    sample = Dataset
    dataset = Dataset[26:34]

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    command += " --submitDir="
    command += "Outputs_"
    command += Dataset

    # Config
    command += " --config "+localPATH+"2016/xah_run_DATA"
    if JetColl is not "All":
      command += "_"
      command += JetColl
    command += ".json"

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += dataset

    # Logfiles
    command += " > ../GridSubmissions/log_2016data_"
    command += dataset
    command += " 2> ../GridSubmissions/errors_2016data_"
    command += dataset

    command += " &"

    print command
    os.system(command)


if JETM9_25ns_MC_Pythia_MC15c:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ0":
      sample = "mc15_13TeV:mc15_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.merge.DAOD_JETM9.e3569_s2576_s2132_r7725_r7676_p2812"
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.DAOD_JETM9.e3569_s2576_s2132_r7725_r7676_p2812"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.merge.DAOD_JETM9.e3668_s2576_s2132_r7725_r7676_p2812"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.merge.DAOD_JETM9.e3668_s2576_s2132_r7725_r7676_p2812"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.DAOD_JETM9.e3668_s2576_s2132_r7725_r7676_p2812"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.merge.DAOD_JETM9.e3668_s2576_s2132_r7725_r7676_p2812"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.merge.DAOD_JETM9.e3569_s2608_s2183_r7725_r7676_p2812"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.merge.DAOD_JETM9.e3668_s2608_s2183_r7725_r7676_p2812"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.merge.DAOD_JETM9.e3569_s2576_s2132_r7772_r7676_p2812"
    if JZ == "JZ9":
      sample = "mc15_13TeV:mc15_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.merge.DAOD_JETM9.e3569_s2576_s2132_r7772_r7676_p2812"
    if JZ == "JZ10":
      sample = "mc15_13TeV:mc15_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.merge.DAOD_JETM9.e3569_s2576_s2132_r7772_r7676_p2812"
    if JZ == "JZ11":
      sample = "mc15_13TeV:mc15_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.merge.DAOD_JETM9.e3569_s2608_s2183_r7772_r7676_p2812"
    if JZ == "JZ12":
      sample = "mc15_13TeV:mc15_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.merge.DAOD_JETM9.e3668_s2608_s2183_r7772_r7676_p2812"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"2016/xah_run_MC"
    if JetColl is not "All":
      command += "_"
      command += JetColl
    command += ".json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC15c_Pythia_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC15c_Pythia_"
    command += JZ

    command += " &"

    print command
    os.system(command)



if JETM9_25ns_MC_Pythia_MC15c_MC15bmuProfile:
  for JZ in list:
    # Setting the correct sample
    # Updated from p2616 to p2666 on 07/09/2016
    if JZ == "JZ0":
      sample = "mc15_13TeV:mc15_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.merge.DAOD_JETM9.e3569_s2576_s2132_r7773_r7676_p2666"
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.DAOD_JETM9.e3569_s2576_s2132_r7773_r7676_p2666"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.merge.DAOD_JETM9.e3668_s2576_s2132_r7773_r7676_p2666"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.merge.DAOD_JETM9.e3668_s2576_s2132_r7773_r7676_p2666"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.DAOD_JETM9.e3668_s2576_s2132_r7773_r7676_p2666"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.merge.DAOD_JETM9.e3668_s2576_s2132_r7773_r7676_p2666"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.merge.DAOD_JETM9.e3569_s2608_s2183_r7773_r7676_p2666"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.merge.DAOD_JETM9.e3668_s2608_s2183_r7773_r7676_p2666"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.merge.DAOD_JETM9.e3569_s2576_s2132_r7773_r7676_p2666"
    if JZ == "JZ9":
      sample = "mc15_13TeV:mc15_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.merge.DAOD_JETM9.e3569_s2576_s2132_r7773_r7676_p2666"
    if JZ == "JZ10":
      sample = "mc15_13TeV:mc15_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.merge.DAOD_JETM9.e3569_s2576_s2132_r7773_r7676_p2666"
    if JZ == "JZ11":
      sample = "mc15_13TeV:mc15_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.merge.DAOD_JETM9.e3569_s2608_s2183_r7773_r7676_p2666"
    if JZ == "JZ12":
      sample = "mc15_13TeV:mc15_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.merge.DAOD_JETM9.e3668_s2608_s2183_r7773_r7676_p2666"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"2015/xah_run_MC"
    if JetColl is not "All":
      command += "_"
      command += JetColl
    command += ".json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC15c_MC15bmuProfile_Pythia_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC15c_MC15bmuProfile_Pythia_"
    command += JZ

    command += " &"

    print command
    os.system(command)


if JETM9_25ns_MC_Sherpa_MC15c_MC15bmuProfile:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.426131.Sherpa_CT10_jets_JZ1.merge.DAOD_JETM9.e4355_s2608_r7773_r7676_p2768"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.426132.Sherpa_CT10_jets_JZ2.merge.DAOD_JETM9.e4355_s2608_r7773_r7676_p2768"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.426133.Sherpa_CT10_jets_JZ3.merge.DAOD_JETM9.e4355_s2608_r7773_r7676_p2768"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.426134.Sherpa_CT10_jets_JZ4.merge.DAOD_JETM9.e4355_s2608_r7773_r7676_p2768"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.426135.Sherpa_CT10_jets_JZ5.merge.DAOD_JETM9.e4355_s2608_r7773_r7676_p2768"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.426136.Sherpa_CT10_jets_JZ6.merge.DAOD_JETM9.e4355_s2608_r7773_r7676_p2768"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.426137.Sherpa_CT10_jets_JZ7.merge.DAOD_JETM9.e4635_s2726_r7773_r7676_p2768"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.426138.Sherpa_CT10_jets_JZ8.merge.DAOD_JETM9.e4635_s2726_r7773_r7676_p2768"
    if JZ == "JZ9":
      sample = "mc15_13TeV:mc15_13TeV.426139.Sherpa_CT10_jets_JZ9.merge.DAOD_JETM9.e4635_s2726_r7773_r7676_p2768"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"2015/xah_run_MC"
    if JetColl is not "All":
      command += "_"
      command += JetColl
    command += ".json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC15c_MC15bmuProfile_Sherpa_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC15c_MC15bmuProfile_Sherpa_"
    command += JZ

    command += " &"

    print command
    os.system(command)



if JETM9_25ns_MC_Sherpa_MC15c:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.426131.Sherpa_CT10_jets_JZ1.merge.DAOD_JETM9.e4355_s2608_r7725_r7676_p2812"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.426132.Sherpa_CT10_jets_JZ2.merge.DAOD_JETM9.e4355_s2608_r7725_r7676_p2812"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.426133.Sherpa_CT10_jets_JZ3.merge.DAOD_JETM9.e4355_s2608_r7725_r7676_p2812"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.426134.Sherpa_CT10_jets_JZ4.merge.DAOD_JETM9.e4355_s2608_r7725_r7676_p2812"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.426135.Sherpa_CT10_jets_JZ5.merge.DAOD_JETM9.e4355_s2608_r7725_r7676_p2812"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.426136.Sherpa_CT10_jets_JZ6.merge.DAOD_JETM9.e4355_s2608_r7725_r7676_p2812"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.426137.Sherpa_CT10_jets_JZ7.merge.DAOD_JETM9.e4635_s2726_r7725_r7676_p2812"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.426138.Sherpa_CT10_jets_JZ8.merge.DAOD_JETM9.e4635_s2726_r7725_r7676_p2812"
    if JZ == "JZ9":
      sample = "mc15_13TeV:mc15_13TeV.426139.Sherpa_CT10_jets_JZ9.merge.DAOD_JETM9.e4635_s2726_r7725_r7676_p2812"
    if JZ == "JZ10":
      sample = "mc15_13TeV:mc15_13TeV.426140.Sherpa_CT10_jets_JZ10.merge.DAOD_JETM9.e4635_s2726_r7725_r7676_p2812"
    if JZ == "JZ11":
      sample = "mc15_13TeV:mc15_13TeV.426141.Sherpa_CT10_jets_JZ11.merge.DAOD_JETM9.e4635_s2726_s2183_r7725_r7676_p2812"
    if JZ == "JZ12":
      sample = "mc15_13TeV:mc15_13TeV.426142.Sherpa_CT10_jets_JZ12.merge.DAOD_JETM9.e4635_s2726_s2183_r7725_r7676_p2812"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"2016/xah_run_MC"
    if JetColl is not "All":
      command += "_"
      command += JetColl
    command += ".json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC15c_Sherpa_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC15c_Sherpa_"
    command += JZ

    command += " &"

    print command
    os.system(command)

if JETM9_R21_2015Data:
  for Dataset in R21_2015DataContainers:

    # Setting the correct sample
    sample = Dataset
    dataset = Dataset[26:34]

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    command += " --submitDir="
    command += "Outputs_"
    command += Dataset

    # Config
    command += " --config "+localPATH+"R21/xah_run_2015DATA.json"

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += dataset

    # Logfiles
    command += " > ../GridSubmissions/log_R21_2015data_"
    command += dataset
    command += " 2> ../GridSubmissions/errors_R21_2015data_"
    command += dataset

    command += " &"

    print command
    os.system(command)

if JETM9_R21_2016Data:
  for Dataset in R21_2016DataContainers:

    # Setting the correct sample
    sample = Dataset
    dataset = Dataset[26:34]

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    command += " --submitDir="
    command += "Outputs_"
    command += Dataset

    # Config
    command += " --config "+localPATH+"R21/xah_run_2016DATA.json"

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += dataset

    # Logfiles
    command += " > ../GridSubmissions/log_R21_2016data_"
    command += dataset
    command += " 2> ../GridSubmissions/errors_R21_2016data_"
    command += dataset

    command += " &"

    print command
    os.system(command)

if JETM9_R21_2017Data:
  for Dataset in R21_2017DataContainers:

    # Setting the correct sample
    sample = Dataset
    dataset = Dataset[26:34]

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    command += " --submitDir="
    command += "Outputs_"
    command += Dataset

    # Config
    command += " --config "+localPATH+"R21/xah_run_2017DATA.json"

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += dataset

    # Logfiles
    command += " > ../GridSubmissions/log_R21_2017data_"
    command += dataset
    command += " 2> ../GridSubmissions/errors_R21_2017data_"
    command += dataset

    command += " &"

    print command
    os.system(command)


if JETM9_MC16a_Pythia:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ0":
      sample = "mc16_13TeV:mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ1":
      sample = "mc16_13TeV:mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ2":
      sample = "mc16_13TeV:mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ3":
      sample = "mc16_13TeV:mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ4":
      sample = "mc16_13TeV:mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ5":
      sample = "mc16_13TeV:mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ6":
      sample = "mc16_13TeV:mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ7":
      sample = "mc16_13TeV:mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ8":
      sample = "mc16_13TeV:mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ9":
      sample = "mc16_13TeV:mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ10":
      sample = "mc16_13TeV:mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ11":
      sample = "mc16_13TeV:mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ12":
      sample = "mc16_13TeV:mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.deriv.DAOD_JETM9.e3668_s3126_s3136_r9364_r9315_p3260"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"R21/xah_run_MC16a.json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC16a_Pythia_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC16a_Pythia_"
    command += JZ

    command += " &"

    print command
    os.system(command)

if JETM9_MC16a_Sherpa:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc16_13TeV:mc16_13TeV.426131.Sherpa_CT10_jets_JZ1.deriv.DAOD_JETM9.e4355_s3126_r9364_r9315_p3260"
    if JZ == "JZ2":
      sample = "mc16_13TeV:mc16_13TeV.426132.Sherpa_CT10_jets_JZ2.deriv.DAOD_JETM9.e4355_s3126_r9364_r9315_p3260"
    if JZ == "JZ3":
      sample = "mc16_13TeV:mc16_13TeV.426133.Sherpa_CT10_jets_JZ3.deriv.DAOD_JETM9.e4355_s3126_r9364_r9315_p3260"
    if JZ == "JZ4":
      sample = "mc16_13TeV:mc16_13TeV.426134.Sherpa_CT10_jets_JZ4.deriv.DAOD_JETM9.e4355_s3126_r9364_r9315_p3260"
    if JZ == "JZ5":
      sample = "mc16_13TeV:mc16_13TeV.426135.Sherpa_CT10_jets_JZ5.deriv.DAOD_JETM9.e4355_s3126_r9364_r9315_p3260"
    if JZ == "JZ6":
      sample = "mc16_13TeV:mc16_13TeV.426136.Sherpa_CT10_jets_JZ6.deriv.DAOD_JETM9.e4355_s3126_r9364_r9315_p3260"
    if JZ == "JZ7":
      sample = "mc16_13TeV:mc16_13TeV.426137.Sherpa_CT10_jets_JZ7.deriv.DAOD_JETM9.e4635_s3126_r9364_r9315_p3260"
    if JZ == "JZ8":
      sample = "mc16_13TeV:mc16_13TeV.426138.Sherpa_CT10_jets_JZ8.deriv.DAOD_JETM9.e4635_s3126_r9364_r9315_p3260"
    if JZ == "JZ9":
      sample = "mc16_13TeV:mc16_13TeV.426139.Sherpa_CT10_jets_JZ9.deriv.DAOD_JETM9.e4635_s3126_r9364_r9315_p3260"
    if JZ == "JZ10":
      sample = "mc16_13TeV:mc16_13TeV.426140.Sherpa_CT10_jets_JZ10.deriv.DAOD_JETM9.e4635_s3126_r9364_r9315_p3260"
    if JZ == "JZ11":
      sample = "mc16_13TeV:mc16_13TeV.426141.Sherpa_CT10_jets_JZ11.deriv.DAOD_JETM9.e4635_s3126_r9364_r9315_p3260"
    if JZ == "JZ12":
      sample = "mc16_13TeV:mc16_13TeV.426142.Sherpa_CT10_jets_JZ12.deriv.DAOD_JETM9.e4635_s3126_r9364_r9315_p3260"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"R21/xah_run_MC16a.json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC16a_Sherpa_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC16a_Sherpa_"
    command += JZ

    command += " &"

    print command
    os.system(command)

if JETM9_MC16c_Pythia:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ0":
      sample = "mc16_13TeV:mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_JETM9.e3569_s3126_r9781_r9778_p3260"
    if JZ == "JZ1":
      sample = "mc16_13TeV:mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.deriv.DAOD_JETM9.e3569_s3126_r9781_r9778_p3260"
    if JZ == "JZ2":
      sample = "mc16_13TeV:mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_JETM9.e3668_s3126_r9781_r9778_p3260"
    if JZ == "JZ3":
      sample = "mc16_13TeV:mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.deriv.DAOD_JETM9.e3668_s3126_r9781_r9778_p3260"
    if JZ == "JZ4":
      sample = "mc16_13TeV:mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.deriv.DAOD_JETM9.e3668_s3126_r9781_r9778_p3260"
    if JZ == "JZ5":
      sample = "mc16_13TeV:mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.deriv.DAOD_JETM9.e3668_s3126_r9781_r9778_p3260"
    if JZ == "JZ6":
      sample = "mc16_13TeV:mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.deriv.DAOD_JETM9.e3569_e5984_s3126_r9781_r9778_p3260"
    if JZ == "JZ7":
      sample = "mc16_13TeV:mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.deriv.DAOD_JETM9.e3668_s3126_r9781_r9778_p3260"
    if JZ == "JZ8":
      sample = "mc16_13TeV:mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.deriv.DAOD_JETM9.e3569_s3126_r9781_r9778_p3260"
    if JZ == "JZ9":
      sample = "mc16_13TeV:mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.deriv.DAOD_JETM9.e3569_s3126_r9781_r9778_p3260"
    if JZ == "JZ10":
      sample = "mc16_13TeV:mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.deriv.DAOD_JETM9.e3569_s3126_r9781_r9778_p3260"
    if JZ == "JZ11":
      sample = "mc16_13TeV:mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.deriv.DAOD_JETM9.e3569_s3126_r9781_r9778_p3260"
    if JZ == "JZ12":
      sample = "mc16_13TeV:mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.deriv.DAOD_JETM9.e3668_s3126_r9781_r9778_p3260"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"R21/xah_run_MC16c.json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC16c_Pythia_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC16c_Pythia_"
    command += JZ

    command += " &"

    print command
    os.system(command)

if JETM9_MC16c_Sherpa:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc16_13TeV.426131.Sherpa_CT10_jets_JZ1.deriv.DAOD_JETM9.e4355_s3126_r9781_r9778_p3260"
    if JZ == "JZ2":
      sample = "mc16_13TeV.426132.Sherpa_CT10_jets_JZ2.deriv.DAOD_JETM9.e4355_s3126_r9781_r9778_p3260"
    if JZ == "JZ3":
      sample = "mc16_13TeV.426133.Sherpa_CT10_jets_JZ3.deriv.DAOD_JETM9.e4355_s3126_r9781_r9778_p3260"
    if JZ == "JZ4":
      sample = "mc16_13TeV.426134.Sherpa_CT10_jets_JZ4.deriv.DAOD_JETM9.e4355_s3126_r9781_r9778_p3260"
    if JZ == "JZ5":
      sample = "mc16_13TeV.426135.Sherpa_CT10_jets_JZ5.deriv.DAOD_JETM9.e4355_s3126_r9781_r9778_p3260"
    if JZ == "JZ6":
      sample = "mc16_13TeV.426136.Sherpa_CT10_jets_JZ6.deriv.DAOD_JETM9.e4355_s3126_r9781_r9778_p3260"
    if JZ == "JZ7":
      sample = "mc16_13TeV.426137.Sherpa_CT10_jets_JZ7.deriv.DAOD_JETM9.e4635_s3126_r9781_r9778_p3260"
    if JZ == "JZ8":
      sample = "mc16_13TeV.426138.Sherpa_CT10_jets_JZ8.deriv.DAOD_JETM9.e4635_s3126_r9781_r9778_p3260"
    if JZ == "JZ9":
      sample = "mc16_13TeV.426139.Sherpa_CT10_jets_JZ9.deriv.DAOD_JETM9.e4635_s3126_r9781_r9778_p3260"
    if JZ == "JZ10":
      sample = "mc16_13TeV.426140.Sherpa_CT10_jets_JZ10.deriv.DAOD_JETM9.e4635_s3126_r9781_r9778_p3260"
    if JZ == "JZ11":
      sample = "mc16_13TeV.426141.Sherpa_CT10_jets_JZ11.deriv.DAOD_JETM9.e4635_s3126_r9781_r9778_p3260"
    if JZ == "JZ12":
      sample = "mc16_13TeV.426142.Sherpa_CT10_jets_JZ12.deriv.DAOD_JETM9.e4635_s3126_r9781_r9778_p3260"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"R21/xah_run_MC16c.json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC16c_Sherpa_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC16c_Sherpa_"
    command += JZ

    command += " &"

    print command
    os.system(command)

if JETM9_MC16d_Pythia:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ0":
      sample = "mc16_13TeV:mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_JETM9.e3569_s3126_r10201_r10210_p3401"
    if JZ == "JZ1":
      sample = "mc16_13TeV:mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.deriv.DAOD_JETM9.e3569_s3126_r10201_r10210_p3401"
    if JZ == "JZ2":
      sample = "mc16_13TeV:mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_JETM9.e3668_s3126_r10201_r10210_p3401"
    if JZ == "JZ3":
      sample = "mc16_13TeV:mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.deriv.DAOD_JETM9.e3668_s3126_r10201_r10210_p3401"
    if JZ == "JZ4":
      sample = "mc16_13TeV:mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.deriv.DAOD_JETM9.e3668_s3126_r10201_r10210_p3401"
    if JZ == "JZ5":
      sample = "mc16_13TeV:mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.deriv.DAOD_JETM9.e3668_s3126_r10201_r10210_p3401"
    if JZ == "JZ6":
      sample = "mc16_13TeV:mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.deriv.DAOD_JETM9.e3569_e5984_s3126_r10201_r10210_p3401"
    if JZ == "JZ7":
      sample = "mc16_13TeV:mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.deriv.DAOD_JETM9.e3668_s3126_r10201_r10210_p3401"
    if JZ == "JZ8":
      sample = "mc16_13TeV:mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.deriv.DAOD_JETM9.e3569_s3126_r10201_r10210_p3401"
    if JZ == "JZ9":
      sample = "mc16_13TeV:mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.deriv.DAOD_JETM9.e3569_s3126_r10201_r10210_p3401"
    if JZ == "JZ10":
      sample = "mc16_13TeV:mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.deriv.DAOD_JETM9.e3569_s3126_r10201_r10210_p3401"
    if JZ == "JZ11":
      sample = "mc16_13TeV:mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.deriv.DAOD_JETM9.e3569_s3126_r10201_r10210_p3401"
    if JZ == "JZ12":
      sample = "mc16_13TeV:mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.deriv.DAOD_JETM9.e3668_s3126_r10201_r10210_p3401"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"R21/xah_run_MC16d.json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC16d_Pythia_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC16d_Pythia_"
    command += JZ

    command += " &"

    print command
    os.system(command)

if JETM9_MC16d_Sherpa:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc16_13TeV.426131.Sherpa_CT10_jets_JZ1.deriv.DAOD_JETM9.e4355_s3126_r10201_r10210_p3401"
    if JZ == "JZ2":
      sample = "mc16_13TeV.426132.Sherpa_CT10_jets_JZ2.deriv.DAOD_JETM9.e4355_s3126_r10201_r10210_p3401"
    if JZ == "JZ3":
      sample = "mc16_13TeV.426133.Sherpa_CT10_jets_JZ3.deriv.DAOD_JETM9.e4355_s3126_r10201_r10210_p3401"
    if JZ == "JZ4":
      print "JZ4 slice is missing, exiting"
      sys.exit(0)
    if JZ == "JZ5":
      sample = "mc16_13TeV.426135.Sherpa_CT10_jets_JZ5.deriv.DAOD_JETM9.e4355_s3126_r10201_r10210_p3401"
    if JZ == "JZ6":
      sample = "mc16_13TeV.426136.Sherpa_CT10_jets_JZ6.deriv.DAOD_JETM9.e4355_s3126_r10201_r10210_p3401"
    if JZ == "JZ7":
      print "JZ7 slice is missing, exiting"
      sys.exit(0)
    if JZ == "JZ8":
      sample = "mc16_13TeV.426138.Sherpa_CT10_jets_JZ8.deriv.DAOD_JETM9.e4635_s3126_r10201_r10210_p3401"
    if JZ == "JZ9":
      sample = "mc16_13TeV.426139.Sherpa_CT10_jets_JZ9.deriv.DAOD_JETM9.e4635_s3126_r10201_r10210_p3401"
    if JZ == "JZ10":
      sample = "mc16_13TeV.426140.Sherpa_CT10_jets_JZ10.deriv.DAOD_JETM9.e4635_s3126_r10201_r10210_p3401"
    if JZ == "JZ11":
      sample = "mc16_13TeV.426141.Sherpa_CT10_jets_JZ11.deriv.DAOD_JETM9.e4635_s3126_r10201_r10210_p3401"
    if JZ == "JZ12":
      sample = "mc16_13TeV.426142.Sherpa_CT10_jets_JZ12.deriv.DAOD_JETM9.e4635_s3126_r10201_r10210_p3401"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    # Config
    command += " --config "+localPATH+"R21/xah_run_MC16d.json"

    command += " --submitDir="
    command += "Outputs_"
    command += JZ

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir=1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > ../GridSubmissions/log_MC16d_Sherpa_"
    command += JZ
    command += " 2> ../GridSubmissions/errors_MC16d_Sherpa_"
    command += JZ

    command += " &"

    print command
    os.system(command)

