#!/usr/bin/python

import os, sys

param=sys.argv

R21 = True

JetColl = "All"  # 2LC and 6LC
#JetColl = "2LC" # not supported in R21 
#JetColl = "6LC" # not supported in R21

DatasetType = "MC16a" # other: MC16c, data15, data16, data17, MC15

localPATH = ""   # only for R21
  
sample = "PATH/sample.root"

##################################################################################
#
##################################################################################
config = ""

MC = False
if "MC" in DatasetType:
  MC = True

if not MC:
  config  = "xah_run_"
  if R21:
    if "15" in DatasetType:
      config += "2015"
    elif "16" in DatasetType:
      config += "2016"
    elif "17" in DatasetType:
      config += "2017"
    else:
      print "Not recognised DatasetType, exiting"
      sys.exit(0)
  config += "DATA"
  if JetColl is not "All":
    config += "_"
    config += JetColl
if MC:
  config = "xah_run_MC"
  if R21:
    if "16a" in DatasetType:
      config += "16a"
    elif "16c" in DatasetType:
      config += "16c"
    else:
     print "Not recognised DatasetType, exiting"
     sys.exit(0)
  if JetColl is not "All":
    config += "_"	  
    config += JetColl

command = "xAH_run.py --files "
command += sample

command += " --submitDir="
command += "Outputs/Output_"
command += config

command += " --config "
if R21:
  command += localPATH+"/source/xAODAnaHelpers/data/"
if not R21:
  if "16" in DatasetType:
    command += "2016/"
  else:
    command += "2015/"
else:
  command += "R21/"
command += config
command += ".json"
command += " direct"

# Logfile's Path
command += "> LogFiles/log_"
command += config
command += " 2> "

# Errors File's Path
command += "ErrFiles/errors_"
command += config
command += " &"

print command
os.system(command)

