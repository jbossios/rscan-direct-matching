#! /bin/bash

mkdir GridSubmissions
cd source
setupATLAS
lsetup rucio "asetup AnalysisBase,21.2.19" panda
voms-proxy-init -voms atlas 
cd ../build
cmake ../source
make -j
source ${CMTCONFIG}/setup.sh
cd ..
        
