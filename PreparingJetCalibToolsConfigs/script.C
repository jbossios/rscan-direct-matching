{

  // Check
  bool LC_2 = true;
  bool LC_6 = true;
  bool LC_8 = false;  

  // Check
  Version = "05122018";

  // Smoothed?
  bool smoothed = false;

  // R21
  TString MCversion   = "MC16a";
  TString Dataversion = "2015+2016data";
  //TString MCversion   = "MC16c";
  //TString Dataversion = "2017data";
  //TString MCversion   = "MC16d";
  //TString Dataversion = "2017data";

  ///////////////////////////////////////////////////////
  // DO NOT MODIFY
  ///////////////////////////////////////////////////////

  // In this path you should have the folder ROOTfiles
  TString PATH = "../";

  // R20.7
  //TString MCversion   = "MC15cPythia_MC15bMuProfile";
  //TString Dataversion = "2015data";
  //TString MCversion   = "MC15cPythia";
  //TString Dataversion = "2016data";

  TString in_PATH = PATH;  
  in_PATH += "ROOTfiles/DirectMatching/SmoothingTool_Outputs/";
  in_PATH += Dataversion;
  in_PATH += "_vs_";
  in_PATH += MCversion;
  in_PATH += "/";

  // 2LC (Corr)
  TString f2name = in_PATH; f2name += "2LC/Correction_";
  if(!smoothed) f2name += "not";
  f2name += "Smoothed.root";
  TH1D* h2Corr;
  TH1D* h2Absolute;
  if(LC_2){
    TFile f2(f2name.Data(),"read");
    h2Corr     = (TH1D*)f2.Get("AntiKt2LCTopo_EtaInterCalibration"); 
    h2Corr->SetDirectory(0); // to decouple it from the open file directory
    h2Absolute = (TH1D*)f2.Get("AntiKt2LCTopo_InsituCalib"); // empty
    h2Absolute->SetDirectory(0);
    f2.Close();
  }

  // 6LC (Corr)
  TString f6name = in_PATH; f6name += "6LC/Correction_";
  if(!smoothed) f6name += "not";
  f6name += "Smoothed.root";
  TH1D* h6Corr;
  TH1D* h6Absolute;
  if(LC_6){
    TFile f6(f6name.Data(),"read");
    h6Corr     = (TH1D*)f6.Get("AntiKt6LCTopo_EtaInterCalibration"); 
    h6Corr->SetDirectory(0);
    h6Absolute = (TH1D*)f6.Get("AntiKt6LCTopo_InsituCalib"); // empty
    h6Absolute->SetDirectory(0);
    f6.Close();
  }

  // 8LC (Corr)
  TString f8name = in_PATH; f8name += "8LC/Correction_";
  if(!smoothed) f8name += "not";
  f8name += "Smoothed.root";
  TH1D* h8Corr;
  TH1D* h8Absolute;
  if(LC_8){
    TFile f8(f8name.Data(),"read");
    h8Corr     = (TH1D*)f8.Get("AntiKt8LCTopo_EtaInterCalibration"); 
    h8Corr->SetDirectory(0);
    h8Absolute = (TH1D*)f8.Get("AntiKt8LCTopo_InsituCalib"); // empty
    h8Absolute->SetDirectory(0);
    f8.Close();
  }

  TString outputFile = PATH;
  outputFile += "ROOTfiles/DirectMatching/JetCalibToolsConfigs/";
  outputFile += Dataversion;
  outputFile += "_vs_";
  outputFile += MCversion;
  outputFile += "/InsituCalibration_Rscan_";
  if(!smoothed) outputFile += "notSmoothed_";
  outputFile += Dataversion;
  outputFile += "_"; outputFile += Version;
  if(LC_2) outputFile += "_2LC";
  if(LC_6) outputFile += "_6LC";
  if(LC_8) outputFile += "_8LC";
  outputFile += ".root";

  // Output file -> Final Insitu config with all the Insitu configs
  TFile fout(outputFile.Data(),"RECREATE");

  fout.cd();
  if(LC_2){
    h2Corr->Write();
    h2Absolute->Write();
  }
  if(LC_6){
    h6Corr->Write();
    h6Absolute->Write();
  }
  if(LC_8){
    h8Corr->Write();
    h8Absolute->Write();
  }
  fout.Close();

}

