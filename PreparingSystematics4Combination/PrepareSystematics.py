#!/usr/bin/python
import os, sys
import array
import math
import inspect
from ROOT import *

# Check
jetColl = "2LC"
#jetColl = "6LC"

ZJetStudy = True
ApplyEtaRebinning = True

# Check
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

Systs = [
  "DeltaRUp",
  "DeltaRDown",
  "IsolationUp",
  "IsolationDown",
  "JVTUp",
  "JVTDown",
  "MCSherpa",
]

###################################################################
## DO NOT MODIFY
###################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

nSigma                = 1
maxRebin              = 5

gROOT.SetBatch(True)  # so does not pop up plots!

Version = DataVersion + "_vs_" + MCVersion

# Eta bins
netaBins = 60
Etabins = [-3.0,-2.9,-2.8,-2.7,-2.6,-2.5,-2.4,-2.3,-2.2,-2.1,-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1 ,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0]

etabins =array.array('d',Etabins)

def PrintFrame():
  callerframerecord = inspect.stack()[1]
  frame = callerframerecord[0]
  info = inspect.getframeinfo(frame)
  print(info.lineno)

if ApplyEtaRebinning:
  netaBins = 16 
  EtaBins = [-3.,-2.8, -2.5, -1.8, -1.3, -1.0, -0.7,-0.2, 0., 0.2, 0.7, 1.0, 1.3, 1.8, 2.5, 2.8, 3.]
  etabins = array.array('d', EtaBins)

# Functions
def checkFile(File,FileName):
  if not File:
    print FileName+" not found, exiting"
    sys.exit(0)

###########################################
# Rebin pT bins based on significance
###########################################
print ">> Rebinn pT binning until systematic uncertainty is significant <<"
Files     = []
FileNames = []
for syst in Systs:
  # Open Input Files
  InputFile  = PATH +"ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/" + Version+ "/" + jetColl + "/" + "SystematicUncertainty"
  if ZJetStudy:
    InputFile += "_ZJetStudy"
  InputFile += "_"
  InputFile += syst
  if ApplyEtaRebinning:
    InputFile += "_etaRebinned"
  InputFile += ".root"
  print "Opening: " + InputFile
  File = TFile.Open(InputFile,"Read")
  checkFile(File,InputFile)
  Files.append(File)
  FileNames.append(syst)
# Loop over Files
counter      = 0 
TGraphs      = []
Significance = []
PostSignificance = []
# Loop over Files
for File in Files:
  print "Running "+FileNames[counter]
  # Get TGraph for each eta bin
  for eta in range(0,netaBins):
    name_tmp  = "Systematic_vs_pTRscan_Eta_"
    yeta = eta+15
    if ApplyEtaRebinning:
      yeta = eta
    name_tmp += str(yeta)
    graph = File.Get(name_tmp)
    if not graph:
      print "Graph: ",name_tmp, " not found, exiting"
      sys.exit(0)
    npoints = graph.GetN() 
    firstnotzero  = True
    lowbin        = True
    bins1  = []
    cent1  = []
    cont1  = []
    err1   = []
    mu     = 0.0
    sig    = 0.0
    center = 0.0
    upEdge = 0.0
    firstx = 0.0
    gSignificance = TGraphAsymmErrors()
    gPostSignificance = TGraphAsymmErrors()
    outName  = "Significane_"+FileNames[counter]
    outName += "_Eta_"
    outName += str(yeta)
    gSignificance.SetName(outName)
    gPostSignificance.SetName(outName+"_postRebinning")
    # Loop over points
    noRebinCounter = 0
    for point in range(0,npoints):
      x = Double(0) # pT
      y = Double(0) # systematic uncertainty
      graph.GetPoint(point,x,y)
      exl = graph.GetErrorXlow(point) 
      exh = graph.GetErrorXhigh(point)
      eyl = graph.GetErrorYlow(point)
      eyh = graph.GetErrorYhigh(point)
      if eyl<1E-100 or eyh<1E-100:
        continue
      if firstnotzero:
        bins1.append(x-exl)
      firstnotzero = False
      if lowbin:
        firstx = x
        lowbin = False
      center = x
      upEdge = x+exh
      mu  += y/eyl/eyl if eyl!=0 else 0.
      sig += 1.0/eyl/eyl if eyl!=0 else 0.
      
      # Plot significance vs pTrscan
      gSignificance.SetPoint(point,x,abs(mu)/sqrt(sig))
      gSignificance.SetPointError(point,exl,exh,0.,0.) #only interested in bin size

      # Now it's significant!
      if (sig > 0 and abs(mu)/sqrt(sig) > nSigma) or noRebinCounter>maxRebin-2: # n sigma significance
        bins1.append(upEdge)
        if firstx==center:
          cent1.append(x)
        else:
          cent1.append(0.5*(center+firstx))
        cont1.append(mu/sig)
        err1.append(1.0/sqrt(sig))
        mu  = 0.0
        sig = 0.0
        lowbin = True
        noRebinCounter = 0
      else:
        noRebinCounter += 1
    Significance.append(gSignificance)

    # merge bins not rebinned
    if mu!=0:
      bins1.append(upEdge)
      cont1.append(mu/sig if sig!=0 else 0.)
      ncent1 = len(cent1)
      if ncent1 != 0: # some bins were merged already
        cent1.append(0.5*(upEdge+bins1[ncent1]))
      else:
        x0 = Double(0)
    	y0 = Double(0)
    	graph.GetPoint(0,x0,y0)
        cent1.append(0.5*(upEdge+(x0-graph.GetErrorXlow(0))))
      err1.append(1.0/sqrt(sig) if sig!=0 else 0.)

    # Fill output graph with rebinned systematic uncertainties
    npoints1 = len(cont1)
    outgraph = TGraphAsymmErrors(npoints1)
    outName  = FileNames[counter]
    outName += "_Eta_"
    outName += str(yeta)
    outgraph.SetName(outName)
    for point in range(0,npoints1):
      outgraph.SetPoint(point,cent1[point],cont1[point])
      outgraph.SetPointError(point,cent1[point]-bins1[point],bins1[point+1]-cent1[point],err1[point],err1[point])
      x0 = Double(0)
      y0 = Double(0)
      outgraph.GetPoint(point,x0,y0)
      exl = outgraph.GetErrorXlow(point)
      exh = outgraph.GetErrorXhigh(point)
      eyl = outgraph.GetErrorYlow(point)
      eyh = outgraph.GetErrorYhigh(point)
      mu  = y0/eyl/eyl if eyl!=0 else 0.
      sig = 1.0/eyl/eyl if eyl!=0 else 0.
      gPostSignificance.SetPoint(point,x0,abs(mu)/sqrt(sig))
      gPostSignificance.SetPointError(point,exl,exh,0.,0.) #only interested in bin size
    TGraphs.append(outgraph)
    PostSignificance.append(gPostSignificance)
  counter = counter+1

# Output ROOT File
outFileName = PATH + "ROOTfiles/DirectMatching/SystematicInputsToCombination/" + Version
outFileName += "/RebinnedSystematics_vs_ptrscan"
if ZJetStudy:
    outFileName += "_ZJetStudy"
outFileName += "_" + jetColl
if ApplyEtaRebinning:
  outFileName += "_etaRebinned"
outFileName += ".root"
print("Writing graphs to "+outFileName)
outFile = TFile(outFileName,"RECREATE")
print("Number of graphs: "+str(len(TGraphs)))
outFile.cd()
for graph in TGraphs:
  graph.Write()
for sig in Significance:
  sig.Write()
for postsig in PostSignificance:
  postsig.Write()
outFile.Close()

################################################################3
# Prepare final inputs (systematics in original pT binning)
################################################################3
print ">> Prepare final inputs (systematics in original pT binning) <<"
TGraphs = [] # Final Graphs
# Open Input File
InputFile  = PATH +"ROOTfiles/DirectMatching/SystematicInputsToCombination/" + Version
InputFile += "/RebinnedSystematics_vs_ptrscan"
if ZJetStudy:
  InputFile += "_ZJetStudy"
InputFile += "_" + jetColl
if ApplyEtaRebinning:
  InputFile += "_etaRebinned"
InputFile += ".root"
print "Opening: " + InputFile
file = TFile.Open(InputFile,"Read")
if not file:
  print InputFile+" is not found, exiting"
  sys.exit(0)
# Loop over systs
for syst in Systs:
  print "Running "+syst
  InputFile  = PATH +"ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/" + Version+ "/" + jetColl + "/" + "SystematicUncertainty"
  if ZJetStudy:
    InputFile += "_ZJetStudy"
  InputFile += "_"
  InputFile += syst
  if ApplyEtaRebinning:
    InputFile +="_etaRebinned"
  InputFile += ".root"
  print "Opening: " + InputFile
  File = TFile.Open(InputFile,"Read")
  checkFile(File,InputFile)
  # Loop over etabins
  for eta in range(0,netaBins):
    yeta = eta+15
    if ApplyEtaRebinning:
      yeta = eta
    # Open graph with original binning
    name_tmp    = "Systematic_vs_pTRscan_Eta_"
    name_tmp   += str(yeta)
    ORIGgraph   = File.Get(name_tmp)
    checkFile(File,name_tmp)
    nORIGpoints = ORIGgraph.GetN()
    # Open graph with rebinned systematics
    name_tmp  = syst+"_Eta_"
    name_tmp += str(yeta)
    graph     = file.Get(name_tmp)
    checkFile(file,name_tmp)
    npoints   = graph.GetN()
    # Final Graph
    nFinalPoints = 0
    Xs    = []
    Ys    = []
    errXl = []
    errXh = []
    errYl = []
    errYh = []
    # Loop over original binning
    oneUsed = False
    for point in range(0,nORIGpoints):
      x = Double(0) # pT
      y = Double(0) # original systematic uncertainty
      ORIGgraph.GetPoint(point,x,y)
      exl = ORIGgraph.GetErrorXlow(point)
      exh = ORIGgraph.GetErrorXhigh(point)
      eyl = ORIGgraph.GetErrorYlow(point)
      eyh = ORIGgraph.GetErrorYhigh(point)
      if eyl<1E-100 or eyh<1E-100:
        if not oneUsed:
          continue
      newX     = x
      newY     = Double(0)
      newerryl = Double(0)
      newerryh = Double(0)
      binFound = False
      maxx = 0
      # get rebinning value
      for pnt in range(0,npoints):
        rebX = Double(0)
        rebY = Double(0)
        graph.GetPoint(pnt,rebX,rebY)
        rebexl = graph.GetErrorXlow(pnt)
        rebexh = graph.GetErrorXhigh(pnt)
        xmin = rebX - rebexl
        xmax = rebX + rebexh
        if xmax > maxx:
          maxx = xmax
        if newX < xmax and xmin < newX: # x within merged bin
          newY = rebY
          newerryl = graph.GetErrorYlow(pnt)
          newerryh = graph.GetErrorYhigh(pnt)
          binFound = True
          oneUsed  = True
          break
      if not binFound and newX < maxx:
        print "ERROR: original point not found in rebinnned systematics, exiting"
        sys.exit(0)
      if binFound:
        Xs.append(newX)
        Ys.append(newY)
        errXl.append(exl)
        errXh.append(exh)
        errYl.append(newerryl)
        errYh.append(newerryh)
        nFinalPoints += 1
    # Output graph with final syst
    outName  = "Final_"+syst
    outName += "_Eta_"
    outName += str(yeta)
    outgraph = TGraphAsymmErrors(nFinalPoints)
    outgraph.SetName(outName)
    # Fill final graph
    for point in range(0,nFinalPoints):
      outgraph.SetPoint(point,Xs[point],Ys[point])
      outgraph.SetPointError(point,errXl[point],errXh[point],errYl[point],errYh[point])
    TGraphs.append(outgraph)

# Remove dummy points
FinalTGraphs = []
for graph in TGraphs:
  pointCounter = 0
  finalgraph = TGraphAsymmErrors()
  finalgraph.SetName(graph.GetName()+'_')
  for point in range(0,graph.GetN()):
    x = Double(0)
    y = Double(0)
    graph.GetPoint(point,x,y)
    exl = graph.GetErrorXlow(point)
    exh = graph.GetErrorXhigh(point)
    eyl = graph.GetErrorYlow(point)
    eyh = graph.GetErrorYhigh(point)
    if eyh >=0.07 and x>0 :
      break
    finalgraph.SetPoint(pointCounter,x,y)
    finalgraph.SetPointError(pointCounter,exl,exh,eyl,eyh)
    pointCounter += 1
  FinalTGraphs.append(finalgraph)

# Create TH1D with the eta binnning
h_etaBinning = TH1D("EtaBinning","",netaBins,etabins)
for Bin in range(1,netaBins+1):
  h_etaBinning.SetBinContent(Bin,Bin)

# Output ROOT File
outFileName = PATH + "ROOTfiles/DirectMatching/SystematicInputsToCombination/" + Version
outFileName += "/Final_Systematics_vs_ptrscan"
if ZJetStudy:
  outFileName += "_ZJetStudy"
outFileName += "_" + jetColl
if ApplyEtaRebinning:
  outFileName += "_etaRebinned"
outFileName += ".root"
print("Writing graphs to "+outFileName)
outFile = TFile(outFileName,"RECREATE")
print("Number of graphs: "+str(len(TGraphs)))
outFile.cd()
h_etaBinning.Write()
for graph in FinalTGraphs:
  graph.Write()
outFile.Close()

