INSTRUCTIONS

Needed: tree from DxAOD sample with Rscan jets

1) Use PreparingTrees/MC/movingfiles.py

2) Use PreparingTrees/MC/renamingfiles.py

(reweighting scripts can be ommited as no sample weights are used to determine bJES uncertainty)

3) "selectBranches_final.C" creates smaller tree with all needed variables

4) "Plotter_leadingJet.C" - currently this file has multi-purpose
    a) store histograms in root-file
    b) fit the responses
    c) plotter