import pyAMI.client
import pyAMI.atlas.api as AtlasAPI
import os, sys

client = pyAMI.client.Client('atlas')
AtlasAPI.init()

######################################################################################################
# EDIT
######################################################################################################

samples = [ # Example
  "mc15_13TeV.363364.Sherpa_NNPDF30NNLO_Zmumu_Pt0_70_CVetoBVeto.merge.DAOD_JETM3.e4716_s2726_r7725_r7676_p3327" ,
]

###########################################################################################################
# DO NOT MODIFY
###########################################################################################################

# Loop over samples
for dataset in samples:
  run = dataset.split(".",2)
  info = str(run[1])
  prov = AtlasAPI.get_dataset_prov(client,dataset)
  arr = prov['node']
  for dict in arr:
    d = dict['logicalDatasetName']
    if run[1] in d and "minbias" not in d:
      if "evgen" in d:
        dd = AtlasAPI.get_dataset_info(client,d)
        xsec = dd[0]['crossSection']
        eff = dd[0]['genFiltEff']
        break
  info += " " + str(xsec) + " " + str(eff) 
  print info			  
	
