// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
//const TString version("/afs/cern.ch/user/j/jbossios/work/public/SM/new_xAODJetAnalysis/macros_and_results/macros/my_plotting_from_tree/");
//TString PATH("/afs/cern.ch/user/j/jbossios/work/public/SM/new_xAODJetAnalysis/macros_and_results/macros/MC15TreetoROOTMacro/");
TString PATH("/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu_Rscan/ROOTfiles/");

// Version
//TString versionData("v_02/");
TString versionData("v_07/");

bool MCScaling = true;

// Data Luminosity
//double DataLuminosity = 91.3951; // pb-1 70 version
//double DataLuminosity = 244.323419; // pb-1 72 version
//double DataLuminosity = 333.522; // pb-1 72 version
//double DataLuminosity = 1022.6; // pb-1 76 version
double DataLuminosity = 121.958; // pb-1 76 version
//double DataLuminosity = 380.808; // pb-1 76 version G data only
//double DataLuminosity = 460.404; // pb-1 76 version E data only
//double DataLuminosity = 126.852; // pb-1 76 version E3 data only
//double DataLuminosity = 43.4195; // pb-1 76 version E2 data only

bool PDF = true;

TString strLuminosity = "0.12";


//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TGraphPlotter.h"
#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

//---------------
// Main Function
//---------------

int main(){

  SetAtlasStyle();

  //----------
  // Triggers
  //----------

//  Triggers.push_back("HLT_j15");
//  Triggers.push_back("HLT_j25");
//  Triggers.push_back("HLT_j35");
  Triggers.push_back("HLT_j45");
  Triggers.push_back("HLT_j55");
  Triggers.push_back("HLT_j60");
  Triggers.push_back("HLT_j85");
  Triggers.push_back("HLT_j110");
  Triggers.push_back("HLT_j150");
  Triggers.push_back("HLT_j175");
  Triggers.push_back("HLT_j200");
  Triggers.push_back("HLT_j260");
  Triggers.push_back("HLT_j360");

  std::map<std::string,double> minPtTriggers;
  minPtTriggers["HLT_j45"] = 70.;
  minPtTriggers["HLT_j55"] = 70.;
  minPtTriggers["HLT_j60"] = 85.;
  minPtTriggers["HLT_j85"] = 116.;
  minPtTriggers["HLT_j110"] = 152.;
  minPtTriggers["HLT_j150"] = 194.;
  minPtTriggers["HLT_j175"] = 216.;
  minPtTriggers["HLT_j200"] = 240.;
  minPtTriggers["HLT_j260"] = 318.;
  minPtTriggers["HLT_j360"] = 408.;

  //-------------------
  //Opening input files
  //-------------------

  // Data
  TString inputfileAll = PATH;
  inputfileAll += "Data/";
  inputfileAll += versionData;
  //inputfileAll += "Results_v_01.root";
  //inputfileAll += "Results_v_03.root";
  inputfileAll += "Data_25ns_v_11.root";
  TFile* tinAll = new TFile(inputfileAll,"read");

  std::cout << "Opening: " << inputfileAll << std::endl;

  //TString OutputFolder = "TurnOnxSections/78_version/";
  //TString OutputFolder = "TurnOnxSections/78_version/Eta3/";
  //TString OutputFolder = "TurnOnxSections/78_version/Eta2/";
  //TString OutputFolder = "TurnOnxSections/78_version/Eta1/";
  //TString OutputFolder = "TurnOnxSections/v_02/4EM/pTAvg/";
  //TString OutputFolder = "TurnOnxSections/v_02/6LC/pTAvg/";
  TString OutputFolder = "TurnOnxSections/v_07/pTAvg_type3/";

  TGraphPlotter *Base = new TGraphPlotter("Base");
  Base->SetFiles(1,tinAll);
  Base->SetFiles(2,tinAll);
  Base->SetOutputFolder(OutputFolder);
  Base->SetTitle("X","p_{T} [GeV]");
  Base->SetTitle("Y","Efficiency");
  if(!PDF) Base->SetEPSFormat();
  Base->SetLogx();
  //Base->SetLegend(1,"Data (stat)");
  //Base->SetLegend(2,"MMHT2014");
  Base->SetmcScale(1);
  Base->noAutoXlimits();
  Base->SetRange("X",0,2000);
  //Base->SetRange("Y",-0.1,1.2);
  Base->SetRange("Y",-0.1,2.0);
  //Base->Debug();
  Base->SetLuminosity(strLuminosity);
    
  TGraphPlotter *TG = 0;
  TString outName;
  //TString hBaseName = "h_pt_j0_4EM_";
  //TString hBaseName = "h_pt_Avg_4EM_";
  //TString hBaseName = "h_pt_j0_6LC_";
  //TString hBaseName = "h_pt_Avg_6LC_";
  TString hBaseName = "h_pTavg_";
  TString hName1, hName2;
  for(unsigned int i=0;i<Triggers.size()-1;++i){
    outName = Triggers.at(i+1);
    outName += "vs";
    outName += Triggers.at(i);
    hName1 = hBaseName; 
    hName1 += Triggers.at(i+1);
    hName2 = hBaseName; 
    hName2 += Triggers.at(i);
    TG = new TGraphPlotter(outName.Data(),hName1.Data(),hName2.Data());
    TG->ApplyFormat(Base);
    //TG->Debug();
    TG->SetType("pt_allLeft");

    //if(i==5) TG->SetRange("X",50,500);
    TG->SetRange("X",50,500);
    //if(i>5 && i<=6) TG->SetRange("X",100,500);
    //if(i>6 && i<=8) TG->SetRange("X",100,500);
    if(i>5) TG->SetRange("X",200,800);

    // Lines
    for(int j=0;j<36;j++){

      if(i<=5 && ptBins[j]<30) continue;
      if(i<5 && ptBins[j]>300) continue;
      if(i==5 && ptBins[j]>500) continue;
      if(i>5 && i<=6 && ptBins[j]<100) continue;
      if(i>5 && i<=6 && ptBins[j]>500) continue;
      if(i>6 && i<=8 && ptBins[j]<100) continue;
      if(i>6 && i<=8 && ptBins[j]>500) continue;
      if(i>8 && ptBins[j]<200) continue;
      if(i>8 && ptBins[j]>800) continue;

      if(ptBins[j]==minPtTriggers[Triggers.at(i+1)]){
        TG->DrawLine("Y",-0.1,1.2,ptBins[j],kRed);
      }
      else{TG->DrawLine("Y",-0.1,1.2,ptBins[j]);}

    }

    //TG->Debug(); 
    TG->Write(); 
    delete TG;
  }

  // Comparing xSections
  TH1DPlotter *xSecBase = new TH1DPlotter("xSecBase");
  xSecBase->SetFiles(1,tinAll);
  xSecBase->SetFiles(2,tinAll);
  xSecBase->SetOutputFolder(OutputFolder);
  xSecBase->SetTitle("X","p_{T}^{Avg} [GeV]");
  xSecBase->SetTitle("Y","d#sigma/dp_{T}^{Avg} [pb/GeV]");
  if(!PDF) xSecBase->SetEPSFormat();
  xSecBase->SetLogx();
  //xSecBase->SetLogy();
  xSecBase->SetmcScale(1);
  //xSecBase->SetType("pt");
  xSecBase->SetType("pt_allLeft");
  xSecBase->SetRange("X",85,__xmax_pT);
  xSecBase->SetRange("Y",0,1.2);
  xSecBase->SetRatioRange("Y",0,1.2);
  //xSecBase->DisableRatio();
  //xSecBase->EnableRatio();
  xSecBase->OnlyRatio();
  //xSecBase->SetRatioRange("Y",0,1.2);
  xSecBase->SetRebin(2);
  xSecBase->DivideBinbyBinWidth(1);
  xSecBase->DivideBinbyBinWidth(2);
  xSecBase->SetLuminosity(strLuminosity);
  xSecBase->NoLegends();

  TH1DPlotter *TH = 0;
  //hBaseName = "h_pt_Avg_4EM_";
  //hBaseName = "h_pt_Avg_6LC_";
  hBaseName = "h_pTavg_";
  for(unsigned int i=0;i<Triggers.size()-1;++i){
    outName = "xSection_";
    outName += Triggers.at(i+1);
    outName += "vs";
    outName += Triggers.at(i);
    hName1 = hBaseName; 
    hName1 += Triggers.at(i+1);
    hName2 = hBaseName; 
    hName2 += Triggers.at(i);
    TH = new TH1DPlotter(outName.Data(),hName1.Data(),hName2.Data());
    TH->ApplyFormat(xSecBase);
    TH->SetLegend(1,Triggers.at(i+1));
    TH->SetLegend(2,Triggers.at(i));
    if(Triggers.at(i+1)=="HLT_j360" && hBaseName.Contains("6LC")) TH->PlotLine("Y",0,1.2,405,kRed);
    if(Triggers.at(i+1)=="HLT_j260" && hBaseName.Contains("6LC")) TH->PlotLine("Y",0,1.2,350,kRed);
    if(Triggers.at(i+1)=="HLT_j200" && hBaseName.Contains("6LC")) TH->PlotLine("Y",0,1.2,250,kRed);
    if(Triggers.at(i+1)=="HLT_j175" && hBaseName.Contains("6LC")) TH->PlotLine("Y",0,1.2,210,kRed);
    if(Triggers.at(i+1)=="HLT_j150" && hBaseName.Contains("6LC")) TH->PlotLine("Y",0,1.2,200,kRed);
    if(Triggers.at(i+1)=="HLT_j110" && hBaseName.Contains("6LC")) TH->PlotLine("Y",0,1.2,150,kRed);

    if(Triggers.at(i+1)=="HLT_j360" && hBaseName.Contains("4EM")) TH->PlotLine("Y",0,1.2,405,kRed);
    if(Triggers.at(i+1)=="HLT_j260" && hBaseName.Contains("4EM")) TH->PlotLine("Y",0,1.2,350,kRed);
    if(Triggers.at(i+1)=="HLT_j200" && hBaseName.Contains("4EM")) TH->PlotLine("Y",0,1.2,250,kRed);
    if(Triggers.at(i+1)=="HLT_j175" && hBaseName.Contains("4EM")) TH->PlotLine("Y",0,1.2,210,kRed);
    if(Triggers.at(i+1)=="HLT_j150" && hBaseName.Contains("4EM")) TH->PlotLine("Y",0,1.2,200,kRed);
    if(Triggers.at(i+1)=="HLT_j110" && hBaseName.Contains("4EM")) TH->PlotLine("Y",0,1.2,150,kRed);

    //TH->SetRebin(4);
    //TH->Debug();
    TH->Write(); 
    delete TH;
  }

  
  return 0;

}


