// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("/afs/cern.ch/work/j/jbossios/public/JetEtmiss/Insitu_Rscan/DeriveInsituRscan/v_8/");

bool MCScaling = false;

TString JetCollection = "6LC";

// Input Files
TString inputData("RootFiles/SmoothedCorrection/v_13_v_08/");
TString inputMC("RootFiles/Correction/v_13_v_08/");

// Data Luminosity
double DataLuminosity = 3209.25; // pb-1 v_13
TString strLuminosity = "3.2";

const int nEtaBins = 90;
TString EtaBins[nEtaBins+1] = {"-4.5", "-4.4", "-4.3", "-4.2", "-4.1", "-4.0", "-3.9", "-3.8", "-3.7", "-3.6", "-3.5", "-3.4", "-3.3", "-3.2", "-3.1", "-3.0", "-2.9", "-2.8", "-2.7", "-2.6", "-2.5", "-2.4", "-2.3", "-2.2", "-2.1", "-2.0", "-1.9", "-1.8", "-1.7", "-1.6", "-1.5", "-1.4", "-1.3", "-1.2", "-1.1", "-1.0", "-0.9", "-0.8", "-0.7", "-0.6", "-0.5", "-0.4", "-0.3", "-0.2", "-0.1" ,"0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0", "2.1", "2.2", "2.3", "2.4", "2.5", "2.6", "2.7", "2.8", "2.9", "3.0", "3.1", "3.2", "3.3", "3.4", "3.5", "3.6", "3.7", "3.8", "3.9", "4.0", "4.1", "4.2", "4.3", "4.4", "4.5"};

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(){

  SetAtlasStyle();

  //-------------------
  //Opening input files
  //-------------------

  // Data
  TString inputfileData = "/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu_Rscan/Revolution/ROOTfiles/TreetoHists_Outputs/Data/v_13/6LC/Data_25ns_numericalInversion_v_27.root";
  std::cout << "Opening: " << inputfileData << std::endl;
  TFile* tinData = new TFile(inputfileData,"read");

  // MC
  TString inputfileMC = "/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu_Rscan/Revolution/ROOTfiles/TreetoHists_Outputs/MC/v_08/6LC/MC15b_25ns_Pythia_noPRW_JZAll_numericalInversion_PRW_v_27.root";
  //TString inputfileMC = "/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu_Rscan/Revolution/ROOTfiles/TreetoHists_Outputs/MC/v_08/6LC/MC15b_25ns_Pythia_noPRW_JZAll_numericalInversion_v_27.root";
  std::cout << "Opening: " << inputfileMC << std::endl;
  TFile* tinMC = new TFile(inputfileMC,"read");

  double mcScale = 1.;
  if(MCScaling) mcScale = HelperFunctions::GetMCFactor(tinData, tinMC, "h_j0_pt_with_wgt_y_inclusive", DataLuminosity);

  TString Normtype = "Integral_1_2";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;
  if(MCScaling){
    Normtype = "Factor_2"; //Only MC
    NormFactor2 = DataLuminosity*mcScale;
  }

  TString outputFolder  = "PRW/";

  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tinData);
  pTBase->SetFiles(2,tinMC);
  pTBase->SetOutputFolder(outputFolder);
  pTBase->SetTitle("X","<#mu>");
  pTBase->SetTitle("Y","");
  if(!PDF) pTBase->SetEPSFormat();
  //pTBase->SetLogx();
  pTBase->SetLegend(1,"Data");
  pTBase->SetLegend(2,"MC");
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetmcScale(mcScale);
  pTBase->SetRange("X",0,40);
  pTBase->EnableRatio();
  //pTBase->YRangeManual();
  pTBase->JetColl(JetCollection);
  pTBase->ShowErrors();
  pTBase->SetRatioRange("Y",0,2);
  pTBase->SetRange("Y",0,0.3);
  pTBase->SetLuminosity("");

  TString name("h_mu_inclusive");
  TString outputName = JetCollection;
  outputName += "/Mu_AfterPRW";
  //outputName += "/Mu_BeforePRW";
  std::cout << "Opening: " << name << std::endl;

  TH1DPlotter* Mu = new TH1DPlotter(outputName.Data(),name.Data(),name.Data());
  Mu->ApplyFormat(pTBase);
  Mu->YRangeManual();
  Mu->Debug();
  Mu->Write();

  name = "h_NPV_inclusive";
  outputName = JetCollection;
  //outputName += "/NPV_AfterPRW";
  outputName += "/NPV_BeforePRW";
  std::cout << "Opening: " << name << std::endl;

  TH1DPlotter* NPV = new TH1DPlotter(outputName.Data(),name.Data(),name.Data());
  NPV->ApplyFormat(pTBase);
  NPV->SetTitle("X","NPV");
  NPV->YRangeManual();
  NPV->Debug();
  NPV->Write();


  name = "j0_pT_Ref";
  TString name2 = "j1_pT_Ref";
  outputName = JetCollection; outputName += "/pT_MC";
  TH1DPlotter* pT_j0 = new TH1DPlotter(outputName.Data(),name.Data(),name2.Data());
  pT_j0->ApplyFormat(pTBase);
  pT_j0->SetFiles(1,tinMC);
  pT_j0->SetFiles(2,tinMC);
  pT_j0->SetLegend(1,"j0");
  pT_j0->SetLegend(2,"j1");
  pT_j0->SetLogx();
  pT_j0->SetLogy();
  pT_j0->SetRange("X",0,3000);
  pT_j0->Write();

  /*
  name = "h_mu_15";
  outputName = JetCollection;
  outputName += "Mu_15";
  TH1DPlotter* Mu2 = new TH1DPlotter(outputName.Data(),name.Data(),name.Data());
  Mu2->ApplyFormat(pTBase);
  Mu2->Debug();
  Mu2->Write();
  */


  return 0;

}


