// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("../ROOTfiles/DirectMatching/TriggerEmulation/");

bool MCScaling = false;

TString Dataset;
TString JetColl;
TString InputFile;
TString Thresholds;

// Data Luminosity
TString strLuminosity = "3.2";

bool PDF = true;
bool m_debug = false;

TString var = "pTrscan";
//TString var = "pTavg";
//TString var = "pTavg_rscan";

//-----------------------------------------------------------------------------------------------------------------------------------------
// DO NOT EDIT
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(int argc, char* argv[]){

  std::string m_dataset = "";
  std::string m_jetCollection = "";  
  std::string m_inputFile = "";
  std::string m_thresholds = "";

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--dataset=")   != std::string::npos) m_dataset = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

    if ( opt.find("--inputFile=")   != std::string::npos) m_inputFile = v[1];

    if ( opt.find("--thresholds=")   != std::string::npos) m_thresholds = v[1];

  }//End: Loop over input options 
  
  Dataset = m_dataset;
  JetColl = m_jetCollection;
  InputFile = m_inputFile;
  Thresholds = m_thresholds;
 
  // PATH of Input File       	
  if(Dataset=="2015") PATH += "R21_2015data";
  if(Dataset=="2016") PATH += "R21_2016data";
  if(Dataset=="2017") PATH += "2017data";
  PATH += "/";

  // Output Folder
  TString OutputFolder = "../Plots_DirectMatching/TriggerEmulation/";
  if(Dataset=="2015") OutputFolder += "R21_2015data/";
  if(Dataset=="2016") OutputFolder += "R21_2016data/";
  if(Dataset=="2017") OutputFolder += "2017data/";
  OutputFolder += JetColl; 

  SetAtlasStyle();

  //----------
  // Triggers
  //----------
  
  if(Dataset=="2015"){
    Triggers.push_back("HLT_j15");
    Triggers.push_back("HLT_j25");
    Triggers.push_back("HLT_j35");
    Triggers.push_back("HLT_j45");
    Triggers.push_back("HLT_j55");
    Triggers.push_back("HLT_j60");
    Triggers.push_back("HLT_j85");
    Triggers.push_back("HLT_j110");
    Triggers.push_back("HLT_j150");
    Triggers.push_back("HLT_j175");
    Triggers.push_back("HLT_j200");
    Triggers.push_back("HLT_j260");
    Triggers.push_back("HLT_j320");
    Triggers.push_back("HLT_j360");
  } else if(Dataset=="2016"){
    Triggers.push_back("HLT_j15");
    Triggers.push_back("HLT_j25");
    Triggers.push_back("HLT_j35");
    Triggers.push_back("HLT_j45");
    Triggers.push_back("HLT_j55");
    Triggers.push_back("HLT_j60");
    Triggers.push_back("HLT_j85");
    Triggers.push_back("HLT_j110");
    Triggers.push_back("HLT_j150");
    Triggers.push_back("HLT_j175");
    Triggers.push_back("HLT_j260");
    Triggers.push_back("HLT_j380");
    Triggers.push_back("HLT_j400");
  } else if(Dataset=="2017"){
    Triggers.push_back("HLT_j15");
    Triggers.push_back("HLT_j25");
    Triggers.push_back("HLT_j35");
    Triggers.push_back("HLT_j45");
    Triggers.push_back("HLT_j60");
    Triggers.push_back("HLT_j85");
    Triggers.push_back("HLT_j110");
    Triggers.push_back("HLT_j175");
    Triggers.push_back("HLT_j260");
    Triggers.push_back("HLT_j360");
    Triggers.push_back("HLT_j420");
  }

  std::vector<int> m_triggerThresholds;

  std::string token;
  std::istringstream thresholds(m_thresholds);
  while ( std::getline(thresholds, token, ',') ){
    m_triggerThresholds.push_back(atoi(token.c_str()));
  }

  // Protection
  if(m_triggerThresholds.size()!=Triggers.size()){
    std::cout << "Number of triggers and thresholds don't match, exiting" << std::endl;
    return 0;
  }

  std::map<std::string,int> TriggerThresholds;
  for(unsigned int itrigger=0;itrigger<Triggers.size();++itrigger){
    TriggerThresholds[Triggers.at(itrigger)] = m_triggerThresholds.at(itrigger);
  }
  
  //--------------------
  // Opening input files
  //--------------------

  // MC
  TString input = PATH; input += InputFile;
  std::cout << "Opening: " << input << std::endl;
  TFile* tin = new TFile(input,"read");

  double mcScale = 1.;

  TString Normtype = "";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;

  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tin);
  pTBase->SetFiles(2,tin);
  pTBase->SetOutputFolder(OutputFolder);
  if(var.Contains("avg")) pTBase->SetTitle("X","p_{T}^{Avg} [GeV]");
  else{pTBase->SetTitle("X","p_{T}^{R-scan} [GeV]");}
  pTBase->SetTitle("Y","Efficiency");
  if(!PDF) pTBase->SetEPSFormat();
  pTBase->SetLogx();
  pTBase->SetLegend(1,"Efficiency");
  pTBase->SetLegend(2,"");
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetmcScale(mcScale);
  pTBase->SetRange("X",55.,1000.);
  pTBase->OnlyRatio();
  pTBase->YRangeManual();
  pTBase->JetColl(JetColl);
  //pTBase->ShowErrors();
  pTBase->SetRatioRange("Y",0,1.1);
  pTBase->SetLuminosity(strLuminosity);
  pTBase->BinomialErrors();
  pTBase->NoErrorsRatioPlot();

  TString num;
  TString den;
  TString outputName;
  TString Type;
  TH1DPlotter *pT_Inclusive;
  for(unsigned int j=0;j<Triggers.size();++j){
    num = "pT_offline_num_";
    num += Triggers.at(j);
    std::cout << "Getting: " << num << std::endl;
    den = "pT_offline_den_";
    den += Triggers.at(j);
    std::cout << "Getting: " << den << std::endl;
    outputName = Triggers.at(j);
    pT_Inclusive = new TH1DPlotter(outputName.Data(),num.Data(),den.Data());
    pT_Inclusive->ApplyFormat(pTBase);

    // Show Threshold
    pT_Inclusive->PlotLine("Y",0,1.1,TriggerThresholds[Triggers.at(j)]);

    if(Triggers.at(j)=="HLT_j85") pT_Inclusive->SetRange("X",10,300);
    if(Triggers.at(j)=="HLT_j60") pT_Inclusive->SetRange("X",10,300);
    if(Triggers.at(j)=="HLT_j55") pT_Inclusive->SetRange("X",10,300);
    if(Triggers.at(j)=="HLT_j45") pT_Inclusive->SetRange("X",10,300);
    if(Triggers.at(j)=="HLT_j420") pT_Inclusive->SetRange("X",100.,550.);
    if(Triggers.at(j)=="HLT_j400") pT_Inclusive->SetRange("X",100.,550.);
    if(Triggers.at(j)=="HLT_j380") pT_Inclusive->SetRange("X",100.,550.);
    pT_Inclusive->SetType("allLeft");
    if(Triggers.at(j)=="HLT_j15" || Triggers.at(j)=="HLT_j25" || Triggers.at(j)=="HLT_jj35"){
      //pT_Inclusive->UnSetLogx();
      pT_Inclusive->SetRange("X",10,300);
    }
    pT_Inclusive->SetMoreLogLabels();
    if(m_debug) pT_Inclusive->Debug();
    pT_Inclusive->Write();
    delete pT_Inclusive;
  }
  
  return 0;

}


