// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

const int npTavgBins = 43;
TString pTavgBins[npTavgBins+1] = {"15", "20", "25", "35", "45", "55", "70", "85", "100", "116", "134", "152", "172", "194", "216", "240", "264", "290", "318", "346", "376", "408", "442", "478", "516", "556", "598", "642", "688", "736", "786", "838", "894", "952", "1012", "1076", "1162", "1310", "1530", "1992", "2500", "3137", "3937", "4941"};

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu_Rscan/Projections/v_1/");

bool MCScaling = false;

bool m_emScale = true;
bool m_pileupScale = false;
bool m_jesScale = false;

TString JetCollection = "6LC";

// Input Files
TString inputMC("RootFiles/MC/v_08/");
TString inputData("RootFiles/Data/v_07/");

// Data Luminosity
//double DataLuminosity = 91.3951; // pb-1 70 version
//double DataLuminosity = 244.323419; // pb-1 72 version
//double DataLuminosity = 333.522; // pb-1 72 version
//double DataLuminosity = 508.657; // pb-1 75 version
//double DataLuminosity = 1022.6; // pb-1 76 version
double DataLuminosity = 1714.32; // pb-1 76 version
TString strLuminosity = "1.4";

// Procedure to obtain DataLuminosity factor
// Scale MC to Data
// by 44.4068/1000. 
// 44.4068 is the Luminosity of Data obtained with ATLAS Luminosity calculator and divied by 1000. to pass pb-1 to fb-1
// Then change 44.4068 to have Data/MC > 1 (example 30) and then multiply this number (30) by Data/MC ratio (30*1.15)
// by 30*1.15/1000. (1/1000: DataLuminosity pb-1 to fb-1) 1000 pb-1 = 1 fb-1

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(){

  SetAtlasStyle();

  inputMC += JetCollection;
  inputMC += "/Outputs_vs_pT";
  if(m_pileupScale) inputMC += "_pileupScale";
  else if(m_jesScale) inputMC += "_jesScale";
  inputMC += ".root";
  inputData += JetCollection;
  inputData += "/Outputs_vs_pT";
  if(m_pileupScale) inputData += "_pileupScale";
  else if(m_jesScale) inputData += "_jesScale";
  inputData += ".root";

  //----------
  // Triggers
  //----------

  Triggers.push_back("HLT_j15");
  Triggers.push_back("HLT_j25");
  Triggers.push_back("HLT_j35");
  Triggers.push_back("HLT_j55");
  Triggers.push_back("HLT_j60");
  Triggers.push_back("HLT_j85");
  Triggers.push_back("HLT_j110");
  Triggers.push_back("HLT_j150");
  Triggers.push_back("HLT_j175");
  Triggers.push_back("HLT_j200");
  Triggers.push_back("HLT_j260");
  Triggers.push_back("HLT_j360");

  //-------------------
  //Opening input files
  //-------------------

  // Data
  TString inputfileData = PATH;
  inputfileData += inputData;
  TFile* tinData = new TFile(inputfileData,"read");

  // MC
  TString inputfileMC = PATH;
  inputfileMC += inputMC;
  TFile* tinMC = new TFile(inputfileMC,"read");

  double mcScale = 1.;
  if(MCScaling) mcScale = HelperFunctions::GetMCFactor(tinData, tinMC, "h_j0_pt_with_wgt_y_inclusive", DataLuminosity);

  TString Normtype = "Integral_1_2";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;
  if(MCScaling){
    Normtype = "Factor_2"; //Only MC
    NormFactor2 = DataLuminosity*mcScale;
  }

  TString outputFolder  = "OOC_Studies/";
  outputFolder  += JetCollection;
  outputFolder  += "/";
  if(m_pileupScale) outputFolder  += "pileupScale/";
  if(m_jesScale) outputFolder  += "jesScale/";

  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tinData);
  pTBase->SetFiles(2,tinMC);
  pTBase->SetOutputFolder(outputFolder);
  pTBase->SetTitle("X","Relative difference");
  pTBase->SetTitle("Y","");
  if(!PDF) pTBase->SetEPSFormat();
  pTBase->SetLegend(1,"Data");
  pTBase->SetLegend(2,"MC");
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetmcScale(mcScale);
  pTBase->SetRange("X",-0.001,0.1);
  if(m_pileupScale) pTBase->SetRange("X",0,0.5);
  else if(m_jesScale) pTBase->SetRange("X",0,0.5);
  //if(JetCollection=="2LC") pTBase->SetRange("Y",0.8,1.3);
  //else if(JetCollection=="6LC") pTBase->SetRange("Y",0.9,1.3);
  //else if(JetCollection=="8LC") pTBase->SetRange("Y",0.9,1.3);
  pTBase->SetRatioRange("Y",0,2);
  //pTBase->SetRange("Y",0,0.05);
  pTBase->EnableRatio();
  //pTBase->YRangeManual();
  pTBase->SetRebin(4);
  pTBase->JetColl(JetCollection);
  pTBase->ShowErrors();
  pTBase->SetLuminosity(strLuminosity);

  TString name;
  TString outputName;
  TString Type = "";
  TH1DPlotter *pT_Inclusive;
  for(int i=0;i<10;++i){
    for(int j=5;j<npTavgBins;++j){
      name = "RelDiff_vs_pT_Eta_";
      name += i;
      name += "_projy_";
      name += pTavgBins[j];
      name += "_";
      name += pTavgBins[j+1];
      outputName = "RelDiff_vs_pT_";
      if(j<10) outputName += "0";
      outputName += j;
      /*
      outputName += pTavgBins[j];
      outputName += "_";
      outputName += pTavgBins[j+1];
      */
      outputName += "_Eta_";
      outputName += i;
      Type = "pT_";
      Type += pTavgBins[j];
      Type += "_";
      Type += pTavgBins[j+1];
      std::cout << "Getting " << name << " histogram" << std::endl;
      std::cout << "Type: " << Type << std::endl;
      pT_Inclusive = new TH1DPlotter(outputName.Data(),name.Data(),name.Data());
      pT_Inclusive->ApplyFormat(pTBase);
      pT_Inclusive->SetType(Type.Data());
      //pT_Inclusive->Debug();
      pT_Inclusive->FitRatio();
      pT_Inclusive->Write();
      delete pT_Inclusive;
    }
  }

  return 0;

}


