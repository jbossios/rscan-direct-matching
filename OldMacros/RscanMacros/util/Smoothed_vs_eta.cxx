// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("");

bool MCScaling = false;
bool m_debug   = false;

//TString JetCollection = "2LC";
TString JetCollection = "6LC";
//TString JetCollection = "8LC";

TString MCVersion = ""; 
TString DataVersion = "";

// Data Luminosity
double DataLuminosity = 3209.25; // pb-1 v_13
TString strLuminosity = "3.2";

const int nEtaBins = 90;
TString EtaBins[nEtaBins+1] = {"-4.5", "-4.4", "-4.3", "-4.2", "-4.1", "-4.0", "-3.9", "-3.8", "-3.7", "-3.6", "-3.5", "-3.4", "-3.3", "-3.2", "-3.1", "-3.0", "-2.9", "-2.8", "-2.7", "-2.6", "-2.5", "-2.4", "-2.3", "-2.2", "-2.1", "-2.0", "-1.9", "-1.8", "-1.7", "-1.6", "-1.5", "-1.4", "-1.3", "-1.2", "-1.1", "-1.0", "-0.9", "-0.8", "-0.7", "-0.6", "-0.5", "-0.4", "-0.3", "-0.2", "-0.1" ,"0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0", "2.1", "2.2", "2.3", "2.4", "2.5", "2.6", "2.7", "2.8", "2.9", "3.0", "3.1", "3.2", "3.3", "3.4", "3.5", "3.6", "3.7", "3.8", "3.9", "4.0", "4.1", "4.2", "4.3", "4.4", "4.5"};

// pT binning
const int npTBins = 43+3;
double pTBins[npTBins+1] = {15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941., 6200., 7780., 9763.};

const int nstrpTBins = 43+3;
TString strpTBins[npTBins+1] = {"15", "20", "25", "35", "45", "55", "70", "85", "100", "116", "134", "152", "172", "194", "216", "240", "264", "290", "318", "346", "376", "408", "442", "478", "516", "556", "598", "642", "688", "736", "786", "838", "894", "952", "1012", "1076", "1162", "1310", "1530", "1992", "2500", "3137", "3937", "4941", "6200", "7780", "9763"};

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(int argc, char* argv[]){

  std::string m_mcVersion = "";
  std::string m_dataVersion = "";
  std::string m_jetCollection = "";
  std::string m_systName = ""; // Default Nominal
  std::string m_path = "";
  //bool m_numericalInversion = false;

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--mcVersion=")   != std::string::npos) m_mcVersion = v[1];
    if ( opt.find("--dataVersion=")   != std::string::npos) m_dataVersion = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

    /*if ( opt.find("--nInversion=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
      if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
    }*/

    if ( opt.find("--debug=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debug= true;
      if (v[1].find("FALSE") != std::string::npos) m_debug= false;
    }


    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
      if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
      if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
      if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
      if (v[1].find("MCsherpa") != std::string::npos) m_systName = "MCSherpa";
    }

  }//End: Loop over input options

  JetCollection   = m_jetCollection;
  MCVersion       = m_mcVersion;
  DataVersion     = m_dataVersion;
  PATH            = m_path;

  SetAtlasStyle();


  //--------------------
  // Opening input files
  //--------------------

  // Data
  TString inputfile = PATH;
  inputfile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorrvsEta/";
  inputfile += DataVersion;
  inputfile += "_vs_";
  inputfile += MCVersion;
  inputfile += "/";
  inputfile += JetCollection;
  inputfile += "/SmoothedCorr_vs_Eta";
  if(m_systName!=""){
    inputfile += "_";
    inputfile += m_systName;
  }
  inputfile += ".root";
  std::cout << "Opening: " << inputfile << std::endl;
  TFile* tin = new TFile(inputfile,"read");

  double mcScale = 1.;

  TString Normtype = "";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;
  if(MCScaling){
    Normtype = "Factor_2"; //Only MC
    NormFactor2 = DataLuminosity*mcScale;
  }

  TString outputFolder = "Plots_DirectMatching/Smoothing/";
  outputFolder += DataVersion;
  outputFolder += "_vs_";
  outputFolder += MCVersion;
  outputFolder += "/";
  outputFolder += JetCollection;
  outputFolder += "/";

  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tin);
  pTBase->SetFiles(2,tin);
  pTBase->SetOutputFolder(outputFolder);
  pTBase->SetTitle("X","#eta");
  pTBase->SetTitle("Y","Relative Response");
  if(!PDF) pTBase->SetEPSFormat();
  //pTBase->SetLogx();
  pTBase->SetLegend(1,"SmoothedCorr");
  pTBase->SetLegend(2,"Corr");
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetmcScale(mcScale);
  pTBase->SetRange("X",-3,3);
  if(JetCollection=="2LC") pTBase->SetRange("Y",0.8,1.3);
  else if(JetCollection=="6LC") pTBase->SetRange("Y",0.9,1.3);
  else if(JetCollection=="8LC") pTBase->SetRange("Y",0.9,1.3);
  pTBase->EnableRatio();
  pTBase->YRangeManual();
  pTBase->JetColl(JetCollection);
  pTBase->ShowErrors();
  //pTBase->SetMoreLogLabels();
  pTBase->SetRatioRange("Y",0.8,1.2);
  pTBase->SetLuminosity(strLuminosity);

  TString name;
  TString name2;
  TString outputName;
  TString Type;
  TH1DPlotter *pT_Inclusive;
  for(int i=1;i<=nstrpTBins;++i){
    name = "Corr_vs_Eta_pT_";
    name += i;
    name2 = "SmoothedCorr_vs_Eta_pT_";
    name2 += i;
    outputName = "Comparison_vs_Eta_pT_";
    if(i<10){
     outputName += 0;
    }
    outputName += i;
    outputName += "_";
    if(m_systName=="") outputName += "Nominal";
    if(m_systName!="") outputName += m_systName;
    if(m_debug){
      std::cout << "Openning: " << name << std::endl;
      std::cout << "Openning: " << name2 << std::endl;
    }
    pT_Inclusive = new TH1DPlotter(outputName.Data(),name2.Data(),name.Data());
    pT_Inclusive->ApplyFormat(pTBase);
    TString legend = strpTBins[i-1];
    legend += " #leq p_{T} [GeV] < ";
    legend += strpTBins[i];
    pT_Inclusive->AddLegend(legend);
    if(m_debug) pT_Inclusive->Debug();
    pT_Inclusive->Write();
    delete pT_Inclusive;
  }

  return 0;

}


