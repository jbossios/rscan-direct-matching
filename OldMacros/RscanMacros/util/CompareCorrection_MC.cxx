// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("/afs/cern.ch/work/j/jbossios/public/JetEtmiss/Insitu_Rscan/DeriveInsituRscan/v_8/");

bool MCScaling = false;

TString JetCollection = "6LC";

// Input Files
TString inputData("RootFiles/SmoothedCorrection/v_13_v_08/");
TString inputMC("RootFiles/Correction/v_13_v_08/");

// Data Luminosity
double DataLuminosity = 3209.25; // pb-1 v_13
TString strLuminosity = "3.2";

const int nEtaBins = 90;
TString EtaBins[nEtaBins+1] = {"-4.5", "-4.4", "-4.3", "-4.2", "-4.1", "-4.0", "-3.9", "-3.8", "-3.7", "-3.6", "-3.5", "-3.4", "-3.3", "-3.2", "-3.1", "-3.0", "-2.9", "-2.8", "-2.7", "-2.6", "-2.5", "-2.4", "-2.3", "-2.2", "-2.1", "-2.0", "-1.9", "-1.8", "-1.7", "-1.6", "-1.5", "-1.4", "-1.3", "-1.2", "-1.1", "-1.0", "-0.9", "-0.8", "-0.7", "-0.6", "-0.5", "-0.4", "-0.3", "-0.2", "-0.1" ,"0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0", "2.1", "2.2", "2.3", "2.4", "2.5", "2.6", "2.7", "2.8", "2.9", "3.0", "3.1", "3.2", "3.3", "3.4", "3.5", "3.6", "3.7", "3.8", "3.9", "4.0", "4.1", "4.2", "4.3", "4.4", "4.5"};

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(){

  SetAtlasStyle();

  inputMC += JetCollection;
  inputMC += "/Corr_vs_pTrscan_numericalInversion.root";
  inputData += JetCollection;
  inputData += "/SmoothedCorr_vs_pTRscan.root";

  //-------------------
  //Opening input files
  //-------------------

  // Data
  TString inputfileData = PATH;
  inputfileData += inputData;
  std::cout << "Opening: " << inputfileData << std::endl;
  TFile* tinData = new TFile(inputfileData,"read");

  // MC
  TString inputfileMC = PATH;
  inputfileMC += inputMC;
  std::cout << "Opening: " << inputfileMC << std::endl;
  TFile* tinMC = new TFile(inputfileMC,"read");

  double mcScale = 1.;
  if(MCScaling) mcScale = HelperFunctions::GetMCFactor(tinData, tinMC, "h_j0_pt_with_wgt_y_inclusive", DataLuminosity);

  TString Normtype = "";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;
  if(MCScaling){
    Normtype = "Factor_2"; //Only MC
    NormFactor2 = DataLuminosity*mcScale;
  }

  TString outputFolder  = "Smoothed_correction/";

  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tinData);
  pTBase->SetFiles(2,tinMC);
  pTBase->SetOutputFolder(outputFolder);
  pTBase->SetTitle("X","p_{T} [GeV]");
  pTBase->SetTitle("Y","Relative Response");
  if(!PDF) pTBase->SetEPSFormat();
  pTBase->SetLogx();
  pTBase->SetLegend(1,"SmoothedCorr");
  pTBase->SetLegend(2,"Corr");
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetmcScale(mcScale);
  pTBase->SetRange("X",55.,1310);
  if(JetCollection=="2LC") pTBase->SetRange("Y",0.8,1.3);
  else if(JetCollection=="6LC") pTBase->SetRange("Y",0.9,1.3);
  else if(JetCollection=="8LC") pTBase->SetRange("Y",0.9,1.3);
  pTBase->EnableRatio();
  pTBase->YRangeManual();
  pTBase->JetColl(JetCollection);
  pTBase->ShowErrors();
  //pTBase->SetMoreLogLabels();
  pTBase->SetRatioRange("Y",__ymin_Ratio_pT,__ymax_Ratio_pT);
  pTBase->SetLuminosity(strLuminosity);

  TString name;
  TString name2;
  TString outputName;
  TString Type;
  TH1DPlotter *pT_Inclusive;
  //for(int i=0;i<=30;++i){
  for(int i=15;i<=74;++i){
    name = "Corr_vs_pT_Eta_";
    name += i;
    name2 = "SmoothedCorr_vs_pT_Eta_";
    name2 += i;
    outputName = JetCollection;
    outputName += "/Comparison_vs_pT_Eta_";
    outputName += i;
    std::cout << "Opening: " << name << std::endl;
    pT_Inclusive = new TH1DPlotter(outputName.Data(),name2.Data(),name.Data());
    pT_Inclusive->ApplyFormat(pTBase);
    Type = "Eta";
    if(i<10) Type += "0";
    Type += i;
    pT_Inclusive->SetType(Type.Data());
    if(i<10) pT_Inclusive->SetRatioRange("Y",0.85,1.1);
    if(i>=10){
      pT_Inclusive->SetRatioRange("Y",0.85,1.2);
      pT_Inclusive->SetRange("X",55,1000);
    }
    if(i==29){
      pT_Inclusive->SetRange("X",55,500);
    }
    TString legend = EtaBins[i];
    legend += " #leq #eta < ";
    legend += EtaBins[i+1];
    pT_Inclusive->AddLegend(legend);
    pT_Inclusive->Debug();
    pT_Inclusive->Write();
    delete pT_Inclusive;
  }

  return 0;

}


