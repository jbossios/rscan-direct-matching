// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("");

bool MCScaling = false;

TString JetCollection;
TString MCVersion;
TString DataVersion;

// Data Luminosity
double DataLuminosity = 3209.25; // pb-1 v_13 and v_14
TString strLuminosity = "3.2";

const int nEtaBins = 90;
TString EtaBins[nEtaBins+1] = {"-4.5", "-4.4", "-4.3", "-4.2", "-4.1", "-4.0", "-3.9", "-3.8", "-3.7", "-3.6", "-3.5", "-3.4", "-3.3", "-3.2", "-3.1", "-3.0", "-2.9", "-2.8", "-2.7", "-2.6", "-2.5", "-2.4", "-2.3", "-2.2", "-2.1", "-2.0", "-1.9", "-1.8", "-1.7", "-1.6", "-1.5", "-1.4", "-1.3", "-1.2", "-1.1", "-1.0", "-0.9", "-0.8", "-0.7", "-0.6", "-0.5", "-0.4", "-0.3", "-0.2", "-0.1" ,"0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0", "2.1", "2.2", "2.3", "2.4", "2.5", "2.6", "2.7", "2.8", "2.9", "3.0", "3.1", "3.2", "3.3", "3.4", "3.5", "3.6", "3.7", "3.8", "3.9", "4.0", "4.1", "4.2", "4.3", "4.4", "4.5"};

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(int argc, char* argv[]){

  std::string m_mcVersion = "";
  std::string m_dataVersion = "";
  std::string m_jetCollection = "";
  std::string m_systName = ""; // Default Nominal
  std::string m_path = "";
  bool m_numericalInversion = false;
  bool m_mc15b = false; 
  bool m_mc15c = false;  
  bool m_mc16a = false;  
  bool m_mc16c = false;  
  bool m_mc16d = false;  
  bool m_closurePlots = false;

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--mcVersion=")   != std::string::npos) m_mcVersion = v[1];
    if ( opt.find("--dataVersion=")   != std::string::npos) m_dataVersion = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];
    
    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

    if ( opt.find("--closurePlots=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_closurePlots= true;
      if (v[1].find("FALSE") != std::string::npos) m_closurePlots= false;
    }
    
    if ( opt.find("--nInversion=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
      if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
    }

    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
      if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
      if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
      if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
      if (v[1].find("MCsherpa") != std::string::npos) m_systName = "MCSherpa";
    }

  }//End: Loop over input options

  JetCollection   = m_jetCollection;
  MCVersion       = m_mcVersion;
  DataVersion     = m_dataVersion;
  PATH            = m_path;

  SetAtlasStyle(); // ATLAS Style

  // Set correct luminosity
  if(DataVersion=="2016data") strLuminosity = "32.9";
  if(DataVersion=="2015+2016data") strLuminosity = "36.2";
  if(DataVersion=="2017data")      strLuminosity = "43.6";

  if(MCVersion.Contains("MuProfile")) m_mc15b = true;
  else if(MCVersion.Contains("MC15")) m_mc15c = true;
  else if(MCVersion.Contains("MC16a")) m_mc16a = true;
  else if(MCVersion.Contains("MC16c")) m_mc16c = true;
  else if(MCVersion.Contains("MC16d")) m_mc16d = true;

  //--------------------
  // Opening input files
  //--------------------
  //Data
  TString InputFile1 = "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/Data/";
  InputFile1 += DataVersion;
  InputFile1 += "/";
  InputFile1 += JetCollection; 
  InputFile1 += "/Outputs_vs_pT";
  if(m_systName!="" && m_systName!="MCSherpa"){
    InputFile1 += "_";
    InputFile1 += m_systName;
  }
  if(m_closurePlots) InputFile1 += "_Insitu";
  InputFile1 += ".root";
  
  //MC
  TString InputFile2 = "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/MC/";
  InputFile2 += MCVersion;
  InputFile2 += "/";
  InputFile2 += JetCollection; 
  if(m_mc15c)InputFile2 += "/MC15c_";
  else if(m_mc15b) InputFile2 += "MC15bmuProfile_";
  else if(m_mc16a) InputFile2 += "MC16a_";
  else if(m_mc16c) InputFile2 += "MC16c_";
  else if(m_mc16d) InputFile2 += "MC16d_";
  InputFile2 += "Pythia_Outputs_vs_pT";
  if(m_systName!=""){
    InputFile2 += "_";
    InputFile2 += m_systName;
  }
  InputFile2 += ".root";

  // Output File
  TString OutputFolder = "Plots_DirectMatching/Response_Data_vs_MC/";
  OutputFolder += DataVersion;
  OutputFolder += "_vs_";
  OutputFolder += MCVersion;
  OutputFolder += "/";
  OutputFolder += JetCollection;

  TString input1 = PATH; input1 += InputFile1;
  
  TString input2 = PATH; input2 += InputFile2;
  std::cout << "Opening: " << input1 << std::endl;
  TFile* tinData = new TFile(input1,"read");

    
  std::cout << "Opening: " << input2 << std::endl;
  TFile* tinMC = new TFile(input2,"read");
  double mcScale = 1.;


  TString Normtype = "";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;



  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tinData);
  pTBase->SetFiles(2,tinMC);
  pTBase->SetOutputFolder(OutputFolder);
  pTBase->SetTitle("X","p_{T}^{Rscan} [GeV]");
  if(m_numericalInversion) pTBase->SetTitle("X","p_{T}^{Rscan} [GeV]");
  pTBase->SetTitle("Y","Relative Response");
  if(!PDF) pTBase->SetEPSFormat();
  pTBase->SetLogx();
  pTBase->SetLegend(1,"Data");
  pTBase->SetLegend(2,"MC");
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetmcScale(mcScale);
  double maxpT = 3000;
  if(JetCollection=="2LC"){
    maxpT = 1991.9;
    pTBase->SetRange("Y",0.8,1.3);
  }
  else if(JetCollection=="6LC"){
    maxpT = 1991.9; 
    pTBase->SetRange("Y",0.9,1.3);
  }
  else if(JetCollection=="8LC"){
    maxpT = 3000.;
    pTBase->SetRange("Y",0.9,1.3);
  }
  pTBase->SetRange("X",55.,maxpT);
  if(JetCollection=="2LC") pTBase->SetRange("X",25.,maxpT);
  if(JetCollection=="6LC") pTBase->SetRange("X",45.,maxpT);
  if(JetCollection=="8LC") pTBase->SetRange("X",70.,maxpT);
  pTBase->EnableRatio();
  pTBase->YRangeManual();
  pTBase->JetColl(JetCollection);
  pTBase->ShowErrors();
  pTBase->SetMoreLogLabels();
  pTBase->SetRatioRange("Y",__ymin_Ratio_pT,__ymax_Ratio_pT);
  if(m_closurePlots)  pTBase->SetRatioRange("Y",0.95,1.05);
  pTBase->SetLuminosity(strLuminosity);

  TString name;
  TString outputName;
  TString Type;
  TH1DPlotter *pT_Inclusive;
  for(int i=15;i<=74;++i){
    name = "Response_vs_pTRscan_Eta_";
    name += i;
    outputName = "/Response_vs_pTRscan_Eta_";
    outputName += i;
    if(m_systName!=""){
      outputName += "_";
      outputName += m_systName;
    }
    if(m_closurePlots) outputName += "_Insitu";
    pT_Inclusive = new TH1DPlotter(outputName.Data(),name.Data(),name.Data());
    pT_Inclusive->ApplyFormat(pTBase);
    Type = "Eta";
    if(i<10) Type += "0";
    Type += i;
    pT_Inclusive->SetType(Type.Data());
    double min = 55;
    if(JetCollection=="6LC") min = 85;
    if(!m_closurePlots) pT_Inclusive->SetRatioRange("Y",0.85,1.2);
    TString legend = EtaBins[i];
    legend += " #leq #eta < ";
    legend += EtaBins[i+1];
    pT_Inclusive->AddLegend(legend);
    if(m_closurePlots) pT_Inclusive->PlotLine("X", min, maxpT, 1.01);
    if(m_closurePlots) pT_Inclusive->PlotLine("X", min, maxpT, 0.99);
    //pT_Inclusive->Debug();
    pT_Inclusive->Write();
    delete pT_Inclusive;
  }

  return 0;

}


