// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
//const TString version("/afs/cern.ch/user/j/jbossios/work/public/SM/new_xAODJetAnalysis/macros_and_results/macros/my_plotting_from_tree/");
TString PATH("/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu_Rscan/DeriveInsituRscan/v_4/");

TString JetCollection = "8LC";

TString pTAvgDef = "";
//TString pTAvgDef = "_pTRefAvg";
//TString pTAvgDef = "_pTRscanAvg";

// Input Files
TString inputMC("RootFiles/MC/v_08/");
TString inputData("RootFiles/Closure/MC/v_08/");

// Data Luminosity
//double DataLuminosity = 91.3951; // pb-1 70 version
//double DataLuminosity = 244.323419; // pb-1 72 version
//double DataLuminosity = 333.522; // pb-1 72 version
//double DataLuminosity = 508.657; // pb-1 75 version
//double DataLuminosity = 1022.6; // pb-1 76 version
double DataLuminosity = 1714.32; // pb-1 76 version
TString strLuminosity = "1.4";

// Procedure to obtain DataLuminosity factor
// Scale MC to Data
// by 44.4068/1000. 
// 44.4068 is the Luminosity of Data obtained with ATLAS Luminosity calculator and divied by 1000. to pass pb-1 to fb-1
// Then change 44.4068 to have Data/MC > 1 (example 30) and then multiply this number (30) by Data/MC ratio (30*1.15)
// by 30*1.15/1000. (1/1000: DataLuminosity pb-1 to fb-1) 1000 pb-1 = 1 fb-1

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(){

  SetAtlasStyle();

  inputMC += JetCollection;
  inputMC += "/Outputs_vs_pT";
  inputMC += pTAvgDef;
  inputMC += ".root";
  inputData += JetCollection;
  inputData += "/Outputs_vs_pT_f1";
  inputData += pTAvgDef;
  inputData += ".root";

  //-------------------
  //Opening input files
  //-------------------

  // Data
  TString inputfileData = PATH;
  inputfileData += inputData;
  TFile* tinData = new TFile(inputfileData,"read");

  // MC
  TString inputfileMC = PATH;
  inputfileMC += inputMC;
  TFile* tinMC = new TFile(inputfileMC,"read");

  TString Normtype = "";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;

  TString outputFolder  = "Closure/";

  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tinData);
  pTBase->SetFiles(2,tinMC);
  pTBase->SetOutputFolder(outputFolder);
  pTBase->SetTitle("X","p_{T}^{Avg} [GeV]");
  pTBase->SetTitle("Y","Relative response");
  if(!PDF) pTBase->SetEPSFormat();
  pTBase->SetLogx();
  pTBase->SetLegend(1,"Shifted");
  pTBase->SetLegend(2,"Nominal");
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetRange("X",55.,1310);
  if(JetCollection=="2LC") pTBase->SetRange("Y",0.8,1.3);
  else if(JetCollection=="6LC") pTBase->SetRange("Y",0.9,1.3);
  else if(JetCollection=="8LC") pTBase->SetRange("Y",0.9,1.3);
  pTBase->EnableRatio();
  //pTBase->OnlyRatio();
  //pTBase->EnableSubtraction();
  pTBase->YRangeManual();
  pTBase->JetColl(JetCollection);
  pTBase->ShowErrors();
  //pTBase->SetMoreLogLabels();
  pTBase->SetLuminosity(strLuminosity);

  TString name;
  TString outputName;
  TString Type;
  TH1DPlotter *pT_Inclusive;
  for(int i=0;i<30;++i){
    name = "Response_vs_pT_Eta_";
    name += i;
    outputName = JetCollection;
    outputName += "/f1_Response_vs_pTavg_Eta_";
    outputName += i;
    outputName += pTAvgDef;
    pT_Inclusive = new TH1DPlotter(outputName.Data(),name.Data(),name.Data());
    pT_Inclusive->ApplyFormat(pTBase);
    Type = "Eta";
    if(i<10) Type += "0";
    Type += i;
    pT_Inclusive->SetType(Type.Data());
    //pT_Inclusive->SetRatioRange("Y",-0.1,0.02);
    pT_Inclusive->SetRatioRange("Y",0.8,1.1);
    if(i==29) pT_Inclusive->SetRange("X",55,500);
    //if(i==0) pT_Inclusive->FitRatio();
    //pT_Inclusive->Debug();
    pT_Inclusive->Write();
    delete pT_Inclusive;
  }

  return 0;

}


