// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("");

bool MCScaling = false;
bool m_debug   = false;

//TString JetCollection = "2LC";
TString JetCollection = "6LC";
//TString JetCollection = "8LC";

// Input Files
TString DataVersion = "";
TString MCVersion   = "";

// Data Luminosity
double DataLuminosity = 3209.25; // pb-1 v_13
TString strLuminosity = "3.2";

const int nEtaBins = 90;
TString EtaBins[nEtaBins+1] = {"-4.5", "-4.4", "-4.3", "-4.2", "-4.1", "-4.0", "-3.9", "-3.8", "-3.7", "-3.6", "-3.5", "-3.4", "-3.3", "-3.2", "-3.1", "-3.0", "-2.9", "-2.8", "-2.7", "-2.6", "-2.5", "-2.4", "-2.3", "-2.2", "-2.1", "-2.0", "-1.9", "-1.8", "-1.7", "-1.6", "-1.5", "-1.4", "-1.3", "-1.2", "-1.1", "-1.0", "-0.9", "-0.8", "-0.7", "-0.6", "-0.5", "-0.4", "-0.3", "-0.2", "-0.1" ,"0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0", "2.1", "2.2", "2.3", "2.4", "2.5", "2.6", "2.7", "2.8", "2.9", "3.0", "3.1", "3.2", "3.3", "3.4", "3.5", "3.6", "3.7", "3.8", "3.9", "4.0", "4.1", "4.2", "4.3", "4.4", "4.5"};

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(int argc, char* argv[]){

  std::string m_mcVersion = "";
  std::string m_dataVersion = "";
  std::string m_jetCollection = "";
  std::string m_systName = ""; // Default Nominal
  std::string m_path = "";
  //bool m_numericalInversion = false;

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--mcVersion=")   != std::string::npos) m_mcVersion = v[1];
    if ( opt.find("--dataVersion=")   != std::string::npos) m_dataVersion = v[1];

    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

    if ( opt.find("--path=")   != std::string::npos) m_path = v[1];

    /*if ( opt.find("--nInversion=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
      if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
    }*/

    if ( opt.find("--debug=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debug= true;
      if (v[1].find("FALSE") != std::string::npos) m_debug= false;
    }

    if ( opt.find("--syst=") != std::string::npos){
      if (v[1].find("deltaRUp") != std::string::npos) m_systName = "DeltaRUp";
      if (v[1].find("deltaRDown") != std::string::npos) m_systName = "DeltaRDown";
      if (v[1].find("isolationUp") != std::string::npos) m_systName = "IsolationUp";
      if (v[1].find("isolationDown") != std::string::npos) m_systName = "IsolationDown";
      if (v[1].find("JVTDown") != std::string::npos) m_systName = "JVTDown";
      if (v[1].find("JVTUp") != std::string::npos) m_systName = "JVTUp";
      if (v[1].find("MCsherpa") != std::string::npos) m_systName = "MCSherpa";
    }

  }//End: Loop over input options

  JetCollection   = m_jetCollection;
  MCVersion       = m_mcVersion;
  DataVersion     = m_dataVersion;
  PATH            = m_path;

  SetAtlasStyle();

  //--------------------
  // Opening input files
  //--------------------

  // Data
  TString inputfileData = PATH;
  inputfileData += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/";
  inputfileData += DataVersion;
  inputfileData += "_vs_";
  inputfileData += MCVersion;
  inputfileData += "/";
  inputfileData += JetCollection;
  inputfileData += "/SmoothedCorr_vs_pTRscan";
  if(m_systName!=""){
    inputfileData += "_";
    inputfileData += m_systName;
  }
  inputfileData += ".root";
  std::cout << "Opening: " << inputfileData << std::endl;
  TFile* tinData = new TFile(inputfileData,"read");

  // MC
  TString inputfileMC = PATH;
  inputfileMC += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/";
  inputfileMC += DataVersion;
  inputfileMC += "_vs_";
  inputfileMC += MCVersion;
  inputfileMC += "/";
  inputfileMC += JetCollection;
  inputfileMC += "/Corr_vs_pTrscan";
  if(m_systName!=""){
    inputfileMC += "_";
    inputfileMC += m_systName;
  }
  inputfileMC += ".root";
  std::cout << "Opening: " << inputfileMC << std::endl;
  TFile* tinMC = new TFile(inputfileMC,"read");

  double mcScale = 1.;
  if(MCScaling) mcScale = HelperFunctions::GetMCFactor(tinData, tinMC, "h_j0_pt_with_wgt_y_inclusive", DataLuminosity);

  TString Normtype = "";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;
  if(MCScaling){
    Normtype = "Factor_2"; //Only MC
    NormFactor2 = DataLuminosity*mcScale;
  }

  // Check
  TString outputFolder = "Plots_DirectMatching/Smoothing/";
  outputFolder += DataVersion;
  outputFolder += "_vs_";
  outputFolder += MCVersion;
  outputFolder += "/";
  outputFolder += JetCollection;
  outputFolder += "/";


  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tinData);
  pTBase->SetFiles(2,tinMC);
  pTBase->SetOutputFolder(outputFolder);
  pTBase->SetTitle("X","p_{T} [GeV]");
  pTBase->SetTitle("Y","Relative Response");
  if(!PDF) pTBase->SetEPSFormat();
  pTBase->SetLogx();
  pTBase->SetLegend(1,"SmoothedCorr");
  pTBase->SetLegend(2,"Corr");
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetmcScale(mcScale);
  pTBase->SetRange("X",55.,3000);
  if(JetCollection=="2LC") pTBase->SetRange("X",25,1991.9);
  else if(JetCollection=="6LC") pTBase->SetRange("X",45,1991.9);
  else if(JetCollection=="8LC") pTBase->SetRange("X",70,3000.);
  if(JetCollection=="2LC") pTBase->SetRange("Y",0.8,1.3);
  else if(JetCollection=="6LC") pTBase->SetRange("Y",0.9,1.3);
  else if(JetCollection=="8LC") pTBase->SetRange("Y",0.9,1.3);
  pTBase->EnableRatio();
  pTBase->YRangeManual();
  pTBase->JetColl(JetCollection);
  pTBase->ShowErrors();
  pTBase->SetMoreLogLabels();
  pTBase->SetRatioRange("Y",__ymin_Ratio_pT,__ymax_Ratio_pT);
  pTBase->SetLuminosity(strLuminosity);

  TString name;
  TString name2;
  TString outputName;
  TString Type;
  TH1DPlotter *pT_Inclusive;
  //for(int i=0;i<=30;++i){
  for(int i=15;i<=74;++i){
    name = "Corr_vs_pT_Eta_";
    name += i;
    name2 = "SmoothedCorr_vs_pT_Eta_";
    name2 += i;
    outputName = "Comparison_vs_pT_Eta_";
    outputName += i;
    outputName += "_";
    if(m_systName=="") outputName += "Nominal";
    if(m_systName!="") outputName += m_systName;
    if(m_debug){
      std::cout << "Opening: " << name << std::endl;
      std::cout << "Opening: " << name2 << std::endl;
    }
    pT_Inclusive = new TH1DPlotter(outputName.Data(),name2.Data(),name.Data());
    pT_Inclusive->ApplyFormat(pTBase);
    Type = "Eta";
    if(i<10) Type += "0";
    Type += i;
    pT_Inclusive->SetType(Type.Data());
    if(i<10) pT_Inclusive->SetRatioRange("Y",0.85,1.1);
    if(i>=10) pT_Inclusive->SetRatioRange("Y",0.85,1.2);
    TString legend = EtaBins[i];
    legend += " #leq #eta < ";
    legend += EtaBins[i+1];
    pT_Inclusive->AddLegend(legend);
    if(m_debug) pT_Inclusive->Debug();
    pT_Inclusive->Write();
    delete pT_Inclusive;
  }

  return 0;

}


