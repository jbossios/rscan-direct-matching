#include "RscanMacros/includes.h"
#include "RscanMacros/global.h"

//____________________________________________
//ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
//#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.C"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"  

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("/afs/cern.ch/user/j/jbossios/work/public/SM/new_xAODJetAnalysis/macros_and_results/macros/MC15TreetoROOTMacro/DataHists/78_version/");

bool MCScaling = true;

// Input Files
TString input("TurnOnCurves_Inputs.root");

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//-----------
// Functions
//-----------

void Draw2TH1D_fromsamefile(TFile*, TString, TString, TString, TString, TString, std::string, std::string);

//---------------
// Main Function
//---------------

int main() {

  SetAtlasStyle();

  //----------
  // Triggers
  //----------

  Triggers.push_back("HLT_j15");
  Triggers.push_back("HLT_j25");
  Triggers.push_back("HLT_j35");
  Triggers.push_back("HLT_j55");
  Triggers.push_back("HLT_j60");
  Triggers.push_back("HLT_j85");
  Triggers.push_back("HLT_j110");
  Triggers.push_back("HLT_j150");
  Triggers.push_back("HLT_j175");
  Triggers.push_back("HLT_j200");
  Triggers.push_back("HLT_j260");
  Triggers.push_back("HLT_j360");

  //-------------------
  // Opening input files
  //-------------------

  // Input File
  TString inputfileAll = PATH;
  inputfileAll += input;
  TFile* tinAll = new TFile(inputfileAll,"read");

  __xmin_pT = 0;
  __xmax_pT = 500;
  __ymin_Ratio_pT = 0;

  // Turn On Curves
  for(unsigned int i=1;i<Triggers.size();i++){
    TString Denominator = "h_pt_y_inclusive_Temporary_";
    TString Numerator = "h_pt_y_inclusive_";
    TString Out = Triggers.at(i);
    Denominator += Triggers.at(i-1);
    Numerator += Triggers.at(i);
    Draw2TH1D_fromsamefile(tinAll, Denominator, Numerator, "", "", Out, "p_{T} [GeV]", "");
  }

  return 0;

}

void Draw2TH1D_fromsamefile(TFile* finAll, TString hname, TString hname2, TString legend1, TString legend2 ,TString out, std::string xlabel, std::string ytitle){  

  std::map<std::string,double> minPtTriggers;
  minPtTriggers["HLT_j85"] = 116.;
  minPtTriggers["HLT_j110"] = 152.;
  minPtTriggers["HLT_j150"] = 194.;
  minPtTriggers["HLT_j175"] = 216.;
  minPtTriggers["HLT_j200"] = 240.;
  minPtTriggers["HLT_j260"] = 318.;
  minPtTriggers["HLT_j360"] = 408.;
   
  //-----------------
  // Creating TCanvas 
  //-----------------

  TCanvas *can = new TCanvas();
  TString ps = "../TurnOnCurves/78_version/";
  ps        += out;
  if(PDF) ps += ".pdf";
  else{ps += ".eps";}
  can->Print(ps+"[");

  //-------------------
  // Getting histograms
  //-------------------

  TString name = hname;
  TString name2 = hname2;
  std::cout << "Openining: " << name << std::endl;
  TH1D* h_tmp = (TH1D*)finAll->Get(hname);    // Before 
  std::cout << "Openining: " << name2 << std::endl;
  TH1D* h_tmp2 = (TH1D*)finAll->Get(hname2); // After

  std::cout << "Histograms opened" << std::endl;

  // Labels
  TString Xlabel = xlabel;
  TString YTitle = ytitle;
  
  h_tmp->SetMarkerSize(0.5);
  h_tmp2->SetMarkerSize(0.5);

  h_tmp->SetFillStyle(3004);
  
  //-----------
  // Ratio Plot
  //-----------

  // Define the ratio plot
  //TH1D *h3 = (TH1D*)h_tmp2->Clone("h3");
  TH1D *h3 = new TH1D("Final","",1000.,0.,5000.);
  h3->SetLineColor(kBlue);
  h3->SetMarkerColor(kBlue);
  //h3->SetFillStyle(3004);
  h3->SetMarkerSize(0.5);
  h3->SetMarkerSize(0.5);

  //h3->SetTitleOffset(0.2,"X");
  h3->SetXTitle(Xlabel);
    
  //h3->Divide(h_tmp);
  h3->Divide(h_tmp2,h_tmp,1,1,"b");
  // Setting x range
  double xminuser;
  double xmaxuser;
  if(name.Contains("m12")){
    xminuser = __xmin_mjj;
    xmaxuser = __xmax_mjj;
    h3->SetAxisRange(xminuser,xmaxuser,"X");
    h3->SetAxisRange(xminuser,xmaxuser,"X");
  }
  else if(name.Contains("pt")){
    xminuser = __xmin_pT;
    xmaxuser = __xmax_pT;
    h3->SetAxisRange(xminuser,xmaxuser,"X");
    h3->SetAxisRange(xminuser,xmaxuser,"X");
  }
    
  h3->Draw("P");       // Draw the ratio plot

  // Y range
  double ymax = __ymax_Ratio_pT;
  h3->SetMinimum(__ymin_Ratio_pT);  // Define Y ..
  //h3->SetMaximum(__ymax_Ratio_pT); // .. range

  if(name2.Contains("360")) ymax = 1.1;
  else if(name2.Contains("260")) ymax = 0.2;
  else if(name2.Contains("175")) ymax = 0.1;
  else if(name2.Contains("150")) ymax = 0.1;
  else if(name2.Contains("110")) ymax = 0.02;
  else if(name2.Contains("85")) ymax = 0.1;
  else if(name2.Contains("60")) ymax = 0.001;
  h3->SetMaximum(ymax); // .. range

  // Ratio plot (h3) settings
  h3->SetTitle(""); // Remove the ratio title

  TString Title = legend2;
  Title += "";
  Title += legend1;
  h3->GetYaxis()->SetTitle(Title);
  // Y axis ratio plot settings
  h3->GetYaxis()->SetTitleSize(20);
  h3->GetYaxis()->SetTitleFont(43);
  //h3->GetYaxis()->SetTitleOffset(1.35);
  h3->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  h3->GetYaxis()->SetLabelSize(16);

  // X axis ratio plot settings
  h3->GetXaxis()->SetTitleSize(20);
  h3->GetXaxis()->SetTitleFont(43);
  //h3->GetXaxis()->SetTitleOffset(3.);
  h3->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  h3->GetXaxis()->SetLabelSize(16);
  //h3->GetXaxis()->SetMoreLogLabels(0);
  //h3->GetXaxis()->SetMoreLogLabels();
  //if(!name.Contains("m12")) h3->GetXaxis()->SetMoreLogLabels();
 
  //---------------------------------
  // Showing line at 1 in ratio plots
  //---------------------------------

  double nbins = h3->GetNbinsX();
  double minimum = h3->GetXaxis()->GetBinLowEdge(1);
  double maximum = h3->GetXaxis()->GetBinUpEdge(nbins);
  if(name.Contains("pt")){
    minimum = __xmin_pT;
    maximum = __xmax_pT;
  }
  else if(name.Contains("m12")){
    minimum = __xmin_mjj;
    maximum = __xmax_mjj;
  }
  else if(name.Contains("rho")){
    minimum = __xmin_rho;
    maximum = __xmax_rho;
  }
  else if(name.Contains("phi")){
    minimum = __xmin_phi;
    maximum = __xmax_phi;
  }
  else if(name.Contains("trackWIDTH")){
    minimum = __xmin_trackWIDTH;
    maximum = __xmax_trackWIDTH;
  }
  
  TLine* line = new TLine(minimum, 1., maximum, 1.);
  line->SetLineStyle(2);
  line->Draw("same");

  TString trigger = "";
  if(name2.Contains("HLT_j360")) trigger = "HLT_j360";
  else if(name2.Contains("HLT_j260")) trigger = "HLT_j260";
  else if(name2.Contains("HLT_j200")) trigger = "HLT_j200";
  else if(name2.Contains("HLT_j175")) trigger = "HLT_j175";
  else if(name2.Contains("HLT_j150")) trigger = "HLT_j150";
  else if(name2.Contains("HLT_j110")) trigger = "HLT_j110";
  else if(name2.Contains("HLT_j85")) trigger = "HLT_j85";

  for(int i=0;i<36;i++){
    TLine* line = new TLine(ptBins[i], __ymin_Ratio_pT, ptBins[i], ymax);
    line->SetLineStyle(2);
    if(ptBins[i]==minPtTriggers[trigger.Data()]){
      line->SetLineColor(kRed);
      line->SetLineWidth(2);
    }
    line->Draw("same");
  }

  can->Print(ps);
  delete h_tmp;
  delete h_tmp2;
  delete h3;

  can->Print(ps+"]");

}


