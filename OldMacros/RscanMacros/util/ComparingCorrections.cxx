// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("/afs/cern.ch/user/j/jbossios/work/public/JetEtmiss/Insitu_Rscan/DeriveInsituRscan/");

bool MCScaling = false;

TString JetCollection;
TString MCVersion;
TString DataVersion;
TString PATHNum;
TString PATHDen;

// Input Files
TString inputMC = "";
TString inputData = "";

// Data Luminosity
double DataLuminosity = 3209.25; // pb-1 v_13
TString strLuminosity = "3.2";

const int nEtaBins = 90;
TString EtaBins[nEtaBins+1] = {"-4.5", "-4.4", "-4.3", "-4.2", "-4.1", "-4.0", "-3.9", "-3.8", "-3.7", "-3.6", "-3.5", "-3.4", "-3.3", "-3.2", "-3.1", "-3.0", "-2.9", "-2.8", "-2.7", "-2.6", "-2.5", "-2.4", "-2.3", "-2.2", "-2.1", "-2.0", "-1.9", "-1.8", "-1.7", "-1.6", "-1.5", "-1.4", "-1.3", "-1.2", "-1.1", "-1.0", "-0.9", "-0.8", "-0.7", "-0.6", "-0.5", "-0.4", "-0.3", "-0.2", "-0.1" ,"0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0", "2.1", "2.2", "2.3", "2.4", "2.5", "2.6", "2.7", "2.8", "2.9", "3.0", "3.1", "3.2", "3.3", "3.4", "3.5", "3.6", "3.7", "3.8", "3.9", "4.0", "4.1", "4.2", "4.3", "4.4", "4.5"};

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(int argc, char* argv[]){

  std::string m_mcVersion = "";
  std::string m_dataVersion = "";
  std::string m_jetCollection = "";
  std::string m_pathNum = "";
  std::string m_pathDen = "";
  std::string m_nameNum = "";
  std::string m_nameDen = "";
  bool m_numericalInversion = false;

  //-------------------------
  // Decode the user settings
  //-------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
      v.push_back(item);
    }

    if ( opt.find("--pathNum=")   != std::string::npos) m_pathNum = v[1];
    if ( opt.find("--pathDen=")   != std::string::npos) m_pathDen = v[1];
    
    if ( opt.find("--nameNum=")   != std::string::npos) m_nameNum = v[1];
    if ( opt.find("--nameDen=")   != std::string::npos) m_nameDen = v[1];

    if ( opt.find("--mcVersion=")   != std::string::npos) m_mcVersion = v[1];
    if ( opt.find("--dataVersion=")   != std::string::npos) m_dataVersion = v[1];
    
    if ( opt.find("--jetColl=")   != std::string::npos) m_jetCollection = v[1];

    if ( opt.find("--nInversion=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_numericalInversion= true;
      if (v[1].find("FALSE") != std::string::npos) m_numericalInversion= false;
    }

  }//End: Loop over input options

  JetCollection       = m_jetCollection;
  MCVersion           = m_mcVersion;
  DataVersion         = m_dataVersion;
  PATHNum             = m_pathNum;
  PATHDen             = m_pathDen;

  if(JetCollection==""){
    std::cout << "ERROR: Jet collection should be provided, exiting" << std::endl;
    return 0;
  }
  if(MCVersion==""){
    std::cout << "ERROR: MC Version should be provided, exiting" << std::endl;
    return 0;
  }
  if(DataVersion==""){
    std::cout << "ERROR: Data Version should be provided, exiting" << std::endl;
    return 0;
  }
  if(PATHNum==""){
    std::cout << "ERROR: PATH for the Numerator should be provided, exiting" << std::endl;
    return 0;
  }
  if(PATHDen==""){
    std::cout << "ERROR: PATH for the Denominator should be provided, exiting" << std::endl;
    return 0;
  }
  if(m_nameNum==""){
    std::cout << "ERROR: Name1 should be provided, exiting" << std::endl;
    return 0;
  }
  if(m_nameDen==""){
    std::cout << "ERROR: Name2 should be provided, exiting" << std::endl;
    return 0;
  }

  // Denominator
  inputMC = PATHDen;
  inputMC += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/";
  inputMC += DataVersion;
  inputMC += "_vs_";
  inputMC += MCVersion;
  inputMC += "/";
  inputMC += JetCollection;
  inputMC += "/SmoothedCorr_vs_pT";
  if(m_numericalInversion) inputMC += "rscan_numericalInversion";
  else{inputMC += "Rscan";}
  inputMC += ".root";

  // Numerator
  inputData = PATHNum;
  inputData += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/";
  inputData += DataVersion;
  inputData += "_vs_";
  inputData += MCVersion;
  inputData += "/";
  inputData += JetCollection;
  inputData += "/SmoothedCorr_vs_pT";
  if(m_numericalInversion) inputData += "rscan_numericalInversion";
  else{inputData += "Rscan";}
  inputData += ".root";

  SetAtlasStyle();

  //-------------------
  //Opening input files
  //-------------------

  // Data
  std::cout << "Opening: " << inputData << std::endl;
  TFile* tinData = new TFile(inputData,"read");

  // MC
  std::cout << "Opening: " << inputMC << std::endl;
  TFile* tinMC = new TFile(inputMC,"read");

  double mcScale = 1.;
  if(MCScaling) mcScale = HelperFunctions::GetMCFactor(tinData, tinMC, "h_j0_pt_with_wgt_y_inclusive", DataLuminosity);

  TString Normtype = "";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;
  if(MCScaling){
    Normtype = "Factor_2"; //Only MC
    NormFactor2 = DataLuminosity*mcScale;
  }

  TString OutputFolder = "Plots_DirectMatching/Response_Data_vs_MC/";
  OutputFolder += DataVersion;
  OutputFolder += "_vs_";
  OutputFolder += MCVersion;
  OutputFolder += "/";

  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tinData);
  pTBase->SetFiles(2,tinMC);
  pTBase->SetOutputFolder(OutputFolder);
  pTBase->SetTitle("X","p_{T} [GeV]");
  pTBase->SetTitle("Y","Correction");
  if(!PDF) pTBase->SetEPSFormat();
  pTBase->SetLogx();
  pTBase->SetLegend(1,m_nameNum);
  pTBase->SetLegend(2,m_nameDen);
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetmcScale(mcScale);
  pTBase->SetRange("X",55.,1310);
  if(JetCollection=="2LC") pTBase->SetRange("Y",0.8,1.3);
  else if(JetCollection=="6LC") pTBase->SetRange("Y",0.9,1.3);
  else if(JetCollection=="8LC") pTBase->SetRange("Y",0.9,1.3);
  pTBase->EnableRatio();
  pTBase->YRangeManual();
  pTBase->JetColl(JetCollection);
  pTBase->ShowErrors();
  pTBase->SetRatioRange("Y",__ymin_Ratio_pT,__ymax_Ratio_pT);
  pTBase->SetRatioRange("Y",0.95,1.05);
  pTBase->SetLuminosity(strLuminosity);

  TString name;
  TString outputName;
  TString Type;
  TString NameNum = m_nameNum;
  TString NameDen = m_nameDen;
  TH1DPlotter *pT_Inclusive;
  double min = 55;
  double max = 1000;
  for(int i=15;i<=74;++i){
    name = "SmoothedCorr_vs_pT_Eta_";
    name += i;
    outputName = JetCollection;
    outputName += "/Compare_";
    outputName += NameNum;
    outputName += "_vs_";
    outputName += NameDen;
    outputName += "_vs_pT_Eta_";
    outputName += i;
    std::cout << "Opening: " << name << std::endl;
    pT_Inclusive = new TH1DPlotter(outputName.Data(),name.Data(),name.Data());
    pT_Inclusive->ApplyFormat(pTBase);
    Type = "Eta";
    if(i<10) Type += "0";
    Type += i;
    pT_Inclusive->SetType(Type.Data());
    if(i>=10){
      pT_Inclusive->SetRange("X",55,1000);
    }
    if(i==29){
      pT_Inclusive->SetRange("X",55,500);
    }
    TString legend = EtaBins[i];
    legend += " #leq #eta < ";
    legend += EtaBins[i+1];
    pT_Inclusive->AddLegend(legend);
    pT_Inclusive->PlotLine("X", min, max, 1.01);
    pT_Inclusive->PlotLine("X", min, max, 0.99);
    pT_Inclusive->Write();
    delete pT_Inclusive;
  }

  return 0;

}


