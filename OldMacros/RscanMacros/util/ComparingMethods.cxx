// Includes
#include "RscanMacros/includes.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "RscanMacros/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("/afs/cern.ch/user/j/jbossios/rscan/ROOTfiles/ComparingMethods/");

bool MCScaling = false;

TString DirectFile = "DirectMatchingMethod/MC15c_MC15bmuProfile_Pythia_Outputs_vs_pT.root";
TString DijetsFile  = "DijetMethod/MC15c_MC15bmuProfile_Pythia_Outputs_vs_pT.root";

TString JetCollection = "6LC";

const int nEtaBins = 90;
TString EtaBins[nEtaBins+1] = {"-4.5", "-4.4", "-4.3", "-4.2", "-4.1", "-4.0", "-3.9", "-3.8", "-3.7", "-3.6", "-3.5", "-3.4", "-3.3", "-3.2", "-3.1", "-3.0", "-2.9", "-2.8", "-2.7", "-2.6", "-2.5", "-2.4", "-2.3", "-2.2", "-2.1", "-2.0", "-1.9", "-1.8", "-1.7", "-1.6", "-1.5", "-1.4", "-1.3", "-1.2", "-1.1", "-1.0", "-0.9", "-0.8", "-0.7", "-0.6", "-0.5", "-0.4", "-0.3", "-0.2", "-0.1" ,"0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0", "2.1", "2.2", "2.3", "2.4", "2.5", "2.6", "2.7", "2.8", "2.9", "3.0", "3.1", "3.2", "3.3", "3.4", "3.5", "3.6", "3.7", "3.8", "3.9", "4.0", "4.1", "4.2", "4.3", "4.4", "4.5"};

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/TH1DPlotter.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperFunctions;

//---------------
// Main Function
//---------------

int main(){

  SetAtlasStyle();

  TString inputDirect = PATH; inputDirect += DirectFile;

  TString inputDijets = PATH; inputDijets += DijetsFile;

  //---------------------
  // Opening input files
  //---------------------

  // Direct
  std::cout << "Opening: " << inputDirect << std::endl;
  TFile* tinDirect = new TFile(inputDirect,"read");

  // Dijets
  std::cout << "Opening: " << inputDijets << std::endl;
  TFile* tinDijets = new TFile(inputDijets,"read");

  double mcScale = 1.;

  TString Normtype = "";
  double NormFactor1 = -999.;
  double NormFactor2 = -999.;

  TString outputFolder  = "ComparingMethods/26072016/";

  //---------------------------------
  // Relative Response Distributions
  //---------------------------------

  TH1DPlotter *pTBase = new TH1DPlotter("pTBase");
  pTBase->SetFiles(1,tinDirect);
  pTBase->SetFiles(2,tinDijets);
  pTBase->SetOutputFolder(outputFolder);
  pTBase->SetTitle("X","p_{T}^{Avg} [GeV]");
  pTBase->SetTitle("Y","Relative Response");
  if(!PDF) pTBase->SetEPSFormat();
  pTBase->SetLogx();
  pTBase->SetLegend(1,"Direct");
  pTBase->SetLegend(2,"Dijets");
  pTBase->Normalize(Normtype);
  pTBase->SetNormFactor(1,NormFactor1);
  pTBase->SetNormFactor(2,NormFactor2);
  pTBase->SetmcScale(mcScale);
  pTBase->SetRange("Y",0.9,1.3);
  pTBase->EnableRatio();
  pTBase->YRangeManual();
  pTBase->JetColl(JetCollection);
  pTBase->ShowErrors();
  //pTBase->SetMoreLogLabels();
  pTBase->SetRatioRange("Y",__ymin_Ratio_pT,__ymax_Ratio_pT);
  pTBase->SetRatioRange("Y",0.95,1.05);
  pTBase->SetRange("X",85,1000);
  pTBase->SetLuminosity("3.2");

  TString name;
  TString outputName;
  TString Type;
  TH1DPlotter *pT_Inclusive;
  for(int i=15;i<=74;++i){
    name = "Response_vs_pT_Eta_";
    name += i;
    outputName = "/Response_vs_pT_Eta_";
    outputName += i;
    pT_Inclusive = new TH1DPlotter(outputName.Data(),name.Data(),name.Data());
    pT_Inclusive->ApplyFormat(pTBase);
    Type = "Eta";
    if(i<10) Type += "0";
    Type += i;
    pT_Inclusive->SetType(Type.Data());
    double min = 85;
    double max = 1000;
    TString legend = EtaBins[i];
    legend += " #leq #eta < ";
    legend += EtaBins[i+1];
    pT_Inclusive->AddLegend(legend);
    pT_Inclusive->PlotLine("X", min, max, 1.01);
    pT_Inclusive->PlotLine("X", min, max, 0.99);
    pT_Inclusive->Write();
    delete pT_Inclusive;
  }

  return 0;

}


