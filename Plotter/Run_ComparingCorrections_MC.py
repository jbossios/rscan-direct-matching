#!/usr/bin/python
import os, sys
param=sys.argv

################
# DEPRECATED !!!
################

# Not implemented yet
#Debug = False

# Check
#jetColl = "2LC"
jetColl = "6LC"
#jetColl = "8LC"

# Check
#deriveInsituVersion = "v_9"
deriveInsituVersion = "v_12"

# Check
#OutputFolder = "Powheg_vs_Pythia/"
OutputFolder = "ClosurePlots_20_7/"

# Check
#numericalInversion = False
numericalInversion = True

#Insitu = False
Insitu = True

# Check (2/1)
#InputFolder1 = "v_13Data__vs__v_14Powheg"
#InputFolder2 = "v_13Data__vs__v_08Pythia"
InputFolder1 = "v_19Data__vs__v_20Pythia"
InputFolder2 = "v_19Data__vs__v_20Pythia"
#Name1 = "Powheg"
Name1 = "Pythia"
Name2 = "Pythia"

##################################################################################
# DO NOT MODIFY
##################################################################################

command = "ComparingCorrections "

command += "--jetColl="
command += jetColl
command += " --inputFolder1="
command += InputFolder1
command += " --inputFolder2="
command += InputFolder2
command += " --deriveInsituVersion="
command += deriveInsituVersion
command += " --name1="
command += Name1
command += " --name2="
command += Name2
command += " --outputs="
command += OutputFolder
if numericalInversion:
  command += " --nInversion=TRUE"
if Insitu:
  command += " --insitu=TRUE"	

print command
os.system(command)

