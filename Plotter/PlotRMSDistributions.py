#!/usr/bin/python
import os, sys
import array
from ROOT import *

###  Plot histogram of systematic values of each replica in pT and eta bins. 
###  prepared to compare resluts from sets of different amount of replicas.

# Check
jetColl = "2LC"
#jetColl = "6LC"

ZJetStudy = True
ApplyEtaRebinning = True

# Check
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

Systs = [
  "DeltaRDown",
  "DeltaRUp",
  "IsolationDown",
  "IsolationUp",
  "JVTDown",
  "JVTUp",
  "MCSherpa",
]

Compare_N_Replicas = False

Replicas = [
    ## IMPORTANT: if comparing number of bootstraps replicas, put the ROOTfiles folder of each set of replicas in folders labelled "n#Replicas" where # should be replaced by the amount of replicas used. All these folders should live in a common directory that needs to be specified in PATH below. For example, "../../Results/".

# "n500Replicas",
# "n200Replicas",
"n100Replicas",   # DEFAULT: if not comparing # of bootstrap replicas, only use n100
]

###################################################################
## DO NOT MODIFY
###################################################################

# In this path you should have the folder ROOTfiles
PATH = "../../Results/"

gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

Version = DataVersion + "_vs_" + MCVersion

# Eta bins
netaBins = 60
Etabins = [-3.0,-2.9,-2.8,-2.7,-2.6,-2.5,-2.4,-2.3,-2.2,-2.1,-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1 ,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0]
etarange = range(15,74)

if ApplyEtaRebinning:
 netaBins = 16
 Etabins = [-3.,-2.8, -2.5, -1.8, -1.3, -1.0, -0.7,-0.2, 0., 0.2, 0.7, 1.0, 1.3, 1.8, 2.5, 2.8, 3.] 
 etarange = range(0,16)

npTBins = 43+3
pTBins = [15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941., 6200., 7780., 9763.]

ptbins = array.array('d',pTBins)
etabins =array.array('d',Etabins)

for syst in Systs:
  for eta in etarange:
    InputFiles = []
    for nrep in Replicas: 
      InputFile = PATH + nrep + "/"
      InputFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Statistical_errors/" + Version+ "/" + jetColl +"/" + "Stat_error"
      if ZJetStudy:
	InputFile += "_ZJetStudy"
      InputFile += "_etaSlice_"
      InputFile += str(eta)
      InputFile += "_"
      InputFile += syst
      if ApplyEtaRebinning:
	InputFile += "_EtaRebinned"
      InputFile += ".root"
      print "Opening: " + InputFile
      file = TFile.Open(InputFile,"Read")
      if not file:
	print InputFile+" is not found, exiting"
	sys.exit(0)
      InputFiles.append(file)
    
    SetAtlasStyle()
    text1 = DataVersion
    if jetColl is "2LC":
      text2 = "anti-#it{k}_{t} jets, R = 0.2, LC+JES"
    elif jetColl is "6LC":
      text2 = "anti-#it{k}_{t} jets, R = 0.6, LC+JES"

    can = TCanvas()
    can.SetMargin(0.12,0.04,0.14,0.04)
    # output PDF
    ps = PATH + "n100Replicas/Plots_DirectMatching/Systematics/" + Version + "/" + jetColl + "/RMSdistributions"
    if ZJetStudy:
      ps += "_ZJetStudy"
    ps += "_etaSlice_"
    ps += str(eta)
    ps += "_" + syst
    if ApplyEtaRebinning:
      ps += "_etaRebinned"
    ps += ".pdf"
    can.Print(ps + "[")

    # colors
    Colors = [kAzure+10, kRed, kBlack]
    LineStyles =[1,9,5] 
    Alpha = [.4,.7,.4]
   
    # Loop over ptbins
    for pt in range(1,43):
      graphs = []
      can.Clear()
      name_tmp = "SystematicValue_Eta_"
      name_tmp += str(eta)
      name_tmp += "_Pt_"
      name_tmp += str(pt)
      mg = TMultiGraph()
      # Loop over nReplicas
      n = 0 # counter for number of graphs (rep)
      for tfile in InputFiles:
	graph = TH1D()	  
	tfile.GetObject(name_tmp, graph)
	graph.GetXaxis().SetTitle("(Nom-Var)/Nom")
	graph.GetYaxis().SetTitle("Entries")
	graph.Rebin(20)
	graph.SetMarkerColor(Colors[n])
	graph.SetMarkerSize(0.2)
	graph.SetLineStyle(LineStyles[n])
	graph.SetLineColorAlpha(Colors[n],Alpha[n])
        graph.GetXaxis().SetTitleOffset(1.2)
        graph.GetYaxis().SetTitleOffset(1.1)
        graph.SetAxisRange(-0.04,0.06,"X")
	if graph.GetEntries() != 0.00:
	  if n is 0:
	    graph.DrawNormalized()
	  else:
	    graph.DrawNormalized("same")
	else:
	  if n is 0:
	    graph.Draw()
	  else:
	    graph.Draw("same")
        n+=1
	graphs.append(graph)
      
      # Legends
      leg = TLegend(.45,.7,.85,.9) # x1, y1, x2, y2
      leg.SetTextFont(42)
      leg.SetTextSize(0.04)
      m = 0 
      for graph in graphs:
	leg.AddEntry(graph,Replicas[m]+ "_" + syst +".Entries: "+ str(graph.GetEntries()))
	m+=1
      leg.Draw("same")
      
      # Extra Text
      l = TLatex( 0.16, 0.87, text1)
      l.SetNDC()
      l.SetTextFont(42)
      l.SetTextSize(0.04)
      l.SetTextColor(1)
      l.Draw("same")
      
      l1 = TLatex( 0.16, 0.82, text2)
      l1.SetNDC()
      l1.SetTextFont(42)
      l1.SetTextSize(0.035)
      l1.SetTextColor(1)
      l1.Draw("same")
      
      text4 = str(Etabins[eta-15]) + " #leq #eta < " + str(Etabins[eta-14])
      if ApplyEtaRebinning:
	text4 = str(Etabins[eta]) + " #leq #eta < " + str(Etabins[eta+1])

      l3 = TLatex(0.16, 0.77, text4)
      l3.SetNDC()
      l3.SetTextFont(42)
      l3.SetTextSize(0.033)
      l3.SetTextColor(1)
      l3.Draw("same")
      
      text5 = str(pTBins[pt-1]) + " #leq p_{t}^{Ref} < " + str(pTBins[pt])
      l4 = TLatex(0.16, 0.72, text5)
      l4.SetNDC()
      l4.SetTextFont(42)
      l4.SetTextSize(0.033)
      l4.SetTextColor(1)
      l4.Draw("same")
           
      can.Print(ps)
    can.Print(ps +"]")
	
    


