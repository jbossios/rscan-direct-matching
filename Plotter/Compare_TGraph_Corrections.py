#!/usr/bin/python
import os, sys
import array
from ROOT import *

# Check
jetColl = "2LC"
#jetColl = "6LC"

# Check
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

###################################################################
## DO NOT MODIFY
###################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

Version = DataVersion + "_vs_" + MCVersion

# Eta bins
netaBins = 60
Etabins = [-3.0,-2.9,-2.8,-2.7,-2.6,-2.5,-2.4,-2.3,-2.2,-2.1,-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1 ,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0]

etabins =array.array('d',Etabins)

# Open Input Files

InputFileDijets = PATH +"ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/" + Version+ "/" + jetColl +"/" + "Corr_vs_pTrscan.root"
print "Opening: " + InputFileDijets
fileDijets = TFile.Open(InputFileDijets,"Read")
if not fileDijets:
  print "Dijets correction input file is not found, exiting"
  sys.exit(0)

InputFileZjets = PATH + "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/" + Version + "/"+ jetColl + "/" + "Corr_vs_pTrscan_ZJet.root"
print "Opening: " + InputFileZjets
fileZjets = TFile.Open(InputFileZjets,"read")
if not fileZjets:
  print "Z+Jets correction input file is not found, exiting"
  sys.exit(0)

SetAtlasStyle()

text1 = DataVersion
if jetColl is "2LC":
  text2 = "anti-#it{k}_{t} jets, R = 0.2, LC+JES"
elif jetColl is "6LC":
  text2 = "anti-#it{k}_{t} jets, R = 0.6, LC+JES"

# Retrievig graphs

gDijets = TGraphAsymmErrors()
gZjets  = TGraphAsymmErrors()
can = TCanvas()
can.SetMargin(0.12,0.04,0.14,0.04)

# output PDF
ps = PATH + "Plots_DirectMatching/Calibrations/" + Version + "/" + jetColl + "/CompareCalibrations_vs_ptrscan.pdf"
can.Print(ps + "[")
can.SetLogx()

for eta in range(0,netaBins):
  can.Clear()
  name_tmp = "Corr_vs_pTRscan_Eta_"
  name_tmp += str(eta+15)
  fileDijets.GetObject(name_tmp, gDijets)
  gDijets.GetXaxis().SetTitle("#it{p}_{T}^{Rscan} [GeV]")
  gDijets.GetYaxis().SetTitle("Correction")
  gDijets.SetMaximum(1.2)
  gDijets.SetMinimum(0.9)
  gDijets.GetXaxis().SetMoreLogLabels()
  
  fileZjets.GetObject(name_tmp,gZjets)
  gZjets.SetMarkerColor(2)
  gZjets.SetLineColor(2)
  gZjets.GetXaxis().SetMoreLogLabels()
  
  mg = TMultiGraph()
  mg.Add(gDijets)
  mg.Add(gZjets)
  mg.Draw("AP")
  mg.GetHistogram().GetXaxis().SetTitle("#it{p}_{T}^{Rscan} [GeV]")
  mg.GetHistogram().GetYaxis().SetTitle("Correction")
  mg.GetHistogram().GetXaxis().SetTitleOffset(1.2)
  mg.GetHistogram().GetYaxis().SetTitleOffset(1.1)
  mg.GetHistogram().GetXaxis().SetMoreLogLabels()
  mg.GetHistogram().GetYaxis().SetRangeUser(0.9,1.2);

  # Legends

  leg = TLegend(.6,.8,.87,.9)
  leg.SetTextFont(42)
  leg.SetTextSize(0.04);
  leg.AddEntry(gDijets,"Dijets")
  leg.AddEntry(gZjets,"Z+jets")
  leg.Draw("same")
  
  # Extra Text

  l = TLatex( 0.16, 0.87, text1)
  l.SetNDC()
  l.SetTextFont(42)
  l.SetTextSize(0.04)
  l.SetTextColor(1)
  l.Draw("same")
  l1 = TLatex( 0.16, 0.82, text2)
  l1.SetNDC()
  l1.SetTextFont(42)
  l1.SetTextSize(0.035)
  l1.SetTextColor(1)
  l1.Draw("same")
  text4 = str(Etabins[eta]) + " #leq #eta < " + str(Etabins[eta+1])
  l3 = TLatex(0.16, 0.77, text4)
  l3.SetNDC()
  l3.SetTextFont(42)
  l3.SetTextSize(0.033)
  l3.SetTextColor(1)
  l3.Draw("same")
  can.Print(ps)

can.Print(ps +"]")

fileDijets.Close()
fileZjets.Close()
