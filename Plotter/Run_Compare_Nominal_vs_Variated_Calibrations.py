#!/usr/bin/python

##############################################################################################
# Run_Compare_Nominal_vs_Variated_Calibrations.py
# author: Jona Bossio
# date: 19/08/2018
# purpose: Compare nominal calibration with variated calibration (e.g. DeltaRUp)
##############################################################################################

import os, sys
import ROOT

# Check
Debug = False
Syst  = "DeltaRUp" # Example

# Check 
jetColl = "2LC"
#jetColl = "6LC"

# Check
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

##################################################################################
# DO NOT MODIFY
##################################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

ROOT.gROOT.SetBatch(True)  # so does not pop up plots!
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Set Luminosity
strLumi = ""
if DataVersion=="2016data":
  strLumi = "32.9"
if DataVersion=="2015data": # R21 only
  strLumi = "3.2"
elif DataVersion=="2015+2016data":
  strLumi = "36.2"
elif DataVersion=="2017data":
  strLumi = "43.6"
else:
  print "DataVersion not recognised, exiting"
  sys.exit(0)

# Set Eta binning
EtaBins = [
  "-3.0",
  "-2.9",
  "-2.8",
  "-2.7",
  "-2.6",
  "-2.5",
  "-2.4",
  "-2.3",
  "-2.2",
  "-2.1",
  "-2.0",
  "-1.9",
  "-1.8",
  "-1.7",
  "-1.6",
  "-1.5",
  "-1.4",
  "-1.3",
  "-1.2",
  "-1.1",
  "-1.0",
  "-0.9",
  "-0.8",
  "-0.7",
  "-0.6",
  "-0.5",
  "-0.4",
  "-0.3",
  "-0.2",
  "-0.1",
  "0.0",
  "0.1",
  "0.2",
  "0.3",
  "0.4",
  "0.5",
  "0.6",
  "0.7",
  "0.8",
  "0.9",
  "1.0",
  "1.1",
  "1.2",
  "1.3",
  "1.4",
  "1.5",
  "1.6",
  "1.7",
  "1.8",
  "1.9",
  "2.0",
  "2.1",
  "2.2",
  "2.3",
  "2.4",
  "2.5",
  "2.6",
  "2.7",
  "2.8",
  "2.9",
  "3.0",
]

# Open input files

# Denominator
inputDen  = PATH
inputDen += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/"
inputDen += DataVersion
inputDen += "_vs_"
inputDen += MCVersion
inputDen += "/"
inputDen += jetColl
inputDen += "/Corr_vs_pTref.root"
print "Openning: "+inputDen
fDen = ROOT.TFile.Open(inputDen)
if not fDen:
  print "File not found, exiting"
  sys.exit(0)

# Numerator
inputNum  = PATH
inputNum += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/"
inputNum += DataVersion
inputNum += "_vs_"
inputNum += MCVersion
inputNum += "/"
inputNum += jetColl
inputNum += "/Corr_vs_pTref_"
inputNum += Syst
inputNum += ".root"
print "Openning: "+inputNum
fNum = ROOT.TFile.Open(inputNum)
if not fNum:
  print "File not found, exiting"
  sys.exit(0)

# Output Folder
outputFolder = PATH
outputFolder += "Plots_DirectMatching/Calibrations/"
outputFolder += DataVersion
outputFolder += "_vs_"
outputFolder += MCVersion
outputFolder += "/"
outputFolder += jetColl

ROOT.SetAtlasStyle()

# Format instance
Format = ROOT.Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Ratio")   # Will show second panel with ratio
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
Format.SetLegendPosition("Top")  # Other options: TopRight, TopLeft, Bottom
Format.SetAxisTitles("#it{p}_{T}^{Ref} [GeV]","Correction") # x-axis, y-axis
Format.SetYTitleSecondPanel(Syst+"/Nominal") # y-axis title of second panel
Format.ShowLuminosity(strLumi)
Format.ShowCME("13")
Format.SetOutputFolder(outputFolder)
Format.SetLogx()
Format.SetMoreLogLabelsX()
Format.SetRange("X",55.,1310)
if jetColl=="2LC":
  Format.SetRange("Y",0.8,1.4)
elif jetColl=="6LC":
  Format.SetRange("Y",0.9,1.4)
elif jetColl=="8LC":
  Format.SetRange("Y",0.9,1.4)
Format.SetYRangeSecondPanel(0.99,1.01)

# Loop over eta bins
for bin in range(15,75):
  name  = "Corr_vs_pT_Eta_"
  name += str(bin)
  outputName  = "CompareCalibrations_"
  outputName += Syst
  outputName += "_vs_Eta_"
  outputName += str(bin)
  outputName += "_vs_ptrscan"
  Plot = ROOT.Plotter1D(outputName)
  Plot.ApplyFormat(Format)
  Plot.AddHist("Nominal",fDen,name,"HIST")
  Plot.AddHist(Syst,fNum,name,"HIST")
  extraText = "anti-#it{k}_{t} jets, R="
  if jetColl is "2LC":
    extraText += "0.2"
  if jetColl is "6LC":
    extraText += "0.6"
  if jetColl is "8LC":
    extraText += "0.8"
  extraText += ", LC+JES"
  Plot.AddText(0.2,0.55,extraText)
  etaBin  = EtaBins[bin-15]
  etaBin += " #leq #eta < "
  etaBin += EtaBins[bin-14]
  Plot.AddText(0.2,0.47,etaBin)
  Plot.PlotLineSecondPanel("Y",1.001)
  Plot.PlotLineSecondPanel("Y",0.999)
  if Debug:
    Plot.Debug()	  
  Plot.Write()

  




