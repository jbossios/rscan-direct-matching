#!/usr/bin/python
import os, sys
import ROOT
ROOT.gROOT.SetBatch(True)  # so does not pop up plots!
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Check
Debug        = False
ZJetStudy    = True
ClosurePlots_insituSmoothed = False
ClosurePlots_insituNOTSmoothed = False

ApplyEtaRebinning = True

# Check
jetColl = "2LC"
#jetColl = "6LC"
#jetColl = "8LC"

# Check
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

# Check
syst = "Nominal"
#syst = "DeltaRUp"
#syst = "DeltaRDown"
#syst = "MCSherpa"

##################################################################################
# DO NOT MODIFY
##################################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

if ClosurePlots_insituSmoothed and ClosurePlots_insituNOTSmoothed:
  print "Choose only one of the closure plots options, exiting"
  sys.exit(0)

# R20.7
#MCVersion = "MC15cPythia_MC15bMuProfile"
#DataVersion = "2015data"
#MCVersion = "MC15cPythia"
#DataVersion = "2016data"
# Check Deprecated
numericalInversion = False
#numericalInversion = True

# Set Luminosity
strLumi = ""
if DataVersion=="2016data":
  strLumi = "32.9" # 32994.9
if DataVersion=="2015data": # R21 only
  strLumi = "3.2"  # 3219.56
elif DataVersion=="2015+2016data":
  strLumi = "36.2" # 36214.46
elif DataVersion=="2017data":
  strLumi = "43.6" # 43584.4
else:
  print "DataVersion not recognised, exiting"
  sys.exit(0)

# Determine MC campaign
MC16a = False
MC16c = False
MC16d = False
MC15b = False
MC15c = False
if "MC15bMuProfile" in MCVersion:
  MC15b = True
elif "MC15c" in MCVersion:
  MC15c = True
elif "MC16a" in MCVersion:
  MC16a = True
elif "MC16c" in MCVersion:
  MC16c = True
elif "MC16d" in MCVersion:
  MC16d = True
else:
  print "MC Campaign not recognised, exiting"
  sys.exit(0)

# Set Eta binning
EtaBins = ["-3.0","-2.9","-2.8","-2.7","-2.6","-2.5","-2.4","-2.3","-2.2","-2.1","-2.0","-1.9","-1.8","-1.7","-1.6","-1.5","-1.4","-1.3","-1.2","-1.1","-1.0","-0.9","-0.8","-0.7","-0.6","-0.5","-0.4","-0.3","-0.2","-0.1","0.0","0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9","1.0","1.1","1.2","1.3","1.4","1.5","1.6","1.7","1.8","1.9","2.0","2.1","2.2","2.3","2.4","2.5","2.6","2.7","2.8","2.9","3.0",]
etarange = range(15,74)

if ApplyEtaRebinning:
 EtaBins = ["-3.","-2.8", "-2.5", "-1.8"," -1.3"," -1.0"," -0.7","-0.2"," 0."," 0.2"," 0.7"," 1.0"," 1.3"," 1.8"," 2.5"," 2.8"," 3."] 
 etarange = range(0,16)


# Determine Plotting range
if ZJetStudy:
  if jetColl == "2LC":
    minpt = 20.
    maxpt = 408.
    miny = 0.7 
    maxy = 1.2
  elif jetColl == "6LC":
    minpt = 20.
    maxpt = 598.
    miny = 0.95
    maxy = 1.45

if not ZJetStudy:
  if jetColl == "2LC":
    minpt = 35.
    maxpt = 1991.9
    miny = 0.8
    maxy = 1.3
  elif jetColl == "6LC":
    minpt = 35.
    maxpt = 1991.9
    miny = 0.9
    maxy = 1.3
  elif jetColl == "8LC":
    minpt = 70.
    maxpt = 3000.
    miny = 0.9
    maxy = 1.3


# Open input files

# Data
DataFile  = PATH
DataFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/Data/"
DataFile += DataVersion
DataFile += "/"
DataFile += jetColl
DataFile += "/Outputs_vs_pT"
if ZJetStudy:
  DataFile += "_ZJetStudy"
if syst != "Nominal" and syst != "MCSherpa": 
  DataFile += "_"
  DataFile += syst
if ApplyEtaRebinning:
  DataFile += "_EtaRebinned"
if ClosurePlots_insituSmoothed:
  DataFile += "_Insitu"
if ClosurePlots_insituNOTSmoothed:
  DataFile += "_InsitunotSmoothed"
DataFile += ".root"

# MC
MCFile  = PATH
MCFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/RelativeResponse/MC/"
MCFile += MCVersion
MCFile += "/"
MCFile += jetColl
if MC15c:
  MCFile += "/MC15c_"
elif MC15b:
  MCFile += "/MC15c_MC15bmuProfile_"
elif MC16a:
  MCFile += "/MC16a_"
elif MC16c:
  MCFile += "/MC16c_"
elif MC16d:
  MCFile += "/MC16d_"
if not ZJetStudy:
  MCFile += "Pythia_Outputs_vs_pT"
elif ZJetStudy:
  MCFile += "ZJetStudy_Outputs_vs_pT"
if syst != "Nominal": 
  if syst is "MCSherpa":
    MCFile2 = MCFile + "_" + syst + ".root"
  else:
    MCFile += "_" + syst  
if ApplyEtaRebinning:
  MCFile += "_EtaRebinned"
  if syst is "MCSherpa":
    MCFile2 += "_EtaRebinned"
MCFile += ".root"

print "Opening: "+DataFile
fData = ROOT.TFile.Open(DataFile)
if not fData:
  print "DataFile not found, exiting"
  sys.exit(0)
print "Opening: "+MCFile
fMC = ROOT.TFile.Open(MCFile)
if not fMC:
  print "MCFile not found, exiting"
  sys.exit(0)
if syst is "MCSherpa":
  print "Opening: "+MCFile2
  fMC2 = ROOT.TFile.Open(MCFile2)
  if not fMC2:
    print "MCFileSyst not found, exiting"
    sys.exit(0)


# Output File
OutputFolder  = PATH
OutputFolder += "Plots_DirectMatching/Response_Data_vs_MC/"
OutputFolder += DataVersion
OutputFolder += "_vs_"
OutputFolder += MCVersion
OutputFolder += "/";
OutputFolder += jetColl

ROOT.SetAtlasStyle()
# Format instance
Format = ROOT.Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Ratio")   # Will show second panel with ratio
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
Format.SetLegendPosition("Top")  # Other options: TopRight, TopLeft, Bottom
Format.SetAxisTitles("#it{p}_{T}^{Ref} [GeV]","#LT #it{p}_{T}^{Rscan}/#it{p}_{T}^{Ref} #GT") # x-axis, y-axis
Format.SetYTitleSecondPanel("MC/Data") # y-axis title of second panel
Format.ShowLuminosity(strLumi)
Format.ShowCME("13")
Format.SetOutputFolder(OutputFolder)
Format.SetLogx()
Format.SetMoreLogLabelsX()
Format.SetRange("X",minpt,maxpt)
Format.SetRange("Y",miny,maxy)
Format.SetYRangeSecondPanel(0.85,1.2)
if ClosurePlots_insituSmoothed or ClosurePlots_insituNOTSmoothed:
  Format.SetYRangeSecondPanel(0.95,1.05)

# Loop over eta bins
for bin in etarange:
 name = "Response_vs_pTRef_Eta_"
 name += str(bin)
 outputName = "/Response_vs_pTRef_Eta_"
 outputName += str(bin)
 if syst!="Nominal":
   outputName += "_"
   outputName += syst
 if ApplyEtaRebinning:
   outputName += "_etaRebinned"
 if ClosurePlots_insituSmoothed:
   outputName += "_Insitu"
 if ClosurePlots_insituNOTSmoothed:
   outputName += "_InsitunotSmoothed"

 # Instance of Plotter1D
 Plot = ROOT.Plotter1D(outputName)
 Plot.ApplyFormat(Format)
 extraText = "anti-#it{k}_{t} jets, R="
 if jetColl is "2LC":
   extraText += "0.2"
 if jetColl is "6LC":
   extraText += "0.6"
 if jetColl is "8LC":
   extraText += "0.8"
 extraText += ", LC+JES"
 Plot.AddText(0.2,0.55,extraText)
 etaBin  = EtaBins[bin-15]
 if ApplyEtaRebinning:
   etaBin  = EtaBins[bin]
 etaBin += " #leq #eta < "
 if ApplyEtaRebinning:
   etaBin += EtaBins[bin+1]
 else:
   etaBin += EtaBins[bin-14]

 Plot.AddText(0.2,0.47,etaBin)
 if ClosurePlots_insituSmoothed or ClosurePlots_insituNOTSmoothed:
   Plot.PlotLineSecondPanel("Y",1.01)
   Plot.PlotLineSecondPanel("Y",0.99)
 if Debug:
   Plot.Debug()
 Plot.AddHist("Data",fData,name)
 if syst is "MCSherpa":
   if ZJetStudy:
     Plot.AddHist("Powheg+Pythia",fMC,name)
   else:
     Plot.AddHist("Pythia8",fMC,name)
   Plot.AddHist("Sherpa",fMC2,name)
 else:
   Plot.AddHist("MC",fMC,name)
  
 
 Plot.Write()  # Saves plot into "pT.pdf"
