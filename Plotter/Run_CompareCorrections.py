#!/usr/bin/python

##############################################################################################
# Run_CompareCorrections.py
# author: Jona Bossio
# date: 27/10/2017
# purpose: Compare smoothed calibration factors (run first Run_SplitSmoothedCorrbyEtaBins.py)
#          for instance, two calibrations were derived with different versions of codes
#          so this helps to determine the impact on the calibration factors
#          due to the changes implemented in the new version
##############################################################################################

import os, sys
import ROOT
ROOT.gROOT.SetBatch(True)  # so does not pop up plots!
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Check
Debug = False

# Check
PATHNum = "PATHNum/"
PATHDen = "PATHDen/"

NameNum = "New"
NameDen = "Old"

# Check
jetColl = "2LC"
#jetColl = "6LC"
#jetColl = "8LC"

# R21
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

# R20.7
#MCVersion = "MC15cPythia_MC15bMuProfile"
#DataVersion = "2015data"

##################################################################################
# DO NOT MODIFY
##################################################################################

# Set Luminosity
strLumi = ""
if DataVersion=="2016data":
  strLumi = "32.9"
if DataVersion=="2015data": # R21 only
  strLumi = "3.2"
elif DataVersion=="2015+2016data":
  strLumi = "36.2"
elif DataVersion=="2017data":
  strLumi = "43.6"
else:
  print "DataVersion not recognised, exiting"
  sys.exit(0)

# Set Eta binning
EtaBins = [
  "-3.0",
  "-2.9",
  "-2.8",
  "-2.7",
  "-2.6",
  "-2.5",
  "-2.4",
  "-2.3",
  "-2.2",
  "-2.1",
  "-2.0",
  "-1.9",
  "-1.8",
  "-1.7",
  "-1.6",
  "-1.5",
  "-1.4",
  "-1.3",
  "-1.2",
  "-1.1",
  "-1.0",
  "-0.9",
  "-0.8",
  "-0.7",
  "-0.6",
  "-0.5",
  "-0.4",
  "-0.3",
  "-0.2",
  "-0.1",
  "0.0",
  "0.1",
  "0.2",
  "0.3",
  "0.4",
  "0.5",
  "0.6",
  "0.7",
  "0.8",
  "0.9",
  "1.0",
  "1.1",
  "1.2",
  "1.3",
  "1.4",
  "1.5",
  "1.6",
  "1.7",
  "1.8",
  "1.9",
  "2.0",
  "2.1",
  "2.2",
  "2.3",
  "2.4",
  "2.5",
  "2.6",
  "2.7",
  "2.8",
  "2.9",
  "3.0",
]

# Open input files

# Denominator
inputDen  = PATHDen
inputDen += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/"
inputDen += DataVersion
inputDen += "_vs_"
inputDen += MCVersion
inputDen += "/"
inputDen += jetColl
inputDen += "/SmoothedCorr_vs_pTRscan.root"
print "Openning: "+inputDen
fDen = ROOT.TFile.Open(inputDen)
if not fDen:
  print "File not found, exiting"
  sys.exit(0)

# Numerator
inputNum  = PATHNum
inputNum += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/"
inputNum += DataVersion
inputNum += "_vs_"
inputNum += MCVersion
inputNum += "/"
inputNum += jetColl
inputNum += "/SmoothedCorr_vs_pTRscan.root"
print "Openning: "+inputNum
fNum = ROOT.TFile.Open(inputNum)
if not fNum:
  print "File not found, exiting"
  sys.exit(0)

# Output Folder
outputFolder = NameNum+"_vs_"+NameDen+"_"+jetColl
command = "mkdir "+outputFolder
os.system(command)


ROOT.SetAtlasStyle()

# Format instance
Format = ROOT.Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Ratio")   # Will show second panel with ratio
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
Format.SetLegendPosition("Top")  # Other options: TopRight, TopLeft, Bottom
Format.SetAxisTitles("#it{p}_{T}^{Rscan} [GeV]","Correction") # x-axis, y-axis
Format.SetYTitleSecondPanel(NameNum+"/"+NameDen) # y-axis title of second panel
Format.ShowLuminosity(strLumi)
Format.ShowCME("13")
Format.SetOutputFolder(outputFolder)
Format.SetLogx()
Format.SetMoreLogLabelsX()
Format.SetRange("X",55.,1310)
if jetColl=="2LC":
  Format.SetRange("Y",0.8,1.4)
elif jetColl=="6LC":
  Format.SetRange("Y",0.9,1.4)
elif jetColl=="8LC":
  Format.SetRange("Y",0.9,1.4)
Format.SetYRangeSecondPanel(0.95,1.05)

# Loop over eta bins
for bin in range(15,75):
  name  = "SmoothedCorr_vs_pT_Eta_"
  name += str(bin)
  outputName = "Compare_"
  outputName += NameNum
  outputName += "_vs_"
  outputName += NameDen
  outputName += "_vs_pT_Eta_"
  outputName += str(bin)
  Plot = ROOT.Plotter1D(outputName)
  Plot.ApplyFormat(Format)
  Plot.AddHist(NameNum,fNum,name,"HIST C")
  Plot.AddHist(NameDen,fDen,name,"HIST C")
  extraText = "anti-#it{k}_{t} jets, R="
  if jetColl is "2LC":
    extraText += "0.2"
  if jetColl is "6LC":
    extraText += "0.6"
  if jetColl is "8LC":
    extraText += "0.8"
  extraText += ", LC+JES"
  Plot.AddText(0.2,0.55,extraText)
  etaBin  = EtaBins[bin-15]
  etaBin += " #leq #eta < "
  etaBin += EtaBins[bin-14]
  Plot.AddText(0.2,0.47,etaBin)
  Plot.PlotLineSecondPanel("Y",1.01)
  Plot.PlotLineSecondPanel("Y",0.99)
  if Debug:
    Plot.Debug()	  
  Plot.Write()

  




