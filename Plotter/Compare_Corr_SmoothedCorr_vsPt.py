#!/usr/bin/python
import os, sys
import array
from ROOT import *

gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Check
Debug = False
ZJet  = True

# Check
jetColl = "2LC"
#jetColl = "6LC"

# Check
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

###############################################################
## DO NOT MODIFY
###############################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

Version = DataVersion + "_vs_" + MCVersion

#-------------------------------#
netaBins = 60
Etabins = [-3.0,-2.9,-2.8,-2.7,-2.6,-2.5,-2.4,-2.3,-2.2,-2.1,-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1 ,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0]

etabins =array.array('d',Etabins)

#------- Open Input File -------#

InputFileS = PATH +"ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/" + Version+ "/" + jetColl +"/" + "SmoothedCorr_vs_pTRscan.root"
print "Opened: " + InputFileS
fileS = TFile.Open(InputFileS,"Read")

InputFileC = PATH + "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/" + Version + "/"+ jetColl + "/" + "Corr_vs_pTrscan" 
if ZJet:
  InputFileC += "_ZJet"
InputFileC += ".root"

print "Opened: " + InputFileC
fileC = TFile.Open(InputFileC,"read")

#------- Style -------#

SetAtlasStyle()

text1 = DataVersion
if jetColl is "2LC":
  text2 = "anti-#it{k}_{t} jets, R = 0.2, LC+JES"
elif jetColl is "6LC":
  text2 = "anti-#it{k}_{t} jets, R = 0.6, LC+JES"

#-------  Retrievig graphs -------#

gC = TGraphAsymmErrors()
gS = TGraph()
can = TCanvas()
can.SetMargin(0.12,0.04,0.14,0.04)
ps = PATH + "Plots_DirectMatching/Smoothing/" + Version +"/" + jetColl +"/CompareSmoother_vs_pt.pdf"
can.Print(ps + "[")
if not ZJet:
  can.SetLogx()
for eta in range(0,netaBins):
  can.Clear()
  name_tmp = "Corr_vs_pTRscan_Eta_"
  name_tmp += str(eta+15)
  fileC.GetObject(name_tmp, gC)
  gC.GetXaxis().SetTitle("#it{p}_{T}^{Rscan} [GeV]")
  gC.GetYaxis().SetTitle("Correction")
  gC.SetMaximum(1.3)
  gC.SetMinimum(0.7)
  gC.GetXaxis().SetTitleOffset(1.26)
  gC.GetYaxis().SetTitleOffset(1)
  gC.Draw("AP")
  name_tmp = "SmoothedCorr_vs_pT_Eta_"
  name_tmp += str(eta+15)
  fileS.GetObject(name_tmp,gS)
  gS.SetLineColor(2)
  gS.Draw("C")

  # LEGEND
  leg = TLegend(.6,.8,.87,.9)
  leg.SetTextFont(42)
  leg.SetTextSize(0.04);
  leg.AddEntry(gC,"Correction")
  leg.AddEntry(gS,"Smoothed Correction","l")
  leg.Draw("same")
  
  # Extra Text
  l = TLatex( 0.16, 0.87, text1)
  l.SetNDC()
  l.SetTextFont(42)
  l.SetTextSize(0.04)
  l.SetTextColor(1)
  l.Draw("same")
  l1 = TLatex( 0.16, 0.82, text2)
  l1.SetNDC()
  l1.SetTextFont(42)
  l1.SetTextSize(0.035)
  l1.SetTextColor(1)
  l1.Draw("same")
  text3 = "Dijets"
  if ZJet:
    text3 = "Z+Jets"
  l2 = TLatex(0.16, 0.77, text3)
  l2.SetNDC()
  l2.SetTextFont(42)
  l2.SetTextSize(0.035)
  l2.SetTextColor(1)
  l2.Draw("same")
  text4 = str(Etabins[eta]) + " #leq #eta < " + str(Etabins[eta+1])
  l3 = TLatex(0.16, 0.72, text4)
  l3.SetNDC()
  l3.SetTextFont(42)
  l3.SetTextSize(0.033)
  l3.SetTextColor(1)
  l3.Draw("same")
  can.Print(ps)

can.Print(ps +"]")

fileC.Close()
fileS.Close()
