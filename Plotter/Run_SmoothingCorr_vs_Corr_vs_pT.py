#!/usr/bin/python
import os, sys
import ROOT
ROOT.gROOT.SetBatch(True)  # so does not pop up plots!
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Check
Debug     = False
ZJetStudy = True
ApplyMap  = True

# Check
jetColl = "2LC"
#jetColl = "6LC"
#jetColl = "8LC"

# Check
#MCVersion = "MC16a"
#DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
MCVersion = "MC16d"
DataVersion = "2017data"

# Check
syst = "Nominal"
#syst = "deltaRUp"
#syst = "deltaRDown"
#syst = "pT3Up"
#syst = "pT3Down"
#syst = "MCSherpa"


##################################################################################
# DO NOT MODIFY
##################################################################################

# R20.7
#MCVersion = "MC15cPythia_MC15bMuProfile"
#DataVersion = "2015data"

# In this path you should have the folder ROOTfiles
PATH = "../"

# Set Luminosity
strLumi = ""
if DataVersion=="2016data":
  strLumi = "32.9"
if DataVersion=="2015data": # R21 only
  strLumi = "3.2"
elif DataVersion=="2015+2016data":
  strLumi = "36.2"
elif DataVersion=="2017data":
  strLumi = "43.6"
else:
  print "DataVersion not recognised, exiting"
  sys.exit(0)

# Set Eta binning
EtaBins = ["-3.0","-2.9","-2.8","-2.7","-2.6","-2.5","-2.4","-2.3","-2.2","-2.1","-2.0","-1.9","-1.8","-1.7","-1.6","-1.5","-1.4","-1.3","-1.2","-1.1","-1.0","-0.9","-0.8","-0.7","-0.6","-0.5","-0.4","-0.3","-0.2","-0.1","0.0","0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9","1.0","1.1","1.2","1.3","1.4","1.5","1.6","1.7","1.8","1.9","2.0","2.1","2.2","2.3","2.4","2.5","2.6","2.7","2.8","2.9","3.0",]

# Determine Plotting range
if ZJetStudy:
  if jetColl == "2LC":
    minpt = 20.
    maxpt = 408.
    miny = 0.85 
    maxy = 1.35
  elif jetColl == "6LC":
    minpt = 20.
    maxpt = 598.
    miny = 0.95
    maxy = 1.45

if not ZJetStudy:
  if jetColl == "2LC":
    minpt = 35.
    maxpt = 1991.9
    miny = 0.8
    maxy = 1.3
  elif jetColl == "6LC":
    minpt = 35.
    maxpt = 1991.9
    miny = 0.9
    maxy = 1.3
  elif jetColl == "8LC":
    minpt = 70.
    maxpt = 3000.
    miny = 0.9
    maxy = 1.3

# Open input files

# Smoother Correction
SFile  = PATH
SFile +=  "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorr/"
SFile += DataVersion
SFile += "_vs_"
SFile += MCVersion
SFile += "/"
SFile += jetColl
SFile += "/SmoothedCorr_vs_pTRscan"
if syst !="Nominal":
  SFile += "_"
  SFile += syst
SFile += ".root"
print "Opening: "+SFile
fS = ROOT.TFile.Open(SFile)
if not fS:
  print "File not found, exiting"	 
  sys.exit(0)

# Correction
CFile  = PATH
CFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/Correction/"
CFile += DataVersion
CFile += "_vs_"
CFile += MCVersion
CFile += "/"
CFile += jetColl
CFile += "/Corr_vs_pTrscan"
if(ZJetStudy):
  CFile += "_ZJet"
if syst != "Nominal":
  CFile += "_"
  CFile += syst
CFile += ".root"
print "Openning: "+CFile
fC = ROOT.TFile.Open(CFile)
if not fC:
  print "File not found, exiting"
  sys.exit(0)

# Output Folder
outputFolder  = PATH
outputFolder += "Plots_DirectMatching/Smoothing/"
outputFolder += DataVersion
outputFolder += "_vs_"
outputFolder += MCVersion
outputFolder += "/"
outputFolder += jetColl

ROOT.SetAtlasStyle()

# Format instance
Format = ROOT.Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Ratio")   # Will show second panel with ratio
Format.SetLegendPosition("Top")  # Other options: TopRight, TopLeft, Bottom
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
Format.SetAxisTitles("#it{p}_{T} [GeV]","Correction") # x-axis, y-axis
Format.SetYTitleSecondPanel("#scale[0.9]{SmoothedCorr/Corr}") # y-axis title of second panel
Format.SetYRangeSecondPanel(0.95,1.05)
Format.ShowLuminosity(strLumi)
Format.ShowCME("13")
Format.SetOutputFolder(outputFolder)
Format.SetLogx()
Format.SetMoreLogLabelsX()
Format.SetRange("X",minpt,maxpt)
Format.SetRange("Y",miny,maxy)

for bin in range(15,75):
  name = "Corr_vs_pT_Eta_"
  name += str(bin)
  name2 = "SmoothedCorr_vs_pT_Eta_"
  name2 += str(bin)
  outputName = "Comparison_vs_pT_Eta_"
  outputName += str(bin)
  outputName += "_"
  outputName += syst
  Plot = ROOT.Plotter1D(outputName)
  Plot.ApplyFormat(Format)
  Plot.PlotLineSecondPanel("Y",1.01)
  Plot.PlotLineSecondPanel("Y",0.99)
  if ApplyMap:
    Plot.AddHist("Correction",fS,name,"E")
  else:
    Plot.AddHist("Correction",fC,name)
  Plot.AddHist("Smoothed Correction",fS,name2,"HIST C")
  extraText = "anti-#it{k}_{t} jets, R="
  if jetColl is "2LC":
    extraText += "0.2"
  if jetColl is "6LC":
    extraText += "0.6"
  if jetColl is "8LC":
    extraText += "0.8"
  extraText += ", LC+JES"
  Plot.AddText(0.2,0.55,extraText)
  etaBin  = EtaBins[bin-15]
  etaBin += " #leq #eta < "
  etaBin += EtaBins[bin-14]
  Plot.AddText(0.2,0.47,etaBin)
  Plot.SetYRangeSecondPanel(0.95,1.05)
  if Debug:
    Plot.Debug()
  Plot.Write()
