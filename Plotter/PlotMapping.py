import os, sys
from ROOT import *
import array

gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Check
ZJets = True

# Check
jetColl = "2LC"
#jetColl = "6LC"

# Check
Version = "2015+2016data"
#Version = "2017data"

ApplyEtaRebinning = True

##########################################################################
## DO NOT MODIFY
##########################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

if ZJets:
  minx = 10.
  maxx = 300.
else: #DIJETS!!
  minx = 35.
  maxx = 2000.

#-------------------------------#
netaBins = 60
Etabins = [-3.0,-2.9,-2.8,-2.7,-2.6,-2.5,-2.4,-2.3,-2.2,-2.1,-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1 ,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0]

etabins =array.array('d',Etabins)

if ApplyEtaRebinning:
 netaBins = 16
 Etabins = [-3.,-2.8, -2.5, -1.8, -1.3, -1.0, -0.7,-0.2, 0., 0.2, 0.7, 1.0, 1.3, 1.8, 2.5, 2.8, 3.] 
 etarange = range(0,16)
 netaBins = 16
 etabins =array.array('d',Etabins)

#------- Open Input File -------#

InputFile = PATH + "ROOTfiles/DirectMatching/Mapping/" + Version+ "/" + jetColl +"/" + "MappingtoPtRscan"
if ZJets:
  InputFile += "_ZJetStudy"
if ApplyEtaRebinning:
  InputFile += "_etaRebinned"
InputFile += ".root"
file = TFile.Open(InputFile,"Read")
print "Opened: " + InputFile

SetAtlasStyle()

can = TCanvas()
if not ZJets:
  can.SetLogx()
  can.SetLogy()
#can.SetMargin(0.12,0.04,0.12,0.04)

gr1 = TGraphErrors()
gr2 = TGraphErrors()
gr3 = TGraphErrors()
gr4 = TGraphErrors()
gr5 = TGraphErrors()

Gr = [gr1,gr2,gr3,gr4,gr5]
ran = [45, 50, 56, 65, 73]  # just a few etas in one plot
if ApplyEtaRebinning:
  ran = [8,9,10,12,14]
colors = [kBlack,kRed+2,kGreen+2,kMagenta+1,kOrange+2]
leg = TLegend(.75,.2,.85,.45)
leg.SetTextFont(42)
leg.SetTextSize(0.04);

for i, c, gr in zip(ran,colors,Gr):
  tmp = "Mapping_in_PtRefBins_eta_"
  if ApplyEtaRebinning:
    tmp += str(i)
  else:
    tmp += str(i-15)
  file.GetObject(tmp, gr)
  gr.SetMarkerColor(c)
  gr.SetLineColor(c)
  gr.SetMarkerSize(0.7)
  if i is ran[0]:
    gr.GetYaxis().SetRangeUser(minx-10.,maxx+50)
    gr.Draw("AP")
    gr.GetXaxis().SetLimits(minx,maxx)
    gr.GetXaxis().SetTitleOffset(0.98)
    gr.GetYaxis().SetTitleOffset(1.3)
    gr.GetXaxis().SetTitle("#it{p}_{T}^{Ref} [GeV]")
    gr.GetXaxis().SetTitleOffset(1.3)
    gr.GetYaxis().SetTitle("#LT #it{p}_{T}^{Rscan} #GT [GeV]")
  else:
    gr.Draw("P same")
  if ApplyEtaRebinning:
    tmp = str(etabins[i]) +" #leq #eta < "+str(etabins[i+1])
  else:
    tmp = str(etabins[i-15])+" #leq #eta < "+str(etabins[i-14])
  leg.AddEntry(gr,tmp)

text1 = Version
if jetColl is "2LC":
  text2 = "anti-#it{k}_{t} jets, R = 0.2, LC+JES"
elif jetColl is "6LC":
  text2 = "anti-#it{k}_{t} jets, R = 0.6, LC+JES+GSC"
if ZJets:
  text3 = "Z+Jets"
else:
  text3 = "Dijets"

#l = TLatex( 0.16, 0.87, text1)
l = TLatex( 0.2, 0.87, text1)
l.SetNDC()
l.SetTextFont(42)
l.SetTextSize(0.04)
l.SetTextColor(1)
l.Draw("same")

#l1 = TLatex( 0.16, 0.82, text2)
l1 = TLatex( 0.2, 0.82, text2)
l1.SetNDC()
l1.SetTextFont(42)
l1.SetTextSize(0.04)
l1.SetTextColor(1)
l1.Draw("same")

#l2 = TLatex( 0.16, 0.82, text2)
l2 = TLatex( 0.2, 0.77, text3)
l2.SetNDC()
l2.SetTextFont(42)
l2.SetTextSize(0.04)
l2.SetTextColor(1)
l2.Draw("same")


leg.Draw("same")

OutputFile = PATH +"Plots_DirectMatching/Mapping/" + Version+ "/" + jetColl +"/" + "MappingPlot"
if ZJets:
  OutputFile += "_ZJetStudy"
if ApplyEtaRebinning:
  OutputFile += "_etaRebinned"
OutputFile += ".pdf"

can.Print(OutputFile)


  
  

