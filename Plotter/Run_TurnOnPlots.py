#!/usr/bin/python
import os, sys
import ROOT
ROOT.gROOT.SetBatch(True)  # so does not pop up plots!
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Check
Debug = False

# Check
jetColl = "2LC"
#jetColl = "6LC"

# Check
Dataset = "2015"
#Dataset = "2016"
#Dataset = "2017"

# Merge all the outputs of the trigger emulation and provide that file
InputFile = "TrigEmu_DATE_2LC_Final.root" # Example

# Define them based on pT binning
Thresholds2015_2LC = [ # UPDATE THEM
  25,
  35,
  45,
  55,
  70,
  85,
  116,
  134,
  194,
  216,
  264,
  318,
  442,
  442,
]
Thresholds2015_6LC = [ # UPDATE THEM
  25,
  35,
  45,
  55,
  70,
  85,
  116,
  134,
  194,
  216,
  264,
  318,
  442,
  442,
]
Thresholds2016_2LC = [ # UPDATE THEM
  25,
  35,
  45,
  55,
  70,
  85,
  116,
  134,
  194,
  216,
  264,
  318,
  442,
]
Thresholds2016_6LC = [ # UPDATE THEM
  25,
  35,
  45,
  55,
  70,
  85,
  116,
  134,
  194,
  216,
  264,
  318,
  442,
]
Thresholds2017_2LC = [ # UPDATE THEM
  25,
  35,
  45,
  55,
  70,
  85,
  116,
  194,
  264,
  318,
  442,
]
Thresholds2017_6LC = [ # UPDATE THEM
  25,
  35,
  45,
  55,
  70,
  85,
  116,
  194,
  264,
  318,
  442,
]

##################################################################################
# DO NOT MODIFY
##################################################################################

PATH = "../ROOTfiles/DirectMatching/TriggerEmulation/"

var = "pTrscan"
#var = "pTavg"
#var = "pTavg_rscan"

# Set Luminosity
strLumi = ""
if Dataset=="2016":
  strLumi = "32.9"
elif Dataset=="2015": # R21 only
  strLumi = "3.2"
elif Dataset=="2015+2016":
  strLumi = "36.2"
elif Dataset=="2017":
  strLumi = "43.6"
else:
  print "Dataset not recognised, exiting"
  sys.exit(0)

Thresholds = []
if Dataset is "2015":
  if jetColl is "2LC":	
    Thresholds = Thresholds2015_2LC
  if jetColl is "6LC":	
    Thresholds = Thresholds2015_6LC
elif Dataset is "2016":
  if jetColl is "2LC":	
    Thresholds = Thresholds2016_2LC
  if jetColl is "6LC":	
    Thresholds = Thresholds2016_6LC
elif Dataset is "2017":
  if jetColl is "2LC":	
    Thresholds = Thresholds2017_2LC
  if jetColl is "6LC":	
    Thresholds = Thresholds2017_6LC
else:
  print "Dataset not recognised, exiting"
  sys.exit(0)

Triggers15 = [
  "HLT_j15",
  "HLT_j25",
  "HLT_j35",
  "HLT_j45",
  "HLT_j55",
  "HLT_j60",
  "HLT_j85",
  "HLT_j110",
  "HLT_j150",
  "HLT_j175",
  "HLT_j200",
  "HLT_j260",
  "HLT_j320",
  "HLT_j360",
]
Triggers16 = [
  "HLT_j15",
  "HLT_j25",
  "HLT_j35",
  "HLT_j45",
  "HLT_j55",
  "HLT_j60",
  "HLT_j85",
  "HLT_j110",
  "HLT_j150",
  "HLT_j175",
  "HLT_j260",
  "HLT_j380",
  "HLT_j400",
]
Triggers17 = [
  "HLT_j15",
  "HLT_j25",
  "HLT_j35",
  "HLT_j45",
  "HLT_j60",
  "HLT_j85",
  "HLT_j110",
  "HLT_j175",
  "HLT_j260",
  "HLT_j360",
  "HLT_j420",
]
Triggers = []
if Dataset is "2015":
  Triggers = Triggers15
if Dataset is "2016":
  Triggers = Triggers16
if Dataset is "2017":
  Triggers = Triggers17

# PATH to input file
if Dataset is "2015":
  PATH += "R21_2015data"
elif Dataset is "2016":
  PATH += "R21_2016data"
elif Dataset is "2017":
  PATH += "2017data"
PATH += "/"

# Output Folder
outputFolder = "../Plots_DirectMatching/TriggerEmulation/"
if Dataset is "2015":
  outputFolder += "R21_2015data/"
if Dataset is "2016":
  outputFolder += "R21_2016data/"
if Dataset is "2017":
  outputFolder += "2017data/"
outputFolder += jetColl

#Protection
if len(Triggers) != len(Thresholds):
  print "Number of triggers and thresholds don't match, exiting"
  sys.exit(0)

# Open input file
print "Opennning: "+PATH+InputFile
File = ROOT.TFile.Open(PATH+InputFile)
if not File:
  print "InputFile not found, exiting"
  sys.exit(0)


ROOT.SetAtlasStyle()

# Format instance
Format = ROOT.Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("RatioOnly")   # Will show second panel with ratio
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
Format.SetLegendPosition("TopLeft")  # Other options: TopRight, TopLeft, Bottom
if var == "pTrscan":
  Format.SetAxisTitles("#it{p}_{T}^{Rscan} [GeV]","Efficiency") # x-axis, y-axis
elif "avg" in var: 
  Format.SetAxisTitles("#it{p}_{T}^{Avg} [GeV]","Efficiency") # x-axis, y-axis
Format.SetYTitleSecondPanel("Efficiency") # y-axis title of second panel
Format.ShowLuminosity(strLumi)
Format.ShowCME("13")
Format.SetOutputFolder(outputFolder)
Format.SetLogx()
Format.SetMoreLogLabelsX()
Format.SetRange("X",55.,1000.)
Format.SetRange("Y",0,1.1)
Format.ShowNoErrorsSecondPanel()
#if jetColl=="2LC":
#elif jetColl=="6LC":
#elif jetColl=="8LC":

for bin in range(0,len(Triggers)):
  trigger = Triggers[bin]	
  num  = "pT_offline_num_"
  num += trigger
  den  = "pT_offline_den_"
  den += trigger
  outputName = trigger
  Plot = ROOT.Plotter1D(outputName)
  Plot.ApplyFormat(Format)
  # Show Threshold
  Plot.PlotLine("Y",Thresholds[bin])
  Plot.AddHist("1",File,den)
  Plot.AddHist("2",File,num)
  Plot.NoTLegends()
  if trigger == "HLT_j85":
    Plot.SetRange("X",10,300)
  if trigger == "HLT_j60":
    Plot.SetRange("X",10,300)
  if trigger == "HLT_j55":
    Plot.SetRange("X",10,300)
  if trigger == "HLT_j45":
    Plot.SetRange("X",10,300)
  if trigger == "HLT_j420":
    Plot.SetRange("X",100.,550.)
  if trigger == "HLT_j400":
    Plot.SetRange("X",100.,550.)
  if trigger == "HLT_j380":
    Plot.SetRange("X",100.,550.)
  if trigger == "HLT_j15" or trigger == "HLT_j25" or trigger == "HLT_j35":
    Plot.SetRange("X",10,300)
  if Debug:
    Plot.Debug()
  Plot.Write()

