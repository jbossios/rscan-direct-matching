#!/usr/bin/python
import os, sys
import array
from ROOT import *

# Check
jetColl = "2LC"
#jetColl = "6LC"

ZJetStudy = True
ApplyEtaRebinning = True

# Check
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

Systs = [
  "DeltaRUp",
  "DeltaRDown",
  "IsolationUp",
  "IsolationDown",
  "JVTUp",
  "JVTDown",
  "MCSherpa",
]

###################################################################
## DO NOT MODIFY
###################################################################

# In this path you should have the folder ROOTfiles
PATH = "../"

gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

Version = DataVersion + "_vs_" + MCVersion

# Eta bins
netaBins = 60
Etabins = [-3.0,-2.9,-2.8,-2.7,-2.6,-2.5,-2.4,-2.3,-2.2,-2.1,-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1 ,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0]

etabins =array.array('d',Etabins)
if ApplyEtaRebinning:
 netaBins = 16
 Etabins = [-3.,-2.8, -2.5, -1.8, -1.3, -1.0, -0.7,-0.2, 0., 0.2, 0.7, 1.0, 1.3, 1.8, 2.5, 2.8, 3.] 
 etarange = range(0,16)
 netaBins = 16


SetAtlasStyle()
text1 = DataVersion
if jetColl is "2LC":
  text2 = "anti-#it{k}_{t} jets, R = 0.2, LC+JES"
elif jetColl is "6LC":
  text2 = "anti-#it{k}_{t} jets, R = 0.6, LC+JES"

# Open File with rebinned systematic uncertainties
RebinFileName  = PATH +"ROOTfiles/DirectMatching/SystematicInputsToCombination/" + Version+ "/RebinnedSystematics_vs_ptrscan"
if ZJetStudy:
  RebinFileName += "_ZJetStudy"
RebinFileName += "_"
RebinFileName += jetColl
if ApplyEtaRebinning:
  RebinFileName += "_etaRebinned"
RebinFileName += ".root"
RebinFile = TFile.Open(RebinFileName,"READ")
if not RebinFile:
  print RebinFileName+" not found, exiting"
  sys.exit(0)

# Open Input File with final systs
InputFile  = PATH +"ROOTfiles/DirectMatching/SystematicInputsToCombination/" + Version + "/Final_Systematics_vs_ptrscan"
if ZJetStudy:
  InputFile += "_ZJetStudy"
InputFile += "_" + jetColl
if ApplyEtaRebinning:
  InputFile += "_etaRebinned"
InputFile += ".root"
print "Opening: " + InputFile
file = TFile.Open(InputFile,"Read")
if not file:
  print InputFile+" is not found, exiting"
  sys.exit(0)

for syst in Systs:
  
  can = TCanvas()
  can.SetMargin(0.12,0.04,0.14,0.04)

  # output PDF
  ps  = PATH + "Plots_DirectMatching/Systematics/" + Version + "/" + jetColl + "/SimVSnotSim_"
  ps += syst
  ps += "_vs_ptrscan"
  if ZJetStudy:
    ps += "_ZJetStudy"
  if ApplyEtaRebinning:
    ps += "_etaRebinned"
  ps += ".pdf"
  can.Print(ps + "[")
  can.SetLogx()

  # Loop over etabins
  for eta in range(0,netaBins):
    can.Clear()
    name_tmp = "Final_"+syst+"_Eta_"
    yeta = eta+15
    if ApplyEtaRebinning:
      yeta = eta
    name_tmp += str(yeta)
    name_tmp += "_"
    mg = TMultiGraph()
    # Loop over systematics
    n = 0 # counter for number of graphs (systs)
    graph = TGraphAsymmErrors()	  
    file.GetObject(name_tmp, graph)
    graph.GetXaxis().SetTitle("#it{p}_{T}^{Rscan} [GeV]")
    graph.GetYaxis().SetTitle("Uncertainty")
    graph.SetMaximum(1.2)
    graph.SetMinimum(0.9)
    graph.GetXaxis().SetMoreLogLabels()
    graph.SetMarkerColor(kBlack)
    graph.SetMarkerSize(0.5)
    #graph.SetLineStyle(LineStyles[n])
    graph.SetLineColor(kBlack)
    mg.Add(graph)
    rebingraph = TGraphAsymmErrors()
    RebinFile.GetObject(syst+"_Eta_"+str(yeta),rebingraph)
    rebingraph.SetMarkerColor(kRed)
    rebingraph.SetLineColor(kRed)
    rebingraph.SetMarkerSize(0.5)
    mg.Add(rebingraph)
  
    # Plot MultiGraph
    mg.Draw("AP")
    mg.GetHistogram().GetXaxis().SetTitle("#it{p}_{T}^{Rscan} [GeV]")
    mg.GetHistogram().GetYaxis().SetTitle("Uncertainty")
    mg.GetHistogram().GetXaxis().SetTitleOffset(1.2)
    mg.GetHistogram().GetYaxis().SetTitleOffset(1.1)
    mg.GetHistogram().GetXaxis().SetMoreLogLabels()
    mg.GetXaxis().SetLimits(10.,3000.)
    mg.GetHistogram().GetYaxis().SetRangeUser(-0.04,0.04)

    # Legends
    leg = TLegend(.6,.7,.87,.9) # x1, y1, x2, y2
    leg.SetTextFont(42)
    leg.SetTextSize(0.04)
    leg.AddEntry(graph,"original bins")
    leg.AddEntry(rebingraph,"rebinned bins")
    leg.Draw("same")
  
    # Extra Text
    l = TLatex( 0.16, 0.87, text1)
    l.SetNDC()
    l.SetTextFont(42)
    l.SetTextSize(0.04)
    l.SetTextColor(1)
    l.Draw("same")
    l1 = TLatex( 0.16, 0.82, text2)
    l1.SetNDC()
    l1.SetTextFont(42)
    l1.SetTextSize(0.035)
    l1.SetTextColor(1)
    l1.Draw("same")
    text4 = str(Etabins[eta]) + " #leq #eta < " + str(Etabins[eta+1])
    l3 = TLatex(0.16, 0.77, text4)
    l3.SetNDC()
    l3.SetTextFont(42)
    l3.SetTextSize(0.033)
    l3.SetTextColor(1)
    l3.Draw("same")

    # Draw line at zero
    xmin = mg.GetXaxis().GetXmin()
    xmax = mg.GetXaxis().GetXmax()
    Line = TLine(xmin, 0., xmax, 0.);
    Line.SetLineStyle(2);
    Line.SetLineColor(kBlack);
    Line.Draw("same");

    can.Print(ps)

  can.Print(ps +"]")
