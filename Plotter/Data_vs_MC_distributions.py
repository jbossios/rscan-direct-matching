#!/usr/bin/python
import os, sys
import ROOT
ROOT.gROOT.SetBatch(True)  # so does not pop up plots!
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Check
Debug = False

ZJets = False

# Check
jetColl = "2LC"
#jetColl = "6LC"

# Check
MCVersion = "MC16a"
DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
#MCVersion = "MC16d"
#DataVersion = "2017data"

# In this path you should have the folder ROOTfiles
PATH = "../"

OutputFolder = PATH + "Plots_DirectMatching/Kinematics/" + MCVersion + "_vs_" + DataVersion + "/" + jetColl
PATH += "ROOTfiles/DirectMatching/TreetoHists_Outputs/"
MC  = PATH
if ZJets:
  MC += "MC/"+MCVersion+"/"+jetColl+"/"+MCVersion+"_ZJetStudy_PRW_MinPt_Rscan10_Ref20.root"
else:
  MC += "MC/"+MCVersion+"/"+jetColl+"/"+MCVersion+"_Pythia_JZAll_PRW.root"
MCSherpa  = PATH
MCSherpa += "MC/"+MCVersion+"/"+jetColl+"/MC16a_Pythia_JZAll_PRW_MCSherpa_bootstrap.root"
Data  = PATH
Data += "Data/"+DataVersion+"/"+jetColl+"/DataAll.root"

##################################################################################
# DO NOT MODIFY
##################################################################################



# Set Luminosity
strLumi = ""
if DataVersion=="2016data":
  strLumi = "32.9" # 32994.9
if DataVersion=="2015data": # R21 only
  strLumi = "3.2"  # 3219.56
elif DataVersion=="2015+2016data":
  strLumi = "36.2" # 36214.46
elif DataVersion=="2017data":
  strLumi = "43.6" # 43584.4
else:
  print "DataVersion not recognised, exiting"
  sys.exit(0)

# Determine MC campaign
MC16a = False
MC16c = False
MC16d = False
MC15b = False
MC15c = False
if "MC15bMuProfile" in MCVersion:
  MC15b = True
elif "MC15c" in MCVersion:
  MC15c = True
elif "MC16a" in MCVersion:
  MC16a = True
elif "MC16c" in MCVersion:
  MC16c = True
elif "MC16d" in MCVesion:
  MC16d = True
else:
  print "MC Campaign not recognised, exiting"
  sys.exit(0)

# Open input files

print "Opening: "+Data
fData = ROOT.TFile.Open(Data)
if not fData:
  print "DataFile not found, exiting"
  sys.exit(0)
print "Opening: "+MC
fMC = ROOT.TFile.Open(MC)
if not fMC:
  print "MCFile not found, exiting"
  sys.exit(0)
print "Opening: "+MCSherpa
fMCSherpa = ROOT.TFile.Open(MCSherpa)
if not fMCSherpa:
  print "MCSherpa File not found, exiting"
  sys.exit(0)

ROOT.SetAtlasStyle()

# Format instance
Format = ROOT.Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Ratio")   # Will show second panel with ratio
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
Format.NormalizeAll("Integral")
Format.SetLegendPosition("TopRight")  # Other options: TopRight, TopLeft, Bottom
Format.SetAxisTitles("<#mu>","Arbitrary Units") # x-axis, y-axis
Format.SetYTitleSecondPanel("MC/Data") # y-axis title of second panel
Format.ShowLuminosity(strLumi)
Format.ShowCME("13")
Format.SetRange("X",0,100)
Format.SetRange("Y",0,0.07)
Format.SetYRangeSecondPanel(0,2)
Format.SetReferenceSecondPanel("Data")
Format.SetOutputFolder(OutputFolder)

# Instance of Plotter1D
outputName = "Mu_PRW_ZJets"
Plot = ROOT.Plotter1D(outputName)
Plot.ApplyFormat(Format)
if Debug:
  Plot.Debug()
name = "AverageMu"
Plot.AddHist("Data",fData,name)
Plot.AddHist("MC",fMC,name)
if ZJets:
  Plot.Write()  # Saves plot into "outputName.pdf"

outputName = "Mu_noPRW_ZJets"
Plot_noPRW = ROOT.Plotter1D(outputName)
Plot_noPRW.ApplyFormat(Format)
if Debug:
  Plot_noPRW.Debug()
name = "AverageMu_noPRW"
Plot_noPRW.AddHist("Data",fData,name)
Plot_noPRW.AddHist("MC",fMC,name)
if ZJets:
  Plot_noPRW.Write()  # Saves plot into "outputName.pdf"

outputName = "ActualMu_PRW_ZJets"
Plot_ac = ROOT.Plotter1D(outputName)
Plot_ac.ApplyFormat(Format)
name = "ActualMu"
Plot_ac.AddHist("Data",fData,name)
Plot_ac.AddHist("MC",fMC,name)
if ZJets:
  Plot_ac.Write()

outputName = "ActualMu_noPRW_ZJets"
Plot_ac_noPRW = ROOT.Plotter1D(outputName)
Plot_ac_noPRW.ApplyFormat(Format)
name = "ActualMu_noPRW"
Plot_ac_noPRW.AddHist("Data",fData,name)
Plot_ac_noPRW.AddHist("MC",fMC,name)
if ZJets:
  Plot_ac_noPRW.Write()

# Format instance
pTBase = ROOT.Plotter1D()   # This is the constructor for format only
pTBase.SetComparisonType("Ratio")   # Will show second panel with ratio
pTBase.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
pTBase.NormalizeAll("Integral")
pTBase.SetLegendPosition("TopRight")  # Other options: TopRight, TopLeft, Bottom
pTBase.SetYTitleSecondPanel("MC/Data") # y-axis title of second panel
pTBase.ShowLuminosity(strLumi)
pTBase.ShowCME("13")
pTBase.SetLogx()
pTBase.SetLogy()
pTBase.SetMoreLogLabelsX()
pTBase.SetReferenceSecondPanel("Data")
pTBase.SetOutputFolder(OutputFolder)


# Z pT distribution
outputName = "ZpT"
pTZ = ROOT.Plotter1D(outputName)
pTZ.ApplyFormat(pTBase)
name = "ZpT"
pTZ.AddHist("Data",fData,name)
pTZ.AddHist("MC",fMC,name)
pTZ.SetAxisTitles("#it{Z p}_{T} [GeV]","Arbitrary Units") # x-axis, y-axis
if ZJets:
  pTZ.Write()

# Rscan jet pT distribution
if ZJets:
  pTrscan = ROOT.Plotter1D("pT_Rscan_ZJets")
else:
  pTrscan = ROOT.Plotter1D("pT_Rscan")
pTrscan.ApplyFormat(pTBase)
pTrscan.SetAxisTitles("#it{p}_{T} [GeV]","Arbitrary Units") # x-axis, y-axis
if ZJets:
  name = "j_pT_RscanAll"
else:
  name = "j_pT_Rscan"
  if "2LC" in jetColl:
    pTrscan.SetRange("X",49,3900)
  else:
    pTrscan.SetRange("X",59,3900)
  pTrscan.SetYRangeSecondPanel(0.6,1.2)
  pTrscan.SetLegendPosition("Bottom")  # Other options: TopRight, TopLeft, Bottom
if "2LC" in jetColl:
  JetCollLegend = "anti-k_{t} jets, #it{R} = 0.2, LC+JES"
elif "6LC" in jetColl:
  JetCollLegend = "anti-k_{t} jets, #it{R} = 0.6, LC+JES"
pTrscan.AddText(0.6,0.8,JetCollLegend)
pTrscan.AddHist("Data",fData,name)
if ZJets:
  pTrscan.AddHist("Powheg+Pythia",fMC,name)
else:
  pTrscan.AddHist("Pythia",fMC,name)
pTrscan.AddHist("Sherpa",fMCSherpa,name)
pTrscan.Write()


