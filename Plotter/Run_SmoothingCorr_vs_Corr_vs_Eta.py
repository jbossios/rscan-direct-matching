#!/usr/bin/python
import os, sys
import ROOT
ROOT.gROOT.SetBatch(True)  # so does not pop up plots!
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Check
Debug = False

# Check
jetColl = "2LC"
#jetColl = "6LC"
#jetColl = "8LC"

# Check
#MCVersion = "MC16a"
#DataVersion = "2015+2016data"
#MCVersion = "MC16c"
#DataVersion = "2017data"
MCVersion = "MC16d"
DataVersion = "2017data"

# Check
syst = "Nominal"
#syst = "deltaRUp"
#syst = "deltaRDown"
#syst = "pT3Up"
#syst = "pT3Down"
#syst = "MCSherpa"


##################################################################################
# DO NOT MODIFY
##################################################################################

# R20.7
#MCVersion = "MC15cPythia_MC15bMuProfile"
#DataVersion = "2015data"

# In this path you should have the folder ROOTfiles
PATH = "../"

# Set Luminosity
strLumi = ""
if DataVersion=="2016data":
  strLumi = "32.9"
if DataVersion=="2015data": # R21 only
  strLumi = "3.2"
elif DataVersion=="2015+2016data":
  strLumi = "36.2"
elif DataVersion=="2017data":
  strLumi = "43.6"
else:
  print "DataVersion not recognised, exiting"
  sys.exit(0)

nstrpTBins = 43+3
strpTBins = [
  "15",
  "20",
  "25",
  "35",
  "45",
  "55",
  "70",
  "85",
  "100",
  "116",
  "134",
  "152",
  "172",
  "194",
  "216",
  "240",
  "264",
  "290",
  "318",
  "346",
  "376",
  "408",
  "442",
  "478",
  "516",
  "556",
  "598",
  "642",
  "688",
  "736",
  "786",
  "838",
  "894",
  "952",
  "1012",
  "1076",
  "1162",
  "1310", 
  "1530",
  "1992", 
  "2500", 
  "3137",
  "3937", 
  "4941",
  "6200", 
  "7780",
  "9763",
]


# Open input file
InputFile  = PATH
InputFile += "ROOTfiles/DirectMatching/DeriveInsituRscan_Outputs/SmoothedCorrvsEta/"
InputFile += DataVersion
InputFile += "_vs_"
InputFile += MCVersion
InputFile += "/"
InputFile += jetColl
InputFile += "/SmoothedCorr_vs_Eta"
if syst != "Nominal":
  InputFile += "_"
  InputFile += syst
InputFile += ".root"
print "Opening: "+InputFile
File = ROOT.TFile.Open(InputFile)
if not File:
  print "Input file not found, exiting"
  sys.exit(0)

# Output file
OutputFolder  = PATH
OutputFolder += "Plots_DirectMatching/Smoothing/"
OutputFolder += DataVersion
OutputFolder += "_vs_"
OutputFolder += MCVersion
OutputFolder += "/"
OutputFolder += jetColl

ROOT.SetAtlasStyle()

# Format instance
Format = ROOT.Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Ratio")   # Will show second panel with ratio
Format.SetLegendPosition("Top")  # Other options: TopRight, TopLeft, Bottom
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
Format.SetAxisTitles("#eta","Correction") # x-axis, y-axis
Format.SetYTitleSecondPanel("#scale[0.9]{SmoothedCorr/Corr}") # y-axis title of second panel
Format.SetYRangeSecondPanel(0.95,1.05)
Format.ShowLuminosity(strLumi)
Format.ShowCME("13")
Format.SetOutputFolder(OutputFolder)
Format.SetRange("X",-3,3)
if jetColl=="2LC":
  Format.SetRange("Y",0.8,1.4)
elif jetColl=="6LC":
  Format.SetRange("Y",0.9,1.4)
elif jetColl=="8LC":
  Format.SetRange("Y",0.9,1.4)

for bin in range(1,nstrpTBins+1):
  name  = "Corr_vs_Eta_pT_"
  name += str(bin)
  name2  = "SmoothedCorr_vs_Eta_pT_"
  name2 += str(bin)
  outputName = "Comparison_vs_Eta_pT_"
  if bin<10:
    outputName += str(0)
  outputName += str(bin)
  outputName += "_"
  outputName += syst
  Plot = ROOT.Plotter1D(outputName)
  Plot.ApplyFormat(Format)
  Plot.PlotLineSecondPanel("Y",1.01)
  Plot.PlotLineSecondPanel("Y",0.99)
  Plot.AddHist("Correction",File,name)
  Plot.AddHist("Smothed Correction",File,name2,"HIST C")
  extraText = "anti-#it{k}_{t} jets, R="
  if jetColl is "2LC":
    extraText += "0.2"
  if jetColl is "6LC":
    extraText += "0.6"
  if jetColl is "8LC":
    extraText += "0.8"
  extraText += ", LC+JES"
  Plot.AddText(0.2,0.55,extraText)
  legend  = strpTBins[bin-1]
  legend += " #leq #it{p}_{T} [GeV] < "
  legend += strpTBins[bin]
  Plot.AddText(0.2,0.47,legend)
  if Debug:
    Plot.Debug()
  Plot.Write()



